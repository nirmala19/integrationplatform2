import React from "react";
import {withRouter} from "react-router-dom";
import {dataTypes, messageHistory, messageQuery} from "../../api/message";
import "./messages.scss";
import {Button, Dimmer, Dropdown, Icon, Input, Loader, Menu, Modal, Table} from "semantic-ui-react";
import "react-dates/initialize";
import {DateRangePicker} from "react-dates";
import 'react-dates/lib/css/_datepicker.css';
import Moment from "moment";
import PropTypes from "prop-types";
import MessageView from "../../components/message/MessageView";

class Messages extends React.Component {
    constructor() {
        super();

        this.state = ({
            query: {page: 0},
            result: [],
            dataTypes: [],
            traceId: null,
            disabled: false,
            page: 0
        });

    }

    componentWillMount() {
        let dataType = this.props.match.params.datatype;
        let query = Object.assign({}, this.state.query);
        if (dataType) {
            query.dataType = dataType;
        }
        query.pageSize = this.props && this.props.pageSize ? this.props.pageSize : 25;
        if (this.props.store) {
            query.store = this.props.store;
        }
        dataTypes(types => {
            this.setState({
                dataTypes: types.map(t => {
                    return {text: t, value: t}
                }),
                query
            });
            if (dataType || this.props.immediate === true) {
                this.setState({disabled: true});
                messageQuery(query, result => {
                    this.setState({result, disabled: false, page: 0});
                });
            }
        });
    }

    changeDate(value) {
        let q = Object.assign({}, this.state.query);
        q.fromDate = value.startDate;
        q.toDate = value.endDate;
        this.setState({query: q});
    }

    changeDataType(e, select) {
        let q = Object.assign({}, this.state.query);
        q.dataType = select.value;
        this.setState({query: q});
    }

    changeFreetext(e, input) {
        let q = Object.assign({}, this.state.query);
        q.freetext = input.value;
        this.setState({query: q});
    }

    changeFilename(e, input) {
        let q = Object.assign({}, this.state.query);
        q.filename = input.value;
        this.setState({query: q});
    }

    traceMessage(messageId) {
        this.setState({traceId: messageId});
    }

    query(page) {
        this.setState({disabled: true});
        let q = Object.assign({}, this.state.query);
        q.toDate = q.toDate ? q.toDate.startOf('day').toDate() : null;
        q.fromDate = q.fromDate ? q.fromDate.startOf('day').toDate() : null;
        if (page) {
            q.page = page;
        }
        messageQuery(q, result => {
            this.setState({result, disabled: false, page: page});
        });
    }

    nextPage() {
        this.query(this.state.page + 1);
    }

    prevPage() {
        if (this.state.page > 0) {
            this.query(this.state.page - 1);
        }
    }

    isOutsideRange(d) {
        return d.isAfter(new Moment());
    }

    keyPress(e) {
        if (e.key === "Enter") {
            this.query(0);
        }
    }

    render() {

        let openTrace;
        if (this.state.traceId) {
            openTrace = <MessageTrace messageId={this.state.traceId} onClose={this.traceMessage.bind(this, null)}/>
        }

        let loader;
        if (this.state.disabled === true) {
            loader = (
                <Dimmer active inverted>
                    <Loader size='large'>Loading</Loader>
                </Dimmer>
            );
        }
        return (
            <div>
                <h1>Message search</h1>
                <div className="queryArea">
                    <div className="queryField">
                        <div className="label">
                            Data type
                        </div>
                        <div className="queryField">
                            <Dropdown placeholder='Select data type' fluid selection search
                                      value={this.state.query.dataType}
                                      onKeyPress={this.keyPress.bind(this)}
                                      onChange={this.changeDataType.bind(this)} options={this.state.dataTypes}/>
                        </div>
                    </div>
                    <div className="queryField">
                        <div className="label">
                            Between dates
                        </div>
                        <div className="queryField">
                            <DateRangePicker
                                startDate={this.state.query.fromDate} // momentPropTypes.momentObj or null,
                                endDate={this.state.query.toDate} // momentPropTypes.momentObj or null,
                                onDatesChange={this.changeDate.bind(this)} // PropTypes.func.isRequired,
                                focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                displayFormat="YYYY-MM-DD"
                                isOutsideRange={this.isOutsideRange.bind(this)}
                                onFocusChange={focusedInput => this.setState({focusedInput})} // PropTypes.func.isRequired,
                            />
                        </div>
                    </div>
                    <div className="queryField">
                        <div className="label">
                            Filename
                        </div>
                        <div className="queryField">
                            <Input fluid value={this.state.query.filename ? this.state.query.filename : ""}
                                   onKeyPress={this.keyPress.bind(this)}
                                   onChange={this.changeFilename.bind(this)}/>
                        </div>
                    </div>
                    <div className="queryField">
                        <div className="label">
                            Freetext (<i>Warning: SLOW!</i>)
                        </div>
                        <div className="queryField">
                            <Input fluid onKeyPress={this.keyPress.bind(this)}
                                   value={this.state.query.freetext ? this.state.query.freetext : ""}
                                   onChange={this.changeFreetext.bind(this)}/>
                        </div>
                    </div>
                    <div className="queryField">
                        <div className="label">

                        </div>
                        <div className="queryField">
                            <Button disabled={this.state.disabled} onClick={this.query.bind(this, 0)}
                                    className="indio-button">Search</Button>
                        </div>
                    </div>
                </div>
                <div style={{position: "relative", minHeight: "350px"}}>
                    {openTrace}
                    {loader}
                    <br/>
                    <Table>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Message name</Table.HeaderCell>
                                <Table.HeaderCell>Data type</Table.HeaderCell>
                                <Table.HeaderCell>Date received</Table.HeaderCell>
                                <Table.HeaderCell>Trace</Table.HeaderCell>
                                <Table.HeaderCell>View content</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {this.state.result.map(file => {
                                return (
                                    <Table.Row key={file.messageId}>
                                        <Table.Cell>{file.filename}</Table.Cell>
                                        <Table.Cell>{file.dataType}</Table.Cell>
                                        <Table.Cell>{new Moment(file.time).format("YYYY-MM-DD HH:mm:ss.SSS")}</Table.Cell>
                                        <Table.Cell style={{cursor: "pointer"}}
                                                    onClick={this.traceMessage.bind(this, file.messageId)}>Trace</Table.Cell>
                                        <MessageView title={file.filename} content={file.content}
                                                     trigger={<Table.Cell style={{cursor: "pointer"}}>View
                                                         content</Table.Cell>}/>
                                    </Table.Row>
                                )
                            })}
                        </Table.Body>
                        <Table.Footer>
                            <Table.Row>
                                <Table.HeaderCell colSpan='5'>
                                    <Menu floated='right' pagination>
                                        <Menu.Item onClick={this.prevPage.bind(this)}
                                                   disabled={this.state.disabled || this.state.page === 0} as='a' icon>
                                            <Icon name='left chevron'/><span>&nbsp;&nbsp;Previous</span>
                                        </Menu.Item>
                                        <Menu.Item onClick={this.nextPage.bind(this)}
                                                   disabled={this.state.disabled || this.state.result.length < this.pageSize}
                                                   as='a' icon>
                                            <span>Next&nbsp;&nbsp;</span><Icon name='right chevron'/>
                                        </Menu.Item>
                                    </Menu>
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>
                </div>
            </div>
        )
    }

    renderTrace(messageId) {

    }
}

Messages.propTypes = {
    store: PropTypes.number,
    immediate: PropTypes.bool,
    pageSize: PropTypes.number
};

class MessageTrace extends React.Component {
    constructor() {
        super();
        this.state = ({trace: []});
    }

    componentWillMount() {
        messageHistory(this.props.messageId, trace => {
            this.setState({trace});
        })
    }

    retransmitFile() {
        console.log("Retransmit "+this.props.messageId);
    }

    render() {
        return (
            <Modal size="fullscreen" open={true} onClose={this.props.onClose}>
                <Modal.Actions>
                    <Button color="orange" onClick={this.retransmitFile.bind(this)}>
                        Resend message
                    </Button>
                    <Button color="red" onClick={this.props.onClose}>
                        Close
                    </Button>
                </Modal.Actions>
                <Modal.Content scrolling>
                    <Modal.Description>
                        <Table>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Time</Table.HeaderCell>
                                    <Table.HeaderCell>Source</Table.HeaderCell>
                                    <Table.HeaderCell>Source ID</Table.HeaderCell>
                                    <Table.HeaderCell>Target</Table.HeaderCell>
                                    <Table.HeaderCell>Target ID</Table.HeaderCell>
                                    <Table.HeaderCell>Status</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {this.state.trace.map((t, i) => {
                                    let time = new Moment(t.time);
                                    return (
                                        <Table.Row key={i}>
                                            <Table.Cell>{time.format("YYYY-MM-DD HH:mm:ss")}</Table.Cell>
                                            <Table.Cell>{t.source}</Table.Cell>
                                            <Table.Cell>{t.sourceId}</Table.Cell>
                                            <Table.Cell>{t.target}</Table.Cell>
                                            <Table.Cell>{t.targetId}</Table.Cell>
                                            <Table.Cell>{t.status}</Table.Cell>
                                        </Table.Row>
                                    );
                                })}
                            </Table.Body>
                        </Table>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        );
    }

}

MessageTrace.propTypes = {
    messageId: PropTypes.string.isRequired,
    onClose: PropTypes.func.isRequired,
};


export default withRouter(Messages)