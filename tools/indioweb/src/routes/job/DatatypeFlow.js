import React from "react";
import {withRouter} from "react-router-dom";
import {inboundJob, outboundJobs, processingJobs, statusChange} from "../../api/integrations";
import InboundFlows from "../../components/flow/InboundFlows";
import ProcessingFlows from "../../components/flow/ProcessingFlows";
import OutboundFlows from "../../components/flow/OutboundFlows";
import {Button, Modal} from "semantic-ui-react";
import NewInboundJob from "../../components/flow/NewInboundJob";


class DatatypeFlow extends React.Component {
    constructor() {
        super();
        this.state = ({inbound: [], processing: [], outbound: []});
    }

    componentWillMount() {
        inboundJob(this.props.match.params.datatype, response => {
            this.setState({inbound: response});
        });
        processingJobs(this.props.match.params.datatype, response => {
            this.setState({processing: response});
        });
        outboundJobs(this.props.match.params.datatype, response => {
            this.setState({outbound: response});
        });
    }

    changeStatus(type, id, status) {
        statusChange(type, id, status, () => {
            switch (type) {
                case "processing":
                    processingJobs(this.props.match.params.datatype, response => {
                        this.setState({processing: response});
                    });
                    break;
                case "inbound":
                    inboundJob(this.props.match.params.datatype, response => {
                        this.setState({inbound: response});
                    });
                    break;
            }
        })
    }

    render() {
        return (
            <div>
                <h1>Flow for {this.props.match.params.datatype}</h1>
                <div style={{display: "flex"}}>
                    <div style={{width: "30%", paddingRight: "10px"}}>
                        <div style={{display: "flex", alignItems: "flex-start", justifyContent: "space-between"}}>
                            <h2>Inbound</h2><Modal trigger={<Button size="tiny" primary icon="plus"/>}>
                            <Modal.Header>Create new Inbound Job</Modal.Header>
                            <Modal.Content image>
                                <Modal.Description>
                                    <NewInboundJob dataType={this.props.match.params.datatype}/>
                                </Modal.Description>
                            </Modal.Content>
                        </Modal>
                        </div>
                        <InboundFlows dataType={this.props.match.params.datatype} flows={this.state.inbound}
                                      onChangeStatus={this.changeStatus.bind(this, "inbound")}/>
                    </div>
                    <div style={{width: "30%", paddingRight: "10px"}}>
                        <div style={{display: "flex", alignItems: "flex-start", justifyContent: "space-between"}}>
                            <h2>Processing</h2><Button size="tiny" primary icon="plus"/>
                        </div>
                        <ProcessingFlows dataType={this.props.match.params.datatype} flows={this.state.processing}
                                         onChangeStatus={this.changeStatus.bind(this, "processing")}/>
                    </div>
                    <div style={{width: "30%", paddingRight: "10px"}}>
                        <div style={{display: "flex", alignItems: "flex-start", justifyContent: "space-between"}}>
                            <h2>Outbound</h2><Button size="tiny" primary icon="plus"/>
                        </div>
                        <OutboundFlows dataType={this.props.match.params.datatype} flows={this.state.outbound}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(DatatypeFlow);
