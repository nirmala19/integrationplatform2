import React from "react";
import {withRouter} from "react-router";
import {resetAlert, serverAlerts} from "../api/alerts";
import {Button, Popup, Table} from "semantic-ui-react";

class Alerts extends React.Component {
    constructor() {
        super();
        this.state = ({alerts: []})
    }

    componentWillMount() {
        this.getAlerts();
    }

    getAlerts() {
        serverAlerts(alerts => {
            this.setState({alerts})
            console.log(alerts);
        });
    }

    resetAlert(host) {
        resetAlert(host, () => {
            this.getAlerts();
        })
    }

    render() {
        return (
            <div>
                <h1>Server Alert</h1>
                <Table celled striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Severity</Table.HeaderCell>
                            <Table.HeaderCell>Host</Table.HeaderCell>
                            <Table.HeaderCell>Direction</Table.HeaderCell>
                            <Table.HeaderCell>Protocol</Table.HeaderCell>
                            <Table.HeaderCell>Message</Table.HeaderCell>
                            <Table.HeaderCell>Last change</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.state.alerts.map(a => {
                            return (
                                <Table.Row key={a.host+a.direction}>
                                    <Table.Cell
                                        style={{color: a.severity === "WARN" ? "orange" : "red"}}>{a.severity}</Table.Cell>
                                    <Table.Cell>{a.host}</Table.Cell>
                                    <Table.Cell>{a.direction}</Table.Cell>
                                    <Table.Cell>{a.protocol}</Table.Cell>
                                    <Table.Cell>{a.message}</Table.Cell>
                                    <Table.Cell>{new Date(a.event_time).toLocaleString("sv-SE")}</Table.Cell>
                                    <Table.Cell>
                                        <Popup
                                            trigger={<Button color="orange" icon="delete"
                                                             onClick={this.resetAlert.bind(this, a.host)}
                                            />}
                                            content='Clear this alert'
                                        /></Table.Cell>
                                </Table.Row>
                            );
                        })}
                    </Table.Body>
                </Table>
            </div>
        )
    }
}

export default withRouter(Alerts);