import React from "react";
import {withRouter} from "react-router-dom";
import {inboundJob, integrationFlows} from "../api/integrations";
import {Button, Icon, Popup, Table} from "semantic-ui-react";
import prettyCron from "../util/prettycron";
import {dataTypes} from "../api/message";

class IntegrationFlows extends React.Component {
    constructor() {
        super();
        this.state = ({flows: {}, expandedDataType: null, dataTypes: []});
    }

    componentWillMount() {
        integrationFlows(flows => {
            this.setState({flows});
        });
        dataTypes(datatypes => {
            this.setState({dataTypes: datatypes});
        })
    }

    toggleDatatype(dataType) {
        if (dataType === this.state.expandedDataType) {
            this.setState({expandedDataType: null});
        } else {
            this.setState({expandedDataType: dataType});
        }
    }

    edit(dataType) {
        this.props.history.push("/flow/" + dataType);
    }

    goToMessages(dataType) {
        this.props.history.push("/messages/" + dataType);
    }

    render() {
        if (Object.keys(this.state.flows).length === 0) {
            return null;
        }
        let expandButtonText = this.state.expandedDataType === "all" ? "Collapse all" : "Expand all";

        let overview = (
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Status</Table.HeaderCell>
                        <Table.HeaderCell>Data type</Table.HeaderCell>
                        <Table.HeaderCell>Actions</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.state.dataTypes.map(dataType => {
                        let flows = this.state.flows[dataType];
                        let status = "red";
                        let popupData;
                        if (flows) {
                            popupData = (
                                <div>
                                    {flows.map((step, i) => {
                                        return this.flattenSteps(step, 0).map(s => this.displayStep(s, i + s.id + s.eventstep + s.jobid + s.remotelocationid));
                                    })}
                                </div>
                            );
                            if (flows.find(f => f.jobstatus === "ACTIVE")) {
                                status = "green";
                            } else {
                                status = "yellow";
                            }
                        }

                        let statusDescription = {
                            red: "No inbound job exists",
                            yellow: "Inbound job exists but is disabled",
                            green: "Inbound job exist and is active"
                        };

                        return (
                            <Table.Row key={dataType}>
                                <Table.Cell><Popup trigger={<Icon name="circle" color={status}/>}
                                                   content={statusDescription[status]}/></Table.Cell>
                                <Table.Cell><Popup trigger={<span>{dataType}</span>}
                                                   content={popupData}/></Table.Cell>
                                <Table.Cell>
                                    <Button.Group>
                                        <Button onClick={this.edit.bind(this, dataType)} icon="edit"/>
                                        <Button onClick={this.goToMessages.bind(this, dataType)} icon="list layout"/>
                                    </Button.Group>
                                </Table.Cell>
                            </Table.Row>
                        );
                    })}
                </Table.Body>
            </Table>
        );

        return (
            <div>
                <h1>Integration flows</h1>
                <div>{overview}</div>
            </div>
        );
    }

    displayStep(step, key) {
        let stepDepthK = step.depth + 1;
        let indentStyle = {
            width: stepDepthK * 40 + "px",
        };

        return (
            <div className="eventstep" key={key + step.depth}>
                <div style={indentStyle} className="structureindent">&#9492;</div>
                <div>
                    {this.stepInfo(step)}
                </div>
            </div>
        );
    }


    flattenSteps(step, depth) {
        step.depth = depth;
        let result = [step];

        if (step.nextSteps) {
            step.nextSteps.forEach(nextStep => {
                Array.prototype.push.apply(result, this.flattenSteps(nextStep, depth + 1));
            });
        }
        return result;
    }

    stepInfo(step) {
        if (step.inboundMessageCollector) {
            return this.inboundMessageCollector(step)
        } else if (step.eventstep === "STORE_OUTBOUND_TRANSFER") {
            return this.storeOutboundMessage(step);
        } else if (step.eventstep === "OUTBOUND_TRANSFER") {
            return this.outboundMessage(step);
        } else if (step.jobid === "STORE_FILE_COLLECTOR" && step.eventstep === "INBOUND_TRANSFER") {
            return this.storeInboundMessage(step);
        } else {
            return this.regularJobStep(step);
        }
    }

    inboundMessageCollector(step) {
        let sourceUrl;
        if (step.inboundMessageCollector.protocol) {
            sourceUrl = step.inboundMessageCollector.protocol + "://" + step.inboundMessageCollector.remoteserverusername + "@" + step.inboundMessageCollector.remoteserver + "/" + step.inboundMessageCollector.remoteserverdirectory;
            if (step.inboundMessageCollector.filepattern) {
                sourceUrl += "/" + step.inboundMessageCollector.filepattern
            }
        }
        let contentResolver;
        if (step.inboundMessageCollector.contentdatatyperesolver) {
            let resolver = JSON.parse(step.inboundMessageCollector.contentdatatyperesolver);
            contentResolver = (
                <div><i>Content resolver: </i><span>{resolver.fileType}:{resolver.contentResolveMatch}</span>
                </div>
            );
        }
        return (
            <div>
                <b>{step.eventstep} of {step.datatype} messages</b>
                <div><i>Message source: </i><span>{sourceUrl}</span></div>
                <div><i>Schedule: </i><span>{prettyCron.toString(step.schedule, true)}</span></div>
                {contentResolver}
            </div>
        )
    }

    storeOutboundMessage(step) {
        return (
            <div>
                <div><b>Outbound transfer to stores</b></div>
                <div><i>Pattern to determine store id: </i>{step.filenamestorematch}</div>
            </div>
        )
    }

    storeInboundMessage(step) {
        return (
            <div>
                <div><b>storeInboundMessage: Inbound transfer from Stores</b></div>
                <div><i>Description: </i><span>{step.description}</span></div>
                <div><i>Schedule: </i><span>{prettyCron.toString(step.schedule, true)}</span></div>
                <div><i>Filename pattern: </i>{JSON.parse(step.parameters).filename_pattern}</div>
            </div>
        )
    }

    outboundMessage(step) {
        let targetUrl = step.protocol + "://" + step.remoteserverusername + "@" + step.remoteserver + "/" + step.remoteserverdirectory;
        return (
            <div>
                <div><b>Outbound transfer to target</b></div>
                <div><i>Target system: </i><span>{step.remoteservername}</span></div>
                <div><i>Message destination: </i>{targetUrl}</div>
            </div>
        )
    }

    regularJobStep(step) {
        let schedule;
        if (step.schedule) {
            schedule = <div><i>Schedule: </i><span>{prettyCron.toString(step.schedule, true)}</span></div>
        }

        let trigger;
        if (step.jobtype === "event") {
            trigger = <div><i>Triggered by: </i><span>{step.jobtype === "event" ? (step.eventstep) : ""}</span></div>;
        }
        let eventstep;
        if (step.nexteventstep) {
            eventstep = <div><i>Event step: </i><span>{step.nexteventstep}</span></div>;
        }
        return (
            <div>
                <div><b>{step.description}</b></div>
                <div><i>Job Status: </i><span>{step.jobstatus}</span></div>
                <div><i>Job ID: </i><span>{step.jobid}</span></div>
                {eventstep}
                {trigger}
                {schedule}
            </div>
        )
    }

}

export default withRouter(IntegrationFlows);