import React from "react";
import {withRouter} from "react-router-dom";
import {integrationFlows, remoteServers, saveRemoteServer} from "../api/integrations";
import {Button, Dropdown, Form, Icon, Input, Message, Modal, Table, TextArea} from "semantic-ui-react";
import {stores} from "../api/stores";
import PropTypes from "prop-types";
import {serverAlerts} from "../api/alerts";

class RemoteServers extends React.Component {
    constructor() {
        super();
        this.state = ({stores: [], remoteServers: [], selectedServer: null});
    }

    componentWillMount() {
        this.loadServers();
    }

    loadServers() {
        remoteServers(remoteServers => {
            this.setState({remoteServers});
        });
    }

    editServer(server) {
        this.setState({selectedServer: server});
    }

    save(server) {
        if (server) {
            saveRemoteServer(server, () => {
                this.setState({selectedServer: null});
                this.loadServers();
            });
        } else {
            console.warn("Trying to save a server, but no server is selected!");
        }
    }


    render() {
        let editModal;
        if (this.state.selectedServer) {
            editModal = <RemoteServerEdit
                onCancel={this.editServer.bind(this, null)}
                onSave={this.save.bind(this)}
                remoteServer={this.state.selectedServer}
            />
        }
        return (
            <div>
                {editModal}
                <h2>Remote servers</h2>
                <Button onClick={this.editServer.bind(this, {})} primary>New remote server</Button>
                <Table celled striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>ID</Table.HeaderCell>
                            <Table.HeaderCell>Name</Table.HeaderCell>
                            <Table.HeaderCell>Address</Table.HeaderCell>
                            <Table.HeaderCell>Username</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.state.remoteServers.map(server => {
                            return (
                                <Table.Row key={server.id}>
                                    <Table.Cell>{server.id}</Table.Cell>
                                    <Table.Cell>{server.name}</Table.Cell>
                                    <Table.Cell>{server.address}</Table.Cell>
                                    <Table.Cell>{server.username}</Table.Cell>
                                    <Table.Cell style={{textAlign: "center"}}>
                                        <Button onClick={this.editServer.bind(this, server)} icon="edit"/>
                                    </Table.Cell>
                                </Table.Row>
                            );
                        })}
                    </Table.Body>
                </Table>
            </div>
        );
    }
}

class RemoteServerEdit extends React.Component {
    constructor() {
        super();
        this.state = ({server: null, errors: []});
    }

    componentWillMount() {
        this.setState({server: this.props.remoteServer});
    }

    fieldChange(field, value) {
        let server = Object.assign({}, this.state.server);
        server[field] = value;
        this.setState({server});
    }

    save() {
        let errors = [];
        let s = this.state.server;
        if (!s.name || s.name.length < 3) {
            errors.push("Insufficient name, please provide a better name");
        }
        if (!s.address || s.address.length < 9) { //'localhost' is the least we expect
            errors.push("Insufficient address, please provide a FQDN or IP address")
        }
        if (!s.protocol || s.protocol.length === "") {
            errors.push("Please select a protocol");
        }
        if (!s.username || s.username.length < 2) {
            errors.push("Please provide a proper username");
        }

        if (errors.length === 0) {
            this.props.onSave(s);
        } else {
            this.setState({errors});
        }
    }

    render() {
        let protocols = [{value: "smb", text: "CIFS/Windows"}, {value: "ftp", text: "FTP"}, {
            value: "file",
            text: "File"
        }];

        let error;
        if (this.state.errors.length > 0) {
            error = (
                <Form.Field>
                    <Message negative>
                        <Message.Header>The input is not valid</Message.Header>
                        <ul>
                            {this.state.errors.map(e => {
                                return <li>{e}</li>
                            })}
                        </ul>
                    </Message>
                </Form.Field>
            );
        }
        return (
            <Modal open={true} onClose={this.props.onCancel}>
                <Modal.Header>Edit remote server</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        <Form>
                            {error}
                            <Form.Field>
                                <label>Name</label>
                                <Input fluid value={this.state.server.name}
                                       onChange={(e, v) => this.fieldChange("name", v.value)}
                                />
                            </Form.Field>
                            <Form.Field>
                                <label>Address</label>
                                <Input fluid value={this.state.server.address}
                                       onChange={(e, v) => this.fieldChange("address", v.value)}/>
                            </Form.Field>
                            <Form.Field>
                                <label>Protocol</label>
                                <Dropdown placeholder='Select protocol' fluid selection options={protocols}
                                          value={this.state.server.protocol}
                                          onChange={(e, v) => this.fieldChange("protocol", v.value)}/>
                            </Form.Field>
                            <Form.Field>
                                <label>Username</label>
                                <Input fluid value={this.state.server.username}
                                       onChange={(e, v) => this.fieldChange("username", v.value)}/>
                            </Form.Field>
                            <Form.Field>
                                <label>Password</label>
                                <Input type="password" fluid value={this.state.server.password}
                                       onChange={(e, v) => this.fieldChange("password", v.value)}/>
                            </Form.Field>
                            <Form.Field>
                                <label>Protocol config</label>
                                <TextArea autoHeight
                                          fluid value={this.state.server.protocol_config || ""}
                                          style={{fontFamily: "monospace"}}
                                          onChange={(e, v) => this.fieldChange("protocol_config", v.value)}/>
                            </Form.Field>
                            <Form.Field className="buttonArea">
                                <Button onClick={this.props.onCancel}>Cancel</Button>
                                <Button onClick={this.save.bind(this)} primary>Save</Button>
                            </Form.Field>
                        </Form>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        );
    }
}

RemoteServerEdit.propTypes = {
    remoteServer: PropTypes.object.isRequired,
    onCancel: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired
};

export default withRouter(RemoteServers);