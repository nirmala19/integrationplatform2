import React from "react";
import {withRouter} from "react-router-dom";
import {integrationFlows} from "../api/integrations";
import {Button, Checkbox, Dropdown, Icon, Input, Label, Modal, Table} from "semantic-ui-react";
import {stores, updateStore} from "../api/stores";
import StoreMessages from "../components/store/StoreMessages";

class Stores extends React.Component {
    constructor() {
        super();
        this.state = ({
            stores: [],
            showMessagesForStore: null,
            editStore: null,
            editStoreActive: false
        });
    }

    componentWillMount() {
        this.getStores();
    }

    getStores() {
        stores(stores => {
            this.setState({stores});
        });
    }

    showMessages(store) {
        this.setState({showMessagesForStore: store})
    }


    editStore(store) {
        let s;
        if (store) {
            s = {
                id: store.id,
                name: store.name,
                address: store.sourceAddress,
                active: store.active,
                country_code: store.country_code
            };
        }
        this.setState({editStoreActive: true, editStore: s});
    }

    closeEditStore() {
        this.setState({editStoreActive: false, editStore: null});
    }

    setStoreField(store, field, value) {
        store[field] = value;
        this.setState({editStore: store});
    }

    updateStore(store) {
        updateStore(store, (a) => {
            this.getStores();
            this.setState({editStore: null, editStoreActive: false});
        })
    }

    render() {
        let editStore;

        if (this.state.editStoreActive === true) {
            let store = this.state.editStore || {
                id: "",
                name: "",
                address: "",
                active: false,
                isNew: true
            };
            let countryCodes = [
                {
                    text: "Sweden",
                    value: "SE"
                },
                {
                    text: "Norway",
                    value: "NO"
                },
                {
                    text: "Finland",
                    value: "FI"
                },
                {
                    text: "Iceland",
                    value: "IS"
                },
                {
                    text: "Germany",
                    value: "DE"
                }
            ];

            if (this.state.editStore && this.state.editStore.sourceAddress !== this.state.editStore.targetAddress) {
                editStore = (
                    <Modal open={true} onClose={this.closeEditStore.bind(this)}>
                        <Modal.Header>Cannot edit store!</Modal.Header>
                        <Modal.Content>
                            <h3>Cannot edit store</h3>
                            <p>Store {this.state.editStore.name} cannot be edited since it's a complex configuration
                                which is not supported by this user interface. Please contact you local integration
                                developer.</p>
                        </Modal.Content>
                    </Modal>
                );
            } else {

                editStore = (
                    <Modal open={true} onClose={this.closeEditStore.bind(this)}>
                        <Modal.Header>Edit store {store.name}</Modal.Header>
                        <Modal.Content>
                            <div style={{maxWidth: "100%"}}>
                                <div>
                                    <b>Active</b><br/>
                                    <Checkbox onChange={(e, v) => this.setStoreField(store, "active", v.checked)}
                                              checked={store.active}/>
                                    <br/><br/>
                                </div>
                                <div>
                                    <b>Store ID</b><br/>
                                    <Input disabled={!store.isNew}
                                           onChange={(e) => this.setStoreField(store, "id", e.target.value)}
                                           value={store.id}/>
                                    <br/><br/>
                                </div>
                                <div>
                                    <b>Country</b><br/>
                                    <Dropdown selection options={countryCodes}
                                              value={store.country_code}
                                              onChange={(e, v) => this.setStoreField(store, "country_code", v.value)}/>
                                    <br/><br/>
                                </div>
                                <div>
                                    <b>Store name</b><br/>
                                    <Input value={store.name}
                                           onChange={(e) => this.setStoreField(store, "name", e.target.value)}/>
                                    <br/><br/>
                                </div>
                                <div>
                                    <b>Store IP Address</b><br/>
                                    <Input value={store.address}
                                           onChange={(e) => this.setStoreField(store, "address", e.target.value)}/>
                                    <br/><br/>
                                </div>
                                <div className="buttonArea">
                                    <Button onClick={this.closeEditStore.bind(this)}>Cancel</Button>
                                    <Button primary onClick={this.updateStore.bind(this, store)}>Save</Button>
                                </div>
                            </div>
                        </Modal.Content>
                    </Modal>
                );
            }
        }
        let messagesComponent;
        if (this.state.showMessagesForStore) {
            messagesComponent =
                <StoreMessages store={this.state.showMessagesForStore} onClose={this.showMessages.bind(this, null)}/>
        }

        let result = (
            <div>
                <h2>Stores</h2>
                {messagesComponent}
                {editStore}
                <Button primary onClick={this.editStore.bind(this, null)}><Icon name="plus"/> Add new
                    store</Button>
                <Table celled striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>ID</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                            <Table.HeaderCell>Name</Table.HeaderCell>
                            <Table.HeaderCell>Country</Table.HeaderCell>
                            <Table.HeaderCell>Fetch files from</Table.HeaderCell>
                            <Table.HeaderCell>Put files to</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body key={this.state.stores.length}>
                        {this.state.stores.map(store => {
                            return (
                                <Table.Row key={store.id}>
                                    <Table.Cell>{store.id}</Table.Cell>
                                    <Table.Cell><Icon name="dot circle outline"
                                                      color={store.active === true ? "green" : "red"}/></Table.Cell>
                                    <Table.Cell>{store.name}</Table.Cell>
                                    <Table.Cell>{store.country_code}</Table.Cell>
                                    <Table.Cell>{store.source_protocol + "://" + store.sourceUsername + "@" + store.sourceAddress + "/" + store.source_path}</Table.Cell>
                                    <Table.Cell>{store.target_protocol + "://" + store.targetUsername + "@" + store.targetAddress + "/" + store.target_path}</Table.Cell>
                                    <Table.Cell>
                                        <Button.Group>
                                            <Button onClick={this.showMessages.bind(this, store)}
                                                    icon="send outline"/>
                                            <Button onClick={this.editStore.bind(this, store)}
                                                    icon="edit"/>
                                        </Button.Group>
                                    </Table.Cell>
                                </Table.Row>
                            );
                        })}
                    </Table.Body>
                </Table>
            </div>
        );
        return result;
    }

}

export default withRouter(Stores);