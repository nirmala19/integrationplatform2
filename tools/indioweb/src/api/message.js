import {requestGet, requestPost} from "./util";


export const dataTypes = (callback) => {
    requestGet("/api/messages/datatypes", callback);
};

export const messageQuery = (filter, callback) => {
    requestPost("/api/messages/query", filter, callback);
};

export const messageHistory = (messageId, callback) => {
    requestGet("/api/messages/messagepath/" + messageId, callback);
};

export const messageTargets = (messageId, callback) => {
    requestGet("/api/messages/targets/"+messageId, callback);
};