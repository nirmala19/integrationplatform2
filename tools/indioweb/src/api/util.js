import request from "superagent";
import Cookies from 'universal-cookie';
import {notification, validateToken} from "../index";

const cookies = new Cookies();


export const requestGet = (url, bodyCallback) => {
    let token = cookies.get("token");
    request
        .get(url)
        .set('Accept', 'application/json')
        .set("token", token)
        .end(function (err, res) {
            if (res.status === 401) {
                validateToken(() => {
                    notification("You don't have permission for this operation!")
                });
            } else if (err) {
                throw err;
            } else {
                bodyCallback(res.body);
            }
        });
};

export const requestPost = (url, payload, bodyCallback) => {
    let token = cookies.get("token");
    request
        .post(url)
        .set('Accept', 'application/json')
        .set("token", token)
        .send(payload)
        .end(function (err, res) {
            if (res.status === 401) {
                validateToken(() => {
                    notification("You don't have permission for this operation!")
                });
            } else if (err) {
                throw err;
            } else {
                bodyCallback(res.body);
            }
        });
};
export const requestPut = (url, payload, bodyCallback) => {
    let token = cookies.get("token");
    request
        .put(url)
        .set('Accept', 'application/json')
        .set("token", token)
        .send(payload)
        .end(function (err, res) {
            if (res.status === 401) {
                validateToken(() => {
                    notification("You don't have permission for this operation!")
                });
            } else if (err) {
                throw err;
            } else {
                bodyCallback(res.body);
            }
        });
};

export const checkAuth = (group, callback) => {

}