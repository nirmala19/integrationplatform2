import {requestGet, requestPost} from "./util";

export const integrationFlows = (callback) => {
    requestGet("/api/integrations/flow", callback)
};

export const remoteServers = (callback) => {
    requestGet("/api/crud/remoteServers", callback)
};

export const saveRemoteServer = (server, callback) => {
    requestPost("/api/crud/remoteServer", server, callback);
};

export const inboundJob = (dataType, callback) => {
    requestGet("/api/jobs/inbound/" + dataType, callback);
};

export const processingJobs = (dataType, callback) => {
    requestGet("/api/jobs/processing/" + dataType, callback);
};

export const outboundJobs = (dataType, callback) => {
    requestGet("/api/jobs/outbound/" + dataType, callback);
};

export const saveOutboundJob = (outboundJob, callback) => {
    requestPost("/api/jobs/outbound", outboundJob, callback)
};

export const statusChange = (type, id, active, callback) => {
    requestPost("/api/jobs/status/" + type + "/" + id + "/" + active, null, callback);
};

export const saveInboundJob = (inboundJob, callback) => {
    requestPost("/api/jobs/inbound", inboundJob, callback)
};

export const inboundJobTypes = (callback) => {
    requestGet("/api/jobs/inboundJobTypes", callback);
};

export const availableDataTypes = (callback) => {
    requestGet("/api/jobs/datatypes", callback);
};

export const settings = (callback) => {
    requestGet("/api/jobs/settings", callback);
};

export const updateSettings = (settings, callback) => {
    requestPost("/api/jobs/settings", settings, callback);
};





