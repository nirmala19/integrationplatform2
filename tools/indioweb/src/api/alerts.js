import {requestGet, requestPost} from "./util";

export const serverAlerts = (callback) => {
    requestGet("/api/alert/servers", callback);
};

export const resetAlert = (host, callback) => {
    requestPost("/api/alert/servers/"+host, {}, callback);
};
