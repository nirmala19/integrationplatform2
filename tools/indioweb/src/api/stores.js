import request from "superagent";
import Cookies from 'universal-cookie';
import {requestGet, requestPost} from "./util";

const cookies = new Cookies();

export const stores = (callback) => {
    requestGet("/api/stores/", callback);
};

export const inboundMessages = (storeId, dataType, start, offset, fromDate, toDate, callback) => {
    requestGet("/api/stores/" + storeId + "/inboundmessages/" + start + "/" + offset + "/" + dataType + "/" + fromDate + "/" + toDate, callback);

};
export const outboundMessages = (storeId, dataType, start, offset, fromDate, toDate, callback) => {
    requestGet("/api/stores/" + storeId + "/outboundmessages/" + start + "/" + offset + "/" + dataType + "/" + fromDate + "/" + toDate, callback);
};

export const updateStore = (store, callback) => {
    requestPost("/api/stores/" + store.id, store, callback);
};