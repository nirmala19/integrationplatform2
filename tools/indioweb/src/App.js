import React, {Component} from "react";
import Layout from "./Layout";
import "./styles/main.scss";
import {Tab} from "semantic-ui-react";
import {Button, Icon} from "semantic-ui-react";

import {Route, Switch} from "react-router-dom";
import IntegrationFlows from "./routes/IntegrationFlows";
import Stores from "./routes/Stores";
import RemoteServers from "./routes/RemoteServers";
import Messages from "./routes/Messages/Messages";
import DatatypeFlow from "./routes/job/DatatypeFlow";
import PropTypes from "prop-types";
import Alerts from "./routes/Alerts";

// If you use React Router, make this component
// render <Router> with your routes. Currently,
// only synchronous routes are hot reloaded, and
// you will see a warning from <Router> on every reload.
// You can ignore this warning. For details, see:
// https://github.com/reactjs/react-router/issues/2182
export default class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route exact path="/" component={IntegrationFlows}/>
                    <Route path="/flow/:datatype" component={DatatypeFlow}/>
                    <Route exact path="/stores" component={Stores}/>
                    <Route exact path="/remoteservers" component={RemoteServers}/>
                    <Route path="/alerts" component={Alerts}/>
                    <Route path="/messages/:datatype" component={Messages}/>
                    <Route path="/messages" component={Messages}/>
                </Switch>
            </Layout>
        );
    }

}
