import React from 'react';
import Cookies from 'universal-cookie';
import {Button, Header, Icon, Modal} from "semantic-ui-react";
import {withRouter} from "react-router-dom";
import {settings, updateSettings} from "./api/integrations";
import PropTypes from "prop-types";
import {validateToken} from "./index";

const cookies = new Cookies();

class Layout extends React.Component {

    constructor() {
        super();
        this.state = ({path: "/", settings: {}, notificationOpen: true});
    }

    signOut() {
        cookies.remove("token");
        window.location = window.location.href;
    };

    navigate(target) {
        this.props.history.push(target);
        this.setState({path: target});

    }

    componentWillMount() {
        this.setState({path: window.location.pathname});
        this.loadSettings(settings => {
            this.setState({settings});
        });
    }

    loadSettings(callback) {
        settings(settings => {
            callback(settings);
        })
    }

    togglePaused() {
        let settings = Object.assign({}, this.state.settings);
        if (settings.paused === "TRUE") {
            settings.paused = "FALSE";
        } else {
            settings.paused = "TRUE";
        }

        updateSettings(settings, () => {
            this.loadSettings(s => {
                this.setState({settings: s});
            })
        });
    }

    closeNotification() {
        this.props.notification = null;
    }

    render() {
        let logoutStyle = {
            position: "absolute",
            top: "0",
            right: "0",
            margin: "0",
            borderRadius: "0 0 0 4px"

        };

        let routes = [
            {
                title: "Integration flows",
                link: "/"
            },
            {
                title: "Stores",
                link: "/stores"
            },
            {
                title: "Remote servers",
                link: "/remoteservers"
            },
            {
                title: "Server alerts",
                link: "/alerts"
            },
            {
                title: "Messages",
                link: "/messages"
            }

        ];

        let pause;
        if (this.state.settings.paused === "FALSE") {
            pause = <Icon onClick={this.togglePaused.bind(this)} size="huge" name="pause circle" color="red"/>
        } else {
            pause = <Icon onClick={this.togglePaused.bind(this)} size="huge" name="play circle" color="green"/>
        }

        let notice;
        if (window.notification !== null) {
            notice = (
                <Modal
                    open={window.notification !== null}
                    onClose={() => {
                        window.notification = null;
                        this.setState({});
                    }}
                    basic
                    size='small'
                >
                    <Header icon='warning' content='Notification'/>
                    <Modal.Content>
                        <h3>{window.notification}</h3>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button color='red' onClick={() => {
                            window.notification = null;
                            this.setState({});
                        }} inverted>
                            <Icon name='checkmark'/> Got it
                        </Button>
                    </Modal.Actions>
                </Modal>
            );
        }
        let redirect;
        validateToken(() => {
        });

        return (
            <div className="applicationArea">
                {notice}
                <Button onClick={this.signOut} style={logoutStyle} size="large" color="blue" icon="sign out"
                        labelPosition="right"
                        content="Sign out"/>

                <div className="sidebar">
                    <h1 className="title">IndIO</h1>

                    <div className="menu">
                        {routes.map(menuPoint => {
                            let active = this.state.path === menuPoint.link ? "active" : "";
                            return (
                                <div key={menuPoint.link} onClick={this.navigate.bind(this, menuPoint.link)}
                                     className={"menuItem " + active}>
                                    {menuPoint.title}
                                </div>
                            );
                        })}
                    </div>
                    <div className="runMode">
                        <div>{pause}</div>
                        <div>{this.state.settings.paused === "FALSE" ? "Pause" : "Start"}</div>
                    </div>
                </div>
                <div className="content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}


export default withRouter(Layout);