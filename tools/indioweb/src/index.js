import {AppContainer} from 'react-hot-loader';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Cookies from 'universal-cookie';
import {BrowserRouter} from 'react-router-dom';
import request from "superagent";

const cookies = new Cookies();

const rootEl = document.getElementById('root');
var query = window.location.search.substring(1);

const parse_query_string = (query) => {
    var vars = query.split("&");
    var query_string = {};
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
};

var qs = parse_query_string(query);

window.notification = null;

if (qs.token) {
    cookies.set("token", qs.token);
    window.location = window.location.href.substring(0, window.location.href.indexOf("?"));
}


export const validateToken = (callback) => {
    let token = cookies.get("token");
    if (!token) {
        window.location = "/auth/signin?redirect=" + window.location.href;
    } else {
        request.get("/auth/validate/" + token + "/indio")
            .end((err, res) => {
                if (res.statusCode === 200) {
                    callback();
                } else {
                    window.location = "/auth/signin?redirect=" + window.location.href;
                }
            });
    }
};

export const notification = (notice) => {
    window.notification = notice;
    rerender();
};

validateToken(() => {
    rerender();
});

export const rerender = () => {
    const render = Component =>
        ReactDOM.render(
            <AppContainer>
                <BrowserRouter>
                    <Component/>
                </BrowserRouter>
            </AppContainer>,
            rootEl
        );

    render(App);
    if (module.hot) module.hot.accept('./App', () => render(App));
};

