import React from "react";
import {Dimmer, Dropdown, Input, Loader, Modal, Tab, Table} from "semantic-ui-react";
import PropTypes from "prop-types";
import {inboundMessages, outboundMessages} from "../../api/stores";
import StoreMessage from "./StoreMessage";
import "./StoreMessages.scss";
import "react-dates/initialize";
import {SingleDatePicker} from "react-dates";
import Moment from "moment";
import Messages from "../../routes/Messages/Messages";

const offset = 10;
export default class StoreMessages extends React.Component {
    constructor() {
        super();
        this.state = ({
            inboundMessages: null, outboundMessages: null, inboundFileTypes: [], outboundFileTypes: [],
            filter: {
                inbound: {filename: "", fromDate: null, toDate: null, fileType: "All", start: 0},
                outbound: {filename: "", fromDate: null, toDate: null, fileType: "All", start: 0}
            },
            tab: "inbound",
            inboundQuerying: false,
            outboundQuerying: false,
        });
        this.fetchFunctions = {
            inbound: inboundMessages,
            outbound: outboundMessages
        };
        Moment.locale('sv');
    }

    componentWillMount() {
        // this.queryMessages("inbound", this.state.filter.inbound);
        this.queryMessages("outbound", this.state.filter.outbound);
    }

    queryMessages(type, filter) {
        this.setState({querying: true});

        this.fetchFunctions[type](this.props.store.id,
            filter.fileType,
            filter.start,
            offset,
            filter.fromDate || -1,
            filter.toDate || -1,
            messages => {
                let stateChange = {};
                stateChange[type + "Messages"] = messages.items;
                stateChange[type + "FileTypes"] = messages.filetypes;
                stateChange[type + "Querying"] = false;
                this.setState(stateChange);
            });
    }

    updateFilter(field, e, component) {
        let filter = Object.assign({}, this.state.filter);
        let value;
        if (e && e.target && e.target.value) {
            value = e.target.value;
        } else if (component && component.value) {
            value = component.value;
        } else if (e instanceof Moment) {
            value = e.toDate().getTime();
        } else {

        }
        if (field === "toDate") {
            value = value + (3600 * 24 * 1000);
        }

        filter[this.state.tab][field] = value;


        this.setState({filter});
        this.queryMessages(this.state.tab, filter[this.state.tab]);
    }

    tabChange(e, tab) {
        if (tab.activeIndex === 0) {
            this.setState({tab: "inbound"});
        } else if (tab.activeIndex === 1) {
            this.setState({tab: "outbound"});
        }
    }

    render() {
        // if (this.state.tab === "inbound") {
        //     return <Messages store={this.props.store.id}/>
        // }

        let fileTypes = [];
        if (this.state.tab === "inbound") {
            fileTypes = this.state.inboundFileTypes;
        } else if (this.state.tab === "outbound") {
            fileTypes = this.state.outboundFileTypes;
        }

        let loader;
        if (this.state[this.state.tab + "Querying"] === true) {
            loader = (
                <Dimmer active inverted>
                    <Loader size='large'>Loading</Loader>
                </Dimmer>
            );
        }

        let fromDate = this.state.filter[this.state.tab].fromDate;
        let toDate = this.state.filter[this.state.tab].toDate;

        fromDate = fromDate ? new Moment(fromDate) : null;
        toDate = toDate ? new Moment(toDate - (3600000 * 24)) : null;
        let filter = (
            <Table.Row key={this.state.tab}>
                <Table.HeaderCell>
                    <SingleDatePicker
                        date={fromDate} // momentPropTypes.momentObj or null
                        onDateChange={this.updateFilter.bind(this, "fromDate")} // PropTypes.func.isRequired
                        focused={this.state.fromFocused} // PropTypes.bool
                        numberOfMonths={1}
                        placeholder="From"
                        isOutsideRange={d => d.isAfter(new Moment())}
                        onFocusChange={({focused}) => this.setState({fromFocused: focused})}/>
                    <SingleDatePicker
                        date={toDate} // momentPropTypes.momentObj or null
                        onDateChange={this.updateFilter.bind(this, "toDate")} // PropTypes.func.isRequired
                        focused={this.state.toFocused} // PropTypes.bool
                        numberOfMonths={1}
                        placeholder="To"
                        isOutsideRange={d => d.isAfter(new Moment())}
                        onFocusChange={({focused}) => this.setState({toFocused: focused})}/>
                </Table.HeaderCell>
                <Table.HeaderCell>
                    <Dropdown placeholder='Select type' fluid selection
                              onChange={this.updateFilter.bind(this, "fileType")}
                              value={this.state.filter[this.state.tab].fileType}
                              options={fileTypes.map(t => {
                                  return {
                                      text: t,
                                      value: t
                                  }
                              })}/>
                </Table.HeaderCell>
                <Table.HeaderCell><Input fluid value={this.state.filter.filename}
                                         onChange={this.updateFilter.bind(this, "filename")}/> </Table.HeaderCell>
                <Table.HeaderCell>&nbsp;</Table.HeaderCell>
            </Table.Row>
        );

        let inboundPane = {
            menuItem: "Inbound (files received from store)",
            render: () => {
                return <Messages pageSize={10} store={this.props.store.id} immediate={true}/>;
            }
        };
        let outboundPane = {
            menuItem: "Outbound (files transmitted to store)",
            render: () => {
                return <StoreMessageTable messages={this.state.outboundMessages} filter={filter}/>;
            }
        };


        let panes = [inboundPane, outboundPane];

        return (
            <Modal open={true} size="fullscreen" onClose={this.props.onClose}>
                <Modal.Header>Files from/to {this.props.store.name}</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        <Tab onTabChange={this.tabChange.bind(this)} panes={panes}/>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        );
    }
}

class StoreMessageTable extends React.Component {
    render() {
        if (!this.props.messages) {
            return (
                <div style={{minHeight: "200px"}}>
                    <Dimmer active inverted>
                        <Loader inverted>Loading</Loader>
                    </Dimmer>
                </div>
            )
        }
        let body;
        if (this.props.messages) {
            body = this.props.messages.map(m => {
                return <StoreMessage key={m.message_id} message={m}/>
            });

        }
        return (
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Date</Table.HeaderCell>
                        <Table.HeaderCell>Message type</Table.HeaderCell>
                        <Table.HeaderCell>File name</Table.HeaderCell>
                        <Table.HeaderCell>Content</Table.HeaderCell>
                    </Table.Row>
                    {this.props.filter}
                </Table.Header>
                <Table.Body>
                    {body}
                </Table.Body>
            </Table>
        );
    }
}

StoreMessageTable.propTypes = {
    messages: PropTypes.array,
    filter: PropTypes.node
};

StoreMessages.propTypes = {
    store: PropTypes.object.isRequired,
    onClose: PropTypes.func.isRequired,
};