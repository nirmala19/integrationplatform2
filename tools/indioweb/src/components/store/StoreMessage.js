import React from "react";
import PropTypes from "prop-types";
import {Modal, Table} from "semantic-ui-react";
import Moment from "moment";
import MessageView from "../message/MessageView";

export default class StoreMessage extends React.Component {
    constructor() {
        super();
    }


    render() {
        let m = this.props.message;
        let time = new Moment(m.event_time);

        return (
            <Table.Row>
                <Table.Cell>
                    {time.format("YYYY-MM-DD HH:mm:ss")}
                </Table.Cell>
                <Table.Cell>
                    {m.data_type}
                </Table.Cell>
                <Table.Cell>
                    {m.filename}
                </Table.Cell>
                <MessageView title={m.filename} content={m.file_content} trigger={<Table.Cell
                    className="content-cell">
                    {m.file_content}
                </Table.Cell>}/>
            </Table.Row>
        );
    }
}

StoreMessage.propTypes = {
    message: PropTypes.object.isRequired,
};