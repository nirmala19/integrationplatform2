import React from "react";
import PropTypes from "prop-types";
import {Button, Popup} from "semantic-ui-react";
import "./CardActions.scss"

export default class CardActions extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div className="cardActions">
                <Button.Group>
                    {this.props.cardActions.map((ca, i) => {
                        return (
                            <Popup key={i}
                                   trigger={<Button onClick={ca.action} icon={ca.icon} color={ca.color}/>}
                                   content={ca.text}
                            />

                        )
                    })}
                </Button.Group>
            </div>
        );
    }
}

CardActions.propTypes = {
    cardActions: PropTypes.array.isRequired
};