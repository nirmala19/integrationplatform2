import React from "react";
import PropTypes from "prop-types";
import {Button, Card, Checkbox, Dropdown, Form, Input, Modal} from "semantic-ui-react";
import prettyCron from "../../util/prettycron";
import {remoteServers} from "../../api/integrations";

export default class EditInbound extends React.Component {
    constructor() {
        super();
        this.state = ({servers: [], value: {}});
    }


    render() {
        let f = this.props.flow;

        let component;
        if (f.job.id === "STORE_FILE_COLLECTOR") {
            component = <StoreCollectorEdit {...this.props}/>
        } else if (f.inboundMessageCollector) {
            component = <InboundMessageCollectorEdit {...this.props}/>
        }


        if (this.props.noModal === true) {
            return component;
        }
        return (
            <Modal open={true} onClose={this.props.onClose}>
                <Modal.Header>{f.id ? "Edit inbound flow " + f.id + " for " + this.props.dataType : "New inbound flow"}</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        {component}
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        )
    }
}

EditInbound.propTypes = {
    onClose: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    flow: PropTypes.object.isRequired,
    dataType: PropTypes.string.isRequired,
    noModal: PropTypes.bool
};

class StoreCollectorEdit extends React.Component {
    render() {
        let f = this.props.flow;
        return (
            <div>
                <table style={{width: "100%"}}>
                    <tbody>
                    <tr>
                        <th style={{width: "250px"}}>Description</th>
                        <td><Input fluid disabled value={f.description}/></td>
                    </tr>
                    <tr>
                        <th>Schedule</th>
                        <td><Input style={{width: "150px"}}
                                   value={f.schedule}/><span>&nbsp;&nbsp;
                            <i>{prettyCron.toString(f.schedule, true)}</i></span></td>
                    </tr>
                    <tr>
                        <th style={{width: "250px"}}>Filename pattern</th>
                        <td><Input fluid value={f.storeFilenamePattern}/></td>
                    </tr>
                    </tbody>
                </table>
            </div>

        );
    }
}

StoreCollectorEdit.propTypes = {
    onClose: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    flow: PropTypes.object.isRequired,
    dataType: PropTypes.string.isRequired
};

class InboundMessageCollectorEdit extends React.Component {
    constructor() {
        super();
        this.state = ({servers: [], value: {}})
    }

    componentWillMount() {
        remoteServers(servers => {
            this.setState({servers})
        });
        this.setState({value: this.props.flow})
    }

    setSchedule(e) {
        let v = Object.assign({}, this.state.value);
        v.schedule = e.target.value;
        this.setState({value: v})
    }

    setPredefinedSchedule(e, d) {
        let v = Object.assign({}, this.state.value);
        v.schedule = d.value;
        this.setState({value: v})
    }

    setFilenamePattern(e) {
        let v = Object.assign({}, this.state.value);
        v.inboundMessageCollector = Object.assign({}, v.inboundMessageCollector);
        v.inboundMessageCollector.filenamePattern = e.target.value;
        this.setState({value: v})
    }

    setRemoteServer(e, s) {
        let v = Object.assign({}, this.state.value);
        v.inboundMessageCollector = Object.assign({}, v.inboundMessageCollector);
        let remoteServer = this.state.servers.find(rs => rs.id === s.value);
        v.inboundMessageCollector.remoteServer = remoteServer;
        // v.inboundMessageCollector.filenamePattern = e.target.value;
        this.setState({value: v})

    }

    setDirectory(e) {
        let v = Object.assign({}, this.state.value);
        v.inboundMessageCollector = Object.assign({}, v.inboundMessageCollector);
        v.inboundMessageCollector.remoteLocation = Object.assign({}, v.inboundMessageCollector.remoteLocation);
        v.inboundMessageCollector.remoteLocation.directory = e.target.value;
        this.setState({value: v})
    }

    toggleFilterByContent(e, c) {
        let v = Object.assign({}, this.state.value);
        v.inboundMessageCollector = Object.assign({}, v.inboundMessageCollector);
        if (v.inboundMessageCollector.contentDatatypeResolver) {
            v.inboundMessageCollector.contentDatatypeResolver = Object.assign({}, v.inboundMessageCollector.contentDatatypeResolver);
        }
        if (c.checked === true) {
            v.inboundMessageCollector.contentDatatypeResolver = {fileType: "xml"};
        } else {
            v.inboundMessageCollector.contentDatatypeResolver = null;
        }
        this.setState({value: v})
    }

    setContentResolveMatch(e) {
        let v = Object.assign({}, this.state.value);
        v.inboundMessageCollector = Object.assign({}, v.inboundMessageCollector);
        v.inboundMessageCollector.contentDatatypeResolver = Object.assign({}, v.inboundMessageCollector.contentDatatypeResolver);
        v.inboundMessageCollector.contentDatatypeResolver.contentResolveMatch = e.target.value;
        this.setState({value: v});
    }


    render() {
        let serverOptions = this.state.servers.map(s => {
            return {key: s.id, value: s.id, text: s.name + " (" + s.protocol + "://" + s.address + ")"}
        });
        let f = this.state.value;

        let contentFilterComponent;
        if (f.inboundMessageCollector.contentDatatypeResolver !== null) {
            let options = [{key: "xml", value: "xml", text: "XML"}];
            contentFilterComponent = (
                <div>

                    <div>
                        <div>Type of file</div>
                        <Dropdown fluid placeholder="Select file type" selection disabled value="xml"
                                  options={options}/>
                    </div>
                    <div>
                        <div>XPath boolean expression</div>
                        <Input fluid
                               onChange={this.setContentResolveMatch.bind(this)}
                               value={f.inboundMessageCollector.contentDatatypeResolver.contentResolveMatch}/>
                    </div>
                </div>
            );
        }

        const scheduleDefaultOptions = [
            {key: '*/15 * * * * ?', text: '*/15 * * * * ? - Every 15 seconds', value: '*/15 * * * * ?'},
            {key: '*/30 * * * * ?', text: '*/30 * * * * ? - Every 30 seconds', value: '*/30 * * * * ?'},
            {key: '0 * * * * ?', text: '0 * * * * ? - Every minute', value: '0 * * * * ?'},
            {key: '0 */5 * * * ?', text: '0 */5 * * * ? - Every 5 minutes', value: '0 */5 * * * ?'},
            {key: '0 */15 * * * ?', text: '0 * * * * ? - Every 15 minutes', value: '0 */15 * * * ?'},
            {key: '0 */30 * * * ?', text: '0 */30 * * * ? - Every 30 minutes', value: '0 */30 * * * ?'},
            {key: '0 0 * * * ?', text: '0 0 * * * ? - Every hour', value: '0 0 * * * ?'},
            {key: '0 0 */3 * * ?', text: '0 0 */3 * * ? - Every 3 hours', value: '0 0 */3 * * ?'},
        ];

        return (
            <div>
                <table style={{width: "100%"}}>
                    <tbody>
                    <tr>
                        <th style={{width: "250px"}}>Description</th>
                        <td><Input fluid disabled value={f.description}/></td>
                    </tr>
                    <tr>
                        <th>Schedule</th>
                        <td>
                            <Input
                                fluid
                                label={<Dropdown
                                                 placeholder="Predefined schedules"
                                                 value={null}
                                                 onChange={this.setPredefinedSchedule.bind(this)}
                                                 options={scheduleDefaultOptions}/>}
                                labelPosition='right'
                                onChange={this.setSchedule.bind(this)}
                                value={f.schedule}
                            />
                            <span>&nbsp;&nbsp;<i>{prettyCron.toString(f.schedule, true)}</i></span></td>
                    </tr>
                    <tr>
                        <th style={{width: "250px"}}>Filename pattern</th>
                        <td>
                            <Input
                                fluid onChange={this.setFilenamePattern.bind(this)}
                                value={f.inboundMessageCollector.filenamePattern}/></td>
                    </tr>
                    <tr>
                        <th>Remote server</th>
                        <td><Dropdown value={f.inboundMessageCollector.remoteServer.id}
                                      placeholder='Select remote server'
                                      onChange={this.setRemoteServer.bind(this)}
                                      fluid search selection options={serverOptions}/>
                        </td>
                    </tr>
                    <tr>
                        <th>Remote directory</th>
                        <td>
                            <Input fluid
                                   onChange={this.setDirectory.bind(this)}
                                   value={f.inboundMessageCollector.remoteLocation.directory}/>
                        </td>
                    </tr>
                    <tr>
                        <th>Filter by content</th>
                        <td>
                            <Checkbox onChange={this.toggleFilterByContent.bind(this)}
                                      checked={f.inboundMessageCollector.contentDatatypeResolver !== null}/>
                            {contentFilterComponent}
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div className="buttonArea">
                    <Button onClick={this.save.bind(this)} primary>Save</Button>
                </div>
            </div>

        );
    }

    save() {
        this.props.save(this.state.value);
    }
}

InboundMessageCollectorEdit.propTypes = {
    onClose: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    flow: PropTypes.object.isRequired,
    dataType: PropTypes.string.isRequired
};