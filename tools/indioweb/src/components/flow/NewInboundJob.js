import React from "react";
import PropTypes from "prop-types";
import {Button, Dropdown, Form, Modal} from "semantic-ui-react";
import {availableDataTypes, inboundJobTypes, saveInboundJob} from "../../api/integrations";
import {dataTypes} from "../../api/message";
import EditInbound from "./EditInbound";

export default class NewInboundJob extends React.Component {
    constructor() {
        super();
        this.state = ({type: null, types: [], datatypes: [], datatype: null, proceed: false});
    }

    componentWillMount() {
        inboundJobTypes(jobs => {
            this.setState({types: jobs})
        });
        availableDataTypes(datatypes => {
            this.setState({datatypes});
        });
        this.setState({datatype: this.props.dataType});
    }

    selectType(e, v) {
        this.setState({type: v.value});
    }

    selectDatatype(e, v) {
        this.setState({datatype: v.value});
    }

    proceed(e) {
        this.setState({proceed: true});
        e.preventDefault();
        return false;
    }

    render() {
        let jobTypeOptions = this.state.types.map(t => {
            return {
                key: t.id,
                value: t.id,
                text: t.description + " (" + t.id + ")"
            }
        });

        let dataTypeOptions = this.state.datatypes.map(datatype => {
            return {
                key: datatype,
                value: datatype,
                text: datatype
            }
        });

        let form;


        if (!this.state.proceed) {
            return (
                <div>
                    <Form>
                        <Form.Field>
                            <label>Select type of inbound job</label>
                            <Dropdown fluid selection value={this.state.type}
                                      onChange={this.selectType.bind(this)}
                                      options={jobTypeOptions}/>
                        </Form.Field>
                        <Form.Field>
                            <label>Select data type</label>
                            <Dropdown fluid selection value={this.state.datatype}
                                      onChange={this.selectDatatype.bind(this)}
                                      options={dataTypeOptions}/>
                        </Form.Field>
                        <Form.Field>
                            <div className="buttonArea">
                                <Button primary onClick={this.proceed.bind(this)}
                                        disabled={this.state.type === null || this.state.datatype === null}>Continue</Button>
                            </div>
                        </Form.Field>
                    </Form>
                </div>
            );
        } else {

            let job = this.state.types.find(j => j.id === this.state.type);

            let jobObject = {};
            if (job.id === "INBOUND_MESSAGE_COLLECTOR") {
                jobObject.job = job;
                jobObject.dataType = this.state.datatype;
                jobObject.eventStep = job.eventType;
                jobObject.schedule = "";
                jobObject.description = job.description;
                jobObject.inboundMessageCollector = {
                    remoteLocation: {},
                    remoteServer: {},
                    contentDatatypeResolver: null
                };
            }

            return <div>
                <EditInbound onClose={() => {
                }} save={this.save.bind(this)} flow={jobObject} dataType={this.state.datatype} noModal={true}/>
            </div>
        }
    }

    save(inboundFlow) {
        saveInboundJob(inboundFlow, () => {
            console.log("Save successful!");
        })
    }

}

NewInboundJob.propTypes = {
    dataType: PropTypes.string
};

