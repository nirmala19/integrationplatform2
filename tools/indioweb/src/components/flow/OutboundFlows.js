import React from "react";
import PropTypes from "prop-types";
import {Card} from "semantic-ui-react";
import OutboundFlowCard from "./OutboundFlowCard";

export default class OutboundFlows extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div style={{width: "100%"}}>
                {this.props.flows.map((flow, i) => {
                    return <OutboundFlowCard dataType={this.props.dataType} key={i} flow={flow}/>
                })}
            </div>
        );
    }
}

OutboundFlows.propTypes = {
    dataType: PropTypes.string.isRequired,
    flows: PropTypes.array.isRequired
};