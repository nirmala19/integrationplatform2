import React from "react";
import PropTypes from "prop-types";
import {Card, Icon} from "semantic-ui-react";
import prettyCron from "../../util/prettycron";
import EditInbound from "./EditInbound";
import CardActions from "../general/CardActions";
import {saveInboundJob} from "../../api/integrations";

export default class InboundFlowCard extends React.Component {
    constructor() {
        super();
        this.state = ({edit: false});
    }

    toggleEdit() {
        this.setState({edit: !this.state.edit});
    }

    save(inboundFlow) {
        saveInboundJob(inboundFlow, () => {
        })
    }

    render() {
        let edit;
        if (this.state.edit === true) {
            edit = <EditInbound flow={this.props.flow} dataType={this.props.flow.dataType}
                                save={this.save.bind(this)}
                                onClose={this.toggleEdit.bind(this)}/>
        }

        let cardActions = [{
            icon: "edit",
            text: "Edit",
            color: "blue",
            action: this.toggleEdit.bind(this)
        }];


        let status;
        if (this.props.flow.jobStatus === "ACTIVE") {
            status = <div><Icon color="green" name="circle"/><span>Active</span></div>
            cardActions.push({
                icon: "delete",
                text: "Disable",
                color: "orange",
                action: this.changeStatus.bind(this)
            })
        } else {
            status = <div><Icon color="red" name="circle"/><span>Disabled</span></div>
            cardActions.push({
                icon: "check",
                text: "Enable",
                color: "green",
                action: this.changeStatus.bind(this)
            })
        }

        return (
            <Card fluid>
                {edit}
                <Card.Content>
                    <Card.Header>
                        {this.title()}
                    </Card.Header>
                    <Card.Meta>
                        {status}
                    </Card.Meta>
                    {this.content()}
                </Card.Content>
                <CardActions cardActions={cardActions}/>
            </Card>
        );
    }

    content() {
        let f = this.props.flow;
        if (this.props.flow.job.id === "INBOUND_MESSAGE_COLLECTOR") {
            return (
                <Card.Description>
                    <table>
                        <tbody>
                        <tr>
                            <th>Server</th>
                            <td>{f.inboundMessageCollector.remoteServer.address}</td>
                        </tr>
                        <tr>
                            <th>Directory</th>
                            <td>{f.inboundMessageCollector.remoteLocation.directory}</td>
                        </tr>
                        <tr>
                            <th>File pattern</th>
                            <td>{f.inboundMessageCollector.filenamePattern}</td>
                        </tr>
                        <tr>
                            <th>Frequency</th>
                            <td>{prettyCron.toString(f.schedule, true)}</td>
                        </tr>
                        </tbody>
                    </table>
                </Card.Description>
            );
        } else if (this.props.flow.job.id === "STORE_FILE_COLLECTOR") {
            return (
                <Card.Description>
                    <table>
                        <tbody>
                        <tr>
                            <th>File pattern</th>
                            <td>{f.storeFilenamePattern}</td>
                        </tr>
                        <tr>
                            <th>Frequency</th>
                            <td>{prettyCron.toString(f.schedule, true)}</td>
                        </tr>
                        </tbody>
                    </table>
                </Card.Description>
            );
        }

    }

    title() {
        let result = "";
        if (this.props.flow.job.id === "INBOUND_MESSAGE_COLLECTOR") {
            result += this.props.flow.inboundMessageCollector.id + ". " + this.props.flow.inboundMessageCollector.remoteServer.name;
        } else if (this.props.flow.job.id === "STORE_FILE_COLLECTOR") {
            result = "Store file collector";
        }
        return result;
    }

    changeStatus() {
        this.props.onChangeStatus(this.props.flow.id, this.props.flow.jobStatus === "ACTIVE" ? false : true);
    }
}

InboundFlowCard.propTypes = {
    flow: PropTypes.object.isRequired,
    onChangeStatus: PropTypes.func.isRequired
};
