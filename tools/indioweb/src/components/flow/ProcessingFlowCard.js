import React from "react";
import PropTypes from "prop-types";
import {Button, Card, Icon} from "semantic-ui-react";
import prettyCron from "../../util/prettycron";
import CardActions from "../general/CardActions";

export default class ProcessingFlowCard extends React.Component {

    edit() {
        console.log("edit! ", this.props.flow);
    }

    render() {
        let status;
        if (this.props.flow.jobStatus === "ACTIVE") {
            status = <div><Icon color="green" name="circle"/><span>Active</span></div>
        } else {
            status = <div><Icon color="red" name="circle"/><span>Disabled</span></div>
        }

        return (
            <Card fluid>
                <Card.Content>
                    <Card.Header>
                        {this.title()}
                    </Card.Header>
                    <Card.Meta>
                        {status}
                    </Card.Meta>
                    {this.content()}
                </Card.Content>
            </Card>
        );
    }

    content() {
        let f = this.props.flow;

        let actions = [{
            icon: "edit",
            text: "Edit",
            color: "blue",
            action: this.edit.bind(this)
        }];
        if (f.jobStatus === "ACTIVE") {
            actions.push({
                icon: "delete",
                text: "Disable",
                color: "orange",
                action: this.changeStatus.bind(this)
            })
        } else {
            actions.push({
                icon: "check",
                text: "Enable",
                color: "green",
                action: this.changeStatus.bind(this)
            })
        }

        return (
            <Card.Description>
                <div>{f.job.description}</div>
                <div><b>Event step</b> completed job generates <i>{f.job.eventType}</i></div>
                <div><b>Trigger on</b> event step <i>{f.eventStep}</i> completed</div>
                <CardActions cardActions={actions}/>
            </Card.Description>
        );
    }

    title() {
        let result = this.props.flow.job.id === "INBOUND_OUTBOUND_BOUNCER" ? "Passthrough to outbound" : this.props.flow.job.id;
        return result;
    }

    changeStatus() {
        this.props.onChangeStatus(this.props.flow.id, this.props.flow.jobStatus === "ACTIVE" ? false : true);
    }
}

ProcessingFlowCard.propTypes = {
    flow: PropTypes.object.isRequired,
    onChangeStatus: PropTypes.func.isRequired
};
