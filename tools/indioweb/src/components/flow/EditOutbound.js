import React from "react";
import PropTypes from "prop-types";
import {Button, Card, Checkbox, Dropdown, Form, Input, Modal} from "semantic-ui-react";
import {remoteServers} from "../../api/integrations";

export default class EditOutbound extends React.Component {
    constructor() {
        super();
        this.state = ({servers: [], value: {}});
    }

    render() {
        let f = this.props.flow;

        let component;
        if (f.distributionType === "STORE_DISTRIBUTE") {
            component = <StoreDistributeEdit {...this.props}/>
        } else if (f.distributionType === "OUTBOUND_DISTRIBUTE") {
            component = <NormalDistributeEdit {...this.props}/>
        }


        if (this.props.noModal === true) {
            return component;
        }

        return (
            <Modal open={true} onClose={this.props.onClose}>
                <Modal.Header>{f.id ? "Edit inbound flow " + f.id + " for " + this.props.dataType : "New inbound flow"}</Modal.Header>
                <Modal.Content image>
                    <Modal.Description>
                        {component}
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        )
    }
}

EditOutbound.propTypes = {
    onClose: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    flow: PropTypes.object.isRequired,
    dataType: PropTypes.string.isRequired,
    noModal: PropTypes.bool
};

class StoreDistributeEdit extends React.Component {
    render() {
        let f = this.props.flow;
        return (
            <div>
                <table style={{width: "100%"}}>
                    <tbody>
                    <tr>
                        <th style={{width: "250px"}}>Description</th>
                        <td><Input fluid disabled value={f.description}/></td>
                    </tr>
                    <tr>
                        <th>Schedule</th>
                        <td><Input style={{width: "150px"}}
                                   value={f.schedule}/><span>&nbsp;&nbsp;
                            <i>{prettyCron.toString(f.schedule, true)}</i></span></td>
                    </tr>
                    <tr>
                        <th style={{width: "250px"}}>Filename pattern</th>
                        <td><Input fluid value={f.storeFilenamePattern}/></td>
                    </tr>
                    </tbody>
                </table>
            </div>

        );
    }
}

StoreDistributeEdit.propTypes = {
    onClose: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    flow: PropTypes.object.isRequired,
    dataType: PropTypes.string.isRequired
};

class NormalDistributeEdit extends React.Component {
    constructor() {
        super();
        this.state = ({servers: [], value: {}})
    }

    componentWillMount() {
        remoteServers(servers => {
            this.setState({servers})
        });
        this.setState({value: this.props.flow})
    }

    setSubstitutePattern(e) {
        let v = Object.assign({}, this.state.value);
        v.substitutePattern = e.target.value;
        this.setState({value: v})
    }

    setRemoteServer(e, s) {
        let v = Object.assign({}, this.state.value);
        let remoteServer = this.state.servers.find(rs => rs.id === s.value);
        v.remoteServer = remoteServer;
        this.setState({value: v})

    }

    setDirectory(e) {
        let v = Object.assign({}, this.state.value);
        v.remoteLocation = Object.assign({}, v.remoteLocation);
        v.remoteLocation.directory = e.target.value;
        this.setState({value: v})
    }

    render() {
        let serverOptions = this.state.servers.map(s => {
            return {key: s.id, value: s.id, text: s.name + " (" + s.protocol + "://" + s.address + ")"}
        });
        let f = this.state.value;

        return (
            <div>
                <table style={{width: "100%"}}>
                    <tbody>
                    <tr>
                        <th>Remote server</th>
                        <td><Dropdown value={f.remoteServer.id}
                                      placeholder='Select remote server'
                                      onChange={this.setRemoteServer.bind(this)}
                                      fluid search selection options={serverOptions}/>
                        </td>
                    </tr>
                    <tr>
                        <th>Remote directory</th>
                        <td>
                            <Input fluid
                                   onChange={this.setDirectory.bind(this)}
                                   value={f.remoteLocation.directory}/>
                        </td>
                    </tr>
                    <tr>
                        <th style={{width: "250px"}}>Filename substitution pattern</th>
                        <td><Input
                            fluid onChange={this.setSubstitutePattern.bind(this)}
                            value={f.substitutePattern || ""}/></td>
                    </tr>
                    </tbody>
                </table>
                <div className="buttonArea">
                    <Button onClick={this.save.bind(this)} primary>Save</Button>
                </div>
            </div>

        );
    }

    save() {
        this.props.save(this.state.value);
    }
}

NormalDistributeEdit.propTypes = {
    onClose: PropTypes.func.isRequired,
    save: PropTypes.func.isRequired,
    flow: PropTypes.object.isRequired,
    dataType: PropTypes.string.isRequired
};