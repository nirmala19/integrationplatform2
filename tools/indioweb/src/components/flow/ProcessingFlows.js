import React from "react";
import PropTypes from "prop-types";
import {Card} from "semantic-ui-react";
import InboundFlowCard from "./InboundFlowCard";
import ProcessingFlowCard from "./ProcessingFlowCard";

export default class ProcessingFlows extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div style={{width: "100%"}}>
                {this.props.flows.map((flow,i) => {
                    return <ProcessingFlowCard key={i} flow={flow} onChangeStatus={this.props.onChangeStatus}/>
                })}
            </div>
        );
    }
}

ProcessingFlows.propTypes = {
    dataType: PropTypes.string.isRequired,
    flows: PropTypes.array.isRequired,
    onChangeStatus: PropTypes.func.isRequired
};