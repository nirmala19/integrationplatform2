import React from "react";
import PropTypes from "prop-types";
import {Card} from "semantic-ui-react";
import prettyCron from "../../util/prettycron";
import CardActions from "../general/CardActions";
import EditOutbound from "./EditOutbound";
import {saveOutboundJob} from "../../api/integrations";

export default class OutboundFlowCard extends React.Component {
    constructor() {
        super();
        this.state = ({edit: false});
    }

    toggleEdit() {
        console.log("Toggle edit");
        this.setState({edit: !this.state.edit});
    }

    save(flow) {
        console.log("saving ", flow);
        saveOutboundJob(flow, () => {
            console.log("Save successful");
        })
    }

    render() {
        console.log("outbound: ", this.props.flow)
        let cardActions = [{
            icon: "edit",
            text: "Edit",
            color: "blue",
            action: this.toggleEdit.bind(this)
        }];

        let edit;
        if (this.state.edit === true) {
            edit = <EditOutbound onClose={() => this.setState({edit: false})} save={this.save.bind(this)}
                                 flow={this.props.flow} dataType={this.props.dataType}/>
        }

        return (
            <Card fluid>
                {edit}
                <Card.Content>
                    <Card.Header>
                        {this.title()}
                    </Card.Header>
                    <Card.Meta>

                    </Card.Meta>
                    {this.content()}
                </Card.Content>
                <CardActions cardActions={cardActions}/>
            </Card>
        );
    }

    content() {
        let f = this.props.flow;
        let result = (
            <Card.Description>
                <div>{f.distributionType}</div>
                <div><b>Trigger on</b> event step <i>{f.eventStep}</i> completed</div>
            </Card.Description>
        );
        if (f.distributionType === "OUTBOUND_DISTRIBUTE") {
            let substitutePattern;
            if (f.substitutePattern) {
                substitutePattern = (
                    <tr>
                        <th>Substitution pattern</th>
                        <td>{f.substitutePattern}</td>
                    </tr>
                );
            }
            result = (
                <table>
                    <tbody>
                    <tr>
                        <th>Server</th>
                        <td>{f.remoteServer.address}</td>
                    </tr>
                    <tr>
                        <th>Directory</th>
                        <td>{f.remoteLocation.directory}</td>
                    </tr>
                    {substitutePattern}
                    </tbody>
                </table>
            );
        }
        return result;
    }

    title() {
        let result;
        if (this.props.flow.distributionType === "OUTBOUND_DISTRIBUTE") {
            result = "Distribution to " + this.props.flow.remoteServer.name;
        } else {
            result = "Store distribution";
        }
        return result;
    }
}

OutboundFlowCard.propTypes = {
    flow: PropTypes.object.isRequired,
    dataType: PropTypes.string.isRequired,
};
