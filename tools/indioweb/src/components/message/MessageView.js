import React from "react";
import PropTypes from "prop-types";
import {Icon, Modal, Tab} from "semantic-ui-react";
import PrismCode from "react-prism";
import {pd} from "pretty-data";

require('prismjs');
require('prismjs/themes/prism.css');

export default class MessageView extends React.Component {
    render() {

        let content;
        if (this.props.title.toLowerCase().endsWith(".xml")) {
            let xml = (
                <PrismCode component="pre" className="language-markup">
                    {pd.xml(this.props.content)}
                </PrismCode>
            );
            const panes = [
                {menuItem: 'Formatted', render: () => <Tab.Pane>{xml}</Tab.Pane>},
                {menuItem: 'Raw',
                    render: () => <Tab.Pane>
                        <pre>{this.props.content}</pre>
                    </Tab.Pane>
                },
            ];
            content = <Tab panes={panes}/>;
        } else {
            content = <pre>{this.props.content}</pre>;
        }

        return (
            <Modal size="fullscreen" trigger={this.props.trigger}>
                <Modal.Header>
                    {this.props.title}
                    &nbsp;&nbsp;&nbsp;<a
                    href={"data:text/plain;charset=utf-8," + encodeURIComponent(this.props.content)}
                    download={this.props.title}><Icon name="download"/></a>
                </Modal.Header>
                <Modal.Content scrolling>
                    <Modal.Description>
                        {content}
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        );
    }
}

MessageView.propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    trigger: PropTypes.node.isRequired
};