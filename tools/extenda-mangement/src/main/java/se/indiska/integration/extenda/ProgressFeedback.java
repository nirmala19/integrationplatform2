package se.indiska.integration.extenda;

/**
 * Created by heintz on 2017-08-18.
 */
public abstract class ProgressFeedback {
    private int max = 100;
    private int step = 1;
    private int currentStep = 0;

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public void addProgress(String message) {
        synchronized (this) {
            this.currentStep++;
        }
        double progress = (double)currentStep/(double)max;
        feedbackStep(progress, message);
    }

    public abstract void feedbackStep(double progress, String message);
}
