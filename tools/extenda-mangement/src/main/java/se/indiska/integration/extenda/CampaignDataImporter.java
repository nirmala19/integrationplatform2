package se.indiska.integration.extenda;

import com.google.common.util.concurrent.ListenableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.extenda.Store;
import se.indiska.extenda.StoreConnectUtilities;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.ExecutorsFactory;
import se.indiska.integration.framework.util.Counter;

import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Utility class to import campaigns from Extenda POS's to a local database table.
 * Created by heintz on 2017-06-08.
 */
public class CampaignDataImporter {
    private static final Logger logger = LoggerFactory.getLogger(CampaignDataImporter.class);

    public static void main(String[] argv) throws Exception {
        CampaignDataImporter dataImporter = new CampaignDataImporter();
        dataImporter.importCampaigns();
    }

    public void importCampaigns() throws SQLException, ClassNotFoundException, ExecutionException, InterruptedException {
        String truncateLocalTable = "TRUNCATE TABLE stage.store_currentcampaign";


        List<se.indiska.extenda.Store> stores = StoreConnectUtilities.getStores();

        try (Connection localDbConnection = DatasourceProvider.getConnection()) {
            PreparedStatement truncatePs = localDbConnection.prepareStatement(truncateLocalTable);
            truncatePs.executeUpdate();

            Class.forName("net.sourceforge.jtds.jdbc.Driver");

            Map<se.indiska.extenda.Store, ListenableFuture> futureSet = new HashMap<>();

            Counter counter = new Counter();
            for (se.indiska.extenda.Store store : stores) {
                futureSet.put(store, ExecutorsFactory.getExecutorService().submit(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            counter.add(importStore(store, localDbConnection).get());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }));
            }

            for (Store store : futureSet.keySet()) {
                ListenableFuture f = futureSet.get(store);
                try {
                    f.get(1, TimeUnit.MINUTES);

                } catch (TimeoutException e) {
                    logger.warn("Timeout while reading campaigns from store " + store.id + " (" + store.name + "), cancelling the thread if possible");
                    try {
                        f.cancel(true);
                        try {
                            f.get(30, TimeUnit.SECONDS);
                        } catch (TimeoutException e1) {
                            logger.error("Unable to cancel reading campaign!", e);
                        }
                    } catch (CancellationException ce) {
                        logger.info("Cancelled job reading from store " + store.id + " (" + store.name + ")");
                    }
                }
            }

        } catch (SQLException | ClassNotFoundException | ExecutionException | InterruptedException e) {
            logger.error("Unable to run import job!", e);
            throw e;
        }

    }

    Counter importStore(Store store, Connection localDbConnection) throws SQLException {
        Counter counter = new Counter();
        String dbName = "" + store.id;
        while (dbName.length() < 4) {
            dbName = "0" + dbName;
        }
        dbName = "IND" + dbName;

        String localInsertSql = "INSERT INTO stage.store_currentcampaign (store, item, campaign, fromdate, todate, campaign_price, changedate, shop_id) VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement localInsertPs = localDbConnection.prepareStatement(localInsertSql);

        DriverManager.setLoginTimeout(10);
        try (Connection connection = DriverManager.getConnection("jdbc:jtds:sqlserver://" + store.ip + ":1433/" + dbName, "sysadm", "")) {
            String sql = "SELECT CAMPROW.ID AS Item, CAMPROW.CAMPAIGN AS Campaign, CAMPAIGN.FROMDATE AS Fromdate, CAMPAIGN.TODATE AS Todate, CAMPROW.CAMPPRICE AS Campaign_price, CAMPAIGN.CHANGEDATE AS change_date, CAMPAIGNSHOP.SHOP\n" +
                    "FROM CAMPROW\n" +
                    "  JOIN Campaign\n" +
                    "    ON CAMPROW.CAMPAIGN = CAMPAIGN.CAMPAIGN" +
                    "  JOIN campaignshop\n" +
                    "    ON campaign.campaign=campaignshop.campaign\n";
            PreparedStatement remotePs = connection.prepareStatement(sql);

            ResultSet remoteRset = remotePs.executeQuery();

            while (remoteRset.next()) {
                localInsertPs.setLong(1, store.id);
                localInsertPs.setLong(2, remoteRset.getLong(1));
                localInsertPs.setInt(3, remoteRset.getInt(2));
                localInsertPs.setTimestamp(4, remoteRset.getTimestamp(3));
                localInsertPs.setTimestamp(5, remoteRset.getTimestamp(4));
                localInsertPs.setDouble(6, remoteRset.getDouble(5));
                localInsertPs.setTimestamp(7, remoteRset.getTimestamp(6));
                localInsertPs.setInt(8, remoteRset.getInt(7));
                localInsertPs.addBatch();
                counter.add();
                if (counter.get() > 1000) {
                    localInsertPs.executeBatch();
                    counter.reset();
                }
            }
            localInsertPs.executeBatch();
            localInsertPs.close();
        } finally {
            logger.debug("Completed import for store " + store.id + " completed");
        }
        return counter;
    }
}
