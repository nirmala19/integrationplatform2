package se.indiska.integration.extenda;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.indiska.integration.framework.util.Counter;

import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by heintz on 2017-08-18.
 */
@RestController
public class RestInterface {
    private static final Logger logger = LoggerFactory.getLogger(RestInterface.class);

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping(value = "/campaign2past", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity timetravelOldPromotions(@RequestBody PurgeRequest purgeRequest) {
        CampaignShopFixer campaignShopFixer = new CampaignShopFixer();

        try {
            Counter counter = new Counter();
            campaignShopFixer.timeTravelCampaigns(purgeRequest.getItems(),
                    new java.sql.Timestamp(purgeRequest.getEndDate().getTime()),
                    purgeRequest.getStartDate() != null ? new java.sql.Timestamp(purgeRequest.getStartDate().getTime()) : null,
                    new ProgressFeedback() {
                        @Override
                        public void feedbackStep(double progress, String message) {
                            counter.add();
                        }
                    });
            Map<String, Object> result = new HashMap<>();
            result.put("success", true);
            result.put("processed", counter.get());
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            e.printStackTrace();
            Map<String, Object> result = new HashMap<>();
            result.put("false", true);
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(result);
        }
    }


    @RequestMapping(value = "/investigatecampaign", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity campaignInvestigator(@RequestBody Set<String> itemNo) throws JsonProcessingException {
        CampaignInvestigator campaignInvestigator = new CampaignInvestigator();
        Map<Long, List<CampaignStoreItem>> storeItems = campaignInvestigator.getCampaignStoreItems(itemNo);
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new LocalDateTimeModule());
        Map<String, Object> response = new HashMap<>();
        response.put("details", storeItems);
        response.put("stats", calculateStats(storeItems));
        String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response);

        return ResponseEntity.ok(result);
    }

    private Map<String, Object> calculateStats(Map<Long, List<CampaignStoreItem>> storeItems) {
        Map<String, Object> result = new HashMap<>();
        result.put("counts", new HashMap<>());
        Map<String, Integer> countMap = (Map<String, Integer>) result.get("counts");
        Map<String, Map<Double, Set<Long>>> priceMap = new HashMap<>();
        result.put("prices", priceMap);

        DateTimeFormatter dateFormatter = DateTimeFormatter.ISO_DATE;
        Map<String, Set<ProductCampaign>> productCampaigns = new HashMap<>();
        Set<Long> unresponsiveStores = new HashSet<>();
        result.put("unresponsiveStores", unresponsiveStores);

        for (Long storeId : storeItems.keySet()) {
            List<CampaignStoreItem> storeItemList = storeItems.get(storeId);
            if (storeItemList == null) {
                unresponsiveStores.add(storeId);
            } else {
                for (CampaignStoreItem storeItem : storeItemList) {
                    if (storeItem.getItem() == null) {
                        continue;
                    }
                    if (storeItem == null) {
                        logger.error("Got a null store item!!!");
                        continue;
                    }
                    Integer count = countMap.get(storeItem.getItem());

                    if (count == null) {
                        countMap.put(storeItem.getItem(), 1);
                    } else {
                        countMap.put(storeItem.getItem(), count + 1);
                    }

                    Map<Double, Set<Long>> itemPriceMap = priceMap.get(storeItem.getItem());
                    if (itemPriceMap == null) {
                        itemPriceMap = new HashMap<>();
                        if (storeItem.getItem() != null) {
                            priceMap.put(storeItem.getItem(), itemPriceMap);
                        }
                    }

                    Set<Long> stores = itemPriceMap.get(storeItem.getCampaignPrice());
                    if (stores == null) {
                        stores = new HashSet<>();
                        itemPriceMap.put(storeItem.getCampaignPrice(), stores);
                    }

//            String campaignIdentifier = storeItem.getFromDate().format(dateFormatter) + "-" + storeItem.getToDate().format(dateFormatter);
//            Set<ProductCampaign> storeProductCampaigns = productCampaigns.get(campaignIdentifier);
//            if (storeProductCampaigns == null) {
//                storeProductCampaigns = new HashSet<>();
//                productCampaigns.put(campaignIdentifier, storeProductCampaigns);
//            }
//            storeProductCampaigns.add(new ProductCampaign(storeItem.getItem(),
//                    storeItem.getStore().intValue(),
//                    dateFormatter.format(storeItem.getFromDate()),
//                    dateFormatter.format(storeItem.getToDate())));
                    stores.add(storeItem.getStore());
                }
            }
        }

//        result.put("productCampaigns", productCampaigns);


        return result;


    }

    class ProductCampaign {
        private String itemNo = null;
        private Integer store = null;
        private String fromDate = null;
        private String toDate = null;

        public ProductCampaign(String itemNo, Integer store, String fromDate, String toDate) {
            this.itemNo = itemNo;
            this.store = store;
            this.fromDate = fromDate;
            this.toDate = toDate;
        }

        public String getItemNo() {
            return itemNo;
        }

        public Integer getStore() {
            return store;
        }

        public String getFromDate() {
            return fromDate;
        }

        public String getToDate() {
            return toDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ProductCampaign that = (ProductCampaign) o;

            if (!getItemNo().equals(that.getItemNo())) return false;
            if (!getStore().equals(that.getStore())) return false;
            if (!getFromDate().equals(that.getFromDate())) return false;
            return getToDate().equals(that.getToDate());
        }

        @Override
        public int hashCode() {
            int result = getItemNo().hashCode();
            result = 31 * result + getStore().hashCode();
            result = 31 * result + getFromDate().hashCode();
            result = 31 * result + getToDate().hashCode();
            return result;
        }
    }

    class Product {
        private String itemNo = null;
        private Double price = null;

        public Product(String itemNo, Double price) {
            this.itemNo = itemNo;
            this.price = price;
        }

        public String getItemNo() {
            return itemNo;
        }

        public Double getPrice() {
            return price;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Product product = (Product) o;

            if (!getItemNo().equals(product.getItemNo())) return false;
            return getPrice().equals(product.getPrice());
        }

        @Override
        public int hashCode() {
            int result = getItemNo().hashCode();
            result = 31 * result + getPrice().hashCode();
            return result;
        }
    }

//    public static void main(String[] argv) throws Exception {
//        CampaignInvestigator campaignInvestigator = new CampaignInvestigator();
//        List<CampaignStoreItem> storeItems = campaignInvestigator.getCampaignStoreItems("1000005339616");
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.registerModule(new LocalDateTimeModule());
//        mapper.writerWithDefaultPrettyPrinter().writeValue(System.out, storeItems);
//    }
}
