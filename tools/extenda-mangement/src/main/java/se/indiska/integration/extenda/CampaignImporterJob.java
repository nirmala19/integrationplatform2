package se.indiska.integration.extenda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.job.*;

@IdentifiableIndioJob(
        identifier = "CAMPAIGN_DATA_IMPORTER",
        schedule = "0 */15 * * * ?",
        dataType = "STORE_CAMPAIGN",
        eventStep = "REPORT_DATA_COLLECTION",
        jobType = IdentifiableIndioJob.JobType.SCHEDULED,
        description = "Imports data from Extenda POS BO to staging area (used to check campaign prices in stores)"
)
public class CampaignImporterJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(CampaignImporterJob.class);

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        ProcessExecutionResult executionResult = null;
        try {
            logger.error("Importing campaigns from stores");
            CampaignDataImporter dataImporter = new CampaignDataImporter();
            long start = System.currentTimeMillis();
            dataImporter.importCampaigns();
            logger.debug("Time: " + (System.currentTimeMillis() - start));
            executionResult = new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.SUCCESS);
        } catch (Exception e) {
            executionResult = new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.ERROR, e.getMessage(), ProcessError.UNDEFINED_ERROR);
            logger.error("Unable to execute job!", e);
        } finally {
            callback.jobComplete(executionResult);
        }
    }
}
