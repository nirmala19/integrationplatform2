package se.indiska.integration.extenda;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import se.indiska.integration.framework.job.JobServiceRunner;
import se.indiska.security.SecurityFilter;

/**
 * Hello world!
 */
@Configuration
@SpringBootApplication
@ServletComponentScan("se.indiska")
public class ExtendaManagementApplication extends JobServiceRunner {
    @Autowired
    private Environment environment;
    private static final String requiredUserGroup = "indiocampaign";

    public static void main(String[] args) {
        SpringApplication.run(ExtendaManagementApplication.class, args);
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return (container -> {
            container.setPort(8101);
        });
    }

//    @Bean
//    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
//        return args -> {
//        };
//    }

    @Bean
    public FilterRegistrationBean myFilterBean() {
        final FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
        filterRegBean.setFilter(new SecurityFilter("indiocampaign"));
        filterRegBean.addUrlPatterns("/*");
        filterRegBean.setEnabled(Boolean.TRUE);
        filterRegBean.setName("Authentication Filter");
//        filterRegBean.setAsyncSupported(Boolean.TRUE);
        return filterRegBean;
    }

}
