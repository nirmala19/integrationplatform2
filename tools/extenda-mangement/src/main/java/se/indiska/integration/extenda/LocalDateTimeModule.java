package se.indiska.integration.extenda;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by heintz on 2017-08-18.
 */
public class LocalDateTimeModule extends SimpleModule {
    private static final Logger logger = LoggerFactory.getLogger(LocalDateTimeModule.class);

    public LocalDateTimeModule() {
        super();
        this.addSerializer(new StdSerializer<LocalDateTime>(LocalDateTime.class) {
            @Override
            public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
                jsonGenerator.writeString(localDateTime.format(DateTimeFormatter.ISO_DATE_TIME));
            }
        });
    }
}
