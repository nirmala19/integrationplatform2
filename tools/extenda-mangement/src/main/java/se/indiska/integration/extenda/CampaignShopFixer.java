package se.indiska.integration.extenda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.extenda.Store;
import se.indiska.extenda.StoreConnectUtilities;
import se.indiska.integration.framework.ExecutorsFactory;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by heintz on 2017-06-09.
 */
public class CampaignShopFixer {
    private static final Logger logger = LoggerFactory.getLogger(CampaignShopFixer.class);

    public static void main(String[] argv) throws Exception {
//        List<Long> itemNos = new ArrayList<>();
//        itemNos.add(Long.parseLong("1000006292651"));
        CampaignShopFixer shopFixer = new CampaignShopFixer();

        Integer[] storeIds = {76, 570, 575, 418, 405, 406, 407, 417, 583, 37, 23, 5101};
        List<Store> stores = StoreConnectUtilities.getStores(storeIds);
        for (Store store : stores) {
            if (store.ip != null) {
                shopFixer.deleteCampaignStartingOn(store, LocalDate.of(2018, 1, 2));
            }
        }

    }

    public void timeTravelCampaigns(List<String> itemNos, Timestamp endDate, Timestamp startDate) throws Exception {
        timeTravelCampaigns(itemNos, endDate, startDate, new ProgressFeedback() {
            @Override
            public void feedbackStep(double progress, String message) {

            }
        });
    }

    public void timeTravelCampaigns(List<String> itemNos, Timestamp endDateTime, Timestamp startDate, ProgressFeedback feedback) throws Exception {
        List<Store> stores = StoreConnectUtilities.getStores();

        feedback.setMax(stores.size());

        Class.forName("net.sourceforge.jtds.jdbc.Driver");

        List<Future> futures = new ArrayList<>();
        for (Store store : stores) {
            Future future = ExecutorsFactory.getExecutorService().submit(new Runnable() {
                @Override
                public void run() {
                    executeScriptOnStore(store, itemNos, endDateTime, startDate);
                    feedback.addProgress(store.name);
                }
            });
            futures.add(future);
        }
        for (Future future : futures) {
            future.get();
        }
    }

    private void executeScriptOnStore(Store store, List<String> itemNos, Timestamp endDate, Timestamp startDate) {
        String dbName = StoreConnectUtilities.getStoreDatabase(store);

        String itemNosParams = "";
        Iterator<String> itemNoIterator = itemNos.iterator();
        while (itemNoIterator.hasNext()) {
            itemNoIterator.next();
            itemNosParams += " ?";
            if (itemNoIterator.hasNext()) {
                itemNosParams += ",";
            }
        }

        try (Connection connection = StoreConnectUtilities.getStoreConnection(store)) {
            String sql = "update campaign set todate = ? where todate >= ? and campaign in (select campaign from camprow where id in(" + itemNosParams + "))";
//            String sql = "update campaign set todate = ? where campaign in (select campaign from camprow where id in(" + itemNosParams + "))";
            String startDateRestriction = " and fromdate=?";
            if (startDate != null) {
                sql += startDateRestriction;
            }

            PreparedStatement ps = connection.prepareStatement(sql);
            Date now = new Date(System.currentTimeMillis());
            ps.setTimestamp(1, endDate);
            ps.setDate(2, now);
            int i = 3;
            for (String itemNo : itemNos) {
                ps.setString(i, itemNo);
                i++;
            }
            if (startDate != null) {
                ps.setTimestamp(i, startDate);
            }
            int results = ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Cannot connect to database!", e);
        }
    }

    private void deleteCampaignStartingOn(Store store, LocalDate startDate) {
        try (Connection connection = StoreConnectUtilities.getStoreConnection(store)) {
//            String sql = "UPDATE CAMPAIGN SET TODATE=? WHERE FROMDATE=?";
            String sql = "SELECT count(1) FROM CAMPAIGN WHERE FROMDATE=? AND TODATE>?";
            PreparedStatement ps = connection.prepareStatement(sql);
            Date now = Date.valueOf(LocalDate.now());
//            ps.setDate(1, Date.valueOf(LocalDate.now()));
            ps.setDate(1, Date.valueOf(startDate));
            ps.setDate(2, Date.valueOf(startDate));

//            int updateCount = ps.executeUpdate();
//            System.out.println("Updated " + updateCount + " campaigns in store " + store.name + " (" + store.id + "): ");
            ResultSet rset = ps.executeQuery();
            System.out.print("Number of campaigns in store " + store.name + " (" + store.id + "): ");
            if (rset.next()) {
                System.out.println(rset.getInt(1));
            }
        } catch (Exception e) {
            logger.error("Cannot connect to database!", e);
        }

    }

    class StatementDates {
        private Date lastYearMinusOneDay;
        private Date lastYearNow;
        private Date now;

        public StatementDates(Date lastYearMinusOneDay, Date lastYearNow, Date now) {
            this.lastYearMinusOneDay = lastYearMinusOneDay;
            this.lastYearNow = lastYearNow;
            this.now = now;
        }

        public Date getLastYearMinusOneDay() {
            return lastYearMinusOneDay;
        }

        public Date getLastYearNow() {
            return lastYearNow;
        }

        public Date getNow() {
            return now;
        }
    }
}
