package se.indiska.integration.extenda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.extenda.Store;
import se.indiska.extenda.StoreConnectUtilities;
import se.indiska.integration.framework.DatasourceProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by heintz on 2017-08-18.
 */
public class CampaignInvestigator {
    private static final Logger logger = LoggerFactory.getLogger(CampaignInvestigator.class);

    public Map<Long, List<CampaignStoreItem>> getCampaignStoreItems(Set<String> itemNos) {

        Map<Long, List<CampaignStoreItem>> storeItems = new HashMap<>();

        try (Connection connection = DatasourceProvider.getConnection()) {
            String sql = "SELECT * FROM store s LEFT OUTER JOIN stage.store_currentcampaign scc ON scc.store = s.id AND scc.item IN (";
            Iterator<String> itemNoIterator = itemNos.iterator();
            while (itemNoIterator.hasNext()) {
                itemNoIterator.next();
                sql += "?";
                if (itemNoIterator.hasNext()) {
                    sql += ",";
                }
            }
            sql += ") where s.network_address is not null";
            PreparedStatement ps = connection.prepareStatement(sql);
            int c = 1;
            itemNoIterator = itemNos.iterator();
            while (itemNoIterator.hasNext()) {
                ps.setLong(c, Long.parseLong(itemNoIterator.next()));
                c++;
            }
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                Long storeId = rset.getLong("id");
                List<CampaignStoreItem> result = storeItems.get(storeId);
                if (result == null) {
                    result = new ArrayList<>();
                    storeItems.put(storeId, result);
                }
                CampaignStoreItem campaignStoreItem = new CampaignStoreItem();
                Integer campaign = rset.getInt("campaign");
                if (!rset.wasNull()) {
                    campaignStoreItem.setStore(storeId);
                    campaignStoreItem.setCampaign(campaign);
                    campaignStoreItem.setItem(rset.getLong("item") + "");
                    campaignStoreItem.setCampaignPrice(rset.getDouble("campaign_price"));
//                campaignStoreItem.setDiscount(rset.getDouble("DISCOUNT"));
                    campaignStoreItem.setFromDate(rset.getTimestamp("FROMDATE").toLocalDateTime());
                    campaignStoreItem.setToDate(rset.getTimestamp("TODATE").toLocalDateTime());
                    campaignStoreItem.setShop(rset.getInt("SHOP_id"));
                }
                campaignStoreItem.setCountryCode(rset.getString("country_code"));
                campaignStoreItem.setStoreName(rset.getString("name"));
                result.add(campaignStoreItem);

            }
            rset.close();
            ps.close();
        } catch (SQLException e) {
            logger.error("Unable to query database!", e);
        }
//
//        List<Store> stores = StoreConnectUtilities.getStores();
//        List<Future> futures = new ArrayList<>();
//        for (Store store : stores) {
//            Future future = StoreConnectUtilities.getExecutorService().submit(new Runnable() {
//                @Override
//                public void run() {
//                    storeItems.put(store.id.intValue(), getStoreItem(itemNos, store));
//                }
//            });
//            futures.add(future);
////            break;
//        }
//        for (Future future : futures) {
//            try {
//                future.get();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            }
//        }
        return storeItems;
    }

    private List<CampaignStoreItem> getStoreItem(Set<String> itemNos, Store store) {
        String itemNoParams = "";
        Iterator<String> itemNoIterator = itemNos.iterator();
        while (itemNoIterator.hasNext()) {
            itemNoIterator.next();
            itemNoParams += "?";
            if (itemNoIterator.hasNext()) {
                itemNoParams += ",";
            }
        }
        String sql = "SELECT cr.CAMPAIGN, cr.ID, cr.CAMPPRICE, c.DISCOUNT, c.FROMDATE, c.TODATE, cs.SHOP\n" +
                "from CAMPROW cr\n" +
                "  JOIN Campaign c\n" +
                "    ON cr.CAMPAIGN = c.CAMPAIGN\n" +
                "  join campaignshop cs\n" +
                "    on c.campaign=cs.campaign\n" +
                "WHERE todate > CURRENT_TIMESTAMP and cr.ID in (" + itemNoParams + ")";

        List<CampaignStoreItem> result = null;
        try (Connection connection = StoreConnectUtilities.getStoreConnection(store)) {
            result = new ArrayList<>();
            PreparedStatement ps = connection.prepareStatement(sql);
            int i = 1;
            for (String itemNo : itemNos) {
                ps.setString(i, itemNo);
                i++;
            }
//            long start = System.nanoTime();
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                CampaignStoreItem campaignStoreItem = new CampaignStoreItem();
                campaignStoreItem.setStore(store.id);
                campaignStoreItem.setCampaign(rset.getInt("CAMPAIGN"));
                campaignStoreItem.setItem(rset.getString("ID"));
                campaignStoreItem.setCampaignPrice(rset.getDouble("CAMPPRICE"));
                campaignStoreItem.setDiscount(rset.getDouble("DISCOUNT"));
                campaignStoreItem.setFromDate(rset.getTimestamp("FROMDATE").toLocalDateTime());
                campaignStoreItem.setToDate(rset.getTimestamp("TODATE").toLocalDateTime());
                campaignStoreItem.setShop(rset.getInt("SHOP"));
                campaignStoreItem.setCountryCode(store.countryCode);
                campaignStoreItem.setStoreName(store.name);
                result.add(campaignStoreItem);
            }
        } catch (SQLException e) {
            logger.error("Unable to query store " + store.id + " (" + store.name + " " + store.ip + ")", e);
        }
        return result;
    }
}
