package se.indiska.integration.extenda;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.extenda.Store;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heintz on 2017-06-16.
 */
public class CampaignShopFixerOverSSH {
    private static final Logger logger = LoggerFactory.getLogger(CampaignShopFixerOverSSH.class);
    private Session session = null;

    public static void main(String[] argv) throws Exception {
        CampaignShopFixerOverSSH fixer = new CampaignShopFixerOverSSH();

        List<Store> stores = new ArrayList<>();
//        stores.add(new Store(82l, "172.30.82.30"));
//        stores.add(new Store(4100l, "172.30.100.30"));
//        stores.add(new Store(5101l, "172.30.151.30"));
//        stores.add(new Store(567l, "172.30.167.30"));
//        stores.add(new Store(569l, "172.30.169.30"));
//        stores.add(new Store(577l, "172.30.177.30"));
//        stores.add(new Store(579l, "172.30.179.30"));
//        stores.add(new Store(402l, "172.30.202.30"));
//        stores.add(new Store(403l, "172.30.203.30"));
//        stores.add(new Store(404l, "172.30.204.30"));
//        stores.add(new Store(405l, "172.30.205.30"));
//        stores.add(new Store(406l, "172.30.206.30"));
//        stores.add(new Store(407l, "172.30.207.30"));
//        stores.add(new Store(410l, "172.30.210.30"));
//        stores.add(new Store(412l, "172.30.212.30"));
//        stores.add(new Store(416l, "172.30.216.30"));
//        stores.add(new Store(417l, "172.30.217.30"));
//        stores.add(new Store(33l, "172.30.33.30"));
//        stores.add(new Store(45l, "172.30.45.30"));
//        stores.add(new Store(5l, "172.30.5.30"));
//        stores.add(new Store(50l, "172.30.50.30"));
//        stores.add(new Store(6l, "172.30.6.30"));
//        stores.add(new Store(78l, "172.30.78.30"));
//        stores.add(new Store(79l, "172.30.79.30"));
//        stores.add(new Store(81l, "172.30.81.30"));
//        stores.add(new Store(80l, "172.30.80.30"));
//        stores.add(new Store(57l, "172.30.57.30"));
//        stores.add(new Store(58l, "172.30.58.30"));
//        stores.add(new Store(7l, "172.30.7.30"));
//        stores.add(new Store(11l, "172.30.11.30"));
//        stores.add(new Store(75l, "172.30.75.30"));
//        stores.add(new Store(12l, "172.30.12.30"));
//        stores.add(new Store(13l, "172.30.13.30"));
//        stores.add(new Store(15l, "172.30.15.30"));
//        stores.add(new Store(8l, "172.30.8.30"));
//        stores.add(new Store(561l, "172.30.161.30"));
//        stores.add(new Store(9l, "172.30.9.30"));
//        stores.add(new Store(563l, "172.30.163.30"));
//        stores.add(new Store(568l, "172.30.168.30"));
//        stores.add(new Store(571l, "172.30.171.30"));
//        stores.add(new Store(16l, "172.30.16.30"));
        stores.add(new Store(564l, "172.30.164.30"));
//        stores.add(new Store(565l, "172.30.165.30"));
//        stores.add(new Store(17l, "172.30.17.30"));
//        stores.add(new Store(18l, "172.30.18.30"));
//        stores.add(new Store(580l, "172.30.180.30"));
//        stores.add(new Store(584l, "172.30.184.30"));
//        stores.add(new Store(19l, "172.30.19.30"));
//        stores.add(new Store(21l, "172.30.21.30"));
//        stores.add(new Store(22l, "172.30.22.30"));
//        stores.add(new Store(23l, "172.30.23.30"));
//        stores.add(new Store(24l, "172.30.24.30"));
//        stores.add(new Store(25l, "172.30.25.30"));
//        stores.add(new Store(26l, "172.30.26.30"));
//        stores.add(new Store(28l, "172.30.28.30"));
//        stores.add(new Store(29l, "172.30.29.30"));
//        stores.add(new Store(30l, "172.30.30.30"));
//        stores.add(new Store(31l, "172.30.31.30"));
//        stores.add(new Store(36l, "172.30.36.30"));
//        stores.add(new Store(48l, "172.30.48.30"));
//        stores.add(new Store(53l, "172.30.53.30"));
//        stores.add(new Store(71l, "172.30.71.30"));
//        stores.add(new Store(73l, "172.30.73.30"));
//        stores.add(new Store(76l, "172.30.76.30"));
//        stores.add(new Store(77l, "172.30.77.30"));
//        stores.add(new Store(573l, "172.30.173.30"));
//        stores.add(new Store(575l, "172.30.175.30"));
//        stores.add(new Store(578l, "172.30.178.30"));
//        stores.add(new Store(581l, "172.30.181.30"));
//        stores.add(new Store(582l, "172.30.182.30"));
//        stores.add(new Store(583l, "172.30.183.30"));
//        stores.add(new Store(585l, "172.30.185.30"));
//        stores.add(new Store(411l, "172.30.211.30"));
//        stores.add(new Store(418l, "172.30.218.30"));
//        stores.add(new Store(32l, "172.30.32.30"));
//        stores.add(new Store(37l, "172.30.37.30"));
//        stores.add(new Store(38l, "172.30.38.30"));
//        stores.add(new Store(39l, "172.30.39.30"));
//        stores.add(new Store(42l, "172.30.42.30"));
//        stores.add(new Store(43l, "172.30.43.30"));
//        stores.add(new Store(44l, "172.30.44.30"));
//        stores.add(new Store(47l, "172.30.47.30"));
//        stores.add(new Store(49l, "172.30.49.30"));
//        stores.add(new Store(56l, "172.30.56.30"));
//        stores.add(new Store(570l, "172.30.170.30"));
//        stores.add(new Store(572l, "172.30.172.30"));
//        stores.add(new Store(574l, "172.30.174.30"));
//        stores.add(new Store(4l, "172.30.4.30"));
//        stores.add(new Store(40l, "172.30.40.30"));
//        stores.add(new Store(46l, "172.30.46.30"));
//        stores.add(new Store(51l, "172.30.51.30"));
        for (Store store : stores) {
            logger.debug("Connecting to " + store.ip);
            fixer.connect(store);
            logger.debug("Connected.");
            logger.debug("Executing SQL");
            fixer.executeDbJob(store);
            fixer.disconnect();
            logger.debug("Done executing SQL");
        }
    }

    public void connect(Store store) {
        int assigned_port;
        final int local_port = 1433;

        // Remote host and port
        final int remote_port = 1433;
        final String remote_host = "indio.hk.indiska.se";

        try {
            JSch jsch = new JSch();

            // Create SSH session.  Port 22 is your SSH port which
            // is open in your firewall setup.
            session = jsch.getSession("andersh", remote_host, 22);
            session.setPassword("W2yoeas");

            // Additional SSH options.  See your ssh_config manual for
            // more options.  Set options according to your requirements.
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            config.put("Compression", "yes");
            config.put("ConnectionAttempts", "2");

            session.setConfig(config);

            // Connect
            session.connect();

            // Create the tunnel through port forwarding.
            // This is basically instructing jsch session to send
            // data received from local_port in the local machine to
            // remote_port of the remote_host
            // assigned_port is the port assigned by jsch for use,
            // it may not always be the same as
            // local_port.

            assigned_port = session.setPortForwardingL(local_port,
                    store.ip, remote_port);

        } catch (JSchException e) {
            logger.error("Problem connecting!", e);
            return;
        }

        if (assigned_port == 0) {
            logger.error("Port forwarding failed!");
            return;
        }
    }

    public void disconnect() {
        if (session != null && session.isConnected()) {
            session.disconnect();
        }
    }

    public void executeDbJob(Store store) throws ClassNotFoundException, SQLException {
        Class.forName("net.sourceforge.jtds.jdbc.Driver");
        Connection connection = null;
        String dbName = "" + store.id;
        while (dbName.length() < 4) {
            dbName = "0" + dbName;
        }
        dbName = "IND" + dbName;

        try {
            connection = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost:1433/" + dbName, "sysadm", "");
//            String sql = "UPDATE CAMPAIGNSHOP SET SHOP=(SELECT PARAMETER FROM GENP WHERE GENP=5063) where SHOP<>(SELECT PARAMETER FROM GENP WHERE GENP=5063)";
            String sql2 = "update campaign set todate='2018-03-09' where fromdate='2018-03-07' and campaign in (select campaign from camprow where id in ('1000006583124','1000007265098','1000007145109','1000006669088','1000006583100','1000007265111','1000006921117','1000006976117','1000007265081','1000006669118','1000006976087','1000006976100','1000006921100','1000006976094','1000007265104','1000007145093','1000007145130','1000007145086','1000007145147','1000006921094','1000006583117','1000007265128','1000006669101','1000007145116','1000006921087','1000006669095','1000006583094','1000007145123'));";
            logger.info("Updating campaign shop id for shop " + store.id);
            PreparedStatement remotePs = connection.prepareStatement(sql2);
            logger.debug("Updated " + remotePs.executeUpdate() + " for campaign 9th");
//            ResultSet rset = remotePs.executeQuery();
//            while (rset.next()) {
//                logger.debug(""+rset.getObject(1)+": "+rset.getString("FROMDATE") + " - "+rset.getString("TODATE"));
//            }
            remotePs.close();
//            remotePs = connection.prepareStatement(sql2);
//            logger.debug("Updated " + remotePs.executeUpdate() + " for campaign 19th");
//            remotePs.close();
        } catch (Exception e) {
            logger.error("Cannot connect to database!", e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }

    }
}
