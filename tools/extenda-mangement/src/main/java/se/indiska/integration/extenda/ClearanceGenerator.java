package se.indiska.integration.extenda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.extenda.Store;
import se.indiska.extenda.StoreConnectUtilities;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClearanceGenerator {
    private static final Logger logger = LoggerFactory.getLogger(ClearanceGenerator.class);

    public static void main(String[] argv) throws Exception {
        ClearanceGenerator clearanceGenerator = new ClearanceGenerator();
        clearanceGenerator.generateClearances();
    }

    public void generateClearances() throws IOException {
        File f = new File("/tmp/0824campaign.csv");
        List<ClearanceProduct> clearanceProducts = new ArrayList<>();
//        FileReader fr = new FileReader(f);
//        BufferedReader br = new BufferedReader(fr);
//        String line;
//        while ((line = br.readLine()) != null) {
//            line = line.trim();
//            String[] itemPricing = line.split(";");
//            String nok = itemPricing[1].replaceAll(",", ".");
//            String sek = itemPricing[2].replaceAll(",", ".");
//            String eur = itemPricing[3].replaceAll(",", ".");
//            clearanceProducts.add(new ClearanceProduct(itemPricing[0], Double.parseDouble(nok), Double.parseDouble(sek), Double.parseDouble(eur)));
//        }

        clearanceProducts.add(new ClearanceProduct("1000000219494", 10d, 10d, 1d));
        List<Store> stores = StoreConnectUtilities.getStores();
//        clearanceProducts.add(new ClearanceProduct("1000006255809", 125d, 125d, 12.5d));
//        clearanceProducts.add(new ClearanceProduct("1000006255793", 125d, 125d, 12.5d));
//        clearanceProducts.add(new ClearanceProduct("1000006255823", 125d, 125d, 12.5d));
        stores.add(new Store(130l, "172.19.1.66", "Test Backoffice", "SE"));
        for (Store store : stores) {
            generateClearance(clearanceProducts, store, 20140101);
        }
        logger.debug("Complete!");
        System.exit(0);
    }

    public void generateClearance(List<ClearanceProduct> clearanceProducts, Store store, Integer clearanceId) throws IOException {
//        logger.debug("STORE: " + store.id + " (" + store.countryCode + ")");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String filename = "/tmp/clearances/HQ" + dateTimeFormat.format(new Date()) + ".04." + numberPadding(5, store.id.intValue()) + "." + numberPadding(6, 1);
        FileWriter fileWriter = new FileWriter(filename);
        PrintWriter out = new PrintWriter(fileWriter);

        HeaderLine headerLine = new HeaderLine();
        Date from = new Date(117, 7, 20);
        Date to = new Date(199, 11, 31);

        int sequenceNumber = 0;
        out.print(headerLine);
        out.print("\r\n");
        sequenceNumber++;

        CampaignLine campaignLine = new CampaignLine(store.id.intValue(), sequenceNumber, clearanceId, from, to);
        out.print(campaignLine.getLine());
        out.print("\r\n");
        for (ClearanceProduct clearanceProduct : clearanceProducts) {
            sequenceNumber++;
            Double price;
            if (store.countryCode.equalsIgnoreCase("SE")) {
                price = clearanceProduct.sek;
            } else if (store.countryCode.equalsIgnoreCase("NO")) {
                price = clearanceProduct.nok;
            } else {
                price = clearanceProduct.eur;
            }
            price = price * 100;
            CampaignCommodityLine campaignCommodityLine = new CampaignCommodityLine(store.id.intValue(),
                    sequenceNumber,
                    clearanceId,
                    clearanceProduct.sku,
                    price.intValue(),
                    0, 0, dateFormat.format(from)
            );
            out.print(campaignCommodityLine.getLine());
            out.print("\r\n");
        }
        sequenceNumber++;
        CampaignStoreLine campaignStoreLine = new CampaignStoreLine(store.id.intValue(), sequenceNumber, clearanceId);
        out.print(campaignStoreLine.getLine());
        out.print("\r\n");
        sequenceNumber++;
        FooterLine footerLine = new FooterLine(sequenceNumber);
        out.print(footerLine.getLine());
        out.print("\r\n");
        out.flush();
        out.close();
    }

    public void test() {

    }

    class ClearanceProduct {
        private String sku;
        private Double nok;
        private Double sek;
        private Double eur;

        public ClearanceProduct(String sku, Double nok, Double sek, Double eur) {
            this.sku = sku;
            this.nok = nok;
            this.sek = sek;
            this.eur = eur;
        }

        public String getSku() {
            return sku;
        }

        public Double getNok() {
            return nok;
        }

        public Double getSek() {
            return sek;
        }

        public Double getEur() {
            return eur;
        }
    }

    class CampaignStoreLine extends TransactionLine {

        public CampaignStoreLine(Integer storeId,
                                 int sequenceNumber,
                                 int campaign) {
            super("053", "N", storeId, sequenceNumber);
            addLineItem(9, ValueType.NUMERIC, campaign);
            addLineItem(6, ValueType.NUMERIC, storeId);
            addLineItem(11, ValueType.NUMERIC, 0);
            addLineItem(4, ValueType.NUMERIC, 0);
        }
    }

    class HeaderLine extends TransactionLineBase {
        public HeaderLine() {
            addLineItem(new TransactionLineItem(3, ValueType.NUMERIC));
            addLineItem(new TransactionLineItem(7, ValueType.NUMERIC));
            TransactionLineItem lineNumber = new TransactionLineItem(6, ValueType.NUMERIC);
            lineNumber.setValue(1);
            addLineItem(lineNumber);
            TransactionLineItem dateItem = new TransactionLineItem(8, ValueType.STRING);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            dateItem.setValue(sdf.format(new Date()));
            addLineItem(dateItem);
            TransactionLineItem version = new TransactionLineItem(16, ValueType.STRING);
            version.setValue(" VERSION 2.1 #4");
            addLineItem(version);


        }
    }

    class FooterLine extends TransactionLineBase {
        public FooterLine(int noOfTxIncludingHeaderAndFooter) {
            addLineItem(3, ValueType.NUMERIC, 999);
            addLineItem(7, ValueType.NUMERIC, 0);
            addLineItem(6, ValueType.NUMERIC, noOfTxIncludingHeaderAndFooter);

        }
    }

    class CampaignCommodityLine extends TransactionLine {

        public CampaignCommodityLine(Integer storeId,
                                     int sequenceNumber,
                                     Integer campaign,
                                     String articleId,
                                     Integer campaignPrice,
                                     Integer campaignDiscount,
                                     Integer campaignCostPrice,
                                     String text) {
            super("051", "N", storeId, sequenceNumber);

            addLineItem(9, ValueType.NUMERIC, campaign);
            addLineItem(13, ValueType.STRING, articleId);
            addLineItem(8, ValueType.NUMERIC, campaignPrice);
            addLineItem(6, ValueType.NUMERIC, campaignDiscount);
            addLineItem(8, ValueType.NUMERIC, campaignCostPrice);
            addLineItem(10, ValueType.STRING, text);
            addLineItem(9, ValueType.NUMERIC, null);
            addLineItem(4, ValueType.NUMERIC, null);
        }
    }

    class CampaignLine extends TransactionLine {

        public CampaignLine(Integer storeId,
                            int sequenceNumber,
                            Integer campaign,
                            Date fromDate,
                            Date toDate) {
            super("050", "N", storeId, sequenceNumber);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmm");
            addLineItem(9, ValueType.NUMERIC, campaign);
            addLineItem(20, ValueType.STRING, null);
            addLineItem(20, ValueType.STRING, null);
            addLineItem(4, ValueType.NUMERIC, null);
            addLineItem(12, ValueType.STRING, dateFormat.format(fromDate) + "0000");
            addLineItem(12, ValueType.STRING, dateFormat.format(toDate) + "    ");
            addLineItem(4, ValueType.NUMERIC, 0);
            addLineItem(4, ValueType.NUMERIC, 5);
            addLineItem(4, ValueType.NUMERIC, 0);
            addLineItem(9, ValueType.NUMERIC, 0);
            addLineItem(9, ValueType.NUMERIC, 0);
            addLineItem(9, ValueType.NUMERIC, 0);
            addLineItem(4, ValueType.NUMERIC, 0);
            addLineItem(12, ValueType.STRING, dateTimeFormat.format(new Date()));
            addLineItem(8, ValueType.NUMERIC, 0);
            addLineItem(12, ValueType.STRING, dateTimeFormat.format(new Date()));

        }
    }

    class TransactionLine extends TransactionLineBase {
        public TransactionLine(String txType, String txAction, Integer storeId, int sequenceNumber) {
            addLineItem(3, ValueType.NUMERIC, txType);
            addLineItem(1, ValueType.STRING, txAction);
            addLineItem(6, ValueType.NUMERIC, storeId);
            addLineItem(6, ValueType.NUMERIC, sequenceNumber);
        }
    }

    class TransactionLineBase {
        private List<TransactionLineItem> transactionLineItems = new ArrayList<>();

        public String getLine() {
            String line = "";
            for (TransactionLineItem lineItem : transactionLineItems) {
                line += lineItem.getLineValue();
            }
            return line;
        }

        void addLineItem(TransactionLineItem lineItem) {
            this.transactionLineItems.add(lineItem);
        }

        void addLineItem(int length, ValueType valueType, Object value) {
            this.transactionLineItems.add(new TransactionLineItem(length, valueType, value));
        }

        @Override
        public String toString() {
            return getLine();
        }
    }

    class TransactionLineItem {
        private int length = 0;
        private ValueType valueType = null;
        private Object value = null;

        public TransactionLineItem(int length, ValueType valueType) {
            this.length = length;
            this.valueType = valueType;
        }

        public TransactionLineItem(int length, ValueType valueType, Object value) {
            this.length = length;
            this.valueType = valueType;
            this.value = value;
        }

        public void setValue(Object value) {
            this.value = value;
        }

        public String getLineValue() {
            String filler;
            String lineValue = "";
            if (valueType == ValueType.NUMERIC) {
                filler = "0";
                String stringValue = value == null ? "" : value.toString();
                for (int i = 0; i < (length - stringValue.length()); i++) {
                    lineValue += filler;
                }
                lineValue += stringValue;
            } else {
                filler = " ";
                String stringValue = value == null ? "" : value.toString();
                lineValue += stringValue;
                for (int i = 0; i < (length - stringValue.length()); i++) {
                    lineValue += filler;
                }
            }
            return lineValue;
        }
    }

    public String numberPadding(int length, Integer number) {
        String result = number.toString();
        int paddingLength = length - result.length();
        for (int i = 0; i < paddingLength; i++) {
            result = "0" + result;
        }
        return result;
    }

    enum ValueType {
        NUMERIC, STRING
    }
}
