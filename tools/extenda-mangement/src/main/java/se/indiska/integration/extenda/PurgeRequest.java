package se.indiska.integration.extenda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by heintz on 2017-08-21.
 */
public class PurgeRequest {
    private static final Logger logger = LoggerFactory.getLogger(PurgeRequest.class);
    private Date endDate = null;
    private Date startDate = null;
    private List<String> items = new ArrayList<>();

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
