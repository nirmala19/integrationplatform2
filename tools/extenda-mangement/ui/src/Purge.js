import React from "react";
import {Button, TextArea, Form, Segment} from "semantic-ui-react";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import Moment from "moment";
import request from "superagent";
import Cookies from 'universal-cookie';
const cookies = new Cookies();

export default class Purge extends React.Component {
    constructor() {
        super();
        this.state = ({
            itemNos: "",
            summary: null,
            endDate: new Moment().subtract(1, "days"),
            campaignStartDateRestriction: null
        })
    }

    changeItemNos(e) {
        this.setState({itemNos: e.target.value});
    }

    startPurge() {
        this.setState({summary: true});
    }

    cancelPurge() {
        this.setState({summary: false});
    }

    executePurge() {
        let self = this;
        let endDate = new Moment(this.state.endDate);
        let startDate = new Moment(this.state.campaignStartDateRestriction);
        startDate.startOf("day");
        endDate.startOf("day");
        // endDate.millisecond(0);

        let token = cookies.get("token");
        request.post("rest/campaign2past")
            .set('token', token)
            .send(
                {
                    items: this.state.itemNos.replace(/\D+/g, ",").split(",").filter(no => no.length > 0),
                    endDate: endDate.toISOString(),
                    startDate: startDate.toISOString()
                }
            )
            .end((err, res) => {
                if (err) {
                    self.setState({error: err});
                } else if (res.body.success === true) {
                    self.setState({success: true, processed: res.body.processed});
                }

                console.log("err", err);
                console.log("res", res);
            });
    }

    handleDayChange(selectedDay) {
        this.setState({endDate: selectedDay, summary: null})
    }

    handleRestrictionDayChange(selectedDay) {
        this.setState({campaignStartDateRestriction: selectedDay, summary: null})
    }

    reset() {
        this.setState({
            processed: null,
            success: false,
            summary: false,
            itemNos: "",
            endDate: new Moment().subtract(1, "days")
        });
    }

    render() {

        let startPurging, runPurge, purgeSummary, status, restartButton;
        if (this.state.success === true) {
            status = (
                <Segment inverted color='green'>
                    The purge was successfully executed in {this.state.processed} stores
                </Segment>
            );
            restartButton = <Button onClick={this.reset.bind(this)} primary>Clear</Button>;
        } else if (this.state.summary !== true) {
            startPurging = <Button onClick={this.startPurge.bind(this)} primary>Start purging</Button>;
        } else {
            let yesterday = new Moment().subtract(1, "days");
            let endDate = this.state.endDate || yesterday;
            let startDateRestriction;
            if (this.state.campaignStartDateRestriction) {
                startDateRestriction = <span>
                    <b>ONLY campaigns which have the start date
                        <i> {this.state.campaignStartDateRestriction.format("YYYY-MM-DD")}</i>
                    </b>
                    </span>
            } else {
                startDateRestriction = <b><i>all active campaigns</i></b>;
            }
            purgeSummary = (
                <Segment inverted color='orange'>
                    <h3>Warning!</h3>
                    By clicking the <i>Execute purge</i> button you will change the end date
                    for {startDateRestriction} for following items
                    <ul>
                        {this.state.itemNos.replace(/\D+/g, ",").split(",").filter(no => no.length > 0).map((item, i) => {
                            return <li key={item + i}>{item}</li>
                        })}
                    </ul>
                    <p>
                        to
                        the date <b>{endDate.endOf("day").format("YYYY-MM-DD HH:mm:ss")}</b>
                    </p>
                </Segment>
            );
            runPurge = <div>
                <Button onClick={this.executePurge.bind(this)} primary>Execute purge</Button>
                <Button onClick={this.cancelPurge.bind(this)} negative>Modify purge</Button>
            </div>;
        }

        return (
            <div>
                <h2>Purge current campaigns for items</h2>
                <Form>
                <TextArea autoHeight
                          disabled={this.state.summary === true}
                          value={this.state.itemNos} onChange={this.changeItemNos.bind(this)}
                          style={{
                              width: "500px",
                              display: "block",
                              marginBottom: "10px",
                          }}
                          placeholder="Enter one or more item no's" rows={2}/>
                    <div>
                        <label htmlFor="endDate">Enter last day this campaign is valid
                            <small> (campaign is valid until and including this date):</small>
                        </label>
                    </div>
                    <DayPickerInput
                        disabled={this.state.summary === true}
                        id="endDate"
                        onDayChange={this.handleDayChange.bind(this)}
                        placeholder="YYYY-MM-DD"
                        format="YYYY-MM-DD"
                        value={this.state.endDate ? this.state.endDate.format("YYYY-MM-DD") : ""}
                    />
                    <p/>
                    <div>
                        <label htmlFor="startDate">Limit purge to campaigns starting this specific date
                            <small> (leave empty to purge all campaigns):</small>
                        </label>
                    </div>
                    <DayPickerInput
                        disabled={this.state.summary === true}
                        id="startDate"
                        onDayChange={this.handleRestrictionDayChange.bind(this)}
                        placeholder="YYYY-MM-DD"
                        format="YYYY-MM-DD"
                        value={this.state.campaignStartDateRestriction ? this.state.campaignStartDateRestriction.format("YYYY-MM-DD") : ""}
                    />
                    <p/>
                    {startPurging}
                    {purgeSummary}
                    {runPurge}
                    {status}
                    {restartButton}
                </Form>
            </div>
        );
    }
}