import React, {Component} from 'react';
import Layout from './Layout';
import InvestigateCampaigns from "./InvestigateCampaigns";
import Purge from "./Purge";
import "./styles/main.scss";
import {Tab} from "semantic-ui-react";
import {Button, Icon} from "semantic-ui-react";
import Cookies from 'universal-cookie';
const cookies = new Cookies();

// If you use React Router, make this component
// render <Router> with your routes. Currently,
// only synchronous routes are hot reloaded, and
// you will see a warning from <Router> on every reload.
// You can ignore this warning. For details, see:
// https://github.com/reactjs/react-router/issues/2182
export default class App extends Component {
    render() {
        const panes = [
            {
                menuItem: 'Investigate items',
                render: () => <Tab.Pane attached={false}><InvestigateCampaigns /></Tab.Pane>
            },
            {menuItem: 'Purge', render: () => <Tab.Pane attached={false}><Purge/></Tab.Pane>}
        ];

        let logoutStyle = {
            position: "absolute",
            top: "0",
            right: "0",
            margin: "0",
            borderRadius: "0 0 0 4px"

        };
        return (
            <Layout>
                <Button onClick={this.signOut} style={logoutStyle} size="large" color="blue" icon="sign out"
                        labelPosition="right"
                        content="Sign out"/>
                <h2>Indiska Store Campaigns</h2>
                <Tab menu={{pointing: true}} panes={panes}/>

            </Layout>
        );
    }

    signOut() {
        cookies.remove("token");
        window.location = window.location.href;
    }

}
