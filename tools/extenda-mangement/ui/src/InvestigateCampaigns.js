import React from "react";
import request from "superagent";
import Moment from "moment";
import {TextArea, Button, Form, Icon, Dimmer, Loader, Segment, Tab} from "semantic-ui-react";
import Cookies from 'universal-cookie';

const cookies = new Cookies();

export default class InvestigateCampaigns extends React.Component {
    constructor() {
        super();
        this.state = ({itemNo: "", campaigns: null, loading: false, unresponsive: []})
    }

    changeItemNo(e) {
        let itemNos = e.target.value.replace(/\D+/g, ", ");
        this.setState({itemNo: itemNos});
    }

    investigate() {
        let self = this;

        let cleanItemNos = this.state.itemNo.replace(/\D+/g, ",");
        let itemNos = cleanItemNos.split(",");

        let requestedItemNos = cleanItemNos.split(",").filter(item => item.length > 0);
        self.setState({loading: true, campaigns: null, details: null, unresponsive: []})

        let token = cookies.get("token");
        request.post("rest/investigatecampaign")
            .set('token', token)
            .send(itemNos)
            .end(function (err, res) {
                let details = {};
                let summary = {};
                let storeIds = Object.keys(res.body.details);
                console.log("storeIds: ", storeIds);

                requestedItemNos.map(itemNo => {
                    summary[itemNo] = {count: 0, missingStores: {}, inStores: {}, uniqueCampaigns: {}};
                });


                storeIds.map(storeId => {
                    let items = res.body.details[storeId];
                    let itemMap = {};
                    items.map(item => {
                        if (item.item) {
                            if (!itemMap[item.item]) {
                                itemMap[item.item] = {};
                            }
                            itemMap[item.item][item.fromDate] = item;
                        }
                    });

                    requestedItemNos.map(itemNo => {
                        if (!itemMap[itemNo]) {
                            summary[itemNo].missingStores[storeId] = true;
                        } else {
                            summary[itemNo].count = summary[itemNo].count + 1;
                            summary[itemNo].inStores[storeId] = true;
                            console.log(itemNo, itemMap[itemNo]);
                            Object.keys(itemMap[itemNo]).map(fromDate => {
                                summary[itemNo].uniqueCampaigns[fromDate] = true;
                            });

                        }
                    });

                    // console.log("ItemMap: ", itemMap);
                    // requestedItemNos.map(itemNo => {
                    //
                    // });
                });
                console.log("summary", summary)


                Object.keys(res.body.details).forEach(storeId => {
                    let itemMap = {};
                    requestedItemNos.forEach(itemNo => {
                        itemMap[itemNo] = [];
                    });
                    if (res.body.details[storeId]) {
                        res.body.details[storeId].forEach(campaign => {
                            if (!itemMap[campaign.item]) {
                                itemMap[campaign.item] = [];
                            }
                            itemMap[campaign.item].push(campaign);
                        });
                    }
                    details[storeId] = itemMap;
                });
                self.setState({
                    campaigns: res.body,
                    details,
                    summary,
                    requestedItemNos: requestedItemNos,
                    unresponsive: res.body.stats.unresponsiveStores,
                    loading: false
                });
            });
    }

    render() {
        let campaigns;
        let unresponsive;
        if (this.state.unresponsive.length > 0) {
            unresponsive = <Segment inverted color='red'>
                There is unresponsive
                stores: {this.state.unresponsive.map(storeId => storeId + ",")}
            </Segment>
        }
        console.log("summary 2 ", this.state.summary);
        if (this.state.campaigns) {
            let panes = [];
            panes.push({
                menuItem: 'Summary', render: () => <Tab.Pane>
                    <h3>Summary</h3>
                    <table>
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Exists in # of stores</th>
                            <th>Missing in # of stores</th>
                            <th># of current campaigns</th>
                            <th>Missing from stores</th>
                        </tr>
                        </thead>
                        <tbody>
                        {Object.keys(this.state.summary).map(itemNo => {
                            return (
                                <tr key={itemNo}>
                                    <td>{itemNo}</td>
                                    <td>{Object.keys(this.state.summary[itemNo].inStores).length}</td>
                                    <td>{Object.keys(this.state.summary[itemNo].missingStores).length}</td>
                                    <td>{Object.keys(this.state.summary[itemNo].uniqueCampaigns).length}</td>
                                    <td>{Object.keys(this.state.summary[itemNo].missingStores).map(storeId => {
                                        return storeId + ", ";
                                    })}</td>
                                </tr>
                            );
                        })}
                        </tbody>
                    </table>
                </Tab.Pane>
            });
            panes.push({
                menuItem: 'Details', render: () => <Tab.Pane>
                    <h3>Details</h3>
                    <table>
                        <thead>
                        <tr>
                            <th colSpan={2}>Store</th>
                            <th>Item no</th>
                            <th>Campaign price</th>
                            <th>Country code</th>
                            <th>Discount</th>
                            <th>From date</th>
                            <th>To date</th>
                            <th>Correct store</th>
                        </tr>
                        </thead>
                        <tbody>
                        {Object.keys(this.state.details).map((storeId, storeCounter) => {

                            if (this.state.details[storeId].length == 0) {
                                return (
                                    <tr key={storeId + "_" + storeCounter}>
                                        <td>{storeId}</td>
                                    </tr>
                                );
                            } else {
                                return this.state.requestedItemNos.map((itemNo, itemCounter) => {
                                    let campaigns = this.state.details[storeId][itemNo];
                                    if (campaigns.length > 0) {
                                        return campaigns.map((campaign, campaignCounter) => {
                                            return (
                                                <tr key={storeId + "_" + storeCounter + "_" + itemCounter + "_" + itemNo + "_" + campaignCounter}>
                                                    <td>{storeId}</td>
                                                    <td>{campaign.storeName}</td>
                                                    <td>{campaign.item}</td>
                                                    <td>{campaign.campaignPrice}</td>
                                                    <td>{campaign.countryCode}</td>
                                                    <td>{campaign.discount}</td>
                                                    <td>{new Moment(campaign.fromDate).format("YYYY-MM-DD HH:mm:ss")}</td>
                                                    <td>{new Moment(campaign.toDate).format("YYYY-MM-DD HH:mm:ss")}</td>
                                                    <td>{campaign.shop === campaign.store ?
                                                        <Icon name="check" color="green"/> :
                                                        <Icon name="delete" color="red"/>}</td>
                                                </tr>
                                            );
                                        });
                                    } else {
                                        return (
                                            <tr key={storeId + "_" + storeCounter + "_" + itemCounter + "_" + itemNo}>
                                                <td>{storeId}</td>
                                                <td></td>
                                                <td>{itemNo}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        );
                                    }
                                });
                                // return Object.keys(this.state.details[storeId]).map((itemNo, itemCounter) => {
                                // });
                            }
                        })}
                        </tbody>
                    </table>
                </Tab.Pane>
            });
            campaigns = (
                <div>
                    {unresponsive}
                    <Tab panes={panes}/>
                </div>
            );
        }
        let loading;
        if (this.state.loading === true) {
            let s = {
                width: "100%",
                height: "100%",
                position: "absolute",
                left: "0",
                top: "0"
            };
            loading = (
                <div style={s}>
                    <Dimmer active>
                        <Loader/>
                    </Dimmer>
                </div>
            );
        }
        return (
            <div>
                <h3>---Investigate current and future campaigns for items</h3>
                <Form>
                <TextArea autoHeight value={this.state.itemNo} onChange={this.changeItemNo.bind(this)}
                          style={{
                              width: "500px",
                              display: "block",
                              marginBottom: "10px",
                          }}
                          placeholder="Enter or paste one or more item no's" rows={2}/>
                    <Button primary onClick={this.investigate.bind(this)}>Investigate</Button>
                    <Loader active={this.state.loading === true} inline/>
                </Form>
                <div style={{marginTop: "10px"}}>
                    {campaigns}
                </div>
            </div>
        );
    }
}
