package se.indiska.auth;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.eclipse.jetty.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by heintz on 2017-08-22.
 */
@RestController
@RequestMapping("/auth")
public class AuthenticationService {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationService.class);
    private static Cache<String, UserSession> tokenCache =
            CacheBuilder.newBuilder()
                    .expireAfterAccess(1, TimeUnit.HOURS)
                    .maximumSize(1000)
                    .build();
    @Autowired
    private ActiveDirectoryAuthentication authenticationService;

    @RequestMapping(value = "/login", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity login(@RequestBody LoginRequest loginRequest) {
        Map result = new HashMap<>();
        try {
            result = authenticate(loginRequest);
            if (result != null) {
                return ResponseEntity.ok(result);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED_401).build();
            }
        } catch (LoginException e) {
            result.put("status", "error");
            result.put("error", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR_500).body(result);
        }
    }

    private Map authenticate(LoginRequest loginRequest) throws LoginException {
        Set<String> authenticated = authenticationService.authenticate(loginRequest.getUsername(), loginRequest.getPassword());
        if (authenticated != null && authenticated.contains(loginRequest.getAdGroup().toLowerCase())) {
            Marker auth = MarkerFactory.getMarker("AUTH");
            Map result = new HashMap<>();
            String token = UUID.randomUUID().toString();
            tokenCache.put(token, new UserSession(loginRequest.getUsername(), authenticated));
            result.put("status", "success");
            result.put("token", token);
            return result;
        } else {
            Marker auth = MarkerFactory.getMarker("AUTH");
            logger.warn(auth, "Failed to authenticate user " + loginRequest.getUsername());
            return null;
        }
    }

    @RequestMapping(value = "/validate/{token}/{group}", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity validateLogin(@PathVariable String token, @PathVariable String group) {
        UserSession user = tokenCache.getIfPresent(token);
//        if (!group.equals("indioadm")) {
            if (user != null && (group != null && user.getUserGroups().contains(group.toLowerCase())) || group == null) {
                return ResponseEntity.ok().build();
            }
//        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED_401).build();
    }

    @RequestMapping(value = "/logout/{token}", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity logout(@PathVariable String token) {
        UserSession user = tokenCache.getIfPresent(token);
        if (user != null) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED_401).build();
    }

    @RequestMapping(value = "/signin", produces = "text/html;charset=UTF-8", method = RequestMethod.GET)
    public ResponseEntity loginView(HttpServletRequest request) {
        return ResponseEntity.ok(getLoginPage(request.getParameter("redirect"), null));
    }

    @RequestMapping(value = "/signin", produces = "text/html;charset=UTF-8", method = RequestMethod.POST)
    public ResponseEntity handleLogin(HttpServletRequest request, HttpServletResponse response) throws LoginException, IOException {
        String redirectUrl = request.getParameter("redirect");
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setAdGroup("indio");
        loginRequest.setUsername(request.getParameter("username"));
        loginRequest.setPassword(request.getParameter("password"));


        String error = null;
        Map authentication = authenticate(loginRequest);
        if (authentication != null) {
            response.sendRedirect(redirectUrl + "?token=" + authentication.get("token"));
        } else {
            error = "Invalid login or credentials not sufficient";
        }
        return ResponseEntity.ok(getLoginPage(request.getParameter("redirect"), error));
    }

    private String getLoginPage(String redirectUrl, String error) {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("index.html");
        StringBuilder sb = new StringBuilder();
        byte[] buf = new byte[1024];
        int c = 0;
        try {
            while ((c = is.read(buf)) > 0) {
                sb.append(new String(buf, 0, c));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (error != null) {
            error = "<div class='error'>" + error + "</div>";
        } else {
            error = "";
        }
        return sb.toString().replaceAll("\\{redirect\\}", redirectUrl).replaceAll("\\{error\\}", error);
    }

    class UserSession {
        private String username;
        private Set<String> userGroups;
        private LocalDateTime now = LocalDateTime.now();

        public UserSession(String username, Set<String> userGroups) {
            this.username = username;
            this.userGroups = userGroups;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Set<String> getUserGroups() {
            return userGroups;
        }

        public void setUserGroups(Set<String> userGroups) {
            this.userGroups = userGroups;
        }

        public LocalDateTime getNow() {
            return now;
        }
    }
}
