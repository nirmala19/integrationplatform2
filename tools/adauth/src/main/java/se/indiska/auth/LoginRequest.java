package se.indiska.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by heintz on 2017-08-22.
 */
public class LoginRequest {
    private static final Logger logger = LoggerFactory.getLogger(LoginRequest.class);
    private String username = null;
    private String password = null;
    private String adGroup = null;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdGroup() {
        return adGroup;
    }

    public void setAdGroup(String adGroup) {
        this.adGroup = adGroup;
    }
}
