package se.indiska.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.security.auth.login.AccountException;
import javax.security.auth.login.LoginException;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created by heintz on 2017-08-22.
 */
public class ActiveDirectoryAuthentication {
    private static final Logger logger = LoggerFactory.getLogger(ActiveDirectoryAuthentication.class);
    private static final String CONTEXT_FACTORY_CLASS = "com.sun.jndi.ldap.LdapCtxFactory";
    public static final String DISTINGUISHED_NAME = "distinguishedName";
    public static final String CN = "cn";
    public static final String MEMBER = "member";
    public static final String MEMBER_OF = "memberOf";
    public static final String SEARCH_BY_SAM_ACCOUNT_NAME = "(SAMAccountName={0})";
    public static final String SEARCH_GROUP_BY_GROUP_CN = "(&(objectCategory=group)(cn={0}))";

    private String ldapServerUrls[];

    private int lastLdapUrlIndex;

    private final String domainName;

    public ActiveDirectoryAuthentication(String domainName) {
        this.domainName = domainName.toUpperCase();

        try {
            ldapServerUrls = nsLookup(domainName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        lastLdapUrlIndex = 0;
    }

    Set<String> authenticate(String username, String password) throws LoginException {
        if (ldapServerUrls == null || ldapServerUrls.length == 0) {
            throw new AccountException("Unable to find ldap servers");
        }
        if (username == null || password == null || username.trim().length() == 0 || password.trim().length() == 0) {
            return null;
        }
        int retryCount = 0;
        int currentLdapUrlIndex = lastLdapUrlIndex;
        do {
            retryCount++;
            try {
                Hashtable<Object, Object> env = new Hashtable<Object, Object>();
                env.put(Context.INITIAL_CONTEXT_FACTORY, CONTEXT_FACTORY_CLASS);
                env.put(Context.PROVIDER_URL, ldapServerUrls[currentLdapUrlIndex]);
                env.put(Context.SECURITY_PRINCIPAL, username + "@" + domainName);
                env.put(Context.SECURITY_CREDENTIALS, password);
                DirContext ctx = new InitialDirContext(env);
                String defaultSearchBase = "OU=Indiska Magasinet AB,DC=hk,DC=indiska,DC=se";
                String groupDistinguishedName = "OU=Groups,OU=Indiska Magasinet AB,DC=hk,DC=indiska,DC=se";

                SearchResult sr = executeSearchSingleResult(ctx, SearchControls.SUBTREE_SCOPE, defaultSearchBase,
                        MessageFormat.format(SEARCH_BY_SAM_ACCOUNT_NAME, new Object[]{username}),
                        new String[]{DISTINGUISHED_NAME, CN, MEMBER_OF}
                );

                String groupCN = getCN(groupDistinguishedName);
                HashMap processedUserGroups = new HashMap();
                HashMap unProcessedUserGroups = new HashMap();

                // Look for and process memberOf
                Attribute memberOf = sr.getAttributes().get(MEMBER_OF);
                HashSet<String> groups = null;
                boolean found = false;
                if (memberOf != null) {
                    groups = new HashSet<>();
                    NamingEnumeration e1 = memberOf.getAll();
                    while (e1.hasMoreElements() && !found) {
                        String unprocessedGroupDN = e1.nextElement().toString();
                        String unprocessedGroupCN = getCN(unprocessedGroupDN);
                        groups.add(unprocessedGroupCN.toLowerCase());
                        // Quick check for direct membership
//                        if (isSame(groupCN, unprocessedGroupCN) && isSame(groupDistinguishedName, unprocessedGroupDN)) {
//                            found = true;
//                        }
                    }
                }
                ctx.close();
                lastLdapUrlIndex = currentLdapUrlIndex;
                return groups;
            } catch (CommunicationException exp) {
                logger.error("Communication error!", exp);
                if (retryCount < ldapServerUrls.length) {
                    currentLdapUrlIndex++;
                    if (currentLdapUrlIndex == ldapServerUrls.length) {
                        currentLdapUrlIndex = 0;
                    }
                    continue;
                }
                return null;
            } catch (Throwable throwable) {
                return null;
            }
        } while (true);
    }

    private static NamingEnumeration executeSearch(DirContext ctx, int searchScope, String searchBase, String searchFilter, String[] attributes) throws NamingException {
        // Create the search controls
        SearchControls searchCtls = new SearchControls();

        // Specify the attributes to return
        if (attributes != null) {
            searchCtls.setReturningAttributes(attributes);
        }

        // Specify the search scope
        searchCtls.setSearchScope(searchScope);

        // Search for objects using the filter
        NamingEnumeration result = ctx.search(searchBase, searchFilter, searchCtls);
        return result;
    }

    private static SearchResult executeSearchSingleResult(DirContext ctx, int searchScope, String searchBase, String searchFilter, String[] attributes) throws NamingException {
        NamingEnumeration result = executeSearch(ctx, searchScope, searchBase, searchFilter, attributes);

        SearchResult sr = null;
        // Loop through the search results
        while (result.hasMoreElements()) {
            sr = (SearchResult) result.next();
            break;
        }
        return sr;
    }

    private static String[] nsLookup(String argDomain) throws Exception {
        try {
            Hashtable<Object, Object> env = new Hashtable<Object, Object>();
            env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.dns.DnsContextFactory");
            env.put("java.naming.provider.url", "dns:");
            DirContext ctx = new InitialDirContext(env);

            Attributes attributes = ctx.getAttributes(String.format("_ldap._tcp.%s", argDomain), new String[]{"srv"});
            // try thrice to get the KDC servers before throwing error
            for (int i = 0; i < 3; i++) {
                Attribute a = attributes.get("srv");
                if (a != null) {
                    List<String> domainServers = new ArrayList<String>();
                    NamingEnumeration<?> enumeration = a.getAll();
                    while (enumeration.hasMoreElements()) {
                        String srvAttr = (String) enumeration.next();
                        // the value are in space separated 0) priority 1)
                        // weight 2) port 3) server
                        String values[] = srvAttr.toString().split(" ");
                        domainServers.add(String.format("ldap://%s:%s", values[3], values[2]));
                    }
                    String domainServersArray[] = new String[domainServers.size()];
                    domainServers.toArray(domainServersArray);
                    return domainServersArray;
                }
            }
            throw new Exception("Unable to find srv attribute for the domain " + argDomain);
        } catch (NamingException exp) {
            throw new Exception("Error while performing nslookup. Root Cause: " + exp.getMessage(), exp);
        }
    }

    public static String getCN(String cnName) {
        if (cnName != null && cnName.toUpperCase().startsWith("CN=")) {
            cnName = cnName.substring(3);
        }
        int position = cnName.indexOf(',');
        if (position == -1) {
            return cnName;
        } else {
            return cnName.substring(0, position);
        }
    }

    public static boolean isSame(String target, String candidate) {
        if (target != null && target.equalsIgnoreCase(candidate)) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        ActiveDirectoryAuthentication authentication = new ActiveDirectoryAuthentication("hk.indiska.se");
        try {
            Set<String> authResult = authentication.authenticate("andersh", "W2yoeas4");

            System.out.print("Auth: " + authResult);
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }
}
