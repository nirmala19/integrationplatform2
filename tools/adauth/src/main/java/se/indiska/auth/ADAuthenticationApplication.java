package se.indiska.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

/**
 * Created by heintz on 2017-08-22.
 */
@SpringBootApplication
public class ADAuthenticationApplication {
    private static final Logger logger = LoggerFactory.getLogger(ADAuthenticationApplication.class);

    @Autowired
    private Environment environment;

    public static void main(String[] args) {
        SpringApplication.run(ADAuthenticationApplication.class, args);
    }

    @Bean
    public ActiveDirectoryAuthentication activeDirectoryAuthentication() {
        return new ActiveDirectoryAuthentication("hk.indiska.se");
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return (container -> {
            container.setPort(8100);
        });
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
        };
    }
}
