import {AppContainer} from 'react-hot-loader';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Cookies from 'universal-cookie';
import request from "superagent";
import {BrowserRouter} from "react-router-dom";

const cookies = new Cookies();

const rootEl = document.getElementById('root');
var query = window.location.href.substring((window.location.protocol + "//" + window.location.host + "/").length);

// let store;
// if (query.length > 0) {
//     store = Number(query.replace(/.*?([0-9]+).*/, "$1"));
// }

const render = Component =>
    ReactDOM.render(
        <AppContainer>
            <BrowserRouter>
                <Component/>
            </BrowserRouter>
        </AppContainer>,
        rootEl
    );

render(App);
if (module.hot) module.hot.accept('./App', () => render(App));


