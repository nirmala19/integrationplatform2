import React from "react";
import {Chart} from "react-google-charts";
import PropTypes from "prop-types";

export default class AvgPcsGauge extends React.Component {
    render() {
        let size = this.props.size || 280;
        let title = this.props.title;
        if (!title) {
            title = <h3>Average pcs/purchase</h3>;
        }
        return (
            <div style={{display: "inline-flex", flexDirection: "column", alignItems: "center"}}>
                {title}
                <Chart
                    width={size + "px"}
                    height={size + "px"}
                    chartType="Gauge"
                    data={[
                        ['Label', 'Value'],
                        ['Pcs', Math.round(this.props.averageTransactionQuantity * 100) / 100],
                    ]}
                    options={
                        {
                            redFrom: 0, redTo: 1.7,
                            yellowFrom: 1.7, yellowTo: 3.0,
                            greenFrom: 3.0, greenTo: 5,
                            max: 5,
                            minorTicks: 10
                        }
                    }
                />
            </div>
        );
    }
}
AvgPcsGauge.propTypes = {
    averageTransactionQuantity: PropTypes.number,
    size: PropTypes.number,
    title: PropTypes.element
};

AvgPcsGauge.defaultProps = {
    averageTransactionQuantity: 0
};