import React from "react";
import {Chart} from "react-google-charts";
import PropTypes from "prop-types";

export default class AvgReceiptGauge extends React.Component {
    render() {
        let size = this.props.size || 280;
        let title = this.props.title;
        if (!title) {
            title = <h3>Average purchase value<br/><span style={{fontSize: "70%"}}>incl returns</span></h3>;
        }
        return (
            <div style={{display: "inline-flex", flexDirection: "column", alignItems: "center"}}>
                {title}
                <Chart
                    chartType="Gauge"
                    width={size + "px"}
                    height={size + "px"}
                    data={[
                        ['Label', 'Value'],
                        ['SEK', Math.round(this.props.averageTransactionValue)],
                    ]}
                    options={
                        {
                            redFrom: 0, redTo: 200,
                            yellowFrom: 201, yellowTo: 350,
                            greenFrom: 351, greenTo: 500,
                            max: 500,
                            minorTicks: 10
                        }
                    }
                />
            </div>
        );
    }
}

AvgReceiptGauge.propTypes = {
    averageTransactionValue: PropTypes.number,
    size: PropTypes.number,
    title: PropTypes.element
};

AvgReceiptGauge.defaultProps = {
    averageTransactionValue: 0
};