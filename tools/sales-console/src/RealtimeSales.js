import React from "react";
import es from 'elasticsearch-browser'
import Moment from "moment";
import {Button, Dropdown} from "semantic-ui-react";
import {Chart} from 'react-google-charts';
import storeOptions from "./StoreOptions.json";
import AnimatedNumber from 'react-animated-number';

var later = require("later");
import budget from "./lysales.json";
import {averageReceiptAndItemsPP, realtimeData, salesCurveShape} from "./ESQueries";
import AvgPcsGauge from "./gauges/AvgPcsGauge";
import AvgReceiptGauge from "./gauges/AvgReceiptGauge";

const END_HOUR = 20;

export default class RealtimeSales extends React.Component {
    constructor() {
        super();

        this.state = ({
            paused: false,
            startHour: 9,
            endHour: END_HOUR,
            dayData: {},
            lastYearData: {},
            lastMonthData: {},
            currentTime: this.getTime(),
            timeHistoricalDataGenerated: new Moment().startOf("year"),
            averageTransactionValue: 0,
            averageTransactionQuantity: 0,
            lyAverageTransactionValue: 0,
            lyAverageTransactionQuantity: 0,
            store: null,
            data: {},
            purchases: 0,
            totalSumToday: 0,
            dailySalesCurve: {},
        });
        this.intervalId = null;
        later.date.localTime();

    }

    getTime() {
        return new Moment();//.subtract(4, "hour");
    }

    componentWillMount() {
        let self = this;
        self.updateSalesData(this.props.store);
        this.setState({store: this.props.store});
        this.intervalId = window.setInterval(() => {
            if (!self.state.paused) {
                self.updateSalesData(this.state.store);
            }
        }, 10000);
        this.hourly();
        let hourlySchedule = later.parse.recur().on(1).hour().on(1).minute(1);
        later.setInterval(this.hourly.bind(this), hourlySchedule);
    }

    hourly() {
        this.setState({endHour: END_HOUR});
        let self = this;
        this.loadDay(this.props.store || this.state.store);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({store: nextProps.store})
    }


    updateSalesData(store) {
        let self = this;
        let nextTime = this.getTime();
        let promises = [];
        promises.push(new Promise((resolve, reject) => {
            self.loadData(store, nextTime, "2m", data => {
                let keys = Object.keys(data.timeline);

                let time = self.getTime();
                if (keys[keys.length - 1]) {
                    time = new Moment(nextTime.format("YYYY-MM-DD") + " " + keys[keys.length - 1]);
                }
                let newState = {
                    dayData: data.timeline,
                    purchases: data.purchases,
                    totalSumToday: data.totalSumToday,
                    averageTransactionValue: data.averageTransactionValue,
                    averageTransactionQuantity: data.averageTransactionQuantity,
                    store: data.store,
                    currentTime: nextTime,
                };
                if (time.hour() + 1 > self.state.endHour) {
                    newState.endHour = time.hour() + 1;
                } else {
                    newState.endHour = self.state.endHour;
                }
                resolve(newState);
                // self.setState(newState);
            });

        }));

        promises.push(new Promise((resolve, reject) => {
            let today = this.getTime();
            let lastYear = new Moment(today)
                .subtract(1, 'year')
                .isoWeek(today.isoWeek())
                .isoWeekday(today.isoWeekday());

            averageReceiptAndItemsPP(store, lastYear, response => {
                // resolve(response);
                console.log("resp: ", response);
                resolve({
                    lyAverageTransactionValue: response.averageTransactionValue,
                    lyAverageTransactionQuantity: response.averageTransactionQuantity,

                });
            });

        }));
        Promise.all(promises).then(newStates => {
            let newState = {};
            newStates.forEach(ns => {
                newState = Object.assign(newState, ns);
            });
            self.setState(newState);
        })
    }

    loadDay(store, callback) {
        let self = this;
        let promises = [];
        let today = this.getTime();
        let lastYear = new Moment(today)
            .subtract(1, 'year')
            .isoWeek(today.isoWeek())
            .isoWeekday(today.isoWeekday())
            .endOf("day");
        promises.push(new Promise((resolve, reject) => {
            self.loadData(store, lastYear, "30m", data => {
                console.log("data: ", data);
                let newState = {
                    lastYearData: data.timeline,
                    store: data.store,

                };

                let keys = Object.keys(data.timeline);

                let time = self.getTime();
                if (keys[keys.length - 1]) {
                    time = new Moment(this.getTime().format("YYYY-MM-DD") + " " + keys[keys.length - 1]);
                }
                if (time.hour() + 1 > END_HOUR) {
                    newState.endHour = time.hour() + 1;
                } else {
                    newState.endHour = END_HOUR;
                }

                resolve(newState);
            });
        }));
        // promises.push(new Promise((resolve, reject) => {
        //     let lastMonth = this.getTime().endOf("day").subtract(4, "weeks");
        //     self.loadData(store, lastMonth, "10m", data => {
        //         resolve({
        //             lastMonthData: data.timeline,
        //             store: data.store
        //         });
        //     });
        // }));
        // promises.push(new Promise((resolve, reject) => {
        //     salesCurveShape(this.getTime(), store, data => {
        //         resolve();
        //     })
        // }));

        Promise.all(promises).then(newStates => {
            let newState = {timeHistoricalDataGenerated: self.getTime()};
            newStates.forEach(ns => {
                newState = Object.assign(newState, ns);
            });
            self.setState(newState);
            if (callback) {
                callback();
            }
        });
    }

    componentWillUnmount() {
        window.clearInterval(this.intervalId);
    }

    loadData(store, to, interval, callback) {
        realtimeData(store, to, interval, callback);
    }

    render() {
        let data = [];

        let budgetSet = budget[this.getTime().format("YYYY-MM-DD")];

        let todaysBudget = budgetSet ? budgetSet[this.state.store ? this.state.store : "TOTAL"] : 0;

        let lastYearTodaySales = {};

        Object.keys(this.state.dailySalesCurve).forEach(t => {
            lastYearTodaySales[t] = Math.round(this.state.dailySalesCurve[t] * todaysBudget);
        });
        if (this.state.endHour > 20) {
            lastYearTodaySales[this.state.endHour + ":00:00"] = todaysBudget;
        }


        let currentTime = new Moment(this.getTime().local()).startOf("day").hour(this.state.startHour).minute(0).second(0).millisecond(0);

        let endTimeToShow = new Moment(this.getTime()).local().hour(this.state.endHour);
        let trendlinePoints = 0;
        while (currentTime.isBefore(endTimeToShow) || currentTime.isSame(endTimeToShow)) {
            let row = [[currentTime.hour(), currentTime.minute(), currentTime.second()]];
            // let lwData = this.state.lastYearData[currentTime.format("HH:mm:ss")];
            // row.push(lwData ? lwData : null);
            // let lmData = this.state.lastMonthData[currentTime.format("HH:mm:ss")];
            // row.push(lmData ? lmData : null);
            // let bData = lastYearTodaySales[currentTime.format("HH:mm:ss")];
            let bData = this.state.lastYearData[currentTime.format("HH:mm:ss")];
            row.push(bData >= 0 ? bData : null);

            let tdData = this.state.dayData[currentTime.format("HH:mm:ss")];
            row.push(tdData ? tdData : null);
            data.push(row);
            if (currentTime.isAfter(this.getTime().subtract(0, "hour").subtract(40, "minute")) && currentTime.isBefore(this.getTime().subtract(10, "minute"))) {
                let trendPoint = tdData ? tdData : null;
                if (trendPoint) {
                    trendlinePoints++;
                }
                row.push(trendPoint);
            } else {
                row.push(null);
            }
            currentTime.add(2, "minute");
        }

        if (data.length === 0) {
            // if (!this.state.store) {
            data = [["No data", 0, 0, 0, 0]];
            // } else {
            //     data = [["No data", 0, 0, 0]];
            // }
        }

        let storeInfo;
        if (!this.state.store) {
            storeInfo = "all stores (except ecom)";
        } else {
            storeInfo = "store " + this.state.store;
        }

        let columns = [
            {
                type: 'timeofday',
                label: 'Time',
            },
        ];
        let series = [
            // {color: "#ffcccc"},
            // {color: "#ccccff"},
        ];

        // if (!this.state.store) {
        columns.push({
            type: 'number',
            label: "Last year",
        });
        series.push({
            color: "#42BA3C",
            lineWidth: 1
        });
        // }
        columns.push({
            type: 'number',
            label: 'Today',
        });
        columns.push({
            type: 'number',
            label: 'Trend'
        });
        series.push({
            color: "#d2007a",
            lineWidth: 5
        });
        series.push({
            color: "#cccccc",
            lineWidth: 0,
            visibleInLegend: false,
        });

        let chartOptions = {
            interpolateNulls: true,
            curveType: 'function',
            chartArea: {
                'width': '93%',
                'height': '90%',
                right: "2px",
                top: 10,
            },
            legend: {'position': 'bottom'},
            axisTitlesPosition: "in",
            hAxis: {
                format: 'HH:mm',
                gridlines: {count: 11},
                viewWindow: {
                    min: [this.state.startHour, 0, 0],
                    max: [this.state.endHour, 0, 0]
                },
                title: "Time",
                textStyle: {
                    bold: true
                },
            },
            vAxis: {
                minValue: 0,
                viewWindow: {
                    min: -5000,
                },
                title: "SEK",
                textStyle: {
                    bold: true
                },
                format: "short"
            },

            series
        };
        if (trendlinePoints > 3) {
            chartOptions.trendlines = {
                2: {
                    visibleInLegend: false,
                }
            };
        }

        let cHeight = window.innerHeight - 100;

        let chartHeight = Math.min(900, cHeight) + "px";
        let gaugeSize = Math.min(280, ((cHeight - 98 - 85 - 51) / 2));

        return (
            <div>
                <div style={{display: "flex", padding: "20px 20px 10px 20px"}}>
                    <h1 style={{fontSize: "40px", marginBottom: "0"}}>Sales stats for {storeInfo} &nbsp;- {this.getTime().local().format("YYYY-MM-DD HH:mm")}</h1>
                </div>

                <div style={{display: "flex", alignItems: "flex-start", justifyContent: "center", maxWidth: "100%"}}>
                    <div style={{flexGrow: 1, height: chartHeight}}>
                        <Chart
                            chartType="LineChart"
                            columns={columns}
                            rows={data}
                            options={chartOptions}
                            graph_id="LastMonthSales"
                            width="100%"
                            height="100%"
                            legend_toggle
                        />
                    </div>
                    <div style={{minWidth: "390px", minHeight: "400px", padding: "0 20px 0 0", textAlign: "center"}}>
                        <div className="centeredColumn">
                            <h4>Average receipt</h4>
                            <div style={{display: "flex"}}>
                                <AvgReceiptGauge size={200}
                                                 averageTransactionValue={this.state.averageTransactionValue || 0}
                                                 title={<h5>Today</h5>}
                                />
                                <AvgReceiptGauge size={200}
                                                 averageTransactionValue={this.state.lyAverageTransactionValue || 0}
                                                 title={<h5>LY ({this.getLY().format("YYYY-MM-DD")})</h5>}

                                />
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div>&nbsp;</div>
                        <div className="centeredColumn">
                            <h4>Average pcs/purchase</h4>
                            <div style={{display: "flex"}}>
                                <AvgPcsGauge size={200}
                                             averageTransactionQuantity={this.state.averageTransactionQuantity || 0}
                                             title={<h5>Today</h5>}
                                />
                                <AvgPcsGauge size={200}
                                             averageTransactionQuantity={this.state.lyAverageTransactionQuantity || 0}
                                             title={<h5>LY ({this.getLY().format("YYYY-MM-DD")})</h5>}
                                />
                            </div>
                        </div>
                        <div>&nbsp;</div>
                        <div style={{fontFamily: "Montserrat, sans-serif"}}>
                            <table style={{width: "100%"}}>
                                <tbody>
                                <tr>
                                    <td style={{textAlign: "left"}}>
                                        <h3>Purchases</h3>
                                    </td>
                                    <td style={{textAlign: "right"}}>
                                        <AnimatedNumber
                                            style={{
                                                transition: '1.8s ease-out',
                                                fontSize: 22,
                                                transitionProperty:
                                                    'background-color, color, opacity'
                                            }}
                                            frameStyle={perc => (
                                                perc === 100 ? {} : {opacity: 0.25}
                                            )}
                                            duration={300}
                                            value={this.state.purchases}
                                            component="text"
                                            formatValue={n => this.prettyNumber(n)}/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{textAlign: "left"}}>
                                        <h3>Approx revenue</h3>
                                    </td>
                                    <td style={{textAlign: "right"}}>
                                        <AnimatedNumber
                                            style={{
                                                transition: '1.8s ease-out',
                                                fontSize: 22,
                                                transitionProperty:
                                                    'background-color, color, opacity'
                                            }}
                                            frameStyle={perc => (
                                                perc === 100 ? {} : {opacity: 0.25}
                                            )}
                                            duration={300}
                                            value={this.state.totalSumToday}
                                            component="text"
                                            formatValue={n => this.prettyNumber(n, true)}/>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        )
    }

    getLY(date) {
        let today = new Moment(this.state.date || date);
        let lastYear = new Moment(today).subtract(1, 'year')
            .isoWeek(today.isoWeek())
            .isoWeekday(today.isoWeekday());
        return lastYear;
    }

    prettyNumber(n, currency) {
        // return Number(100000).toLocaleString("sv-SE");
        if (currency) {
            return n.toLocaleString('sv-SE', {maximumFractionDigits: 0})
        }


        return n.toLocaleString("sv-SE");
    }

}