import React from "react";
import PropTypes from "prop-types";
import Moment from "moment";
import es from 'elasticsearch-browser'
import {Chart} from 'react-google-charts';
import {Button, Menu} from "semantic-ui-react";
import {averageReceiptAndItemsPP, hourlySales, hourlyVisitors} from "./ESQueries";
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import AvgReceiptGauge from "./gauges/AvgReceiptGauge";
import AvgPcsGauge from "./gauges/AvgPcsGauge";
import DayPicker from "react-day-picker";
import SimpleDatePicker from "./SimpleDatePicker";

const client = new es.Client({
    host: window.location.protocol + "//" + window.location.host + "/search",
    log: 'warning'
});

export default class WeekDay extends React.Component {
    constructor() {
        super();
        let yesterday = new Moment().subtract(1, "day");
        this.state = {
            day: new Moment(),
            from: new Moment(yesterday).local().subtract(1, "week"),
            to: new Moment(yesterday),
            dayOfWeek: new Moment(yesterday).local().weekday(),
            hourlySales: [],
            hourlyVisits: [],
            date: new Moment(yesterday).local(),
            averageTransactionValue: 0,
            averageTransactionQuantity: 0,
            lyAverageTransactionValue: 0,
            lyAverageTransactionQuantity: 0,
            lyAvgItemPP: {},
            avgItemPP: {},
            lySales: {},
            lyVisits: {},
            sales: {},
            visits: {}
        }
    }

    componentWillMount() {
        this.loadHourlySales(this.state.date);
    }

    getLY(date) {
        let today = new Moment(this.state.date || date);
        let lastYear = new Moment(today).subtract(1, 'year')
            .isoWeek(today.isoWeek())
            .isoWeekday(today.isoWeekday());
        return lastYear;
    }

    loadHourlySales(date) {
        let self = this;
        let promises = [];

        let from = new Moment(date).startOf("day");
        let lyFrom = this.getLY(date).startOf("day");
        let to = new Moment(date).endOf("day");
        let lyTo = this.getLY(date).endOf("day");

        promises.push(new Promise((resolve, reject) => {
            hourlySales(from, to, this.props.store, result => {
                resolve({sales: result});
            });
        }));
        promises.push(new Promise((resolve, reject) => {
            hourlySales(lyFrom, lyTo, this.props.store, result => {
                resolve({lySales: result});
            });
        }));
        promises.push(new Promise((resolve, reject) => {
            hourlyVisitors(from, to, this.props.store, result => {
                console.log("visits: ", result);
                resolve({visits: result});
            });
        }));

        promises.push(new Promise((resolve, reject) => {
            hourlyVisitors(lyFrom, lyTo, this.props.store, result => {
                resolve({lyVisits: result});
            });
        }));

        promises.push(new Promise((resolve, reject) => {
            averageReceiptAndItemsPP(this.props.store, to, response => {
                resolve({avgItemPP: response});
            });
        }));
        promises.push(new Promise((resolve, reject) => {
            averageReceiptAndItemsPP(this.props.store, lyTo, response => {
                resolve({lyAvgItemPP: response})
            });
        }));

        let newState = {date};
        Promise.all(promises).then(result => {
            result.forEach(r => {
                newState = Object.assign(newState, r);
            });
            self.setState(newState);
        })
    }


    getTime() {
        return new Moment();
    }


    render() {
        let views;
        /**
         * views:
         *  - Full today
         *  - Hourly sales today/ly
         *  - Conversion today/ly
         *  - Visitors today/ly
         * @type {Array}
         */

        let data = [];
        let maxHourlySale = 0, maxConversion = 100, maxVisitors = 0, maxTransactions = 0;
        let currentTime = new Moment(this.getTime().local()).startOf("day").hour(9).minute(0).second(0).millisecond(0);
        let endTimeToShow = new Moment(this.getTime()).local().hour(20);
        while (currentTime.isBefore(endTimeToShow) || currentTime.isSame(endTimeToShow)) {
            let row = [[currentTime.hour(), currentTime.minute(), currentTime.second()]];
            let tdData = this.state.sales && this.state.sales.hourlySales ? this.state.sales.hourlySales[currentTime.format("HH:mm")] : 0;
            let sales = tdData && tdData.salesSum ? tdData.salesSum : 0;
            maxHourlySale = Math.max(sales, maxHourlySale);
            row.push(sales);

            let lyTdData = this.state.lySales && this.state.lySales.hourlySales ? this.state.lySales.hourlySales[currentTime.format("HH:mm")] : 0;
            let lySales = lyTdData && lyTdData.salesSum ? lyTdData.salesSum : 0;
            maxHourlySale = Math.max(lySales, maxHourlySale);
            row.push(lySales);

            let transactions = tdData && tdData.salesSum ? tdData.salesTransactions : 0;
            maxTransactions = Math.max(transactions, maxTransactions);

            let vData = this.state.visits ? this.state.visits[currentTime.format("HH:mm")] : 0;
            let visitors = vData ? vData : 0;
            maxVisitors = Math.max(visitors, maxVisitors);
            row.push((visitors === 0 ? 0 : transactions / visitors) * 100);

            row.push(visitors);
            row.push(transactions);
            data.push(row);
            currentTime = currentTime.add(1, "hour");
        }

        let salesMax = this.round5(maxHourlySale / 1000) * 1000;
        let transactionsMax = this.round5(maxTransactions);
        let vistorsMax = this.round5(maxVisitors / 5) * 5;
        // let conversionMax =
        console.log("conversion", maxConversion)


        let columns = [
            {
                type: 'timeofday',
                label: 'Time',
            },
            {
                type: 'number',
                label: 'Hourly sales'
            },
            {
                type: 'number',
                label: 'Hourly sales LY'
            },
            {
                type: "number",
                label: "Conversion rate"
            },
            {
                type: "number",
                label: "Visitors"
            },
            {
                type: "number",
                label: "Transactions"
            },
        ];

        let chartSalesMax = Math.max(30000, salesMax);

        let lyVisible = false;

        let chartOptions = {
            // interpolateNulls: true,
            curveType: 'function',
            chartArea: {
                'width': '93%',
                'height': '90%',
                right: "120px",
                top: 10,
            },
            legend: {'position': 'bottom'},
            axisTitlesPosition: "in",
            hAxis: {
                format: 'HH:mm',
                gridlines: {count: 11},
                viewWindow: {
                    min: [9, 0, 0],
                    max: [20, 0, 0]
                },
                title: "Time",
                textStyle: {
                    bold: true
                },
            },
            vAxes: {
                0: {
                    minValue: 0,
                    gridlines: {count: 6},
                    title: "SEK",
                    viewWindow: {
                        min: -2000,
                        max: chartSalesMax
                    },
                    textStyle: {
                        bold: true
                    },
                    format: "short",
                    viewWindowMode: 'explicit',
                },
                1: {
                    minValue: 0,
                    gridlines: {count: 6},
                    title: "Conversion rate",
                    format: "#'%'",
                    textPosition: 'in',
                    viewWindow: {
                        min: -4,
                        max: 60
                    }
                },
                2: {
                    minValue: 0,
                    gridlines: {count: 6},
                    title: "Visitors",
                    format: "#",
                    viewWindow: {
                        min: -40,
                        max: 600
                    }
                }
            },
            series: {
                0: {color: "#3366cc"},
                1: {color: "#97beff"},
                2: {type: 'line', targetAxisIndex: 1, pointSize: 8, lineWidth: 4, color: "#ff9900"},
                3: {type: "line", targetAxisIndex: 2, pointSize: 8, lineWidth: 2, color: "#109618"},
                4: {type: "line", targetAxisIndex: 2, pointSize: 8, lineWidth: 2, color: "#0099ff"}
            }
        };

        let period;
        let difference = this.state.to.diff(this.state.from, 'days');
        if (difference === 6) {
            period = "week";
        } else if (difference > 20 && difference < 32) {
            period = "month";
        } else if (difference > 80 && difference < 100) {
            period = "quarter";
        }

        let activeItem = this.state.dayOfWeek;
        return (
            <div>
                <div style={{display: "flex", flexDirection: "row", width: "100%"}}>
                    <div style={{height: "660px", width: "calc(100% - 280px)"}}>
                        <Chart
                            chartType="ColumnChart"
                            columns={columns}
                            rows={data}
                            options={chartOptions}
                            graph_id="LastMonthSales"
                            width="100%"
                            height="100%"
                            hideColumns={[1]}
                            legend_toggle
                        />
                    </div>
                    <div style={{
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        width: "280px",
                        minWidth: "280px"
                    }}>
                        <SimpleDatePicker
                            showOutsideDays={true}
                            selectedDays={this.state.date.toDate()}
                            firstDayOfWeek={1}
                            showWeekNumbers={true}
                            onDayClick={this.handleDayClick.bind(this)}
                            disabledDays={(date) => this.dayDisabled(date)}
                            fromMonth={new Date(2017, 6)}
                            toMonth={new Date()}
                        />
                        <h4>Average receipt</h4>
                        <div style={{display: "flex"}}>
                            <AvgReceiptGauge size={145}
                                             averageTransactionValue={this.state.avgItemPP.averageTransactionValue || 0}
                                             title={<h5>{this.state.date.format("YYYY-MM-DD")}</h5>}
                            />
                            <AvgReceiptGauge size={145}
                                             averageTransactionValue={this.state.lyAvgItemPP.averageTransactionValue || 0}
                                             title={<h5>LY ({this.getLY().format("YYYY-MM-DD")})</h5>}

                            />
                        </div>
                        <h4>Average pcs/purchase</h4>
                        <div style={{display: "flex"}}>
                            <AvgPcsGauge size={145}
                                         averageTransactionQuantity={this.state.avgItemPP.averageTransactionQuantity || 0}
                                         title={<h5>{this.state.date.format("YYYY-MM-DD")}</h5>}
                            />
                            <AvgPcsGauge size={145}
                                         averageTransactionQuantity={this.state.lyAvgItemPP.averageTransactionQuantity || 0}
                                         title={<h5>LY ({this.getLY().format("YYYY-MM-DD")})</h5>}
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    dayDisabled(date) {
        return !new Moment(date).isBefore(new Moment().subtract(1, "day").endOf("day"));
    }

    handleDayClick(date) {
        // console.log(a, b, c);
        this.loadHourlySales(new Moment(date));
        // this.loadHourlySales(weekday);
    }

    round5(x) {
        return Math.ceil(x / 5) * 5;
    }
}

WeekDay.propTypes = {
    store: PropTypes.number.isRequired,
};