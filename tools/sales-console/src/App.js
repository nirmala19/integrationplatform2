import React, {Component} from 'react';
import Layout from './Layout';
import "./styles/main.scss";
import {Tab} from "semantic-ui-react";
import {Button, Icon} from "semantic-ui-react";
import Cookies from 'universal-cookie';
import RealtimeSales from "./RealtimeSales";
import {Route, Switch} from "react-router-dom";
import StoreOverview from "./StoreOverview";

const cookies = new Cookies();

// If you use React Router, make this component
// render <Router> with your routes. Currently,
// only synchronous routes are hot reloaded, and
// you will see a warning from <Router> on every reload.
// You can ignore this warning. For details, see:
// https://github.com/reactjs/react-router/issues/2182
export default class App extends Component {
    render() {
        let logoutStyle = {
            position: "absolute",
            top: "0",
            right: "0",
            margin: "0",
            borderRadius: "0 0 0 4px"

        };

        return (
            <Layout>
                <Switch>
                    <Route exact path="/sales" component={RealtimeSales}/>
                    <Route path="/sales/:store" component={StoreOverview}/>
                    <Route exact path="/" component={RealtimeSales}/>
                    <Route path="/:store" component={StoreOverview}/>
                </Switch>
            </Layout>
        );
    }

    signOut() {
        cookies.remove("token");
        window.location = window.location.href;
    }

}
