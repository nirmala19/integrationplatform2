import React from "react";
import {withRouter} from "react-router-dom";
import {Tab} from "semantic-ui-react";
import RealtimeSales from "./RealtimeSales";
import WeekDay from "./WeekDay";
import Moment from "moment";


class StoreOverview extends React.Component {
    componentWillMount() {
        this.setState({
            store: Number(this.props.match.params.store),
        });
    }

    render() {
        if (!this.props.match.params.store) {
            return <RealtimeSales store={this.state.store}/>
        }
        let panes = [
            {
                menuItem: 'Today for store ' + this.state.store,
                render: () => <Tab.Pane attached={false}><RealtimeSales
                    store={this.state.store}/></Tab.Pane>
            },
            {
                menuItem: 'History',
                render: () => <Tab.Pane attached={false}><WeekDay store={this.state.store}/></Tab.Pane>
            },
        ];
        return (
            <div>
                <Tab menu={{pointing: true}} panes={panes}/>
            </div>
        )
    }
}

export default withRouter(StoreOverview);