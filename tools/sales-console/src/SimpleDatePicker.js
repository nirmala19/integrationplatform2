import React from "react";
import {Button} from "semantic-ui-react";
import DayPicker from "react-day-picker";
import Moment from "moment";

export default class SimpleDatePicker extends React.Component {
    constructor() {
        super();
        this.state = {open: false}
    }

    toggleDatePicker() {
        this.setState({open: !this.state.open})
    }

    render() {
        return (
            <div style={{width: "100%", paddingBottom: "20px"}}>
                <b>Select date</b>
                <div style={{width: "100%"}}>
                    <Button onClick={this.toggleDatePicker.bind(this)}
                            style={{width: "100%"}}>{new Moment(this.props.selectedDays).format("YYYY-MM-DD")}</Button>
                </div>
                <div style={{position: "relative", width: "100%"}}>
                    <div style={{
                        paddingBottom: "20px",
                        border: "1px solid #ccc",
                        borderRadius: "4px 0 4px 4px",
                        position: "absolute",
                        right: "0",
                        top: "0",
                        zIndex: 2,
                        backgroundColor: "#fff",
                        display: this.state.open === true ? "block" : "none"
                    }}>
                        <DayPicker
                            {...this.props}
                            onDayClick={d => {
                                this.toggleDatePicker();
                                this.props.onDayClick(d);
                            }}
                        />
                    </div>
                </div>
            </div>
        )
    }
}