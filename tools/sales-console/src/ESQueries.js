import es from 'elasticsearch-browser'
import Moment from "moment";

const client = new es.Client({
    host: window.location.protocol + "//" + window.location.host + "/search",
    log: 'warning'
});

export const hourlyVisitors = (from, to, store, callback) => {
    /**
     * To work with week days, use
     *
     *                     {
                        "script": {
                            "script": {
                                "inline": "doc['event_time'].date.dayOfWeek == " + weekday,
                                "lang": "painless"
                            }
                        }
                    },

     */
    let query = {
        "query": {
            "bool": {
                "must": [
                    {
                        "range": {
                            "event_time": {
                                "from": from.utc().format("YYYY-MM-DD HH:mm:ss"),
                                "to": to.utc().format("YYYY-MM-DD HH:mm:ss")
                            }
                        }
                    },
                    {
                        "term": {
                            "store_id": {
                                "value": store
                            }
                        }
                    }
                ]
            }
        }
    };

    client.search({
        index: 'indiska',
        type: 'visitormetrics',
        body: query
    }).then(function (resp) {
        let data = {};
        resp.hits.hits.forEach(hit => {
            data[new Moment(hit._source.event_time).format("HH:mm")] = hit._source.visits;
        });
        callback(data);
    }, function (err) {
        console.error("ERROR: ", err);
        reject(err);
    });
};

export const salesCurveShape = (date, store, callback) => {
    let from = new Moment(date).subtract(1, "month").subtract(1, "day");
    let to = new Moment(date).subtract(1, "day");
    let weekday = date.day();

    let bq = baseQuery(store, from, to, weekday);
    let body = {
        "size": 2,
        "query": bq,
        "aggregations": {
            "date_time": {
                "histogram": {
                    "script": "doc['event_time'].date.hourOfDay",
                    "interval": 1
                },
                "aggregations": {
                    "sum_adjusted_amount": {
                        "sum": {
                            "field": "adjusted_amount"
                        }
                    },
                    "cumulative_sales": {
                        "cumulative_sum": {
                            "buckets_path": "sum_adjusted_amount"
                        }
                    },
                    // "avg_adjusted_amount": {
                    //     "avg": {
                    //         "field": "adjusted_amount"
                    //     }
                    // }
                }
            },
            "total_sum": {
                "sum": {
                    "field": "adjusted_amount"
                }
            }
        }
    };
    console.log("body", body);
    client.search({
        index: 'indiska',
        type: 'poslog',
        body: body,
    }).then(function (resp) {
        let data = {};
        let totalSum = resp.aggregations.total_sum.value;
        callback({});
        resp.aggregations.date_time.buckets.forEach(b => {
            data[(b.key + 2 + "").padStart(2, "0") + ":00:00"] = b.cumulative_sales.value / totalSum;
        });
        callback({
            dailySalesCurve: data
        });
    }, function (err) {
        console.error("ERROR: ", err);
        throw err;
    });
};

export const hourlySales = (from, to, store, callback) => {
    /**
     * Use this if you want to work with week days.
     */
// ,
//     {
//         "script": {
//         "script": {
//             "inline": "doc['event_time'].date.dayOfWeek == " + weekday,
//                 "lang": "painless"
//         }
//     }
//     }

    let q = todayBaseQuery(store, to);

    let body = {
        "size": 0,
        "query": q,
        "aggregations": {
            "date_time": {
                "date_histogram": {
                    "field": "event_time",
                    "format": "yyyy-MM-dd HH:mm:ss",
                    "interval": "1h",
                    "offset": 0,
                    "order": {
                        "_key": "asc"
                    },
                    "keyed": false,
                    "min_doc_count": 0
                },
                "aggregations": {
                    "hourly_sales": {
                        "sum": {
                            "field": "adjusted_amount"
                        }
                    },
                    "hourly_transactions": {
                        "cardinality": {
                            "field": "transaction_id"
                        }
                    }
                }
            }
        }
    };

    let self = this;
    client.search({
        index: 'indiska',
        type: 'poslog',
        body: body
    }).then(function (resp) {
        let data = {};
        resp.aggregations.date_time.buckets.forEach(b => {
            if (b.hourly_sales.value < b.hourly_transactions.value) {
                console.log("GOT HIGHER NO OF TRANSACTIONS THAN SALES!")
            }
            data[Moment.utc(b.key_as_string).local().format("HH:mm")] = {
                salesSum: b.hourly_sales.value,
                salesTransactions: b.hourly_transactions.value
            };
        });
        callback({
            hourlySales: data
        });
    }, function (err) {
        console.error("ERROR: ", err);
        throw err;
    });
};

const todayBaseQuery = (store, to) => {
    let from = new Moment(to).startOf("day");
    return baseQuery(store, from, to, null)
};

const baseQuery = (store, from, to, weekday) => {
    let query = {
        "bool": {
            "must": [
                {
                    "range": {
                        "event_time": {
                            "from": from.utc().format("YYYY-MM-DD HH:mm:ss"),
                            "to": to.utc().format("YYYY-MM-DD HH:mm:ss")
                        }
                    }
                }
            ],
            "should": [
                {
                    "term": {
                        "transaction_status": {
                            "value": "Finished"
                        }
                    }
                },
                {
                    "term": {
                        "transaction_status": {
                            "value": "SuspendedRetrieved"
                        }
                    }
                }
            ],
            "must_not": [
                {
                    "term": {
                        "store": {
                            "value": 900
                        }
                    }
                },
                {
                    "term": {
                        "store": {
                            "value": 901
                        }
                    }
                },
                {
                    "term": {
                        "store": {
                            "value": 902
                        }
                    }
                },
                {
                    "term": {
                        "line_cancel_flag": {
                            "value": true
                        }
                    }
                },
                {
                    "term": {
                        "cancel_flag": {
                            "value": true
                        }
                    }
                },
                {
                    "term": {
                        "training_mode": {
                            "value": true
                        }
                    }
                },
                {
                    "term": {
                        "adjusted_amount": {
                            "value": 0
                        }
                    }
                }

            ]
        }
    };

    if (store) {
        query.bool.must.push({
            "term": {
                "store": {
                    "value": store
                }
            }
        });
    }
    if (weekday) {
        query.bool.must.push({
                "script": {
                    "script": {
                        "inline": "doc['event_time'].date.dayOfWeek == " + weekday,
                        "lang": "painless"
                    }
                }
            }
        );
    }
    return query;
};

export const averageReceiptAndItemsPP = (store, to, callback) => {
    let query = todayBaseQuery(store, to);

    let body = {
        "size": 0,
        "query": query,
        "aggregations": {
            "all_transactions": {
                "terms": {
                    "field": "transaction_id",
                    "size": 100000
                },
                "aggs": {
                    "sum_transaction_amount": {
                        "sum": {
                            "field": "adjusted_amount"
                        }
                    },
                    "sum_quantity": {
                        "sum": {
                            "field": "quantity"
                        }
                    }

                }
            },
            "avg_transaction_value": {
                "avg_bucket": {
                    "buckets_path": "all_transactions>sum_transaction_amount"
                }
            },
            "avg_quantity": {
                "avg_bucket": {
                    "buckets_path": "all_transactions>sum_quantity"
                }
            }
        }
    };

    client.search({
        index: 'indiska',
        type: 'poslog',
        filter_path: 'aggregations.avg_transaction_value,aggregations.avg_quantity',
        body: body
    }).then(function (resp) {
        callback({
            store,
            averageTransactionValue: resp.aggregations.avg_transaction_value.value,
            averageTransactionQuantity: resp.aggregations.avg_quantity.value
        });
    }, function (err) {
        console.error("ERROR: ", err);
        throw err;
    });

};

export const realtimeData = (store, to, interval, callback) => {
    interval = interval || "2m";
    let query = todayBaseQuery(store, to);

    let promises = [];
    promises.push(new Promise((resolve, reject) => {
        averageReceiptAndItemsPP(store, to, response => {
            resolve(response);
        });
    }));

    let q = {
        "size": 0,
        "query": query,
        "aggregations": {
            "date_time": {
                "date_histogram": {
                    "field": "event_time",
                    "format": "yyyy-MM-dd HH:mm:ss",
                    "interval": interval,
                    "offset": 0,
                    "order": {
                        "_key": "asc"
                    },
                    "keyed": false,
                    "min_doc_count": 0
                },
                "aggregations": {
                    "sum_adjusted_amount": {
                        "sum": {
                            "field": "adjusted_amount"
                        }
                    },
                    "cumulative_sales": {
                        "cumulative_sum": {
                            "buckets_path": "sum_adjusted_amount"
                        }
                    },
                    "avg_adjusted_amount": {
                        "avg": {
                            "field": "adjusted_amount"
                        }
                    }
                }
            },
            "number_transactions": {
                "cardinality": {
                    "field": "transaction_id"
                }
            },
            "total_sum_today": {
                "sum": {
                    "field": "adjusted_amount"
                }
            }
        }
    };

    promises.push(new Promise((resolve, reject) => {
        client.search({
            index: 'indiska',
            type: 'poslog',
            body: q
        }).then(function (resp) {
            let data = {};
            resp.aggregations.date_time.buckets.forEach(v => {
                let time = new Moment(new Date(v.key)).format("HH:mm:ss");
                data[time] = v.cumulative_sales.value;
            });
            resolve({
                timeline: data,
                purchases: resp.aggregations.number_transactions.value,
                totalSumToday: resp.aggregations.total_sum_today.value
            });
        }, function (err) {
            console.trace(err.message);
            reject(err);
        });
    }));

    Promise.all(promises).then(responses => {
        let callbackData = {};
        responses.forEach(d => callbackData = Object.assign(callbackData, d));
        callback(callbackData);
    })
}