#!/bin/bash

export PGPASSWORD=indiska

echo "Truncating database tables"
psql --username=indiska indiska2 < truncate.sql
echo "Done"
echo

echo "Deleting Elasticsearch index 'indiska'"
curl -X DELETE "http://localhost:9200/indiska"
echo
echo Done
echo

#find /Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound | grep "\.\d$"| xargs rm
echo "Emptying archive directory"
rm -rf /Users/heintz/Projects/Indiska/ICC/testdata/archive
mkdir /Users/heintz/Projects/Indiska/ICC/testdata/archive
echo Done
echo


echo "Cleaning ftp source and target directory"
find /Users/heintz/Projects/Indiska/ICC/testdata/ftp -type d > dirs.txt
rm -rf /Users/heintz/Projects/Indiska/ICC/testdata/ftp
cat dirs.txt | xargs mkdir -p
rm dirs.txt
echo Done

echo "Setting permissions on directories"
sudo chown -R sharing /Users/heintz/Projects/Indiska/ICC/testdata/ftp
sudo chmod -R 777 /Users/heintz/Projects/Indiska/ICC/testdata/ftp

#rm -rf /Users/heintz/Projects/Indiska/ICC/testdata/ftp
#mkdir -p crm/inbound
#mkdir -p epix/in
#mkdir -p epix/out
#mkdir -p oracle/inbound
#mkdir -p oracle/outbound
#mkdir -p shop/inbound
#mkdir -p shop/outbound

#find /Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound/poslog/2017/ -name "*.00018.xml" -exec cp {} /Users/heintz/Projects/Indiska/ICC/testdata/ftp/shop/outbound/1/ \;
#find /Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound/pmlog/2017/ -name "*.00018.xml" -exec cp {} /Users/heintz/Projects/Indiska/ICC/testdata/ftp/shop/outbound/1/ \;
#find /Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound/poslog/2017/ -name "*.00902.xml" -exec cp {} /Users/heintz/Projects/Indiska/ICC/testdata/ftp/shop/outbound/2/ \;
#find /Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound/pmlog/2017/ -name "*.00902.xml" -exec cp {} /Users/heintz/Projects/Indiska/ICC/testdata/ftp/shop/outbound/2/ \;
#
#find /Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound/article/ -name "*.00018.*" -exec cp {} /Users/heintz/Projects/Indiska/ICC/testdata/ftp/oracle/outbound/901/ \;
#find /Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound/articlehierarchy/ -name "*.00018.*" -exec cp {} /Users/heintz/Projects/Indiska/ICC/testdata/ftp/oracle/outbound/901/ \;
#find /Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound/article -name "*.00902.*" -exec cp {} /Users/heintz/Projects/Indiska/ICC/testdata/ftp/oracle/outbound/902/ \;
#find /Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound/articlehierarchy/ -name "*.00902.*" -exec cp {} /Users/heintz/Projects/Indiska/ICC/testdata/ftp/oracle/outbound/902/ \;

