package se.indiska.integration.jobs.inoutbouncer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.trace.DataTrace;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * "Bounces" inbound files to the outbound file table "outbound_file_staging" without
 * touching the files or the content. Used whenever Indio only should be used to pass
 * messages from the source system to the target system without changing them.
 */
@IdentifiableIndioJob(identifier = "INBOUND_OUTBOUND_BOUNCER",
        eventStep = "OUTBOUND_FILE_LOAD",
        description = "\"Bounces\" files from inbound_file_staging to outbound_file_staging"
)
public class InboundOutboundBouncer implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(InboundOutboundBouncer.class);

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        long start = System.nanoTime();
        int count = 0;
        String bounceSql = "INSERT INTO outbound_file_staging (event_time, filename, file_type, file_content, source_identifier, message_id)\n" +
                "SELECT DISTINCT now(), filename, file_type, file_content, cast (target_id AS BIGINT), ifs.message_id FROM process_status ps, process_data_trace pdt, inbound_file_staging ifs\n" +
                "WHERE ps.id=?\n" +
                "  AND pdt.process_id = ps.process_initiator_id\n" +
                "  AND ifs.id=cast(pdt.target_id AS BIGINT)\n" +
//                "  and pdt.status='SUCCESS'\n" + // Don't check this, since it will never reach here if the content of the file wasn't possible to retrieve
                "ON CONFLICT (filename, file_type) DO UPDATE SET event_time=now(), file_content=excluded.file_content, source_identifier=excluded.source_identifier";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(bounceSql, Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, traceHandle.getId());
            ps.executeUpdate();
            ResultSet generatedKeysResultSet = ps.getGeneratedKeys();
            while (generatedKeysResultSet.next()) {
                Long generatedId = generatedKeysResultSet.getLong("id");
                Long sourceId = generatedKeysResultSet.getLong("source_identifier");
                String messageId = generatedKeysResultSet.getString("message_id");
                traceHandle.getProcessTracer().trace(new DataTrace(messageId, traceHandle.getId(), "inbound_file_staging", sourceId + "", "outbound_file_staging", generatedId + "", ProcessStatus.SUCCESS));
                count++;
            }
        }
        logger.debug("Bounce complete for process " + traceHandle.getId() + " bouncing " + count + " " + jobExecutionData.getDataType() + " files took " + (System.nanoTime() - start) / 1e6d + "ms");
        callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), count > 0 ? ProcessStatus.SUCCESS : ProcessStatus.NOOP));
    }
}