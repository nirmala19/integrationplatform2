package se.indiska.integration.jobs.exratefetch;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

@IdentifiableIndioJob(identifier = "EXCHANGE_RATE_FETCHER", description = "Fetches exchange rates from Tullverket and stores in ODS", jobType = IdentifiableIndioJob.JobType.SCHEDULED, eventStep = "ODS_LOAD", dataType = "EXCHANGE_RATE", schedule = "0 0 3 */2 * ?")
public class ExchangeRateFetcherJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(ExchangeRateFetcherJob.class);
    private static DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        logger.debug("Running EXCHANGE_RATE_FETCHER");
        LocalDate from = LocalDate.now().minus(1, ChronoUnit.YEARS);
        LocalDate to = LocalDate.now().plus(1, ChronoUnit.MONTHS);
        List<ExchangeRate> exchangeRates = new ArrayList<>();
        exchangeRates.addAll(getTullverketExchangeRates(from, to, "EUR"));
        exchangeRates.addAll(getTullverketExchangeRates(from, to, "NOK"));
        exchangeRates.addAll(getRiksbankenExchangeRates(from, to, "ISK"));
        exchangeRates.addAll(getSEKRate(from, to));
        exchangeRates.add(new ExchangeRate("EUR", 9.6d, LocalDate.of(1900, 1, 1)));
        exchangeRates.add(new ExchangeRate("SEK", 1d, LocalDate.of(1900, 1, 1)));
        exchangeRates.add(new ExchangeRate("NOK", 1.05d, LocalDate.of(1900, 1, 1)));
        exchangeRates.add(new ExchangeRate("ISK", 0.08d, LocalDate.of(1900, 1, 1)));

        loadExchangeRates(exchangeRates);
        if (jobExecutionData != null && callback != null) {
            callback.jobComplete(new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.SUCCESS));
        }
    }

    private Collection<? extends ExchangeRate> getSEKRate(LocalDate from, LocalDate to) {
        LocalDate currentDate = LocalDate.from(from);
        if (currentDate.getDayOfMonth() > 1) {
            from = from.plusMonths(1);
            currentDate = currentDate.withDayOfMonth(1);
        }

        List<ExchangeRate> exchangeRates = new ArrayList<>();
        while (currentDate.isBefore(to.plusDays(1))) {
            exchangeRates.add(new ExchangeRate("SEK", 1d, currentDate));
            currentDate = currentDate.plusMonths(1);
        }
        return exchangeRates;
    }

    private void loadExchangeRates(List<ExchangeRate> exchangeRates) throws SQLException {
        String sql = "INSERT INTO ods.historical_exchange_rates (currency, rate, from_date) VALUES (?,?,?) ON CONFLICT DO NOTHING";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            for (ExchangeRate exchangeRate : exchangeRates) {
                ps.setString(1, exchangeRate.getCurrency());
                ps.setDouble(2, exchangeRate.getCurrencyToSekMultiplier());
                ps.setTimestamp(3, Timestamp.valueOf(exchangeRate.getDate().atStartOfDay()));
                ps.addBatch();
            }
            ps.executeBatch();
        }

    }

    private List<ExchangeRate> getRiksbankenExchangeRates(LocalDate fromd, LocalDate to, String currency) throws ParserConfigurationException, IOException, SAXException {
        String url = "https://swea.riksbank.se:443/sweaWS/services/SweaWebServiceHttpSoap12Endpoint";
        Map<String, String> currencyCodeMap = new HashMap<>();
        String groupId = "130";
//        currencyCodeMap.put("EUR", "SEKEURPMI");
//        currencyCodeMap.put("NOK", "SEKNOKPMI");
        currencyCodeMap.put("ISK", "SEKISKPMI");
//        currencyCodeMap.put("INR", "SEKINRPMI");
        LocalDate from = fromd.withDayOfMonth(1);

        logger.debug("Getting exchange rates from Riksbanken from " + from + " to " + to);

        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();


        String request = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:xsd=\"http://swea.riksbank.se/xsd\">\n" +
                "   <soap:Header/>\n" +
                "   <soap:Body>\n" +
                "      <xsd:getInterestAndExchangeRates>\n" +
                "         <searchRequestParameters>\n" +
                "            <datefrom>" + from.format(DateTimeFormatter.ISO_DATE) + "</datefrom>\n" +
                "            <dateto>" + to.format(DateTimeFormatter.ISO_DATE) + "</dateto>\n" +
                "            <languageid>en</languageid>\n" +
                "            <!--1 or more repetitions:-->\n" +
                "            <searchGroupSeries>\n" +
                "               <groupid>130</groupid>\n" +
                "               <seriesid>CURRENCY_CODE</seriesid>\n" +
                "            </searchGroupSeries>\n" +
                "            <ultimo>false</ultimo>\n" +
                "         </searchRequestParameters>\n" +
                "      </xsd:getInterestAndExchangeRates>\n" +
                "   </soap:Body>\n" +
                "</soap:Envelope>";

        System.out.println(request);
        List<ExchangeRate> exchangeRates = new ArrayList<>();

        String seriesId = currencyCodeMap.get(currency);
        String cRequest = request.replace("CURRENCY_CODE", seriesId);
        Response response = Request.Post(url).bodyString(cRequest, ContentType.APPLICATION_XML).execute();
        Document document = documentBuilder.parse(response.returnContent().asStream());

        Element series = (Element) document.getElementsByTagName("series").item(0);

        BigDecimal unit = BigDecimal.valueOf(Double.parseDouble(series.getElementsByTagName("unit").item(0).getFirstChild().getNodeValue()));
        System.out.println("Currency " + unit);

        Integer month = -1;
        ExchangeRate lastValue = null;
        NodeList nodeList = series.getElementsByTagName("resultrows");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element element = (Element) nodeList.item(i);
            LocalDate date = LocalDate.parse(((Element) element.getElementsByTagName("date").item(0)).getFirstChild().getNodeValue());

            Node valueNode = element.getElementsByTagName("value").item(0);
            if (valueNode.getFirstChild() == null) {
                continue;
            }
            BigDecimal value = BigDecimal.valueOf(Double.parseDouble((valueNode.getFirstChild().getNodeValue())));
            ExchangeRate er = new ExchangeRate(currency, value.divide(unit).doubleValue(), date);
            if (date.getMonthValue() != month) {

                ExchangeRate exchangeRate = er;
                if (date.getDayOfMonth() > 1 && lastValue != null) {
                    exchangeRate = new ExchangeRate(lastValue.getCurrency(), lastValue.getCurrencyToSekMultiplier(), lastValue.getDate());
                }
                exchangeRate.date = date.withDayOfMonth(1);
                exchangeRates.add(exchangeRate);
                month = date.getMonthValue();
            }

            lastValue = er;

        }


//        for (ExchangeRate exchangeRate : exchangeRates) {
//            logger.debug(exchangeRate.getCurrency() + "\t" + exchangeRate.getDate() + "\t" + exchangeRate.getCurrencyToSekMultiplier());
//
//        }
        return exchangeRates;
    }


    private List<ExchangeRate> getTullverketExchangeRates(LocalDate from, LocalDate to, String currency) {
        String url = "http://tulltaxan.tullverket.se/arctictariff-public-proxy/arctictariff-trusted-rs/v1/ref/moun/er/exchangerates?count=100&language=sv&offset=0&sortcolumn=fromDate&sortorder=A";
        url += "&fromDate=" + from.format(DateTimeFormatter.ISO_DATE);
        url += "&toDate=" + to.format(DateTimeFormatter.ISO_DATE);
        url += "&currency=" + currency;

        String referer = "http://tulltaxan.tullverket.se/";
        List<ExchangeRate> exchangeRates = new ArrayList<>();
        try {
            ObjectMapper mapper = new ObjectMapper();
            Map response = mapper.readValue(Request.Get(url).addHeader("Referer", referer).execute().returnContent().asStream(), Map.class);
            logger.debug("Response: " + response);
            List<Map> ers = (List<Map>) response.get("items");
            for (Map exchangeRate : ers) {
                ExchangeRate er = new ExchangeRate(currency, (Double) exchangeRate.get("exchangeRate"), LocalDate.parse((CharSequence) exchangeRate.get("fromDate")));
                exchangeRates.add(er);
            }
        } catch (IOException e) {
            logger.error("Unable to get exchange rate for dates " + from.format(DateTimeFormatter.ISO_DATE) + "-" + to.format(DateTimeFormatter.ISO_DATE) + " for currency " + currency);
        }
        return exchangeRates;
    }

    public static class ExchangeRate {
        private Double currencyToSekMultiplier = null;
        private LocalDate date = null;
        private String currency = null;

        public ExchangeRate(String currency, Double currencyToSekMultiplier, LocalDate date) {
            this.currency = currency;
            this.currencyToSekMultiplier = currencyToSekMultiplier;
            this.date = date;
        }

        public Double getCurrencyToSekMultiplier() {
            return currencyToSekMultiplier;
        }

        public LocalDate getDate() {
            return date;
        }

        public String getCurrency() {
            return currency;
        }
    }

    public static void main(String[] argv) throws Exception {
        ExchangeRateFetcherJob job = new ExchangeRateFetcherJob();
        job.execute(null, null, null);

    }
}
