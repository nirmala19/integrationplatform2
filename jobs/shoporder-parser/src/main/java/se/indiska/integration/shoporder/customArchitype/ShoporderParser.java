package se.indiska.integration.shoporder.customArchitype;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.general.DateConverter;
import se.indiska.integration.framework.general.FlatFileLoader;
import se.indiska.integration.framework.general.IntegerConverter;
import se.indiska.integration.framework.general.LineParser;
import se.indiska.integration.framework.job.*;

@IdentifiableIndioJob(identifier = "SHOPORDER_STAGE_LOAD", eventStep = "STAGE_LOAD", description = "Parses shop orders and stores in staging area")
public class ShoporderParser extends FlatFileLoader {
    private static final Logger logger = LoggerFactory.getLogger(ShoporderParser.class);

    public ShoporderParser() {
        LineParser line139Parser = new LineParser("shop_order", false);

        line139Parser.addInterval("shop", 0, 6, new IntegerConverter());
        line139Parser.addInterval("ordernumber", 6, 8, true, true);
        line139Parser.addInterval("item", 14, 13, true, true);
        line139Parser.addInterval("quantity", 27, 9, new IntegerConverter());
        line139Parser.addInterval("orderdate", 36, 8, new DateConverter("yyyyMMdd"));
        addParser("139", line139Parser);
    }
}
