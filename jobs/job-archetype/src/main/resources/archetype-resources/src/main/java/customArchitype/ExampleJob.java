package ${groupId}.customArchitype;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.trace.DataTrace;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

@IdentifiableIndioJob(identifier = "EXAMPLE_JOB_ID")
public class ExampleJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(ExampleJob.class);

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        logger.debug("Running EXAMPLE_JOB_ID");
        callback.jobComplete(new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.SUCCESS));
    }
}
