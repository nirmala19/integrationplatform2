package se.indiska.integration.jobs.transform;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.util.InboundFileUtilities;

import java.io.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

@IdentifiableIndioJob(
        identifier = "REPLENISHMENT_PARAMETER_TRANSFORM",
        eventStep = "OUTBOUND_FILE_LOAD",
        description = "Transforms Oracle CSV Replenishment Parameters to Epix format",
        triggerDataType = "REPLENISHMENT_PARAMETER",
        jobType = IdentifiableIndioJob.JobType.EVENT_BASED,
        triggerEventStep = "INBOUND_TRANSFER",
        dataType = "REPLENISHMENT_PARAMETER"
)
public class Oracle2EpixReplenishmentParamTransform implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(Oracle2EpixReplenishmentParamTransform.class);

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        List<InboundFileUtilities.InboundFile> inboundFiles = new ArrayList<>();
        try (Connection connection = DatasourceProvider.getConnection()) {
            inboundFiles = InboundFileUtilities.getInboundFiles(connection, traceHandle.getId());
        }

        for (InboundFileUtilities.InboundFile inboundFile : inboundFiles) {

//            File file = new File("/Users/heintz/Downloads/R0171116.csv");
            String referenceNo = inboundFile.getFileName().substring(1, inboundFile.getFileName().lastIndexOf("."));

            StringReader stringReader = new StringReader(inboundFile.getFileContent());
            String jsonContent = convertToJson(referenceNo, stringReader);

            List<InboundFileUtilities.OutboundFile> outboundFiles = new ArrayList<>();


            String outboundFileName = "R" + referenceNo + ".xml";
            // Fixed ICM-44
            InboundFileUtilities.OutboundFile outboundFile = new InboundFileUtilities.OutboundFile(outboundFileName, inboundFile.getDataType(), jsonContent, inboundFile.getMessageId(), inboundFile.getMessageUuid());
            outboundFiles.add(outboundFile);
            try (Connection connection = DatasourceProvider.getConnection()) {
                InboundFileUtilities.addOutboundFile(connection, traceHandle, outboundFiles.toArray(new InboundFileUtilities.OutboundFile[outboundFiles.size()]));
            }
        }
        callback.jobComplete(new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.SUCCESS));
    }

    private String convertToJson(String referenceNo, Reader reader) throws IOException {
        XMLMessage.ControlArea controlArea = new XMLMessage.ControlArea();
        controlArea.refId.id = referenceNo;
        controlArea.sender.division = "IND";
        controlArea.sender.confirmation = 0;
        XMLMessage.Replenishment dispatchSync = new XMLMessage.Replenishment();
        dispatchSync.setControlArea(controlArea);

        BufferedReader br = new BufferedReader(reader);
        String line;
        boolean first = true;
        while ((line = br.readLine()) != null) {
            if (!first) {
                line = line.trim();
                String[] lineItems = line.split(",");
                XMLMessage.ReplenishmentLine replenishmentLine = new XMLMessage.ReplenishmentLine();
                dispatchSync.getDataArea().getReplenishmentLines().add(replenishmentLine);

                replenishmentLine.id = lineItems[0];
                replenishmentLine.replenishmentMethod = lineItems[1];
                if (lineItems[2].length() > 0) {
                    replenishmentLine.minStock = Integer.parseInt(lineItems[2]);
                }
                if (lineItems[3].length() > 0) {
                    replenishmentLine.maxStock = Integer.parseInt(lineItems[3]);
                }
                replenishmentLine.incrementPercent = Integer.parseInt(lineItems[4]);
                dispatchSync.getDataArea().replenishmentLines.add(replenishmentLine);
            }
            first = false;

        }
        XmlMapper mapper = new XmlMapper();
        XMLMessage.Envelope envelope = new XMLMessage.Envelope();
        envelope.replenishment = dispatchSync;

        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);

        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(envelope);
    }

    public static void main(String[] argv) throws Exception {
        File file = new File("/Users/heintz/Downloads/b018-02-06-12.36.36.383-001.csv");
        FileReader fileReader = new FileReader(file);
        Oracle2EpixReplenishmentParamTransform t = new Oracle2EpixReplenishmentParamTransform();
        String filename = file.getName();
        filename = filename.substring(0, filename.lastIndexOf(".")) + ".xml";

        String referenceNo = file.getName().substring(1, file.getName().lastIndexOf("."));

        FileWriter fw = new FileWriter("/tmp/" + filename);
        String jsonContent = t.convertToJson(referenceNo, fileReader);
        fw.write(jsonContent);
        fw.close();
        fileReader.close();

    }
}
