package se.indiska.integration.jobs.transform;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class XMLMessage {
    private static final Logger logger = LoggerFactory.getLogger(XMLMessage.class);
    @JacksonXmlRootElement(localName = "Astro_Envelope")
    static class Envelope {
        @JacksonXmlProperty(localName = "xsi", isAttribute = true)
        String xsi = "http://www.w3.org/2001/XMLSchema-instance";
        @JacksonXmlProperty(localName = "version", isAttribute = true)
        String version = "0101";
        @JacksonXmlProperty(localName = "ReplenishmentParameters")
        Replenishment replenishment = null;

        public String getXsi() {
            return xsi;
        }

        public void setXsi(String xsi) {
            this.xsi = xsi;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public Replenishment getReplenishment() {
            return replenishment;
        }

        public void setReplenishment(Replenishment replenishment) {
            this.replenishment = replenishment;
        }
    }


    @JacksonXmlRootElement(localName = "ControlArea")
    static class ControlArea {
        @JacksonXmlProperty(localName = "Sender")
        Sender sender = new Sender();
        @JacksonXmlProperty(localName = "CreationDateTime")
        String creationDateTime = OffsetDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);
        @JacksonXmlProperty(localName = "RefId")
        RefId refId = new RefId();

        public Sender getSender() {
            return sender;
        }

        public void setSender(Sender sender) {
            this.sender = sender;
        }

        public String getCreationDateTime() {
            return creationDateTime;
        }

        public void setCreationDateTime(String creationDateTime) {
            this.creationDateTime = creationDateTime;
        }

        public RefId getRefId() {
            return refId;
        }

        public void setRefId(RefId refId) {
            this.refId = refId;
        }
    }

    @JacksonXmlRootElement(localName = "Sender")
    static class Sender {
        @JacksonXmlProperty(localName = "Division")
        String division = "";
        @JacksonXmlProperty(localName = "Confirmation")
        Integer confirmation = -1;

        public String getDivision() {
            return division;
        }

        public void setDivision(String division) {
            this.division = division;
        }

        public Integer getConfirmation() {
            return confirmation;
        }

        public void setConfirmation(Integer confirmation) {
            this.confirmation = confirmation;
        }
    }

    @JacksonXmlRootElement(localName = "RefId")
    static class RefId {
        @JacksonXmlProperty(localName = "Id")
        String id = "";

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    @JacksonXmlRootElement(localName = "Replenishment")
    static class Replenishment {
        @JacksonXmlProperty(localName = "ControlArea")
        private ControlArea controlArea = new ControlArea();
        @JacksonXmlProperty(localName = "DataArea")
        private DataArea dataArea = new DataArea();

        public ControlArea getControlArea() {
            return controlArea;
        }

        public void setControlArea(ControlArea controlArea) {
            this.controlArea = controlArea;
        }

        public DataArea getDataArea() {
            return dataArea;
        }

        public void setDataArea(DataArea dataArea) {
            this.dataArea = dataArea;
        }
    }

    @JacksonXmlRootElement(localName = "DataArea")
    static class DataArea {
        @JacksonXmlProperty(localName = "CreateExpression")
        CreateExpression createExpression = new CreateExpression();
        @JacksonXmlElementWrapper(localName = "ReplenishmentLines")
        @JacksonXmlProperty(localName = "ReplenishmentLine")
        List<ReplenishmentLine> replenishmentLines = new ArrayList<>();

        public CreateExpression getCreateExpression() {
            return createExpression;
        }

        public void setCreateExpression(CreateExpression createExpression) {
            this.createExpression = createExpression;
        }

        public List<ReplenishmentLine> getReplenishmentLines() {
            return replenishmentLines;
        }

        public void setReplenishmentLines(List<ReplenishmentLine> replenishmentLines) {
            this.replenishmentLines = replenishmentLines;
        }
    }

    @JacksonXmlRootElement(localName = "CreateExpression")
    static class CreateExpression {
        @JacksonXmlProperty(localName = "action", isAttribute = true)
        String action = "Replace";

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }
    }

    @JacksonXmlRootElement(localName = "ReplenishmentLine")
    static class ReplenishmentLine {
        @JacksonXmlProperty(localName = "Id")
        String id = "";
        @JacksonXmlProperty(localName = "ReplenishmentMethod")
        String replenishmentMethod = "";
        @JacksonXmlProperty(localName = "MinStock")
        Integer minStock = 0;
        @JacksonXmlProperty(localName = "MaxStock")
        Integer maxStock = 0;
        @JacksonXmlProperty(localName = "Increment")
        Integer incrementPercent = 0;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getReplenishmentMethod() {
            return replenishmentMethod;
        }

        public void setReplenishmentMethod(String replenishmentMethod) {
            this.replenishmentMethod = replenishmentMethod;
        }

        public Integer getMinStock() {
            return minStock;
        }

        public void setMinStock(Integer minStock) {
            this.minStock = minStock;
        }

        public Integer getMaxStock() {
            return maxStock;
        }

        public void setMaxStock(Integer maxStock) {
            this.maxStock = maxStock;
        }

        public Integer getIncrementPercent() {
            return incrementPercent;
        }

        public void setIncrementPercent(Integer incrementPercent) {
            this.incrementPercent = incrementPercent;
        }
    }
}
