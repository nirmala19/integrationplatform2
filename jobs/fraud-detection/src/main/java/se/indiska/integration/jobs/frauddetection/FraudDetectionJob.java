package se.indiska.integration.jobs.frauddetection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.util.MailSenderProvider;

import javax.mail.Session;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;

@IdentifiableIndioJob(triggerDataType = "POSLOG",
        triggerEventStep = "STAGE_LOAD",
        identifier = "POSLOG_FRAUD_DETECTION",
        eventStep = "ALERTING",
        description = "Implements a very simple fraud detection framework",
        dataType = "POSLOG",
        jobType = IdentifiableIndioJob.JobType.EVENT_BASED)
public class FraudDetectionJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(FraudDetectionJob.class);
    private JavaMailSenderImpl mailSender = null;

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        logger.debug("Running POSLOG_FRAUD_DETECTION with id " + traceHandle.getId());
        String getLastPosLoadProcessIdSql = "SELECT process_initiator_id\n" +
                "FROM process_status\n" +
                "WHERE id=?";

        String fraudCheckSql = "SELECT\n" +
                "  pt.transaction_id,\n" +
                "  pt.card_number,\n" +
                "  pt.store,\n" +
                "  pt.operator_id,\n" +
                "  pt.event_time,\n" +
                "  pts.item_id,\n" +
                "  pts.description,\n" +
                "  pts.extended_amount,\n" +
                "  wl.contacts\n" +
                "FROM process_data_trace pdt\n" +
                "  JOIN stage.pos_transaction pt\n" +
                "    ON pdt.message_id = pt.message_id\n" +
                "  JOIN stage.fraud_card_watch_list wl\n" +
                "    ON regexp_replace(pt.card_number, '[ *]', '', 'g') = wl.card_number\n" +
                "       AND wl.store_id = pt.store\n" +
                "  JOIN stage.pos_transaction_sales pts\n" +
                "    ON pts.is_return = TRUE\n" +
                "       AND pts.transaction_id = pt.transaction_id\n" +
                "WHERE pdt.process_id = ?";

        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(getLastPosLoadProcessIdSql);
            ps.setLong(1, traceHandle.getId());
            ResultSet rset = ps.executeQuery();
            Map<String, SuspectedFraud> result = new HashMap<>();
            if (rset.next()) {
                Long stageLoadProcessId = rset.getLong(1);

                ps = connection.prepareStatement(fraudCheckSql);
                ps.setLong(1, stageLoadProcessId);
                rset = ps.executeQuery();
                while (rset.next()) {
                    String transactionId = rset.getString("transaction_id");
                    SuspectedFraud suspectedFraud = result.get(transactionId);
                    if (suspectedFraud == null) {
                        suspectedFraud = new SuspectedFraud();
                        suspectedFraud.setTransactionId(transactionId);
                        suspectedFraud.setCardNumber(rset.getString("card_number"));
                        suspectedFraud.setOperatorId(rset.getInt("operator_id"));
                        suspectedFraud.setStoreId(rset.getInt("store"));
                        suspectedFraud.setContacts(rset.getString("contacts"));
                        result.put(transactionId, suspectedFraud);
                    }
                    SuspectedFraudItem fraudItem = new SuspectedFraudItem();
                    fraudItem.setDescription(rset.getString("description"));
                    fraudItem.setItemId(rset.getString("item_id"));
                    fraudItem.setPrice(rset.getDouble("extended_amount"));
                    suspectedFraud.getSuspectedFraudItems().add(fraudItem);
                }

                if (!result.isEmpty()) {
                    sendFraudMails(result);
                }
            } else {
                logger.error("Couldn't find initiator process id for process id " + traceHandle.getId());
            }
        }
        callback.jobComplete(new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.SUCCESS));
    }

    void sendFraudMails(Map<String, SuspectedFraud> suspectedFrauds) {
        for (SuspectedFraud suspectedFraud : suspectedFrauds.values()) {
            sendFraudWarningMail(suspectedFraud);
        }
    }

    void sendFraudWarningMail(SuspectedFraud suspectedFraud) {
        String subject = "Fraud rule alert for store " + suspectedFraud.getStoreId();
        String messageText = "A return was detected in store " + suspectedFraud.getStoreId() + " matching card number " + suspectedFraud.getCardNumber() + "\n\n";
        messageText += "The transaction contained the following returned items:\n";
        for (SuspectedFraudItem item : suspectedFraud.getSuspectedFraudItems()) {
            messageText += "  - " + item.getItemId() + " " + item.getDescription() + ": " + item.getPrice() + "\n";
        }

        String[] emailAddresses = suspectedFraud.getContacts().split(",");
        for (int i = 0; i < emailAddresses.length; i++) {
            emailAddresses[i] = emailAddresses[i].toLowerCase().trim();
        }

        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", "localhost");
        Session session = Session.getDefaultInstance(properties);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("Indiska Fraud Alert <no-reply@indiska.se>");
        message.setTo(emailAddresses);
        message.setSubject(subject);
        message.setText(messageText);
        MailSenderProvider.getMailSender().send(message);

        System.out.println("message sent successfully....");
    }


    class SuspectedFraud {
        private String transactionId = null;
        private String cardNumber = null;
        private Integer storeId = null;
        private Integer operatorId = null;
        private Timestamp receiptTime = null;
        private String contacts = null;
        private List<SuspectedFraudItem> suspectedFraudItems = new ArrayList<>();

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getCardNumber() {
            return cardNumber;
        }

        public void setCardNumber(String cardNumber) {
            this.cardNumber = cardNumber;
        }

        public Integer getStoreId() {
            return storeId;
        }

        public void setStoreId(Integer storeId) {
            this.storeId = storeId;
        }

        public Integer getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(Integer operatorId) {
            this.operatorId = operatorId;
        }

        public Timestamp getReceiptTime() {
            return receiptTime;
        }

        public void setReceiptTime(Timestamp receiptTime) {
            this.receiptTime = receiptTime;
        }

        public String getContacts() {
            return contacts;
        }

        public void setContacts(String contacts) {
            this.contacts = contacts;
        }

        public List<SuspectedFraudItem> getSuspectedFraudItems() {
            return suspectedFraudItems;
        }

        public void setSuspectedFraudItems(List<SuspectedFraudItem> suspectedFraudItems) {
            this.suspectedFraudItems = suspectedFraudItems;
        }


    }

    class SuspectedFraudItem {
        private String itemId = null;
        private String description = null;
        private Double price = null;

        public String getItemId() {
            return itemId;
        }

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }
    }
}
