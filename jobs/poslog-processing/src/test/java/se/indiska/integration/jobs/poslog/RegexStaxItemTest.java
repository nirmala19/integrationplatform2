package se.indiska.integration.jobs.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by heintz on 2017-07-11.
 */
public class RegexStaxItemTest {
    public static Logger logger = LoggerFactory.getLogger(RegexStaxItemTest.class);
    @Test
    public void testValue() throws Exception {
        RegexStaxItem regexStaxItem = new RegexStaxItem("ReceiptLine", "ordernumber", "OrderNo:(.*)", ValueItem.Datatype.String);
        String result = (String) regexStaxItem.value("OrderNo:03280778");
        Assert.assertEquals(result, "03280778");
    }

}