package se.indiska.integration.jobs.poslog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import se.indiska.integration.framework.job.JobServiceRunner;

/**
 * Hello world!
 */
@Configuration
@SpringBootApplication
@ComponentScan("se.indiska")
public class PoslogProcessingApplication extends JobServiceRunner {
    @Autowired
    private Environment environment;
    private static final String requiredUserGroup = "indiocampaign";

    public static void main(String[] args) {
        SpringApplication.run(PoslogProcessingApplication.class, args);
    }

}
