package se.indiska.integration.jobs.poslog;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import se.indiska.integration.framework.ExecutorsFactory;

import java.sql.SQLException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/es")
public class RestInterface {
    private static final Logger logger = LoggerFactory.getLogger(RestInterface.class);
    private ESIndexer esIndexer = new ESIndexer();
    private ListenableFuture threadListener = null;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity test() {
        return ResponseEntity.ok("{'status':'ok'}");
    }

    @RequestMapping(value = "/reindex", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity reindex() {
        logger.debug("Indexer running: ", esIndexer.running);
        if (!esIndexer.running) {
            threadListener = ExecutorsFactory.getExecutorService().submit(esIndexer);
            Futures.addCallback(threadListener, new FutureCallback<Object>() {
                @Override
                public void onSuccess(Object result) {
                    threadListener = null;
                }

                @Override
                public void onFailure(Throwable t) {
                    if (t instanceof CancellationException) {
                        logger.info("Indexing was cancelled");
                    } else {
                        logger.error("Failure while indexing! ", t);
                    }
                    threadListener = null;
                }
            });
            return ResponseEntity.ok("Indexer started");
        }
        return ResponseEntity.ok("Indexer is already running");
    }

    @RequestMapping(value = "/cancelIndexing", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity cancelIndexing() {
        if (esIndexer.running) {
            threadListener.cancel(true);
            return ResponseEntity.ok("Stop request has been sent to indexer");
        }
        return ResponseEntity.ok("Indexer is not running");
    }


    class ESIndexer implements Runnable {
        private boolean running = false;

        @Override
        public void run() {
            running = true;
            ESLoader esLoader = new ESLoader();
            try {
                esLoader.loadData(null);
            } catch (SQLException | ExecutionException | InterruptedException e) {
                logger.error("Unable to reindex POSLogs!", e);
                esLoader.stop();
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
            } finally {
                running = false;
            }
        }
    }

}
