package se.indiska.integration.jobs.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PoslogParseException extends Exception {
    private static final Logger logger = LoggerFactory.getLogger(PoslogParseException.class);

    public PoslogParseException(String message) {
        super(message);
    }

    public PoslogParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
