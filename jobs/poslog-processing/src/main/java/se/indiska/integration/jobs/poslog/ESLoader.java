package se.indiska.integration.jobs.poslog;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.IndioProperties;
import se.indiska.integration.framework.job.*;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.*;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by heintz on 28/02/17.
 * se.indiska.integration.integrations.poslog.ESLoader
 */
@IdentifiableIndioJob(identifier = "POSLOG_ELASTICSEARCH_LOAD", eventStep = "ODS_LOAD", description = "Reads stage parsed POSLogs database and indexes the data in Elasticsearch")
public class ESLoader implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(ESLoader.class);
    private static Client esClient = null;
    private boolean finished = false;

    public ESLoader() {

    }

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        long count = loadData(jobExecutionData.getProcessId());
        callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), count > 0 ? ProcessStatus.SUCCESS : ProcessStatus.NOOP));
    }

    private Object getPlainTypeValue(Object object) {
        if (object instanceof Timestamp) {
            return new java.util.Date(((Timestamp) object).getTime());
        } else if (object instanceof BigDecimal) {
            return ((BigDecimal) object).doubleValue();
        } else if (object instanceof BigInteger) {
            return ((BigInteger) object).intValue();
        }
        return object;
    }

    private long addToElasticSearch(List<Map<String, Object>> items) throws ExecutionException, InterruptedException {
        long indexedItems = 0;
        if (!items.isEmpty()) {
            BulkRequestBuilder bulkRequestBuilder = getClient().prepareBulk();
            for (Map<String, Object> item : items) {
                indexedItems++;
                IndexRequestBuilder indexRequestBuilder = getClient().prepareIndex("indiska", "poslog");
                indexRequestBuilder.setSource(item);
                indexRequestBuilder.setId(item.get("transaction_id").toString() + item.get("receipt_number").toString() + item.get("item_id").toString());
                bulkRequestBuilder.add(indexRequestBuilder);
            }
            BulkResponse bulkResponse = bulkRequestBuilder.execute().get();
        }
        return indexedItems;
    }

    private Client getClient() {
        if (esClient == null) {
            try {
                InetAddress elasticHostAddress = InetAddress.getByName("localhost");
                esClient = new PreBuiltTransportClient(Settings.builder()
                        .put("cluster.name", IndioProperties.properties().getProperty("elasticsearch.cluster.name", "indio"))
                        .build())
                        .addTransportAddress(new InetSocketTransportAddress(elasticHostAddress, 9300));

//                esClient = TransportClient.builder().settings(Settings.settingsBuilder()
//                        .put("cluster.name", properties.get("elasticsearch.cluster.name")))
//                        .build().addTransportAddress(
//                                new InetSocketTransportAddress(
//                                        elasticHostAddress,
//                                        Integer.parseInt(properties.get("elasticsearch.port"))));
            } catch (UnknownHostException e) {
                logger.error("Unable to connect to elasticsearch on localhost:9300");
            }
        }
        return esClient;
    }

    public static void main(String[] argv) throws Exception {
        String dateFormat = "yyyy-MM-dd HH:mm:ss.S";
        ESLoader loader = new ESLoader();
        loader.loadData(null);


//        String sql = "SELECT id, name, country_code FROM store WHERE active=TRUE AND id NOT IN (900, 901, 902) ORDER BY id";
//        List<Map<String, Object>> result = new ArrayList<>();
//        try (Connection conn = DatasourceProvider.getConnection()) {
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ResultSet rset = ps.executeQuery();
//            while (rset.next()) {
//                Map<String, Object> row = new HashMap<>();
//                row.put("value", rset.getInt("id"));
//                row.put("text", rset.getInt("id") + ". " + rset.getString("name") + " (" + rset.getString("country_code") + ")");
//                result.add(row);
//            }
//        }
//        ObjectMapper mapper = new ObjectMapper();
//        System.out.println(mapper.writeValueAsString(result));

    }

    protected long loadData(Long processId) throws SQLException, ExecutionException, InterruptedException {
        long start = System.nanoTime();
        long indexedItems = 0;
        int pageSize = 50000;
        finished = false;

        DataSource dataSource;
        String sql = "SELECT\n" +
                "  pt.*,\n" +
                "  pts.sequence_number                         line_sequence_number,\n" +
                "  pts.is_return,\n" +
                "  pts.item_type,\n" +
                "  pts.item_id,\n" +
                "  pts.description,\n" +
                "  pts.tax_included_in_price_flag,\n" +
                "  pts.regular_sales_unit_price,\n" +
                "  pts.actual_sales_unit_price,\n" +
                "  pts.extended_amount,\n" +
                "  pts.quantity,\n" +
                "  pts.web_return,\n" +
                "  pts.tax_type,\n" +
                "  pts.tax_amount,\n" +
                "  pts.tax_percent,\n" +
                "  pts.tax_group_id,\n" +
                "  pts.disposal,\n" +
                "  pts.reason,\n" +
                "  pts.time                                    line_time,\n" +
                "  pts.cancel_flag                             line_cancel_flag,\n" +
                "  pts.new_price,\n" +
                "  pts.previous_price,\n" +
                "  s.timezone,\n" +
                "  CASE WHEN pts.is_return\n" +
                "    THEN -extended_amount * her.rate\n" +
                "  ELSE extended_amount * her.rate END adjusted_amount\n\n" +
                "FROM stage.pos_transaction pt\n" +
                "  JOIN stage.pos_transaction_sales pts\n" +
                "    ON pt.transaction_id = pts.transaction_id\n" +
                "  JOIN store s\n" +
                "    ON s.id = pt.store\n" +
                "  JOIN ods.country_currency_map ccm\n" +
                "    ON ccm.country_code = s.country_code\n" +
                "  JOIN ods.historical_exchange_rates her\n" +
                "    ON ccm.currency_code = her.currency\n" +
                "       AND date_trunc('month', her.from_date) = date_trunc('month', pt.event_time)\n";
        String processDataTraceSql = "  JOIN process_data_trace pdt\n" +
                "    ON pts.transaction_id = pdt.target_id\n" +
                "       AND pdt.target = 'pos_transaction'\n" +
                "  JOIN process_status ps\n" +
                "    ON ps.process_initiator_id = pdt.process_id\n" +
                "       AND ps.id = ?\n";
        String sqlSuffix = "ORDER BY pt.event_time\n" +
                "LIMIT ?\n" +
                "OFFSET ?";

        if (processId != null) {
            sql += processDataTraceSql;
        }
        sql += sqlSuffix;

//        String sql = "SELECT *\n" +
//                "FROM inbound_file_staging ifs\n" +
//                "  LEFT OUTER JOIN message_metadata mm\n" +
//                "    ON ifs.message_id = mm.message_id\n" +
//                "       AND (mm.key = 'remote_server' OR mm.key = 'source_store_id')\n" +
//                "ORDER BY ifs.event_time LIMIT 5000 OFFSET ?";

//        HikariConfig config = new HikariConfig();
//        config.setJdbcUrl("jdbc:postgresql://indio.hk.indiska.se/indio");
//        config.setUsername("indiska");
//        config.setPassword("IndIO42!");
//        config.setMinimumIdle(2);
//        config.setMaximumPoolSize(10);
//
//        config.addDataSourceProperty("cachePrepStmts", "true");
//        config.addDataSourceProperty("prepStmtCacheSize", "250");
//        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
//        config.addDataSourceProperty("leakDetectionThreshold", "20");

//        dataSource = new HikariDataSource(config);

        int offset = 0;

        while (!finished) {
            List<Map<String, Object>> result = new ArrayList<>();
            long start2 = System.nanoTime();
            try (Connection connection = DatasourceProvider.getConnection()) {
                PreparedStatement ps = connection.prepareStatement(sql);
                int pNum = 1;
                if (processId != null) {
                    ps.setLong(pNum++, processId);
                }
                ps.setInt(pNum++, pageSize);
                ps.setInt(pNum++, offset);
                offset += pageSize;
                ResultSet rset = ps.executeQuery();
                ResultSetMetaData metaData = rset.getMetaData();
                int cnt = 0;
                while (rset.next()) {
                    cnt++;
                    Map<String, Object> item = new HashMap<>();
                    for (int i = 1; i <= metaData.getColumnCount(); i++) {
                        String columnName = metaData.getColumnLabel(i);
                        Object value = getPlainTypeValue(rset.getObject(columnName));
                        if (value instanceof String && ((String) value).startsWith("<")) {
                            JSONObject xmlJSONObj = XML.toJSONObject((String) value);
                            item.put("parsed_value", xmlJSONObj.toMap());
                        }
                        if (value instanceof Date) {
                            ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(((Date) value).toInstant(), ZoneId.of(rset.getString("timezone")));
                            value = zonedDateTime;
                        }
                        item.put(columnName, value);
                    }

                    result.add(item);
                    if (result.size() > pageSize) {
                        indexedItems += addToElasticSearch(result);
                        result = new ArrayList<>();
                    }
                }
                long dbTime = System.nanoTime() - start2;
                long s2 = System.nanoTime();
                indexedItems += addToElasticSearch(result);
                long esTime = System.nanoTime() - s2;
                long totalTime = System.nanoTime() - start2;

                if (logger.isTraceEnabled()) {
                    logger.debug("Indexed " + indexedItems + " POS line items in " + (totalTime / 1e6d) + "/" + dbTime / 1e6d + "/" + esTime / 1e6d + " ms, " + totalTime / 1e6d / pageSize + "/" + dbTime / 1e6d / pageSize + "/" + esTime / 1e6d / pageSize + "ms/item");
                }

                finished = cnt < pageSize;
            }

        }
        if (logger.isDebugEnabled() && indexedItems > 0) {
            long tta = System.nanoTime() - start;
            logger.debug("Done indexing " + indexedItems + " POS line items, took " + (tta / 1e6d) + "ms, " + (tta / indexedItems / 1e6d) + "ms/item");
        } else if (indexedItems == 0) {
            logger.debug("no poslogs found for process " + processId);
        }

        return indexedItems;
    }

    public void stop() {
        finished = true;
    }
}
