//package se.indiska.integration.jobs.poslog;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import se.indiska.integration.framework.util.InboundFileUtilities;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//import javax.xml.stream.XMLEventReader;
//import javax.xml.stream.XMLInputFactory;
//import javax.xml.stream.XMLStreamConstants;
//import javax.xml.stream.XMLStreamException;
//import javax.xml.stream.events.Characters;
//import javax.xml.stream.events.EndElement;
//import javax.xml.stream.events.StartElement;
//import javax.xml.stream.events.XMLEvent;
//import java.io.ByteArrayOutputStream;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.Reader;
//import java.util.*;
//
//public class PoslogParser {
//    private static final Logger logger = LoggerFactory.getLogger(PoslogParser.class);
//    private static DocumentBuilder documentBuilder;
//        private static Map<String, NodeHandler> nodeHandlerMap = new HashMap<>();
//    private List<Transaction> transactions = new ArrayList<>();
//    private NodeHandler currentNodeHandler = null;
////    private Map<String>
//
//
//    static {
//        nodeHandlerMap.put("/POSLog/Transaction", new NodeHandler() {
//            @Override
//            public void node(StartElement startElement) {
//                Iterator i = startElement.getAttributes();
//                while (i.hasNext()) {
//                    logger.debug(i.next().toString());
//                }
//            }
//
//            @Override
//            public void characters(String characters) {
//                logger.debug("Chars: " + characters);
//            }
//        });
//    }
//
//    public void parse(String fileContent) throws PoslogParseException {
//        XMLInputFactory factory = XMLInputFactory.newInstance();
//        try (Reader fileReader = InboundFileUtilities.getFileContentReader(fileContent)) {
//            Path path = new Path();
//            XMLEventReader eventReader = factory.createXMLEventReader(fileReader);
//            while (eventReader.hasNext()) {
//                XMLEvent event = eventReader.nextEvent();
//                parse(event, path);
//            }
//
////            Document document = getDocumentBuilder().parse(InboundFileUtilities.getFileContentInputStream(fileContent));
////            NodeList nl = document.getElementsByTagName("Transaction");
////            for (int i=0; i<nl.getLength(); i++) {
////                Element transactionElement = (Element)nl.item(i);
////                Transaction transaction = new Transaction();
////                transaction.parse(transactionElement);
////                transactions.add(transaction);
////            }
//        } catch (IOException | XMLStreamException e) {
//            throw new PoslogParseException("Unable to parse POSLOG!", e);
//        }
//    }
//
//    private void parse(XMLEvent event, Path path) {
//        switch (event.getEventType()) {
//            case XMLStreamConstants.START_ELEMENT:
//                StartElement startElement = event.asStartElement();
//                String qName = startElement.getName().getLocalPart();
//                path.add(qName);
//                logger.debug(path.toString());
//                NodeHandler nodeHandler = nodeHandlerMap.get(path.toString());
//                if (nodeHandler != null) {
//                    nodeHandler.node(startElement);
//                }
//                break;
//            case XMLStreamConstants.END_ELEMENT:
//                EndElement endElement = event.asEndElement();
//                String endElementQName = endElement.getName().getLocalPart();
//                String item = path.poll();
//                break;
//            case XMLStreamConstants.CHARACTERS:
//                Characters characters = event.asCharacters();
//                if (!characters.isWhiteSpace()) {
//                    logger.debug("Char path: " + path.toString());
//                    NodeHandler nh = nodeHandlerMap.get(path.toString());
//                    if (nh != null) {
//                        nh.characters(characters.getData());
//                    }
//                }
//                break;
//        }
//
//    }
//
//    private void printPath(LinkedList<String> path) {
//        StringBuilder sb = new StringBuilder();
//
//        Iterator<String> i = path.iterator();
//        while (i.hasNext()) {
//            sb.append("/");
//            sb.append(i.next());
//        }
//        System.out.println(sb.toString());
//    }
//
//
//    private static DocumentBuilder getDocumentBuilder() {
//        if (documentBuilder == null) {
//            try {
//                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//            } catch (ParserConfigurationException e) {
//                logger.error("Unable to create document builder, SEVERE ERROR!", e);
//            }
//        }
//        return documentBuilder;
//    }
//
//    public static void main(String[] argv) throws Exception {
//        FileInputStream fis = new FileInputStream("/tmp/poslog.xml");
//        byte[] buf = new byte[1024];
//        int c = 0;
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        while ((c = fis.read(buf)) >= 0) {
//            baos.write(buf, 0, c);
//        }
//        fis.close();
//        PoslogParser poslogParser = new PoslogParser();
//        poslogParser.parse(new String(baos.toByteArray()));
//    }
//}
//
//interface NodeHandler {
//    public void node(Path path, StartElement startElement);
//
//    public void characters(Path path, String characters);
//}
//
//class Path {
//    private LinkedList<String> path = new LinkedList<>();
//
//    public void add(String value) {
//        path.add(value);
//    }
//
//    public String poll() {
//        return path.pollLast();
//    }
//
//    public String toString() {
//        StringBuilder sb = new StringBuilder();
//
//        Iterator<String> i = path.iterator();
//        while (i.hasNext()) {
//            sb.append("/");
//            sb.append(i.next());
//        }
//        return sb.toString();
//    }
//}