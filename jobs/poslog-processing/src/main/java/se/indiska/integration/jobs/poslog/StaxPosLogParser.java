package se.indiska.integration.jobs.poslog;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.trace.DataTrace;
import se.indiska.integration.framework.trace.Trace;
import se.indiska.integration.framework.util.InboundFileUtilities;

import javax.sql.DataSource;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.Reader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heintz on 27/02/17.
 * <p>
 * datatype="POSLOG"
 * triggerType="EVENT"
 * triggerStep="INBOUND_TRANSFER"
 * resultStep="ODS_STAGE_LOAD"
 */

@IdentifiableIndioJob(identifier = "POSLOG_STAGE_LOAD", eventStep = "STAGE_LOAD", description = "Reads POSLogs and store in Stage area")
public class StaxPosLogParser implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(StaxPosLogParser.class);
    private static final String eventStep = "POSLOG_STAGING_LOAD";
    private static final String dataType = "POSLOG";
    private static DataSource prodDataSource = null;

    private static void store(List<StaxTransaction> txs, StatementFactory statementFactory) throws SQLException {
        for (StaxTransaction tx : txs) {
            tx.store(statementFactory);

        }
    }

    public static void main(String[] argv) throws Exception {
//        File dir = new File("/Users/heintz/Projects/Indiska/ICC/testdata/archive/inbound/poslog/18/2017/05/09");
//        File file = new File(dir, "POSLog.20170508171215.6544.0002.00018.xml");

        long start = System.currentTimeMillis();
        LocalDate date = LocalDate.of(2017, 5, 30);
        LocalDate end = LocalDate.of(2017, 6, 30);
        int cnt = 0;
        List<File> errorFiles = new ArrayList<>();

        try (Connection connection = getProductionConnectionForTestPurpose()) {
            try (StatementFactory statementFactory = new StatementFactory(connection)) {

                while (date.isBefore(end)) {
                    List<StaxTransaction> transactions = new ArrayList<>();
                    File dir = new File("/Users/heintz/Projects/Indiska/ICC/testdata/posloghistory/" + date.format(DateTimeFormatter.ISO_DATE));
                    if (dir.exists() && dir.isDirectory()) {
                        File[] files = dir.listFiles((dir1, name) -> name.startsWith("POSLog"));
                        for (File f : files) {
                            try {
                                FileInputStream fis = new FileInputStream(f);
                                XMLInputFactory factory = XMLInputFactory.newInstance();
                                XMLEventReader eventReader = factory.createXMLEventReader(fis);
                                StaxPosLogParser parser = new StaxPosLogParser();

                                StaxTransaction transaction = parser.addTransaction("load-" + f.getName(), transactions, eventReader);
                                if (transactions.size() >= 5000) {
                                    List<StaxTransaction> txs = transactions;
                                    transactions = new ArrayList<>();
                                    store(txs, statementFactory);
                                }
                                cnt++;

                            } catch (Exception e) {
//                                logger.error("Unable to load file " + f.getAbsolutePath());
                                errorFiles.add(f);
                            }
                        }
                    }
                    store(transactions, statementFactory);
                    logger.debug("Loaded " + date.format(DateTimeFormatter.ISO_DATE) + " (so far loaded " + cnt + " transactions)");
                    date = date.plus(1, ChronoUnit.DAYS);
                }
            }
        }
        Duration d = Duration.of(System.currentTimeMillis() - start, ChronoUnit.MILLIS);
        logger.info("Successfully parsed " + cnt + " poslogs, unable to parse " + errorFiles.size() + " poslog files in " + d.toMinutes() + " minutes");

        System.exit(0);
    }

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        List<StaxTransaction> transactions = new ArrayList<>();
        long start = System.nanoTime();
        boolean success = true;
        int count = 0;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        String sql = "SELECT * FROM public.inbound_file_staging ifs, " +
                "public.process_data_trace pdt, " +
                "public.process_status ps\n" +
                "WHERE ps.process_initiator_id=pdt.process_id\n" +
                "AND ifs.id=cast(pdt.target_id AS BIGINT)\n" +
                "AND ps.id=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1, traceHandle.getId());

            ResultSet rset = ps.executeQuery();
            StatementFactory statementFactory = new StatementFactory(connection);
            List<Trace> traces = new ArrayList<>();
            while (rset.next()) {
                Long sourceId = rset.getLong("id");
                String messageId = rset.getString("message_id");

                try (Reader fileReader = InboundFileUtilities.getFileContentReader(rset.getString("file_content"))) {
                    count++;
                    XMLEventReader eventReader = factory.createXMLEventReader(fileReader);

                    StaxTransaction transaction = addTransaction(messageId, transactions, eventReader);
                    if (transactions.size() > 1000) {
                        List<StaxTransaction> txs = transactions;
                        transactions = new ArrayList<>();
                        store(txs, statementFactory);
                        traceHandle.getProcessTracer().trace(traces.toArray(new Trace[traces.size()]));
                        traces.clear();
                    }
                    traces.add(new DataTrace(messageId, traceHandle.getId(), "inbound_file_staging", sourceId == null ? null : sourceId.toString(), "pos_transaction", transaction.getId(), ProcessStatus.SUCCESS));
                } catch (XMLStreamException e) {
                    String error = "Unable to parse file content of file " + rset.getString("filename");
                    logger.error(error, e);
                    traceHandle.getProcessTracer().trace(new DataTrace(messageId, traceHandle.getId(), "inbound_file_staging", sourceId + "", "pos_transaction", null, ProcessStatus.ERROR, error, e, ProcessError.INVALID_FILE_CONTENT));
                    success = false;
                }

            }


            store(transactions, statementFactory);
            traceHandle.getProcessTracer().trace(traces.toArray(new Trace[traces.size()]));
            traces.clear();

            statementFactory.executeAllBatches();
            statementFactory.close();
        }
        double tta = (System.nanoTime() - start) / 1e6d;
        if (count > 0) {
            logger.debug("Executed " + count + " transactions in " + tta + "ms (" + (tta / count) + " ms/tx)");
        }
        callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), success ? ProcessStatus.SUCCESS : ProcessStatus.ERROR));
    }

    private static Connection getProductionConnectionForTestPurpose() throws SQLException {
//        return DatasourceProvider.getConnection();
        if (prodDataSource == null) {
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl("jdbc:postgresql://indio.hk.indiska.se/indio");
            config.setUsername("indiska");
            config.setPassword("IndIO42!");
            config.setMinimumIdle(2);
            config.setMaximumPoolSize(10);

            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            config.addDataSourceProperty("leakDetectionThreshold", "20");

            prodDataSource = new HikariDataSource(config);
        }
        return prodDataSource.getConnection();
    }

    private StaxTransaction addTransaction(String messageId, List<StaxTransaction> transactions, XMLEventReader eventReader) throws XMLStreamException {
        StaxTransaction transaction = null;
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            switch (event.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                    StartElement startElement = event.asStartElement();
                    String qName = startElement.getName().getLocalPart();
                    switch (qName) {
                        case "Transaction":
                            transaction = new StaxTransaction(messageId);
                            transaction.parse(eventReader, null, null, startElement.getAttributes(), null);
                            transactions.add(transaction);
                            break;
                    }
                    break;
            }
        }
        return transaction;
    }

}
