package se.indiska.integration.jobs.poslog;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by heintz on 27/02/17.
 */
public class StaxTransaction implements StaxParser {
    private static final Logger logger = LoggerFactory.getLogger(StaxTransaction.class);
    private static Map<String, ValueItem> staxItemMap = new LinkedHashMap<>();
    private static Map<String, ValueItem> callbackItems = new LinkedHashMap<>();

    static {
        staxItemMap.put("TransactionID", new ValueItem("TransactionID", "transaction_id", ValueItem.Datatype.String));
        staxItemMap.put("RetailStoreID", new ValueItem("RetailStoreID", "store", ValueItem.Datatype.Integer));
        staxItemMap.put("WorkstationID", new ValueItem("WorkstationID", "workstation_id", ValueItem.Datatype.Integer));
        staxItemMap.put("SequenceNumber", new ValueItem("SequenceNumber", "sequence_number", ValueItem.Datatype.Integer));
        staxItemMap.put("OperatorID", new ValueItem("OperatorID", "operator_id", ValueItem.Datatype.Integer));
        staxItemMap.put("CurrencyCode", new ValueItem("CurrencyCode", "currency_code", ValueItem.Datatype.String));
        staxItemMap.put("ReceiptNumber", new ValueItem("ReceiptNumber", "receipt_number", ValueItem.Datatype.Integer));
        staxItemMap.put("PosLogDateTime", new ValueItem("PosLogDateTime", "event_time", ValueItem.Datatype.DateTime));
        staxItemMap.put("CustomerID", new ValueItem("CustomerID", "customer_id", ValueItem.Datatype.String));
        staxItemMap.put("LineItem", new StaxChildItem("LineItem", StaxLineItem.class));
        staxItemMap.put("TrainingModeFlag", new ValueItem("TrainingModeFlag", "training_mode", ValueItem.Datatype.Boolean));
        staxItemMap.put("CancelFlag", new ValueItem("CancelFlag", "cancel_flag", ValueItem.Datatype.Boolean));
        staxItemMap.put("TransactionStatus", new ValueItem("TransactionStatus", "transaction_status", ValueItem.Datatype.String));
        staxItemMap.put("ReceiptLine", new RegexStaxItem("ReceiptLine", "ecomorderno", "OrderNo:(.*)", ValueItem.Datatype.String));
        callbackItems.put("PrimaryAccountNumber", new ValueItem("PrimaryAccountNumber", "card_number", ValueItem.Datatype.String));
    }

    private Map<String, Object> values = new HashMap<>();
    private ValueItem currentStaxItem = null;
    private String messageId;

    public StaxTransaction(String messageId) {
        this.messageId = messageId;
    }

    public void parse(XMLEventReader eventReader, String parentName, Object parentId, Iterator attributes, ValueCallback callback) throws XMLStreamException {
        while (attributes.hasNext()) {
            Attribute attribute = (Attribute) attributes.next();
            ValueItem item = staxItemMap.get(attribute.getName().toString());
            if (item != null) {
                values.put(item.getName(), item.value(attribute.getValue()));
            }
        }
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            switch (event.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                    StartElement startElement = event.asStartElement();
                    String qName = startElement.getName().getLocalPart();
                    currentStaxItem = staxItemMap.get(qName);
                    if (currentStaxItem instanceof StaxChildItem) {
                        Collection children = (Collection) values.get(currentStaxItem.getName());
                        if (children == null) {
                            children = new ArrayList();
                            values.put(currentStaxItem.getName(), children);
                        }
                        StaxParser staxParser = ((StaxChildItem) currentStaxItem).getStaxParser(this.messageId);
                        staxParser.parse(eventReader, "TransactionID", values.get("TransactionID"), startElement.getAttributes(), new ValueCallback() {
                            @Override
                            public void foundValue(String key, Object value) {
                                if (key != null) {
                                    ValueItem staxItem = callbackItems.get(key);
                                    if (staxItem != null) {
                                        values.put(staxItem.getName(), value);
                                    }
                                }
                            }
                        });
                        children.add(staxParser);
                    } else {
                        Iterator childAttributes = startElement.getAttributes();
                        while (childAttributes.hasNext()) {
                            Attribute attribute = (Attribute) childAttributes.next();
                            ValueItem childAttributeItem = staxItemMap.get(attribute.getName().toString());
                            if (childAttributeItem != null) {
                                Object val = childAttributeItem.value(attribute.getValue());
                                if (val != null) {
                                    values.put(childAttributeItem.getName(), val);
                                }
                            }
                        }
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    Characters characters = event.asCharacters();
                    if (currentStaxItem != null && currentStaxItem.getDataType() != null) {
                        Object val = currentStaxItem.value(characters.getData());
                        if (val != null) {
                            values.put(currentStaxItem.getName(), val);
                        }
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    currentStaxItem = null;
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("Transaction")) {
                        return;
                    }
                    break;
            }
        }
    }

    @Override
    public void store(StatementFactory statementFactory) throws SQLException {
        String sql = "insert into stage.pos_transaction (";
        Collection<ValueItem> allItems = new ArrayList<>();
        allItems.addAll(staxItemMap.values());
        allItems.addAll(callbackItems.values());
        Iterator<ValueItem> staxItemIterator = allItems.iterator();
        String valueString = "";
        boolean isFirst = true;
        while (staxItemIterator.hasNext()) {
            ValueItem staxItem = staxItemIterator.next();
            if (!(staxItem instanceof StaxChildItem)) {
                if (!isFirst) {
                    sql += ", ";
                    valueString += ", ";
                }
                sql += staxItem.getColumn();
                valueString += "?";
                isFirst = false;
            } else {
                List<StaxParser> children = (List<StaxParser>) values.get(staxItem.getName());
                if (children != null) {
                    for (StaxParser child : children) {
                        child.store(statementFactory);
                    }
                }
            }
        }
        valueString += ",?";
        sql += ", message_id) VALUES (" + valueString + ") on conflict do nothing";
        PreparedStatement ps = statementFactory.getStatement(sql);

        int position = 1;
        for (ValueItem staxItem : allItems) {
            if (!(staxItem instanceof StaxChildItem)) {
                ps.setObject(position++, values.get(staxItem.getName()));
            }
        }
        ps.setString(position, this.messageId);
//        logger.debug("Executing " + sql);
        ps.addBatch();
    }


    @Override
    public String toString() {
        logger.debug(values.toString());
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(values);
        } catch (JsonProcessingException e) {
            logger.error("", e);
            return values.toString();
        }
    }

    public String getId() {
        return (String) values.get("TransactionID");
    }
}
