package se.indiska.integration.jobs.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by heintz on 27/02/17.
 */
public class StaxChildItem extends ValueItem {
    private static final Logger logger = LoggerFactory.getLogger(StaxChildItem.class);
    private Class<? extends StaxParser> parser;

    public StaxChildItem(String name, Class<? extends StaxParser> parser) {
        super(name, null, null);
        this.parser = parser;
    }

    public StaxParser getStaxParser(String messageId) {
        try {
            return parser.getConstructor(String.class).newInstance(messageId);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
