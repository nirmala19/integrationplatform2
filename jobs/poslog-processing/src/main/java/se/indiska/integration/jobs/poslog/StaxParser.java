package se.indiska.integration.jobs.poslog;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import java.sql.SQLException;
import java.util.Iterator;

/**
 * Created by heintz on 27/02/17.
 */
public interface StaxParser {
    void parse(XMLEventReader eventReader, String parentName, Object parentId, Iterator attributes, ValueCallback callback) throws XMLStreamException;

    void store(StatementFactory statementFactory) throws SQLException;
//    String sql();
}

interface ValueCallback {
    void foundValue(String key, Object value);
}