//package se.indiska.integration.jobs.poslog;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.xml.stream.events.StartElement;
//import java.util.HashMap;
//import java.util.Map;
//
//public class Transaction implements NodeHandler {
//    private static final Logger logger = LoggerFactory.getLogger(Transaction.class);
//    private static Map<String, ValueItem> valueItemMap = new HashMap<>();
//    private Map<String, Object> values = new HashMap<>();
//    private static final String trigger = "/POSLog/Transaction";
//
//    static {
//        valueItemMap.put("/POSLog/Transaction/TransactionID", new ValueItem("transactionId", "transaction_id", ValueItem.Datatype.String));
//        valueItemMap.put("/POSLog/Transaction/RetailStoreID", new ValueItem("storeId", "store", ValueItem.Datatype.Integer));
//        valueItemMap.put("/POSLog/Transaction/WorkstationID", new ValueItem("workstationId", "workstation_id", ValueItem.Datatype.Integer));
//        valueItemMap.put("/POSLog/Transaction/SequenceNumber", new ValueItem("sequenceNumber", "sequence_number", ValueItem.Datatype.Integer));
//        valueItemMap.put("/POSLog/Transaction/OperatorID", new ValueItem("operatorId", "operator_id", ValueItem.Datatype.Integer));
//        valueItemMap.put("/POSLog/Transaction/CurrencyCode", new ValueItem("currencyCode", "currency_code", ValueItem.Datatype.Integer));
//        valueItemMap.put("/POSLog/Transaction/ReceiptNumber", new ValueItem("receiptNumber", "receipt_number", ValueItem.Datatype.Integer));
//    }
//
//    public static Map<String, ValueItem> getValueItemMap() {
//        return valueItemMap;
//    }
//
//    public Map<String, Object> getValues() {
//        return values;
//    }
//
//    public static String getTrigger() {
//        return trigger;
//    }
//
//    @Override
//    public void node(Path path, StartElement startElement) {
//
//    }
//
//    @Override
//    public void characters(Path path, String characters) {
//        ValueItem valueItem = valueItemMap.get(path.toString());
//        if (valueItem != null) {
//            values.put(valueItem.getName(), valueItem.value(characters));
//        }
//    }
//}
