package se.indiska.integration.jobs.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by heintz on 2017-07-11.
 */
public class RegexStaxItem extends ValueItem {
    private static final Logger logger = LoggerFactory.getLogger(RegexStaxItem.class);
    private Pattern regex;

    public RegexStaxItem(String name, String columnName, String regex, Datatype datatype) {
        super(name, columnName, datatype);
        this.regex = Pattern.compile(regex);
    }

    @Override
    public Object value(String input) {
        Matcher matcher = regex.matcher(input);
        if (matcher.matches()) {
            return matcher.replaceFirst("$1");
        }
        return null;
    }
}
