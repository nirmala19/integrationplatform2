package se.indiska.integration.jobs.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created by heintz on 27/02/17.
 */
public class ValueItem {
    private static final Logger logger = LoggerFactory.getLogger(ValueItem.class);
    private String name;
    private String column;
    private Datatype dataType;
    public ValueItem(String name, String column, Datatype dataType) {
        this.name = name;
        this.column = column;
        this.dataType = dataType;
    }

    public Object value(String input) {
        if (dataType != null) {
            switch (dataType) {
                case DateTime:
                    if (input.length() > 23) {
                        return new java.sql.Timestamp(ZonedDateTime.parse(input).toInstant().toEpochMilli());
                    } else {
                        return new java.sql.Timestamp(LocalDateTime.parse(input).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                    }
                case String:
                    return input;
                case Decimal:
                    return new Double(input);
                case Integer:
                    return new Long(input);
                case Boolean:
                    return new Boolean(input);
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public String getColumn() {
        return column;
    }

    public Datatype getDataType() {
        return dataType;
    }

    public enum Datatype {String, Decimal, Integer, Boolean, DateTime}
}
