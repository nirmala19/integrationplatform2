package se.indiska.integration.jobs.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.trace.DataTrace;
import se.indiska.integration.framework.trace.Trace;
import se.indiska.integration.framework.util.InboundFileUtilities;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@IdentifiableIndioJob(
        identifier = "PMLOG_STAGE_LOAD",
        eventStep = "STAGE_LOAD",
        description = "Reads POSLogs and store in Stage area",
        dataType = "PMLOG",
        jobType = IdentifiableIndioJob.JobType.EVENT_BASED,
        triggerEventStep = "INBOUND_TRANSFER",
        triggerDataType = "PMLOG"
)
public class PMLogParserJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(PMLogParserJob.class);
    private static DocumentBuilder documentBuilder;

    static {
        try {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error("Unable to instatiate DOM Document Builder!", e);
        }
    }

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        long start = System.nanoTime();
        boolean success = true;
        int count = 0;
        XMLInputFactory factory = XMLInputFactory.newInstance();
        String sql = "SELECT * FROM public.inbound_file_staging ifs, " +
                "public.process_data_trace pdt, " +
                "public.process_status ps\n" +
                "WHERE ps.process_initiator_id=pdt.process_id\n" +
                "AND ifs.id=cast(pdt.target_id AS BIGINT)\n" +
                "AND ps.id=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1, traceHandle.getId());

            ResultSet rset = ps.executeQuery();
            StatementFactory statementFactory = new StatementFactory(connection);
            List<Trace> traces = new ArrayList<>();
            List<PMLog> pmLogs = new ArrayList<>();
            while (rset.next()) {
                Long sourceId = rset.getLong("id");
                String messageId = rset.getString("message_id");
                PMLog pmLog = new PMLog(sourceId, messageId);
                Document document = documentBuilder.parse(InboundFileUtilities.getFileContentInputStream(rset.getString("file_content")));
                Element transaction = (Element) document.getElementsByTagName("Transaction").item(0);
                pmLog.parse(transaction);
                pmLogs.add(pmLog);
//                try (Reader fileReader = InboundFileUtilities.getFileContentReader(rset.getString("file_content"))) {
//                    count++;

//                    traces.add(new DataTrace(messageId, traceHandle.getId(), "inbound_file_staging", sourceId == null ? null : sourceId.toString(), "pos_transaction", transaction.getId(), ProcessStatus.SUCCESS));
//                } catch (XMLStreamException e) {
//                    String error = "Unable to parse file content of file " + rset.getString("filename");
//                    logger.error(error, e);
//                    traceHandle.getProcessTracer().trace(new DataTrace(messageId, traceHandle.getId(), "inbound_file_staging", sourceId + "", "day_closure", null, ProcessStatus.ERROR, error, e, ProcessError.INVALID_FILE_CONTENT));
//                    success = false;
//                }

            }
            rset.close();
            ps.close();

            store(connection, pmLogs);

            for (PMLog pmLog : pmLogs) {
                traces.add(new DataTrace(pmLog.getMessageId(), traceHandle.getId(), "inbound_file_staging", pmLog.getSourceId() == null ? null : pmLog.getSourceId().toString(), "pmlog", pmLog.getTransactionId(), ProcessStatus.SUCCESS));
            }

            traceHandle.getProcessTracer().trace(traces.toArray(new Trace[traces.size()]));
            traces.clear();

            callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), success ? ProcessStatus.SUCCESS : ProcessStatus.ERROR));
        }
    }

    protected void store(Connection connection, List<PMLog> pmLogs) throws SQLException {
        String pmLogSql = "INSERT INTO stage.pmlog (transaction_id, store, workstation_id, sequence_number, system_id, operator_id, reference_id, business_day_date, begin_date_time, message_id) VALUES (?,?,?,?,?,?,?,?,?,?) ON CONFLICT DO NOTHING ";
        String ledgerSql = "INSERT INTO stage.pmlog_ledger (transaction_id, account, cumulative_amount, cumulative_debit_amount, cumulative_credit_amount) VALUES (?,?,?,?,?) on CONFLICT DO NOTHING ";
        PreparedStatement ps = connection.prepareStatement(pmLogSql);
        PreparedStatement ledgerPs = connection.prepareStatement(ledgerSql);

        for (PMLog pmLog : pmLogs) {
            if (!pmLog.getFinancialLedgers().isEmpty()) {
                ps.setString(1, pmLog.getTransactionId());
                ps.setInt(2, pmLog.getStore());
                ps.setInt(3, pmLog.getWorkstationId());
                ps.setInt(4, pmLog.getSequenceNumber());
                ps.setString(5, pmLog.getSystemId());
                ps.setString(6, pmLog.getOperatorId());
                ps.setString(7, pmLog.getReferenceID());
                ps.setTimestamp(8, pmLog.getBusinessDayDate());
                ps.setTimestamp(9, pmLog.getBeginDateTime());
                ps.setString(10, pmLog.getMessageId());
                ps.addBatch();

                for (PMLog.FinancialLedger financialLedger : pmLog.getFinancialLedgers()) {
                    ledgerPs.setString(1, financialLedger.getTransactionId());
                    ledgerPs.setInt(2, financialLedger.getAccount());
                    ledgerPs.setDouble(3, financialLedger.getCumulativeAmount());
                    ledgerPs.setDouble(4, financialLedger.getCumulativeDebitAmount());
                    ledgerPs.setDouble(5, financialLedger.getCumulativeCreditAmount());
                    ledgerPs.addBatch();
                }
            }
        }
        ps.executeBatch();
        ledgerPs.executeBatch();

        ps.close();
    }
}
