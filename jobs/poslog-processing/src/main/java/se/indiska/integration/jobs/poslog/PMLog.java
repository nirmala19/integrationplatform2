package se.indiska.integration.jobs.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import se.indiska.integration.framework.DatasourceProvider;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class PMLog {
    private static final Logger logger = LoggerFactory.getLogger(PMLog.class);
    private String messageId = null;
    private String transactionId = null;
    private Integer store = null;
    private Integer workstationId = null;
    private Integer sequenceNumber = null;
    private String systemId = null;
    private String operatorId = null;
    private String referenceID = null;
    private Timestamp businessDayDate = null;
    private Timestamp beginDateTime = null;
    private Long sourceId = null;
    private List<FinancialLedger> financialLedgers = new ArrayList<>();
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public PMLog(Long sourceId, String messageId) {
        this.sourceId = sourceId;
        this.messageId = messageId;
    }

    public Long getSourceId() {
        return sourceId;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public Integer getStore() {
        return store;
    }

    public Integer getWorkstationId() {
        return workstationId;
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public String getSystemId() {
        return systemId;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public String getReferenceID() {
        return referenceID;
    }

    public Timestamp getBusinessDayDate() {
        return businessDayDate;
    }

    public Timestamp getBeginDateTime() {
        return beginDateTime;
    }

    public List<FinancialLedger> getFinancialLedgers() {
        return financialLedgers;
    }

    public void parse(Element transactionElement) {
        this.store = Integer.parseInt(transactionElement.getElementsByTagName("RetailStoreID").item(0).getFirstChild().getNodeValue());
        this.workstationId = Integer.parseInt(transactionElement.getElementsByTagName("WorkstationID").item(0).getFirstChild().getNodeValue());
        this.sequenceNumber = Integer.parseInt(transactionElement.getElementsByTagName("SequenceNumber").item(0).getFirstChild().getNodeValue());
        this.systemId = transactionElement.getElementsByTagName("SystemID").item(0).getFirstChild().getNodeValue();
        this.operatorId = transactionElement.getElementsByTagName("OperatorID").item(0).getFirstChild().getNodeValue();
        this.referenceID = transactionElement.getElementsByTagName("ReferenceID").item(0).getFirstChild().getNodeValue();
        String businessDayDate = transactionElement.getElementsByTagName("BusinessDayDate").item(0).getFirstChild().getNodeValue();
        try {
            this.businessDayDate = new Timestamp(dateFormat.parse(businessDayDate).getTime());
            this.beginDateTime = new Timestamp(dateFormat.parse(transactionElement.getElementsByTagName("BeginDateTime").item(0).getFirstChild().getNodeValue()).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.transactionId = "" + this.store + this.workstationId + this.sequenceNumber+businessDayDate;

        NodeList ledgers = transactionElement.getElementsByTagName("FinancialLedger");
        for (int i = 0; i < ledgers.getLength(); i++) {
            Element element = (Element) ledgers.item(i);
            FinancialLedger l = new FinancialLedger(this.transactionId);
            l.parse(element);
            financialLedgers.add(l);
        }

    }


    public static void main(String[] argv) throws Exception {
        FileInputStream fis = new FileInputStream("/tmp/pmlog.xml");
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(fis);
        Element transactionElement = (Element) doc.getElementsByTagName("Transaction").item(0);
        PMLog pmLog = new PMLog(1l, "manual:pmlog.xml");
        pmLog.parse(transactionElement);
        PMLogParserJob pmLogParserJob = new PMLogParserJob();
        try (Connection connection = DatasourceProvider.getConnection()) {
            List<PMLog> list = new ArrayList<>();
            list.add(pmLog);
            pmLogParserJob.store(connection, list);
        }
        logger.debug("");
    }

    class FinancialLedger {
        private String transactionId = null;
        private Integer account = null;
        private Double cumulativeAmount = null;
        private Double cumulativeDebitAmount = null;
        private Double cumulativeCreditAmount = null;

        public FinancialLedger(String transactionId) {
            this.transactionId = transactionId;
        }

        public void parse(Element financialLedgerElement) {
            this.account = Integer.parseInt(financialLedgerElement.getAttribute("FinancialLedgerID"));
            this.cumulativeAmount = Double.parseDouble(financialLedgerElement.getElementsByTagName("CumulativeAmount").item(0).getFirstChild().getNodeValue());
            this.cumulativeCreditAmount = Double.parseDouble(financialLedgerElement.getElementsByTagName("CumulativeCreditAmount").item(0).getFirstChild().getNodeValue());
            this.cumulativeDebitAmount = Double.parseDouble(financialLedgerElement.getElementsByTagName("CumulativeDebitAmount").item(0).getFirstChild().getNodeValue());
        }

        public String getTransactionId() {
            return transactionId;
        }

        public Integer getAccount() {
            return account;
        }

        public Double getCumulativeAmount() {
            return cumulativeAmount;
        }

        public Double getCumulativeDebitAmount() {
            return cumulativeDebitAmount;
        }

        public Double getCumulativeCreditAmount() {
            return cumulativeCreditAmount;
        }
    }
}
