package se.indiska.integration.jobs.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by heintz on 27/02/17.
 */
public class StaxLineItem implements StaxParser {
    private static final Logger logger = LoggerFactory.getLogger(StaxLineItem.class);
    private static Map<String, ValueItem> staxItemMap = new LinkedHashMap<>();

    static {
        staxItemMap.put("SequenceNumber", new ValueItem("SequenceNumber", "sequence_number", ValueItem.Datatype.Integer));
        staxItemMap.put("ItemID", new ValueItem("ItemID", "item_id", ValueItem.Datatype.String));
        staxItemMap.put("Description", new ValueItem("Description", "description", ValueItem.Datatype.String));
//        staxItemMap.put("Return", new StaxItem("Return", "is_return", StaxItem.Datatype.Boolean));
        staxItemMap.put("TaxIncludedInPriceFlag", new ValueItem("TaxIncludedInPriceFlag", "tax_included_in_price_flag", ValueItem.Datatype.String));
        staxItemMap.put("RegularSalesUnitPrice", new ValueItem("RegularSalesUnitPrice", "regular_sales_unit_price", ValueItem.Datatype.Decimal));
        staxItemMap.put("ActualSalesUnitPrice", new ValueItem("ActualSalesUnitPrice", "actual_sales_unit_price", ValueItem.Datatype.Decimal));
        staxItemMap.put("ExtendedAmount", new ValueItem("ExtendedAmount", "extended_amount", ValueItem.Datatype.Decimal));
        staxItemMap.put("Quantity", new ValueItem("Quantity", "quantity", ValueItem.Datatype.Decimal));
        staxItemMap.put("WebReturn", new ValueItem("WebReturn", "web_return", ValueItem.Datatype.Decimal));
        staxItemMap.put("Disposal", new ValueItem("Disposal", "disposal", ValueItem.Datatype.String));
        staxItemMap.put("Reason", new ValueItem("Reason", "reason", ValueItem.Datatype.String));
        staxItemMap.put("Amount", new ValueItem("Amount", "tax_amount", ValueItem.Datatype.Decimal));
        staxItemMap.put("Percent", new ValueItem("Percent", "tax_percent", ValueItem.Datatype.Decimal));
        staxItemMap.put("TaxGroupID", new ValueItem("TaxGroupID", "tax_group_id", ValueItem.Datatype.String));
        staxItemMap.put("TransactionID", new ValueItem("TransactionID", "transaction_id", ValueItem.Datatype.String));
        staxItemMap.put("CancelFlag", new ValueItem("CancelFlag", "cancel_flag", ValueItem.Datatype.Boolean));
        staxItemMap.put("ItemType", new ValueItem("ItemType", "item_type", ValueItem.Datatype.String));
        staxItemMap.put("NewPrice", new ValueItem("NewPrice", "new_price", ValueItem.Datatype.Decimal));
        staxItemMap.put("PreviousPrice", new ValueItem("PreviousPrice", "previous_price", ValueItem.Datatype.Decimal));
    }

    private Map<String, Object> generatedValues = new LinkedHashMap<>();
    private Map<String, Object> values = new HashMap<>();
    private ValueItem currentStaxItem = null;
    private boolean store = false;
    private String messageId;

    public StaxLineItem(String messageId) {
        this.messageId = messageId;
    }

    public void parse(XMLEventReader eventReader, String parentName, Object parentId, Iterator attributes, ValueCallback callback) throws XMLStreamException {

        while (attributes.hasNext()) {
            Attribute attribute = (Attribute) attributes.next();
            ValueItem staxItem = staxItemMap.get(attribute.getName().toString());
            if (staxItem != null) {
                values.put(staxItem.getName(), staxItem.value(attribute.getValue()));
            }
        }
        if (parentName != null && parentId != null) {
            values.put(parentName, parentId);
        }
        boolean creditCardFound = false;
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            switch (event.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                    StartElement startElement = event.asStartElement();
                    String qName = startElement.getName().getLocalPart();
                    if (qName.equals("Sale") || qName.equals("Return")) {
                        store = true;
                        generatedValues.put("is_return", qName.equals("Return"));
                    } else if (qName.equals("PrimaryAccountNumber")) {
                        creditCardFound = true;
                    }
                    currentStaxItem = staxItemMap.get(qName);

                    Iterator currentAttributes = startElement.getAttributes();
                    while (currentAttributes.hasNext()) {
                        Attribute attribute = (Attribute) currentAttributes.next();
                        ValueItem staxItem = staxItemMap.get(attribute.getName().toString());
                        if (staxItem != null) {
                            values.put(staxItem.getName(), staxItem.value(attribute.getValue()));
                        }
                    }
                    break;
                case XMLStreamConstants.CHARACTERS:
                    Characters characters = event.asCharacters();
                    if (currentStaxItem != null) {
                        values.put(currentStaxItem.getName(), currentStaxItem.value(characters.getData()));
                    } else if (creditCardFound && callback != null && characters != null) {
                        callback.foundValue("PrimaryAccountNumber", characters.getData());
                        creditCardFound = false;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    currentStaxItem = null;
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equals("LineItem")) {
                        return;
                    }
                    break;
            }
        }
    }

    @Override
    public void store(StatementFactory statementFactory) throws SQLException {
        if (store) {
            String sql = "insert into stage.pos_transaction_sales (";
//            Iterator<StaxItem> staxItemIterator = staxItemMap.values().iterator();
            String valueString = "";
            List<String> columns = staxItemMap.values().stream().filter(staxItem -> !(staxItem instanceof StaxChildItem)).map(staxItem -> staxItem.getColumn()).collect(Collectors.toList());
            columns.addAll(generatedValues.keySet());
            Iterator<String> columnsIterator = columns.iterator();
            while (columnsIterator.hasNext()) {
                sql += columnsIterator.next();
                valueString += "?";
                if (columnsIterator.hasNext()) {
                    sql += ", ";
                    valueString += ", ";
                }
            }
            sql += ", message_id) VALUES (" + valueString + ",?) on conflict do nothing";
            statementFactory.getStatement(sql);
            PreparedStatement ps = statementFactory.getStatement(sql);

            int position = 1;
            for (ValueItem staxItem : staxItemMap.values()) {
                if (staxItem.getDataType() == ValueItem.Datatype.Boolean) {
                    Boolean value = (Boolean) values.get(staxItem.getName());
                    value = value != null ? value : false;
                    ps.setBoolean(position++, value);
                } else {
                    ps.setObject(position++, values.get(staxItem.getName()));
                }
            }
            for (String key : generatedValues.keySet()) {
                ps.setObject(position++, generatedValues.get(key));
            }
            ps.setString(position, this.messageId);

            ps.addBatch();
        }
    }


    @Override
    public String toString() {
        return values.toString();
    }
}
