package se.indiska.integration.jobs.balancelist;

import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import se.indiska.integration.framework.job.IndioJob;
import se.indiska.integration.framework.job.JobCompleteCallback;
import se.indiska.integration.framework.job.ProcessTraceHandle;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FilenameFilter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by heintz on 28/03/17.
 */
public class BalanceListParser implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(BalanceListParser.class);


    public static void main(String[] argv) throws Exception {
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl("jdbc:postgresql://localhost/indiska2");
        ds.setMinimumIdle(10);
        ds.setMaximumPoolSize(50);
        ds.setUsername("indiska");
        ds.setPassword("indiska");

        String sql = "insert into balancelist (item, date, warehouse, quantity, account00qty, account20qty) values (?,?,?,?,?,?)";
        Connection conn = ds.getConnection();

        PreparedStatement ps = conn.prepareStatement(sql);

        File dir = new File("/Users/heintz/Projects/Indiska/ICC/testdata/oracle/epixarkiv/out/");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        File[] subdirs = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.matches("2017-03-2[567]");
            }
        });
        for (File subdir : subdirs) {
            File[] files = subdir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.startsWith("S017-03-2");
                }
            });
            for (File f : files) {
//                logger.debug(f.getName());
                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document doc = documentBuilder.parse(f);
                String creationDateTime = doc.getElementsByTagName("CreationDateTime").item(0).getFirstChild().getNodeValue();

                LocalDateTime time = LocalDateTime.parse(creationDateTime, formatter);

                NodeList partBalanceNodes = doc.getElementsByTagName("PartBalance");

                for (int i = 0; i < partBalanceNodes.getLength(); i++) {
                    Map<String, String> item = new HashMap<>();
                    Element element = (Element) partBalanceNodes.item(i);
                    Element partId = ((Element) element.getElementsByTagName("PartId").item(0));
                    String id = partId.getElementsByTagName("Id").item(0).getFirstChild().getNodeValue();
                    String division = partId.getElementsByTagName("Division").item(0).getFirstChild().getNodeValue();
                    Element storedBalance = ((Element) element.getElementsByTagName("StoredBalance").item(0));
                    Double qty = Double.parseDouble(storedBalance.getElementsByTagName("Quantity").item(0).getFirstChild().getNodeValue());
                    Element account00 = ((Element) storedBalance.getElementsByTagName("Account").item(0));
                    Element account20 = ((Element) storedBalance.getElementsByTagName("Account").item(1));
                    Double account00Qty = Double.parseDouble(account00.getElementsByTagName("Quantity").item(0).getFirstChild().getNodeValue());
                    Double account20Qty = Double.parseDouble(account20.getElementsByTagName("Quantity").item(0).getFirstChild().getNodeValue());
                    logger.debug(id + ";" + time.toString() + ";" + division + ";" + qty + ";" + account00Qty + ";" + account20Qty);
                    ps.setString(1, id);
                    ps.setTimestamp(2, new java.sql.Timestamp(java.util.Date.from(time.toInstant(ZoneOffset.UTC)).getTime()));
                    ps.setString(3, division);
                    ps.setInt(4, qty.intValue());
                    ps.setInt(5, account00Qty.intValue());
                    ps.setInt(6, account20Qty.intValue());
                    ps.addBatch();
                }
            }
        }
        ps.executeBatch();
        ps.close();
        conn.close();
    }

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {

    }
}
