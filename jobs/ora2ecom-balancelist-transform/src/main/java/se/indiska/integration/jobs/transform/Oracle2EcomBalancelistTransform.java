package se.indiska.integration.jobs.transform;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.util.InboundFileUtilities;

import java.io.*;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * datatype="ORACLE_BALANCELIST"
 * triggerType="EVENT"
 * triggerStep="INBOUND_TRANSFER"
 * resultStep="OUTBOUND_FILE_LOAD"
 */
@IdentifiableIndioJob(
        identifier = "BALANCELIST_ORACLE2ECOM_TRANSFORM",
        eventStep = "OUTBOUND_FILE_LOAD",
        description = "Transforms Oracle CSV BalanceLists to JSON for Hybris and adds to outbound queue",
        triggerDataType = "ECOM_BALANCELIST",
        jobType = IdentifiableIndioJob.JobType.EVENT_BASED,
        triggerEventStep = "INBOUND_TRANSFER",
        dataType = "ECOM_BALANCELIST"
)
public class Oracle2EcomBalancelistTransform implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(Oracle2EcomBalancelistTransform.class);

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        List<InboundFileUtilities.InboundFile> inboundFiles = new ArrayList<>();
        try (Connection connection = DatasourceProvider.getConnection()) {
            inboundFiles = InboundFileUtilities.getInboundFiles(connection, traceHandle.getId());
        }


        List<InboundFileUtilities.OutboundFile> outboundFiles = new ArrayList<>();

        for (InboundFileUtilities.InboundFile inboundFile : inboundFiles) {
            StringReader stringReader = new StringReader(inboundFile.getFileContent());
            String jsonContent = convertToJson(stringReader);
            String filename = inboundFile.getFileName();
            filename = filename.substring(0, filename.lastIndexOf(".")) + ".json";


            InboundFileUtilities.OutboundFile outboundFile = new InboundFileUtilities.OutboundFile(filename, jobExecutionData.getDataType(), jsonContent, inboundFile.getMessageId(), inboundFile.getMessageUuid());
            outboundFiles.add(outboundFile);
        }
        try (Connection connection = DatasourceProvider.getConnection()) {
            InboundFileUtilities.addOutboundFile(connection, traceHandle, outboundFiles.toArray(new InboundFileUtilities.OutboundFile[outboundFiles.size()]));
        }
        callback.jobComplete(new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.SUCCESS));
    }

    private String convertToJson(Reader r) throws IOException {
        BufferedReader reader = new BufferedReader(r);
        String line;
        List result = new ArrayList();
        boolean first = true;
        while ((line = reader.readLine()) != null) {
            String[] lineItems = line.trim().split(",");
            if (!first) {
                Map<String, Object> itemQty = new LinkedHashMap<>();
                itemQty.put("productCode", lineItems[0]);
                itemQty.put("available", Integer.parseInt(lineItems[1]));
                result.add(itemQty);
            }
            first = false;
        }

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(result);
    }

    public static void main(String[] argv) throws Exception {
        File file = new File("/Users/heintz/Downloads/b018-02-06-12.36.36.383-001.csv");
        FileReader fileReader = new FileReader(file);
        Oracle2EcomBalancelistTransform t = new Oracle2EcomBalancelistTransform();
        String filename = file.getName();
        filename = filename.substring(0, filename.lastIndexOf(".")) + ".json";

        File out = new File(file.getParent(), filename);
        FileWriter fw = new FileWriter(out);
        String jsonContent = t.convertToJson(fileReader);
        fw.write(jsonContent);
        fw.close();
        fileReader.close();
        System.out.println("Wrote file "+out.getAbsolutePath());
    }
}
