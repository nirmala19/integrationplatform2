package se.indiska.integration.jobs.viametrics;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;

import javax.sql.DataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


@IdentifiableIndioJob(identifier = "VIAMETRICS_ELASTICSEARCH_LOAD", triggerEventStep = "ODS_LOAD", triggerDataType = "VIAMETRICS", jobType = IdentifiableIndioJob.JobType.EVENT_BASED, dataType = "VIAMETRICS", eventStep = "INDEXING_LOAD", description = "Loads Viametrics visitor data into Elasticsearch")
public class ViametricsElasticsearchLoader implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(ViametricsElasticsearchLoader.class);
    private static Client esClient = null;
    private static DateTimeFormatter idFormatter = DateTimeFormatter.ofPattern("yyyyMMdd-HH");
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final String STORE = "store_id";
    private static final String VISITS = "visits";
    private static final String HOUR = "event_time";
    private static final String ENTRY = "entry_id";

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("Running VIAMETRICS_ELASTICSEARCH_LOAD");
        }
        String sql = "SELECT vm.store_id, vm.entry_id, vm.event_time, vm.period, vm.direction, vm.visit_count, vm.message_id FROM process_status ps\n" +
                "  JOIN process_data_trace pdt\n" +
                "  ON pdt.process_id=ps.process_initiator_id\n" +
                "  JOIN ods.visitor_metrics vm\n" +
                "  ON vm.message_id=pdt.message_id\n" +
                "WHERE ps.id=?";
        boolean loadedFiles = false;
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1, traceHandle.getId());
            ResultSet rset = ps.executeQuery();
            List<Map<String, Object>> metricsToIndex = new ArrayList<>();
            long count = 0;
            while (rset.next()) {
                count++;
                Map<String, Object> row = getRow(rset);
                metricsToIndex.add(row);
                if (metricsToIndex.size() >= 5000) {
                    indexData(metricsToIndex);
                    logger.debug("Done loading " + metricsToIndex.size() + " into elasticsearch (total " + count + ")");
                    metricsToIndex.clear();
                }
                loadedFiles = true;
            }
            rset.close();
            ps.close();
            indexData(metricsToIndex);
        }
        callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), loadedFiles ? ProcessStatus.SUCCESS : ProcessStatus.NOOP));
        if (logger.isTraceEnabled()) {
            logger.trace("DONE with VIAMETRICS_ELASTICSEARCH_LOAD " + loadedFiles);
        }
    }

    private long indexData(List<Map<String, Object>> metrics) throws ExecutionException, InterruptedException {
        long indexedItems = 0;
        if (!metrics.isEmpty()) {
            BulkRequestBuilder bulkRequestBuilder = getClient().prepareBulk();
            for (Map<String, Object> metric : metrics) {
                indexedItems++;
//                DeleteRequestBuilder deleteRequestBuilder = getClient().prepareDelete("indiska", "visitormetrics", metric.get(STORE) + ":" + metric.get(ENTRY) + ((LocalDateTime) metric.get(HOUR)).format(formatter));
                IndexRequestBuilder indexRequestBuilder = getClient().prepareIndex("indiska", "visitormetrics");
                indexRequestBuilder.setSource(metric);
                indexRequestBuilder.setId(metric.get(STORE) + ":" + metric.get(ENTRY) + metric.get(HOUR));
                bulkRequestBuilder.add(indexRequestBuilder);
//                bulkRequestBuilder.add(deleteRequestBuilder);
            }
            BulkResponse bulkResponse = bulkRequestBuilder.execute().get();
        }
        return indexedItems;

    }


    private Client getClient() {
        if (esClient == null) {
            try {
//                InetAddress elasticHostAddress = InetAddress.getByName("localhost");
                InetAddress elasticHostAddress = InetAddress.getByName("indio.hk.indiska.se");
                esClient = new PreBuiltTransportClient(Settings.builder()

//                        .put("cluster.name", IndioProperties.properties().getProperty("elasticsearch.cluster.name", "indio"))
                        .put("cluster.name", "indio")
                        .build())
                        .addTransportAddress(new InetSocketTransportAddress(elasticHostAddress, 9300));
            } catch (UnknownHostException e) {
                logger.error("Unable to connect to elasticsearch on localhost:9300");
            }
        }
        return esClient;
    }

    public static void main(String[] argv) throws Exception {
        ViametricsElasticsearchLoader esLoader = new ViametricsElasticsearchLoader();
        String sql = "SELECT * FROM ods.visitor_metrics";
        List<Map<String, Object>> metricsToIndex = new ArrayList<>();
        long count = 0;

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://indio.hk.indiska.se/indio");
        config.setUsername("indiska");
        config.setPassword("IndIO42!");
        config.setMinimumIdle(2);
        config.setMaximumPoolSize(10);

        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        config.addDataSourceProperty("leakDetectionThreshold", "20");

        DataSource dataSource = new HikariDataSource(config);

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                count++;
                Map<String, Object> visitMetric = getRow(rset);
                metricsToIndex.add(visitMetric);
                if (metricsToIndex.size() >= 5000) {
                    esLoader.indexData(metricsToIndex);
                    logger.debug("Done loading " + metricsToIndex.size() + " into elasticsearch (total " + count + ")");
                    metricsToIndex.clear();
                }
            }
            rset.close();
            ps.close();
        }
        logger.debug("Got " + count + " visit metrics");
    }



    public static Map<String, Object> getRow(ResultSet rset) throws SQLException {
        Map<String, Object> visitMetric = new HashMap<>();
        visitMetric.put(STORE, rset.getInt("store_id"));
        visitMetric.put(ENTRY, rset.getInt("entry_id"));
        visitMetric.put(VISITS, rset.getLong("visit_count"));
        visitMetric.put(HOUR, rset.getTimestamp("event_time").toLocalDateTime().format(formatter));
        return visitMetric;
    }

}
