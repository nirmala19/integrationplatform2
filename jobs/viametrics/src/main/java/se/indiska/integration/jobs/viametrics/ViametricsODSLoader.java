package se.indiska.integration.jobs.viametrics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.trace.DataTrace;
import se.indiska.integration.framework.util.InboundFileUtilities;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.Reader;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

@IdentifiableIndioJob(identifier = "VIAMETRICS_ODS_LOAD", triggerEventStep = "INBOUND_TRANSFER", triggerDataType = "VIAMETRICS", jobType = IdentifiableIndioJob.JobType.EVENT_BASED, dataType = "VIAMETRICS", eventStep = "ODS_LOAD", description = "Loads Viametrics visitor data into stage table")
public class ViametricsODSLoader implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(ViametricsODSLoader.class);
    private static XMLInputFactory factory = XMLInputFactory.newInstance();
    private static DocumentBuilder documentBuilder = null;
    private static DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("Running VIAMETRICS_ODS_LOAD");
        }
        String sql = "SELECT * FROM public.inbound_file_staging ifs, " +
                "public.process_data_trace pdt, " +
                "public.process_status ps\n" +
                "WHERE ps.process_initiator_id=pdt.process_id\n" +
                "AND ifs.id=cast(pdt.target_id AS BIGINT)\n" +
                "AND ps.id=?";
        boolean loadedFiles = false;

        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1, traceHandle.getId());

            ResultSet rset = ps.executeQuery();
            List<Map<String, Object>> visitorData = new ArrayList<>();
            while (rset.next()) {
                Long sourceId = rset.getLong("id");
                String messageId = rset.getString("message_id");
                try (Reader fileReader = InboundFileUtilities.getFileContentReader(rset.getString("file_content"))) {
                    visitorData.addAll(parseFile(fileReader, messageId));
                    if (visitorData.size() > 1000) {
                        if (loadData(visitorData, connection)) {
                            visitorData.clear();
                        }
                    }
                    loadedFiles = true;
                    traceHandle.getProcessTracer().trace(new DataTrace(messageId, traceHandle.getId(), "inbound_file_staging", sourceId == null ? null : sourceId.toString(), "visitor_metrics", messageId, ProcessStatus.SUCCESS));
                } catch (XMLStreamException e) {
                    String error = "Unable to parse file content of file " + rset.getString("filename");
                    logger.error(error, e);
                    traceHandle.getProcessTracer().trace(new DataTrace(messageId, traceHandle.getId(), "inbound_file_staging", sourceId + "", "pos_transaction", null, ProcessStatus.ERROR, error, e, ProcessError.INVALID_FILE_CONTENT));
                }
            }
            loadData(visitorData, connection);
        }
        callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), loadedFiles ? ProcessStatus.SUCCESS : ProcessStatus.NOOP));
    }

    private List<Map<String, Object>> parseFile(Reader reader, String messageId) throws XMLStreamException, ParseException {
        XMLEventReader eventReader = factory.createXMLEventReader(reader);
        List<Map<String, Object>> result = new ArrayList<>();
        Map<String, Object> currentVisit = new HashMap<>();
        String currentTerm = null;
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            switch (event.getEventType()) {
                case XMLStreamConstants.START_ELEMENT:
                    StartElement startElement = event.asStartElement();
                    String qName = startElement.getName().getLocalPart();
                    currentTerm = qName;
                    break;
                case XMLStreamConstants.CHARACTERS:
                    String data = event.asCharacters().getData();
                    if (currentTerm != null && data.trim().length() > 0) {
                        switch (currentTerm) {
                            case "FacilityID":
                                currentVisit.put("store_id", Integer.parseInt(data));
                                break;
                            case "EntryID":
                                currentVisit.put("entry_id", Integer.parseInt(data));
                                break;
                            case "Datetime":
                                currentVisit.put("event_time", dateFormatter.parse(data));
                                break;
                            case "Period":
                                currentVisit.put("period", Long.parseLong(data));
                                break;
                            case "Direction":
                                currentVisit.put("direction", data.trim().equals("O") ? "OUTBOUND" : "INBOUND");
                                break;
                            case "Count": {
                                currentVisit.put("visit_count", new Double(data).intValue());
                            }
                        }
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    EndElement endElement = event.asEndElement();
                    String tagName = endElement.getName().getLocalPart();
                    if (tagName.equals("Data")) {
                        if (((Integer) currentVisit.get("visit_count")) > 0) {
                            currentVisit.put("message_id", messageId);
                            result.add(currentVisit);
                        }
                        currentVisit = new HashMap<>();
                    }
                    break;
            }
        }
        return result;
    }

    private boolean loadData(List<Map<String, Object>> data, Connection connection) {
        String sql = "INSERT INTO ods.visitor_metrics (store_id, entry_id, event_time, period, direction, visit_count, message_id) VALUES (?,?,?,?,?,?,?) ON CONFLICT DO NOTHING ";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            for (Map<String, Object> row : data) {
                ps.setInt(1, (Integer) row.get("store_id"));
                ps.setInt(2, (Integer) row.get("entry_id"));
                Date eventTime = (Date) row.get("event_time");
                ps.setTimestamp(3, new Timestamp(eventTime.getTime()));
                ps.setLong(4, (Long) row.get("period"));
                ps.setString(5, (String) row.get("direction"));
                ps.setInt(6, (Integer) row.get("visit_count"));
                ps.setString(7, (String) row.get("message_id"));
                ps.addBatch();
            }
            ps.executeBatch();
            ps.close();
            return true;
        } catch (SQLException e) {
            logger.error("Unable to store visitor data", e);
            return false;
        }
    }

    public static void main(String[] argv) throws Exception {
        long start = System.nanoTime();
        ViametricsODSLoader loader = new ViametricsODSLoader();
        File dir = new File("/Users/heintz/Projects/Indiska/ICC/testdata/viametrics");
        int count = 0;
        File[] files = dir.listFiles(
                new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        return pathname.getName().startsWith("Viametrics") && pathname.getName().endsWith(".xml") && pathname.length() > 20 && pathname.isFile()
                                && pathname.lastModified() < (System.currentTimeMillis() - 3600);
                    }
                });
        logger.debug("File list complete, loading into database");
        int i = 0;
        File crapDir = new File(dir, "crap");
        List<Map<String, Object>> batchData = new ArrayList<>();

        try (Connection connection = DatasourceProvider.getConnection()) {
            for (File f : files) {
                if (f.isFile() && !f.getName().startsWith("Viametrics") && f.length() < 20) {
                    continue;
                }
                try {
                    FileReader fr = new FileReader(f);
                    List<Map<String, Object>> data = loader.parseFile(fr, "message-id-123-345-456-567");
                    batchData.addAll(data);
                    count += data.size();
                    if (batchData.size() > 100000) {
                        logger.debug(i + " loaded, total " + count + " entries");
                        loader.loadData(batchData, connection);
                        batchData.clear();
                    }
                    i++;
                    f.delete();
                } catch (XMLStreamException e) {
                    File crapFile = new File(crapDir, f.getName());
                    f.renameTo(crapFile);
//                logger.error("Unable to parse file " + f.getName(), e);
                }
            }
            loader.loadData(batchData, connection);
            batchData.clear();
        }
        logger.debug("Done reading " + count + " visit items in " + ((System.nanoTime() - start) / 1e6d) + "ms");
    }
}
