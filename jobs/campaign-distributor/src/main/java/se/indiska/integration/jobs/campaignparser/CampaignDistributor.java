package se.indiska.integration.jobs.campaignparser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.extenda.StoreConnectUtilities;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.util.Counter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class CampaignDistributor {
    private static final Logger logger = LoggerFactory.getLogger(CampaignDistributor.class);
    private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmm");
    private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    private void writeCampaigns(Integer storeId, FileWriteback out) throws SQLException {
        String sql = "SELECT\n" +
                "  co.modifier,\n" +
                "  co.campaign_id,\n" +
                "  co.from_date,\n" +
                "  co.to_date,\n" +
                "  co.description,\n" +
                "  co.reason_code\n" +
                "FROM stage.campaign_outbound co\n" +
                "WHERE co.store_id = ?";
        String itemSql = "SELECT * FROM stage.campaign_item_outbound WHERE store_id=? AND campaign_id=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement campaignPs = connection.prepareStatement(sql);
            campaignPs.setInt(1, storeId);
            ResultSet campaignRset = campaignPs.executeQuery();
            while (campaignRset.next()) {
                StringBuilder sb = new StringBuilder();
                Long campaignId = campaignRset.getLong("campaign_id");
                // Campaign number
                sb.append(String.format("%09d", campaignId));
                //Description 2
                String text1 = campaignRset.getString("description");
                sb.append(String.format("%1$-20s", text1 == null ? "" : text1));
                // Description 1
                String text2 = campaignRset.getString("reason_code");
                sb.append(String.format("%1$-20s", text2 == null ? "" : text2));
                // campaign type, 0=new price, 1 = % off
                sb.append("0000");
                // start date
                sb.append(dateTimeFormatter.format(campaignRset.getTimestamp("from_date").toLocalDateTime()));
                // end date
                sb.append(dateTimeFormatter.format(campaignRset.getTimestamp("to_date").toLocalDateTime()));
                // Customer type
                sb.append("0000");
                // Campaign for type:
                // 0 = Sales department
                // 1 = Product group
                // 3 = Article group
                // 4 = Commodity
                // 5 = Article
                sb.append("0005");
                // Discount type, 0=fixed price (new price)
                sb.append("0000");
                // Discount percent
                sb.append("000000000");
                // CONTRIBVALUE
                sb.append("000000000");
                // EXPENCEVALUE
                sb.append("000000000");
                // Quantity limit
                sb.append("0000");
                // Change date
                sb.append(dateTimeFormatter.format(LocalDateTime.now()));
                // Changed by
                sb.append("00000000");
                // TRANSDATE, Creation date
                sb.append(dateTimeFormatter.format(LocalDateTime.now()));
                String campaignModifier = campaignRset.getString("modifier");
                out.writeLine("050", campaignModifier, sb.toString());

                PreparedStatement itemPs = connection.prepareStatement(itemSql);
                itemPs.setInt(1, storeId);
                itemPs.setLong(2, campaignId);
                ResultSet itemRset = itemPs.executeQuery();
                while (itemRset.next()) {
                    Long item = itemRset.getLong("item");
                    String change = itemRset.getString("modifier");
                    Double price = itemRset.getDouble("final_price");
                    StringBuilder line = new StringBuilder();
                    line.append(String.format("%09d", campaignId));
                    line.append(String.format("%013d", item));
                    line.append(String.format("%08d", Math.round(price * 100)));
                    // Campaign discount
                    line.append("000000");
                    // Campaign cost price
                    line.append("00000000");
                    // Text
                    line.append(String.format("%1$-10s", text1 == null ? "" : text2.length() > 10 ? text2.substring(0, 10) : text2));
                    //Quantity
                    line.append("000000000");
                    // Deleted (filler?)
                    line.append("0000");
                    out.writeLine("051", change, line.toString());


                }
                if (campaignModifier.equals("N")) {
                    out.writeLine("053", campaignModifier, String.format("%06d", storeId) + "00000000000" + "0000");
                }
            }
        }
    }

    interface FileWriteback {
        void writeLine(String transactionType, String modifier, String string);
    }

    public static void main(String[] argv) throws Exception {
        long start = System.nanoTime();
        CampaignDistributor campaignDistributor = new CampaignDistributor();
        List<Integer> storeIds = StoreConnectUtilities.getStoreIds();

        for (Integer store : storeIds) {
//            int store = 18;
            long s = System.nanoTime();
            Counter counter = new Counter(1);
            FileWriter fileWriter = new FileWriter("/tmp/campaign_store" + store + ".txt");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write("0000000000" + String.format("%06d", counter.get()) + dateFormatter.format(LocalDate.now()) + " VERSION 2.1  #4\r\n");
            counter.add();

            campaignDistributor.writeCampaigns(store, new FileWriteback() {
                @Override
                public void writeLine(String transactionType, String modifier, String string) {
                    try {
                        bufferedWriter.write(transactionType);
                        bufferedWriter.write(modifier);
                        bufferedWriter.write(String.format("%06d", store));
                        bufferedWriter.write(String.format("%06d", counter.get()));
                        bufferedWriter.write(string);
                        bufferedWriter.write("\r\n");
                        counter.add();
                    } catch (IOException e) {
                        logger.error("Unable to write to file!", e);
                    }
                }
            });
            bufferedWriter.write("9990000000");
            bufferedWriter.write(String.format("%06d", counter.get()));
            bufferedWriter.write("\r\n");
            bufferedWriter.close();
            fileWriter.close();
            logger.debug("Time to write file for store " + store + ": " + (System.nanoTime() - s) / 1e6d + "ms");
        }
        logger.debug("DONE, total processing time was " + (System.nanoTime() - start) / 1e6d + "ms");
    }
}
