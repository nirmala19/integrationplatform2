package se.indiska.integration.jobs.campaignparser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DatabaseProcessingException extends Exception {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseProcessingException.class);

    public DatabaseProcessingException(String message) {
        super(message);
    }

    public DatabaseProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
