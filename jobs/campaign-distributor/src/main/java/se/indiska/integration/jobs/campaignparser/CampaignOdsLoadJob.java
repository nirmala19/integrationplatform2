package se.indiska.integration.jobs.campaignparser;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.extenda.StoreConnectUtilities;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Read promotion/clearance CSV exports from Oracle and loads into Stage/ODS
 */
@IdentifiableIndioJob(identifier = "CAMPAIGN_ODS_UPDATE_JOB", triggerEventStep = "INBOUND_TRANSFER", dataType = "CAMPAIGN_BATCH", eventStep = "ODS_LOAD", description = "Parses, creates and distributes Campaign files to stores and Hybris", jobType = IdentifiableIndioJob.JobType.EVENT_BASED, triggerDataType = "CAMPAIGN_BATCH")
public class CampaignOdsLoadJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(CampaignOdsLoadJob.class);

    private static Map<String, StringValueParser> stringValueParserMap = new HashMap<>();
    private Map<Long, Long> promoIdMap = new HashMap<>();

    static {
        stringValueParserMap.put("Promo ID", new StringValueParser<Long>() {
            @Override
            public Long parse(String stringValue) {
                return stringValue.trim().length() > 0 ? new Long(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Promo Display ID", new StringValueParser<Long>() {
            @Override
            public Long parse(String stringValue) {
                return stringValue.trim().length() > 0 ? new Long(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Promo Comp ID", new StringValueParser<Long>() {
            @Override
            public Long parse(String stringValue) {
                return stringValue.trim().length() > 0 ? new Long(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Promo Detail ID", new StringValueParser<Integer>() {
            @Override
            public Integer parse(String stringValue) {
                return stringValue.trim().length() > 0 ? new Integer(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Zone", new StringValueParser<Integer>() {
            @Override
            public Integer parse(String stringValue) {
                return stringValue.trim().length() > 0 ? new Integer(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Start Date", new StringValueParser<LocalDate>() {
            @Override
            public LocalDate parse(String stringValue) {
                return stringValue.trim().length() > 0 ? LocalDate.parse(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("End Date", new StringValueParser<LocalDate>() {
            @Override
            public LocalDate parse(String stringValue) {
                return stringValue.trim().length() > 0 ? LocalDate.parse(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Change Type", new StringValueParser<Integer>() {
            @Override
            public Integer parse(String stringValue) {
                return stringValue.trim().length() > 0 ? new Integer(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Change Amount", new StringValueParser<Double>() {
            @Override
            public Double parse(String stringValue) {
                return stringValue.trim().length() > 0 ? new Double(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Change Percent", new StringValueParser<Double>() {
            @Override
            public Double parse(String stringValue) {
                return stringValue.trim().length() > 0 ? new Double(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Final Price Calculated", new StringValueParser<Double>() {
            @Override
            public Double parse(String stringValue) {
                return stringValue.trim().length() > 0 ? new Double(stringValue.trim()) : null;
            }
        });
        stringValueParserMap.put("Promotion Description", new StringValueParser<String>() {
            @Override
            public String parse(String stringValue) {
                return stringValue.trim().length() > 0 ? stringValue.trim() : null;
            }
        });
        stringValueParserMap.put("Promotion Reason Code", (StringValueParser<String>) stringValue -> stringValue.trim().length() > 0 ? stringValue.trim() : null);
    }

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        long start = System.nanoTime();
        callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.SUCCESS));
        logger.debug("Clearance job took " + (System.nanoTime() - start) / 1e6d + "ms");
    }

    private int loadData(Reader fileReader, String messageId, LocalDateTime now) throws IOException {
        long start = System.nanoTime();
        Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(fileReader);
        logger.debug("Time to parse file: " + (System.nanoTime() - start) / 1e6d + "ms");
        int count = 0;
        String truncateSql = "TRUNCATE TABLE stage.campaigns_proposed";
        String sql = "INSERT INTO stage.campaigns_proposed (campaign_id, zone_id, from_date, to_date, product_id, " +
                "color, item, change_type, change_amount, event_time, message_id, change_percent, final_price," +
                "promo_description, promo_reason_code) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON CONFLICT (campaign_id, zone_id, item) DO UPDATE SET event_time=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(truncateSql);
            ps.executeUpdate();
            ps.close();
            ps = connection.prepareStatement(sql);

            for (CSVRecord record : records) {
                StringValueParser<String> dummyParser = new StringValueParser<String>() {
                    @Override
                    public String parse(String stringValue) {
                        return stringValue.trim();
                    }
                };

                if (record.get("Detail Display ID").startsWith("reset:") || record.get("Zone").trim().length() == 0) {
                    continue;
                }

                Map<String, Object> values = new HashMap<>();

                for (String header : record.toMap().keySet()) {
                    StringValueParser svp = stringValueParserMap.getOrDefault(header.trim(), dummyParser);
                    values.put(header.trim(), svp.parse(record.get(header)));
                }

                Double changeAmount = (Double) values.get("Change Amount");
                Double changePercent = (Double) values.get("Change Percent");
                Double finalPrice = (Double) values.get("Final Price Calculated");

                LocalDate startDate = (LocalDate) values.get("Start Date");
                LocalDate endDate = (LocalDate) values.get("End Date");
                if (endDate == null) {
                    endDate = LocalDate.parse("2099-12-31");
                }


                Long promoId = (Long) values.get("Promo Display ID");
                if (promoId == null) {
                    promoId = Long.parseLong(startDate.format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                }

                promoId = getExtendaCampaignId(promoId, changePercent, startDate, endDate, connection);

                ps.setLong(1, promoId);
                ps.setInt(2, (Integer) values.get("Zone"));
                ps.setTimestamp(3, Timestamp.valueOf(startDate.atStartOfDay()));

                if (finalPrice == null) {
                    logger.error("Final price for " + values.get("Item") + " is NULL!!");
                }

                ps.setTimestamp(4, Timestamp.valueOf(endDate.atStartOfDay()));
                ps.setString(5, (String) values.get("Parent Item"));
                ps.setString(6, (String) values.get("Color"));
                ps.setLong(7, new Long((String) values.get("Item")));
                ps.setInt(8, (Integer) values.get("Change Type"));
                if (changeAmount != null) {
                    ps.setDouble(9, changeAmount);
                } else {
                    ps.setNull(9, Types.NUMERIC);
                }
                ps.setTimestamp(10, Timestamp.valueOf(now));
                ps.setString(11, messageId);
                if (changePercent != null) {
                    ps.setDouble(12, changePercent);
                } else {
                    ps.setNull(12, Types.NUMERIC);
                }
                ps.setDouble(13, finalPrice);
                ps.setString(14, (String) values.get("Promotion Description"));
                ps.setString(15, (String) values.get("Promotion Reason Code"));
                ps.setTimestamp(16, Timestamp.valueOf(now));
                ps.addBatch();
                count++;
                if (count % 10000 == 0) {
                    ps.executeBatch();
                }
            }
            ps.executeBatch();
            ps.close();

//            ps = connection.prepareStatement("select * from stage.store_currentcampaign where ")
        } catch (SQLException e) {
            logger.error("Unable to insert data!!!");
        }
        long tta = System.nanoTime() - start;
        System.out.println("Total number of campaigns: " + count + " in " + tta / 1e6d + "ms " + tta / count / 1e6d + " ms/item");
        return 0;
    }

    private Long getExtendaCampaignId(Long promoId, Double changePercent, LocalDate startDate, LocalDate endDate, Connection connection) throws SQLException {
        Long campaignId = promoIdMap.get(promoId);
        if (campaignId == null) {
            String sql = "INSERT INTO stage.campaign_extenda_id_map (campaign_id, percent_off, from_date, to_date) VALUES (?,?,?,?) ON CONFLICT  DO NOTHING ";
            changePercent = changePercent == null ? 0 : changePercent;
            PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setLong(1, promoId);
            if (changePercent != null) {
                ps.setDouble(2, changePercent);
            } else {
                ps.setNull(2, Types.NUMERIC);
            }
            ps.setTimestamp(3, Timestamp.valueOf(startDate.atStartOfDay()));
            ps.setTimestamp(4, Timestamp.valueOf(endDate.atStartOfDay()));
            if (ps.executeUpdate() > 0) {
                ResultSet rset = ps.getGeneratedKeys();
                if (rset.next()) {
                    campaignId = rset.getLong("id");
                    promoIdMap.put(promoId, campaignId);
                    return campaignId;
                }

            }
            ps = connection.prepareStatement("SELECT id FROM stage.campaign_extenda_id_map WHERE campaign_id=? AND percent_off=? AND from_date=? AND to_date=?");
            ps.setLong(1, promoId);
            if (changePercent != null) {
                ps.setDouble(2, changePercent);
            } else {
                ps.setNull(2, Types.NUMERIC);
            }
            ps.setTimestamp(3, Timestamp.valueOf(startDate.atStartOfDay()));
            ps.setTimestamp(4, Timestamp.valueOf(endDate.atStartOfDay()));
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                campaignId = rset.getLong("id");
                promoIdMap.put(promoId, campaignId);
                return campaignId;
            }
        }
        return campaignId;
    }

    public Map<Long, Campaign> readNewCampaigns(Integer store) throws DatabaseProcessingException {
        Map<Long, Campaign> result = new HashMap<>();
        String sql = "SELECT * FROM stage.campaigns_proposed cp JOIN ods.zone_store_map zsm ON zsm.zone_id=cp.zone_id WHERE zsm.store_id=? ORDER BY campaign_id";
        String currentCampaignsSql = "SELECT * FROM stage.store_currentcampaign cc WHERE cc.store=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            boolean finished = false;
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, store);
            ResultSet rset = ps.executeQuery();
            int count = 0;
            while (rset.next()) {
                LocalDate fromDate = rset.getDate("from_date").toLocalDate();
                LocalDate toDate = rset.getDate("to_date").toLocalDate();
                Long campaignId = rset.getLong("campaign_id");
                Long item = rset.getLong("item");
                Double finalPrice = rset.getDouble("final_price");
                Campaign campaign = result.computeIfAbsent(campaignId, k -> new Campaign(campaignId, fromDate, toDate));
                campaign.setDescription(rset.getString("promo_description"));
                campaign.setReasonCode(rset.getString("promo_reason_code"));
                campaign.getItems().put(item, new CampaignItem(item, finalPrice, fromDate, toDate));
                count++;
            }
            rset.close();
            ps.close();
            ps = connection.prepareStatement(currentCampaignsSql);
            ps.setInt(1, store);
            rset = ps.executeQuery();
            while (rset.next()) {
                Long campaignId = rset.getLong("campaign");
                Campaign c = result.get(campaignId);
                LocalDate fromDate = rset.getDate("fromdate").toLocalDate();
                LocalDate toDate = rset.getDate("todate").toLocalDate();
                if (c == null) {
                    c = new Campaign(campaignId, fromDate, toDate);
                    c.setModifier(Campaign.Modifier.DELETE);
                    result.put(campaignId, c);
                }
                Long itemId = rset.getLong("item");
                Double price = rset.getDouble("campaign_price");
                CampaignItem existingCampaignItem = new CampaignItem(itemId, price, fromDate, toDate);

                CampaignItem newCampaignItem = c.getItems().get(itemId);
                if (newCampaignItem == null) {
                    existingCampaignItem.setModifier(Campaign.Modifier.DELETE);
                    c.getItems().put(itemId, existingCampaignItem);
                } else if (!newCampaignItem.equals(existingCampaignItem)) {
                    newCampaignItem.setModifier(Campaign.Modifier.CHANGE);
                } else {
                    newCampaignItem.setModifier(Campaign.Modifier.REMAIN);
                }
            }

        } catch (SQLException e) {
            throw new DatabaseProcessingException("Unable to read new campaigns from database!", e);
        }
        return result;
    }

    private void truncateOutbound() throws SQLException {
        String cSql = "TRUNCATE TABLE stage.campaign_outbound";
        String ciSql = "TRUNCATE TABLE stage.campaign_item_outbound";
        try (Connection connection = DatasourceProvider.getConnection()) {
            connection.prepareStatement(cSql).executeUpdate();
            connection.prepareStatement(ciSql).executeUpdate();
        }
    }

    private void loadCampaignData(Integer storeId, Map<Long, Campaign> outbound) throws SQLException {
        String campaignInsertSql = "INSERT INTO stage.campaign_outbound (campaign_id, store_id, modifier, event_time, from_date, to_date, description, reason_code) VALUES (?,?,?,?,?,?,?,?)";
        String campaignItemInsertSql = "INSERT INTO stage.campaign_item_outbound (campaign_id, store_id, item, final_price, modifier) VALUES (?,?,?,?,?)";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement campaignPs = connection.prepareStatement(campaignInsertSql);
            PreparedStatement campaignItemPs = connection.prepareStatement(campaignItemInsertSql);

            LocalDateTime now = LocalDateTime.now();

            int cs = 0;
            int cis = 0;

            for (Campaign campaign : outbound.values()) {
                if (campaign.getModifier() == Campaign.Modifier.REMAIN) {
                    continue;
                }
                campaignPs.setLong(1, campaign.getId());
                campaignPs.setInt(2, storeId);
                campaignPs.setString(3, campaign.getModifier().getValue());
                campaignPs.setTimestamp(4, Timestamp.valueOf(now));
                campaignPs.setTimestamp(5, Timestamp.valueOf(campaign.getFromDate().atStartOfDay()));
                campaignPs.setTimestamp(6, Timestamp.valueOf(campaign.getToDate().atStartOfDay()));
                campaignPs.setString(7, campaign.getDescription());
                campaignPs.setString(8, campaign.getReasonCode());
                campaignPs.addBatch();
                cs++;
                if (cs % 2000 == 0) {
                    campaignPs.executeBatch();
                }

                for (CampaignItem campaignItem : campaign.getItems().values()) {
                    if (campaignItem.getModifier() == Campaign.Modifier.REMAIN || campaign.getModifier() == Campaign.Modifier.DELETE) {
                        continue;
                    }
                    campaignItemPs.setLong(1, campaign.getId());
                    campaignItemPs.setInt(2, storeId);
                    campaignItemPs.setLong(3, campaignItem.getId());
                    campaignItemPs.setDouble(4, campaignItem.getFinalPrice());
                    campaignItemPs.setString(5, campaignItem.getModifier().getValue());
                    campaignItemPs.addBatch();
                    cis++;
                    if (cis % 2000 == 0) {
                        campaignItemPs.executeBatch();
                    }
                }
            }
            campaignPs.executeBatch();
            campaignItemPs.executeBatch();
        }
    }


    public static void main(String[] argv) throws Exception {
        String path = "/Users/heintz/Downloads/simpromo/spc17-12-05-12.26.45.650.csv";

        long s = System.nanoTime(), start = System.nanoTime();
        Reader in = new FileReader(path);
        CampaignOdsLoadJob clearanceFixedParser = new CampaignOdsLoadJob();
        clearanceFixedParser.truncateOutbound();
        clearanceFixedParser.loadData(in, "message-test-main", LocalDateTime.now());
        logger.debug("Time to load data: " + (System.nanoTime() - s) / 1e6d + "ms");
        s = System.nanoTime();
        List<Integer> stores = StoreConnectUtilities.getStoreIds();
        for (Integer store : stores) {
//        Integer store = 18;
            s = System.nanoTime();
            Map<Long, Campaign> campaignMap = clearanceFixedParser.readNewCampaigns(store);
            clearanceFixedParser.loadCampaignData(store, campaignMap);
            logger.debug("Time to write campaigns for store " + store + ": " + (System.nanoTime() - s) / 1e6d + "ms: " + campaignMap.size());
        }


        logger.debug("Time to read new campaigns: " + (System.nanoTime() - s) / 1e6d + "ms");

        CampaignDistributor.main(null);
        logger.debug("Total Processing time: " + (System.nanoTime() - start) / 1e6d + "ms");
        System.exit(0);
    }


    interface StringValueParser<T> {
        T parse(String stringValue);
    }

    static class Campaign {
        enum Modifier {
            NEW("N"), CHANGE("C"), DELETE("D"), REMAIN("-");

            String value;

            Modifier(String value) {
                this.value = value;
            }

            public String getValue() {
                return value;
            }


        }

        private String description = null;
        private String reasonCode = null;
        private Long id;
        private Modifier modifier = null;
        private Map<Long, CampaignItem> items = new HashMap<>();
        private LocalDate fromDate = null;
        private LocalDate toDate = null;

        public Campaign(Long id, LocalDate fromDate, LocalDate toDate) {
            this.id = id;
            this.fromDate = fromDate;
            this.toDate = toDate;
        }

        public Long getId() {
            return id;
        }

        public Modifier getModifier() {
            if (this.modifier != null) {
                return this.modifier;
            }
            if (getItems().values().stream().anyMatch(ci -> ci.getModifier() != null && ci.getModifier() != Modifier.NEW)) {
                return Modifier.CHANGE;
            }
            return Modifier.NEW;
        }

        public void setModifier(Modifier modifier) {
            this.modifier = modifier;
        }

        public Map<Long, CampaignItem> getItems() {
            return items;
        }

        public LocalDate getFromDate() {
            return fromDate;
        }

        public LocalDate getToDate() {
            return toDate;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public String getReasonCode() {
            return reasonCode;
        }

        public void setReasonCode(String reasonCode) {
            this.reasonCode = reasonCode;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Campaign campaign = (Campaign) o;

            return id.equals(campaign.id);
        }

        @Override
        public int hashCode() {
            return id.hashCode();
        }
    }

    static class CampaignItem {
        private Long id = null;
        private Double finalPrice = null;
        private LocalDate fromDate = null;
        private LocalDate toDate = null;
        private Campaign.Modifier modifier = null;

        public CampaignItem(Long id, Double finalPrice, LocalDate fromDate, LocalDate toDate) {
            this.id = id;
            this.finalPrice = finalPrice;
            this.fromDate = fromDate;
            this.toDate = toDate;
        }

        public Long getId() {
            return id;
        }

        public Double getFinalPrice() {
            return finalPrice;
        }

        public LocalDate getFromDate() {
            return fromDate;
        }

        public LocalDate getToDate() {
            return toDate;
        }

        public Campaign.Modifier getModifier() {
            if (modifier != null) {
                return modifier;
            }
            return Campaign.Modifier.NEW;
        }

        public void setModifier(Campaign.Modifier modifier) {
            this.modifier = modifier;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CampaignItem that = (CampaignItem) o;

            if (!getId().equals(that.getId())) return false;
            if (!getFinalPrice().equals(that.getFinalPrice())) return false;
            if (!getFromDate().equals(that.getFromDate())) return false;
            return getToDate().equals(that.getToDate());
        }

        @Override
        public int hashCode() {
            int result = getId().hashCode();
            result = 31 * result + getFinalPrice().hashCode();
            result = 31 * result + getFromDate().hashCode();
            result = 31 * result + getToDate().hashCode();
            return result;
        }
    }

}

