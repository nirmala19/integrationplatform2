package se.indiska.integration.jobs.articles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.general.*;
import se.indiska.integration.framework.job.IdentifiableIndioJob;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by heintz on 14/03/17.
 * <p>
 * datatype="ARTICLE"
 * triggerType="EVENT"
 * triggerStep="INBOUND_TRANSFER"
 * resultStep="ODS_STAGE_LOAD"
 */
@IdentifiableIndioJob(identifier = "ARTICLE_STAGE_LOAD", eventStep = "STAGE_LOAD", description = "Loads article information into stage area")
public class ArticleParser extends FlatFileLoader {
    private static final Logger logger = LoggerFactory.getLogger(ArticleParser.class);
    private static boolean tablesCreated = false;
    private Map<String, LineParser> lineParserMap = new HashMap<>();

    public ArticleParser() {
        LineParser line025Parser = new LineParser("articles_tx_025", true);
        LineParser line026Parser = new LineParser("articles_tx_025", true);
        LineParser line040Parser = new LineParser("articles_tx_040", true);
        line025Parser.addInterval("COMMODITY", 0, 13, null, true);
        line025Parser.addInterval("ARTGROUP", 13, 6, new IntegerConverter(), true);
        line025Parser.addInterval("RECEIPTTEXT", 19, 45);
        line025Parser.addInterval("QUANTCODE", 64, 4, new IntegerConverter());
        line025Parser.addInterval("BONUSPOINT", 68, 4, new IntegerConverter());
        line025Parser.addInterval("GUARANTEECODE", 72, 4, new IntegerConverter());
        line025Parser.addInterval("COMMODITYTYPE", 76, 4, new IntegerConverter());
        line025Parser.addInterval("SALESPRICED", 80, 8, new DoubleConverter());
        line025Parser.addInterval("VIPPRICED", 88, 8, new DoubleConverter());
        line025Parser.addInterval("PRICEGROUPD", 96, 4, new DoubleConverter());
        line025Parser.addInterval("CHANGEDATE", 100, 8, new DateConverter("yyyyMMdd"), true);
        line025Parser.addInterval("LASTCHANGEDBY", 108, 6, new IntegerConverter());
        line025Parser.addInterval("TRANSDATE", 114, 8, new DateConverter("yyyyMMdd"));
        line025Parser.addInterval("ADVERTWEEK", 122, 6, new IntegerConverter());
        line025Parser.addInterval("ORDERLEVEL", 128, 4, new IntegerConverter());
        line025Parser.addInterval("TYPE", 132, 4, new IntegerConverter());
        line025Parser.addInterval("CATEGORY", 136, 4, new IntegerConverter());
        line025Parser.addInterval("MATERIAL", 140, 4, new IntegerConverter());
        line025Parser.addInterval("MATERIALCONSTR", 144, 4, new IntegerConverter());
        line025Parser.addInterval("PATTERN", 148, 4, new IntegerConverter());
        line025Parser.addInterval("SIZETYPE", 152, 4, new IntegerConverter());
        line025Parser.addInterval("SIZELIST", 156, 20);
        line025Parser.addInterval("WEIGHT", 176, 20);
        line025Parser.addInterval("COUNTRY", 196, 4, new IntegerConverter());
        line025Parser.addInterval("PRODCOUNTRY", 200, 4, new IntegerConverter());
        line025Parser.addInterval("STORESOLID", 204, 4, new IntegerConverter());
        line025Parser.addInterval("STOREORDERUNIT", 208, 4, new IntegerConverter());
        line025Parser.addInterval("PRICELABELTYPE", 212, 4, new IntegerConverter());
        line025Parser.addInterval("PRICELSUPPLIER", 216, 6, new IntegerConverter());
        line025Parser.addInterval("PRICELABELQTY", 222, 4, new IntegerConverter());
        line025Parser.addInterval("DESCRIPTION", 226, 30);
        line025Parser.addInterval("TEXT", 256, 20);
        line025Parser.addInterval("MATERIAL", 276, 40);
        line025Parser.addInterval("BOX", 316, 4, new IntegerConverter());
        line025Parser.addInterval("UNITY", 320, 4);
        line025Parser.addInterval("STARTDATE", 324, 8);
        line025Parser.addInterval("NROFLABELS", 332, 4, new IntegerConverter());
        line025Parser.addInterval("COLLIQ", 336, 9, new IntegerConverter());
        line025Parser.addInterval("SEASON", 345, 6, new IntegerConverter());
        line025Parser.addInterval("ORDERUNIT", 351, 4, new IntegerConverter());
        line025Parser.addInterval("ASSORTMENT", 355, 4, new IntegerConverter());
        line025Parser.addInterval("DUTY", 359, 4, new IntegerConverter());
        line025Parser.addInterval("CURRENCY", 363, 4, new IntegerConverter());
        line025Parser.addInterval("FILLCODE", 367, 5);
        line025Parser.addInterval("PROP1", 372, 20);
        line025Parser.addInterval("PROP2", 392, 20);
        line025Parser.addInterval("PROP3", 412, 20);
        line025Parser.addInterval("PROP4", 432, 20);
        line025Parser.addInterval("PROP5", 452, 20);
        line025Parser.addInterval("PROP6", 472, 20);
        line025Parser.addInterval("TRADEMARK", 492, 4, new IntegerConverter());
        line025Parser.addInterval("SUPPLIER", 496, 6, new IntegerConverter());
        line025Parser.addInterval("COSTPRICESUPPD", 502, 8, new DoubleConverter());
        line025Parser.addInterval("COSTPRICED", 510, 8, new DoubleConverter());
        line025Parser.addInterval("GROSSMARGINPCD", 518, 4, new DoubleConverter());
        line025Parser.addInterval("COMPPRICED", 522, 8, new DoubleConverter());
        line025Parser.addInterval("PRICEORIGD", 530, 8, new DoubleConverter());
        line025Parser.addInterval("ORDERCODE", 538, 4, new IntegerConverter());
        line025Parser.addInterval("SUPPLIERNRD", 542, 20);
        line025Parser.addInterval("ORDERPACK", 562, 4, new IntegerConverter());
        line025Parser.addInterval("CONSUMERPACK", 566, 4, new IntegerConverter());
        line025Parser.addInterval("COMSUPPLIER", 570, 6, new IntegerConverter());
        line025Parser.addInterval("SUPPLDESCRIPTION", 576, 40);
        line025Parser.addInterval("ARTICLECODE", 616, 20);
        line025Parser.addInterval("PREWASH", 636, 4, new IntegerConverter());
        line025Parser.addInterval("WASHCODE", 640, 4, new IntegerConverter());
        line025Parser.addInterval("PICKSTAT", 644, 4, new IntegerConverter());
        line025Parser.addInterval("SHOPORDERPACK", 648, 4, new IntegerConverter());
        line025Parser.addInterval("PACKING", 652, 4, new IntegerConverter());

        line026Parser.addInterval("ARTICLE", 0, 13, null, true);
        line026Parser.addInterval("COMMODITY", 13, 13, null, true);
        line026Parser.addInterval("SALESPRICED", 26, 8, new DoubleConverter());
        line026Parser.addInterval("DESCRIPTION", 34, 20);
        line026Parser.addInterval("COLOUR", 54, 4, new IntegerConverter(), true);
        line026Parser.addInterval("COSTPRICED", 58, 8, new DoubleConverter());
        line026Parser.addInterval("GROSSMARGINPCD", 66, 4, new DoubleConverter());
        line026Parser.addInterval("ORDERUNIT", 70, 4, new IntegerConverter());
        line026Parser.addInterval("COSTPRICESUPPD", 74, 8, new DoubleConverter());
        line026Parser.addInterval("PRICEORIGD", 82, 8, new DoubleConverter());
        line026Parser.addInterval("BNR", 90, 9, new IntegerConverter());
        line026Parser.addInterval("SIZE", 99, 4, new IntegerConverter());
        line026Parser.addInterval("SUPPLIERNR", 103, 13);

        line040Parser.addInterval("ARTICLE", 0, 13, null, true);
        line040Parser.addInterval("BARCODE", 13, 13);

        addParser("025", line025Parser);
        addParser("026", line026Parser);
        addParser("040", line040Parser);
    }

}
