package se.indiska.integration.framework.connection;


import junit.framework.TestCase;
import org.junit.Assert;
import se.indiska.integration.framework.job.ProcessError;
import se.indiska.integration.framework.util.Counter;

import java.io.File;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by heintz on 2017-07-12.
 */
public class FileConnectorTest extends TestCase {
    public void testFetchFiles() throws Exception {
        Path tempDir = Files.createTempDirectory("FileConnectorTest");
        System.out.println("Creating temporary directory " + tempDir.toAbsolutePath().toString());
        FileConnector.setRootDirectory(tempDir.toAbsolutePath().toString());
        FileConnector fileConnector = new FileConnector("", "", "", null);

        String directory = "/FileConnectorTest/subdir";
        String filename = "filenameToCreate.txt";
        fileConnector.putFile(directory, filename, "This is the content!");
        fileConnector.close();

        fileConnector = new FileConnector("", "", "", null);
        Set<String> files = new HashSet<>();
        fileConnector.fetchFiles(directory, ".*", new Counter(1000), new FileReceiver() {
            @Override
            public void receiveFile(String source, String filename, byte[] content) {
                files.add(filename);
            }

            @Override
            public void receiveFileError(String source, String filename, String error, ProcessError processError) {
                Assert.fail("Received and error when retrieving file " + filename);
            }

            @Override
            public void done() {

            }
        });

        Assert.assertEquals(files.iterator().next(), filename);

        Files.walk(tempDir, FileVisitOption.FOLLOW_LINKS)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);

    }

    public void testPutFile() throws Exception {
        Path tempDir = Files.createTempDirectory("FileConnectorTest");
        System.out.println("Creating temporary directory " + tempDir.toAbsolutePath().toString());
        FileConnector.setRootDirectory(tempDir.toAbsolutePath().toString());
        FileConnector fileConnector = new FileConnector("", "", "", null);

        String directory = "/FileConnectorTest/subdir";
        String filename = "filenameToCreate.txt";
        fileConnector.putFile(directory, filename, "This is the content!");
        fileConnector.close();

        File dir = new File(fileConnector.getRootDirectory(), directory);
        File file = new File(dir, filename);
        Assert.assertTrue(file.exists());

        Files.walk(tempDir, FileVisitOption.FOLLOW_LINKS)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }


    public void testDeleteRemoteFile() throws Exception {
        Path tempDir = Files.createTempDirectory("FileConnectorTest");
        System.out.println("Creating temporary directory " + tempDir.toAbsolutePath().toString());
        FileConnector.setRootDirectory(tempDir.toAbsolutePath().toString());

        FileConnector fileConnector = new FileConnector("", "", "", null);
        String directory = "/FileConnectorTest/subdir";
        String filename = "filenameToDelete.txt";
        fileConnector.putFile(directory, filename, "This is the content to be deleted!");
        fileConnector.close();

        File dir = new File(fileConnector.getRootDirectory(), directory);
        File file = new File(dir, filename);
        Assert.assertTrue(file.exists());

        fileConnector.deleteRemoteFile(directory, filename);
        Assert.assertFalse(file.exists());

        Files.walk(tempDir, FileVisitOption.FOLLOW_LINKS)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }

}