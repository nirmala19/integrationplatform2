package se.indiska.integration.framework.connection;

import se.indiska.integration.framework.job.ProcessError;
import se.indiska.integration.framework.util.Counter;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.Assert;
import org.testng.annotations.Test;


public class AzureConnectorTest
{
	private static final Map<Object, Object> config;

	static
	{
		config = new HashMap<>();
		config.put("BLOB_CONTAINER_NAME", "sys-master-export-import");
		config.put("USE_HTTPS", Boolean.TRUE);
	}

	private static final String TEST_DIRECTORY = "import";
	private static final String TEST_PATTERN = "^test.*";

	private RemoteConnector azureConnector = new AzureConnector("yjeaccbjxuwghedohx2ztit.blob.core.windows.net",
			"yjeaccbjxuwghedohx2ztit", "8TuTp/Klb+vxmTdeFJxAkXqWIT0HnwHEGn9HzcbH2w3HRq/ICh0SFOsbzoMne2VR0mOhoVabBbTBgTzHtPWrJg==",
			config);

	@Test
	public void testFetchFiles() throws ConnectionFailureException
	{
		System.out.println("Listing files in root, max count 5:");
		fetchFiles("/", 5);

		System.out.println("Listing files in root, max count 1:");
		fetchFiles("/", 1);

		System.out.println("Listing files in root, pattern: '" + TEST_PATTERN + "'");
		fetchFiles("/", TEST_PATTERN, 999);
	}

	@Test
	public void testPutFile() throws ConnectionFailureException
	{
		final String fileName = UUID.randomUUID().toString() + ".tmp";
		final String fileContent = UUID.randomUUID().toString();

		System.out
				.println("Posting file '" + fileName + "' with content '" + fileContent + "' to directory '" + TEST_DIRECTORY + "'");
		azureConnector.putFile(TEST_DIRECTORY, fileName, fileContent);

		fetchFiles(TEST_DIRECTORY, 999);
	}

	@Test
	public void testRemoveFile() throws ConnectionFailureException
	{
		final String fileName = UUID.randomUUID().toString() + ".tmp";
		final String fileContent = UUID.randomUUID().toString();

		System.out.println("Posting file '" + fileName + "' to directory '" + TEST_DIRECTORY + "'");
		azureConnector.putFile(TEST_DIRECTORY, fileName, fileContent);

		fetchFiles(TEST_DIRECTORY, 999);

		System.out.println("Removing file '" + fileName + "' from directory '" + TEST_DIRECTORY + "'");
		azureConnector.deleteRemoteFile(TEST_DIRECTORY, fileName);

		fetchFiles(TEST_DIRECTORY, 999);
		System.out.println("Done");
	}

	private void fetchFiles(final String directory, final int maxCount) throws ConnectionFailureException
	{
		fetchFiles(directory, ".*", maxCount);
	}

	private void fetchFiles(final String directory, final String pattern, final int maxCount) throws ConnectionFailureException
	{
		System.out.println("Fetching all files from directory '" + directory + "'");
		azureConnector.fetchFiles(directory, pattern, new Counter(maxCount), new FileReceiver()
		{
			@Override
			public void receiveFile(String source, String filename, byte[] content)
			{
				System.out.println("Received file '" + filename + "' from source '" + source + "' and content:");
				Assert.assertFalse("File name should not contain path!", filename.contains("/"));
				System.out.println(new String(content));
			}

			@Override
			public void receiveFileError(String source, String filename, String error, ProcessError processError)
			{
				Assert.assertFalse("File name should not contain path!", filename.contains("/"));
				System.out.println(
						"Received file error '" + filename + "' from source '" + source + "' and error '" + processError + "'");
			}

			@Override
			public void done()
			{
				System.out.println("Done");
			}
		});
	}
}
