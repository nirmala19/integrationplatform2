package se.indiska.integration.framework.job;

import org.junit.Assert;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageAnnotationHelperTest {
    @Test
    public void testSubstituteDirectory() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String directory = "/path/to/wherever/{date:yyyy-MM-dd}/and/some/trailing/stuff";
        String expected = "/path/to/wherever/" + sdf.format(new Date()) + "/and/some/trailing/stuff";
        String replacement = MessageAnnotationHelper.substituteDirectory(directory);
        Assert.assertEquals(replacement, expected);
    }
}