package se.indiska.integration.framework.connection;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.util.Counter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static org.testng.Assert.assertEquals;

public class RemoteConnectorTest {
    private static final Logger logger = LoggerFactory.getLogger(RemoteConnectorTest.class);

    @Test
    public void testGetCharset() throws Exception {
        String[] protocolConfigStrings = {"{ \"SYSTEM_TYPE\":\"WINDOWS\" }", "{\n" +
                "                \"OUTBOUND\": {\n" +
                "            \"charsetMap\": {\n" +
                "                \".*?\\\\.csv\": \"cp1252\"\n" +
                "            }\n" +
                "        }\n" +
                "    }", null, "", "{ \"SYSTEM_TYPE\":\"WINDOWS\" }", "{ }"};

        ObjectMapper mapper = new ObjectMapper();
        for (int i = 0; i < protocolConfigStrings.length; i++) {
            Map protocolConfig = null;
            String protocolConfigStr = protocolConfigStrings[i];
            if (protocolConfigStr != null && protocolConfigStr.length() > 2) {
                try {
                    protocolConfig = mapper.readValue(protocolConfigStr, Map.class);
                } catch (IOException e) {
                    logger.error("Unable to read protocol configuration JSON into a Map!", e);
                }
            }
            RemoteConnector remoteConnector = new RemoteConnector(null, null, null, protocolConfig) {
                @Override
                public void fetchFiles(String directory, String pattern, Counter maxFiles, FileReceiver fileReceiver) throws ConnectionFailureException {

                }

                @Override
                public void putFile(String directory, String filename, String content) throws ConnectionFailureException {

                }

                @Override
                protected boolean deleteRemoteFile(String directory, String filename) {
                    return false;
                }

                @Override
                public void close() {

                }
            };

            Charset charset = remoteConnector.getCharset("abc123.csv", Direction.OUTBOUND, StandardCharsets.UTF_8);
            if (i == 0) {
                assertEquals(charset.name(), "UTF-8");
            } else if (i == 1) {
                assertEquals(charset, Charset.forName("cp1252"));
            }
            logger.debug("Charset: " + charset);
        }
    }

}