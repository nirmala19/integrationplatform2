#!/usr/bin/env python
#
# client to upload files from local folder to Hybris UAT https://test.indiska.com/inout/inout.py
#
# download python from www.python.org, use python 2.7 (script was tested with it) if you can
#
# Jonas Larson <jlarson_at_redbridge dot se>

BaseDir = "/filetransfer/hybris"
LogDir = "/var/log/indio/"
OutgoingDirectory = BaseDir + "/uat/to"

import xmlrpclib
# TODO error handler
s = xmlrpclib.ServerProxy('https://test.indiska.com/cgi-bin/inout.py')

# upload files to www.indiska.com
import glob
import os
import hashlib
import base64
import sys
import time

def LogMsg(msg):
    """log all actions to a file since windows doesn't support syslog"""
    LogFile = "hybris_outbound_uat.log"
    LogTgt = LogDir + LogFile
    open(LogTgt,'a').write(time.strftime("%Y-%m-%d %H:%M:%S") + ": " + msg + "\n")
    return True

os.chdir(OutgoingDirectory)
OutgoingFiles = glob.glob('*.*')
if not OutgoingFiles:
    msg = "No files to upload"
    print msg
    LogMsg(msg)

for File in OutgoingFiles:
    # method returns list with [<return value>,<message>]
    try:
        r = s.upload_file(File,hashlib.sha224(open(File).read()).hexdigest(),base64.b64encode(open(File).read()))
        if r[0]:
            try:
                # os.rename(File, TgtFile)
                os.unlink(File)
            except Exception as e:
                msg = "unlink of %s failed" %File
                print msg
                LogMsg(msg)
                print e
            msg = "%s uploaded successfully" %File
            print msg
            LogMsg(msg)
        else:
            msg = "%s failed to upload, return error was: %s " %(File,r[1])
            print msg
            LogMsg(msg)

    except Exception as e:
        print e
        pass