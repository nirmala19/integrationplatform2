#!/bin/bash

# This script is used to extract NOOP status from PROCESS_STATUS table (things that
# have been executed but didn't yield any result). Good for error tracing and in
# discussions with partners if things has been done or not, but is too expensive
# to keep in database. Two days is kept in database. This script pauses the Indio
# engine first and then reenables.

export LC_ALL="en_US.UTF-8"
TIME=`date +%Y-%m-%d' '%H:%M:%S`

echo "Exporting NOOP process status messages to file 2 days older than $TIME"

echo "update settings set value='TRUE' where key='paused';"| psql -U indiska indio
sleep 10
echo "copy (select event_time, id, job_id, data_type, event_step, parameters, allow_concurrent FROM process_status WHERE status = 'NOOP' AND event_time < to_timestamp('$TIME','YYYY-MM-DD HH24:MI:SS') - INTERVAL '2 day' ORDER BY event_time asc) to STDOUT" | psql -U indiska indio >> /var/log/indio/n
o_operation.log

echo "DELETE FROM process_data_trace WHERE process_id IN (SELECT id FROM process_status WHERE status = 'NOOP' AND process_status.event_time < to_timestamp('$TIME','YYYY-MM-DD HH24:MI:SS') - INTERVAL '2 day');" | psql -U indiska indio

echo "DELETE FROM process_status WHERE status = 'NOOP' AND event_time < to_timestamp('$TIME','YYYY-MM-DD HH24:MI:SS') - INTERVAL '2 day';" | psql -U indiska indio

echo "VACUUM ANALYZE process_status;" | psql -U indiska indio
echo "VACUUM ANALYZE process_data_trace;" | psql -U indiska indio

echo "update settings set value='FALSE' where key='paused';"| psql -U indiska indio