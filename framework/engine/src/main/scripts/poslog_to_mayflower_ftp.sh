#!/bin/bash
#
# Script to upload zipped poslogs to mayflower. Called from the poslog_to_mayflower_zip.sh script.
#

ftpserver=ftp.mayflower.se
ftpuser=indiska_trans
ftppass=3uPugewu
sourcedir=/filetransfer/abalon/to/poslogzip
archivedir=/filetransfer/abalon/to/ziparchive

numfiles=`find $sourcedir -name *.zip | wc -l`

find $sourcedir -name *.zip -exec curl -T {} ftp://$ftpuser:$ftppass@$ftpserver/ \;

if [ ! $? -eq 0 ]; then
    echo "[`date`]: Error uploading files to mayflower!"
elif [ ! $numfiles -eq 0 ]; then
    echo "Success uploading $numfiles"
    find $sourcedir -name *.zip -exec mv {} $archivedir \;
else
    echo "[`date`]: No files to upload"
fi