#!/usr/bin/env python
#
# client to download files from Hybris PRODUCTION: https://www.indiska.com/inout/inout.py
#
# download python from www.python.org, use python 2.7 (script was tested with it) if you can
#
BaseDir = "/filetransfer/hybris"
LogDir = "/var/log/indio/"
IncomingDirectory = BaseDir + "/from"

import xmlrpclib
# TODO error handler
s = xmlrpclib.ServerProxy('https://admin.indiska.com/cgi-bin/inout.py')

# upload files to www.indiska.com
import glob
import os
import hashlib
import base64
import sys
import time
import traceback

def LogMsg(msg1):
    """log all actions to a file since windows doesn't support syslog"""
    LogFile = "hybris_inbound.log"
    LogTgt = LogDir + LogFile
    open(LogTgt,'a').write(time.strftime("%Y-%m-%d %H:%M:%S") + ": " + msg1 + "\n")
    return True

# download all files from www.indiska.se
os.chdir(IncomingDirectory)

AvailableFiles = s.get_all_files()
if not AvailableFiles:
    msg2 = "no files to download"
    LogMsg(msg2)

for GetFile in AvailableFiles:

    print GetFile
    # don't overwrite a file
    if os.path.isfile(GetFile):
        msg3 = "%s failed to download, file already exists" %GetFile
        LogMsg(msg3)
        sys.exit(1)
    elif GetFile == "cscockpittest":
        print "Not getting that file!"
    else:
        try:
            base64content = s.get_single_file(GetFile);
            content = base64.b64decode(base64content);
            open(GetFile,'w').write(content)
        except:
            print "Unexpected error:", sys.exc_info()[0]
            msg4 = "%s failed to download" %GetFile+"\t"+traceback.format_exc()
            LogMsg(msg4)

        if s.archive_single_file(GetFile):
            msg5 = "%s downloaded successfully" %GetFile
            LogMsg(msg5)
        else:
            msg6 = "%s failed to archive" %GetFile
            LogMsg(msg6)