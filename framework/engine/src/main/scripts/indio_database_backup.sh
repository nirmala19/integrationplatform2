#!/bin/bash

BACKUP_DIR=/filetransfer/backups/db
BACKUP_DIR_NAME=`date +'indio_%Y%m%d.%H%M%S.backup'`
DATABASE=indio
USER=indiska



BACKUP_DESTINATION=$BACKUP_DIR/$BACKUP_DIR_NAME

echo "Pausing Indio processing while taking backup and resting for 15 seconds to pause to take effect"
PAUSE_STATUS_SQL="select value from settings where key='paused'"
PAUSE_STATUS=`psql -A -q -t  -U indiska indio -c "$PAUSE_STATUS_SQL"`
psql -q -U indiska indio -c "update settings set value='BACKUP' where key='paused'"
sleep 15

echo "Backing up indio database to $BACKUP_DESTINATION"

pg_dump -j 8 -Fd -f $BACKUP_DESTINATION -U $USER $DATABASE

echo "Resuming operation of Indio"
psql -q -U indiska indio -c "update settings set value='$PAUSE_STATUS' where key='paused'"

echo "Removing backups older than 3 days"
find $BACKUP_DIR -type d -mtime +3 | xargs rm -rf