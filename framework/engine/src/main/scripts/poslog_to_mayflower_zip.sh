#!/bin/bash
#
# Script to zip the poslogs found in the $sourcedir directory into a zipfile in $targetdir.
# Should be run once every night.
#


sourcedir=/filetransfer/abalon/to/poslogstage
targetdir=/filetransfer/abalon/to/poslogzip
filename=indiskapos_`date +%Y-%m-%d-%H%M`.zip


find $sourcedir -name 'POSLog*' | xargs -s 20000 zip -m -q -j $targetdir/$filename

/filetransfer/bin/poslog_to_mayflower_ftp.sh