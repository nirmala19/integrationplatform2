package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by heintz on 2017-05-08.
 */
public class Timer {
    private static final Logger logger = LoggerFactory.getLogger(Timer.class);
    private Map<String, LocalTimer> timerMap = new HashMap<>();

    public void start(String timerKey) {
        LocalTimer timer = timerMap.get(timerKey);
        if (timer == null) {
            timer = new LocalTimer();
            timerMap.put(timerKey, timer);
        }
        timer.start();
    }

    public void end(String timerKey) {
        LocalTimer timer = timerMap.get(timerKey);
        if (timer != null) {
            timer.end();
        }
    }

    public Long time(String timerKey) {
        LocalTimer timer = timerMap.get(timerKey);
        if (timer != null) {
            return timer.time();
        }
        return -1l;
    }

    public Double timePerOccuranceInMs(String timerKey) {
        LocalTimer timer = timerMap.get(timerKey);
        if (timer != null) {
            return timer.timePerOccuranceInMs();
        }
        return -1d;
    }

    public void reset(String timerKey) {
        LocalTimer timer = timerMap.get(timerKey);
        if (timer != null) {
            timer.reset();
        }
    }

    public Long getOccurrances(String timerKey) {
        LocalTimer timer = timerMap.get(timerKey);
        if (timer != null) {
            return timer.getOccurrances();
        }
        return -1l;
    }


    private class LocalTimer {
        private long time = 0;
        private long start = -1;
        private long occurrances = 0;

        public long getTime() {
            return time;
        }

        public void start() {
            this.start = System.nanoTime();
        }

        public synchronized void end() {
            time += (System.nanoTime() - start);
            start = 0;
            occurrances++;
        }

        public Long time() {
            return time;
        }

        public Double timePerOccuranceInMs() {
            return (double) this.time() / (double) occurrances/1e6d;
        }

        public long getOccurrances() {
            return occurrances;
        }

        public void reset() {
            this.time = 0;
            this.start = 0;
            this.occurrances = 0;
        }
    }
}
