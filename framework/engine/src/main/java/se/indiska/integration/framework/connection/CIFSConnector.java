package se.indiska.integration.framework.connection;

import jcifs.smb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.job.ProcessError;
import se.indiska.integration.framework.util.Counter;
import se.indiska.integration.framework.util.InboundFileUtilities;
import se.indiska.util.EOLUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by heintz on 2017-04-21.
 */
public class CIFSConnector extends RemoteConnector {
    private static final Logger logger = LoggerFactory.getLogger(CIFSConnector.class);
    private NtlmPasswordAuthentication ntlmPasswordAuthentication;

    public CIFSConnector(String host, String username, String credentials, Map protocolConfig) {
        super(host, username, credentials, protocolConfig);
        this.ntlmPasswordAuthentication = new NtlmPasswordAuthentication(null, username, credentials);
    }

    @Override
    public void fetchFiles(String directory, String pattern, Counter maxFileCounter, FileReceiver fileReceiver) throws ConnectionFailureException {
        if (!directory.startsWith("/")) {
            directory = "/" + directory;
        }
        String uri = "smb://" + getHost() + directory;
        if (isReachable(uri)) {
            try {
                if (!uri.endsWith("/")) {
                    uri += "/";
                }

                SmbFile dir = new SmbFile(uri, ntlmPasswordAuthentication);
                LocalDateTime now = LocalDateTime.now();
                now = now.minusSeconds(10);
                SmbFile[] files = dir.listFiles(new SmbFileFilter() {
                    @Override
                    public boolean accept(SmbFile smbFile) throws SmbException {
                        return smbFile.getName().matches(pattern) && maxFileCounter.reduce() > 0 && smbFile.length() > 0;
                    }
                });


                for (SmbFile f : files) {
                    if (f != null) {
                        if (f.isFile()) {
                            if (!f.canRead()) {
                                logger.debug("cannot read file " + f.getName());
                                fileReceiver.receiveFileError(uri, f.getName(),
                                        "Cannot read file, insufficient permissions",
                                        ProcessError.PERMISSION_DENIED);
                            } else {
                                try {
                                    InputStream is = f.getInputStream();
                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                    int c = 0;
                                    byte[] buf = new byte[2048];
                                    while ((c = is.read(buf)) >= 0) {
                                        baos.write(buf, 0, c);
                                    }
                                    is.close();
                                    fileReceiver.receiveFile(uri, f.getName(), baos.toByteArray());
                                } catch (IOException e) {
                                    fileReceiver.receiveFileError(uri, f.getName(),
                                            "Error reading remote files " + f.toString(),
                                            ProcessError.REMOTE_FILE_READ_FAILED);
                                }

                            }

                        }
                    } else {
                        logger.debug("File is null???");
                    }
//                    LocalDateTime date =
//                            LocalDateTime.ofInstant(Instant.ofEpochMilli(f.createTime()), store.getZoneId());
//                    if (date.isBefore(now) && f.isFile()) {
//                        logger.debug(store.getName() + ":\t" + f.getName() + " (" + (f.isDirectory() ? "d" : "f") + ") " + date.toString());
//                    }
//                    if (f.getName().matches("POSLog\\..*")) {
//                        poslogCount++;
//                    } else if (f.getName().matches("PMLog\\..*")) {
//                        pmlogCount++;
//                    }
                }
            } catch (MalformedURLException e) {
                throw new ConnectionFailureException("Cannot connect to " + uri, e);
            } catch (SmbException e) {
                throw new ConnectionFailureException("Error while talking to samba server " + uri, e);
            } finally {
                fileReceiver.done();
            }
        } else {
            logger.debug("Server " + getHost() + " is not reachable on CIFS Protocol!");
            throw new ConnectionFailureException("Server " + getHost() + " is not reachable on CIFS Protocol!");
        }
    }

    @Override
    public void putFile(String directory, String filename, String content) throws ConnectionFailureException {
        if (!directory.startsWith("/")) {
            directory = "/" + directory;
        }
        if (!directory.endsWith("/")) {
            directory += "/";
        }
        String uri = "smb://" + getHost() + directory;
        if (isReachable(uri)) {
            try {

                SmbFile file = new SmbFile(uri, filename, ntlmPasswordAuthentication);
                if (!file.exists()) {
                    file.createNewFile();
                }
                if (file.canWrite()) {
                    content = EOLUtils.convertToWindowsEOL(content, "cp1252");
                    SmbFileOutputStream os = new SmbFileOutputStream(file);
                    Object charsetMapO = getProtocolConfig().get("charsetMap");
                    Charset charset = getCharset(filename, Direction.OUTBOUND, Charset.forName("cp1252"));
                    os.write(content.getBytes(charset));
                    os.close();
                }
            } catch (IOException e) {
                throw new ConnectionFailureException("Cannot write file to " + uri + filename, e);
            }
        } else {
            logger.debug("Server " + getHost() + " is not reachable on CIFS Protocol!");
            throw new ConnectionFailureException("Server " + getHost() + " is not reachable on CIFS Protocol!");
        }
    }

    @Override
    public boolean deleteRemoteFile(String directory, String filename) {
        if (!directory.startsWith("/")) {
            directory = "/" + directory;
        }
        if (!directory.endsWith("/")) {
            directory += "/";
        }
        String uri = "smb://" + getHost() + directory;
        if (isReachable(uri)) {
            try {
                SmbFile file = new SmbFile(uri, filename, ntlmPasswordAuthentication);
                file.delete();
                return true;
            } catch (SmbException | MalformedURLException e) {
                logger.error("Cannot delete file " + uri + filename, e);
            }
        }
        return false;
    }

    @Override
    public void close() {

    }


    public static void main(String[] argv) throws Exception {
//        CIFSConnector cifsConnector = new CIFSConnector("172.30.9.30", "administrator", "flipper", null);
//        cifsConnector.fetchFiles("/ExtPgm/Trans/ToHQ", "POSLog.*", new Counter(100), new FileReceiver() {
//            @Override
//            public void receiveFile(String source, String filename, byte[] content) {
//                logger.debug("Received file " + filename);
//            }
//
//            @Override
//            public void receiveFileError(String source, String filename, String error, ProcessError processError) {
//                logger.error("Error receiving file " + filename);
//            }
//
//            @Override
//            public void done() {
//                logger.debug("DONE!");
//            }
//        });
        CIFSConnector connector = new CIFSConnector("shk-ftp02.hk.indiska.se", "hk\\svcindio", "dblhklASU%HD8o", new HashMap());
        connector.fetchFiles("/filetransfer/Shop/To/900", ".*", new Counter(20), new FileReceiver() {
            @Override
            public void receiveFile(String source, String filename, byte[] content) {
                try {
                    InboundFileUtilities.InboundFile inboundFile =
                            new InboundFileUtilities.InboundFile("ECOM_PRODUCT_ENRICHMENT",
                                    source,
                                    filename,
                                    content, new File("/tmp"),
                                    new Date(), "THIS-IS-TEST-MESSAGE",
                                    new HashMap<>(), "ISO-8859-1");
                    logger.debug("Found file " + filename);
                    logger.debug(inboundFile.getFileContent());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void receiveFileError(String source, String filename, String error, ProcessError processError) {

            }

            @Override
            public void done() {
                logger.debug("Done!");
            }
        });
    }
}
