package se.indiska.integration.framework.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * IndIO Exception for a failed connection. May be a standalone exception or wrap another exception
 * that indicates that a remote connection failed.
 *
 * Created by heintz on 04/04/17.
 */
public class ConnectionFailureException extends Exception {
    private static final Logger logger = LoggerFactory.getLogger(ConnectionFailureException.class);


    public ConnectionFailureException(String message) {
        super(message);
    }

    public ConnectionFailureException(String message, Throwable cause) {
        super(message, cause);
    }
}
