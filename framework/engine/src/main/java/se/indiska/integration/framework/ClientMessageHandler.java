package se.indiska.integration.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.MessageHandler;
import javax.websocket.Session;

/**
 * Created by heintz on 27/03/17.
 */
public class ClientMessageHandler implements MessageHandler.Whole<String> {
    private static final Logger logger = LoggerFactory.getLogger(ClientMessageHandler.class);
    private String token;
    private Session session;

    public ClientMessageHandler(Session session) {
        this.session = session;
    }


    @Override
    public void onMessage(String message) {
        logger.debug("Received message:\n" + message);
    }

    public void writeMessage(String textMessage) {
        boolean sent = false;
        int retries = 10;
        while (!sent && retries > 0) {
            try {
                if (session != null) {
                    session.getAsyncRemote().sendText(textMessage);
                }
                sent = true;
            } catch (IllegalStateException e) {
                retries--;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e1) {
                }
            }
        }
        if (!sent) {
            close();
        }
    }

    public void close() {
        if (token != null) {
        }
    }
}
