package se.indiska.integration.framework.connection;

import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.job.ProcessError;
import se.indiska.integration.framework.util.Counter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.microsoft.azure.storage.blob.ListBlobItem;


public class AzureConnector extends RemoteConnector
{
	private static final Logger LOG = LoggerFactory.getLogger(AzureConnector.class);
	private final String connectionString;
	private final String blobContainerName;

	public AzureConnector(final String host, final String username, final String credentials, final Map protocolConfig)
	{
		super(host, username, credentials, protocolConfig);
		blobContainerName = (String) getProtocolConfig().get("BLOB_CONTAINER_NAME");
		final Boolean useHttps = (Boolean) getProtocolConfig().get("USE_HTTPS");
		connectionString = buildConnectionString(username, credentials, Boolean.FALSE.equals(useHttps));
	}

	private String buildConnectionString(final String username, final String key, boolean useHttps)
	{
		return "DefaultEndpointsProtocol=" + (useHttps ? "https" : "http") + ";"
				+ "AccountName=" + username + ";"
				+ "AccountKey=" + key;
	}

	@Override
	public void fetchFiles(final String directory, final String pattern, final Counter maxFiles, final FileReceiver fileReceiver)
			throws ConnectionFailureException
	{
		final CloudBlobContainer cloudBlobContainer = getCloudBlobContainer();
		final String primaryUrl = cloudBlobContainer.getStorageUri().getPrimaryUri().toString();
		final String normalizedDir = normalizeDir(directory);

		if(LOG.isDebugEnabled())
		{
			LOG.debug("Fetching files from azure storage: " + primaryUrl + "/" + normalizedDir + pattern);
		}

		try
		{
			final Iterable<ListBlobItem> blobItems = cloudBlobContainer.listBlobs(normalizedDir);
			final Iterator<ListBlobItem> blobItemIterator = blobItems.iterator();
			while (blobItemIterator.hasNext() && maxFiles.get() > 0)
			{
				final ListBlobItem listBlobItem = blobItemIterator.next();
				if (listBlobItem instanceof CloudBlob)
				{
					final CloudBlob cloudBlob = (CloudBlob) listBlobItem;
					if (LOG.isDebugEnabled())
					{
						LOG.debug("Found cloud blob '" + cloudBlob.getName() + "' with type '" + cloudBlob.getProperties().getBlobType()
								+ "'");
					}
					if (cloudBlob.getName().matches(pattern))
					{
						if (LOG.isDebugEnabled())
						{
							LOG.debug("Cloud blob '" + cloudBlob.getName() + "' matches pattern '" + pattern + "'");
						}
						final String source = primaryUrl + "/" + normalizedDir;
						try (final ByteArrayOutputStream baos = new ByteArrayOutputStream())
						{
							try
							{
								cloudBlob.download(baos);
								fileReceiver.receiveFile(source, getFileName(cloudBlob.getName()), baos.toByteArray());
							}
							catch (final StorageException e)
							{
								fileReceiver.receiveFileError(source, getFileName(cloudBlob.getName()), "Can't download file",
										ProcessError.REMOTE_FILE_READ_FAILED);
								LOG.error("Can't get blob content for blob '" + cloudBlob.getName() + "' in directory '" + normalizedDir
												+ "'", e);
							}

							maxFiles.reduce();
						}
						catch (final IOException e)
						{
							fileReceiver.receiveFileError(source, getFileName(cloudBlob.getName()), "Can't save file",
									ProcessError.UNDEFINED_ERROR);
							LOG.error("Can't save blob content for blob '" + cloudBlob.getName() + "' from directory '" + normalizedDir
									+ "'", e);
						}
					}
				}
			}
		} finally
		{
			fileReceiver.done();
		}
	}

	@Override
	public void putFile(final String directory, final String filename, final String content) throws ConnectionFailureException
	{
		final CloudBlobContainer cloudBlobContainer = getCloudBlobContainer();
		final String normalizedDir = normalizeDir(directory);

		if(LOG.isDebugEnabled())
		{
			LOG.debug("Uploading file '" + filename + "' into directory '" + normalizedDir + "' with content '" + content +'"');
		}

		try
		{
			final CloudBlockBlob blockBlobReference = cloudBlobContainer.getBlockBlobReference(normalizedDir + filename);
			try (final ByteArrayInputStream bais = new ByteArrayInputStream(
					content.getBytes(getCharset(filename, Direction.OUTBOUND, StandardCharsets.UTF_8))))
			{
				blockBlobReference.upload(bais, bais.available());
			}
			catch (final IOException e)
			{
				throw new ConnectionFailureException("IO exception uploading file '" + filename + "'");
			}
		}
		catch (final URISyntaxException | StorageException e)
		{
			throw new ConnectionFailureException(
					"Can't create blob '" + filename + "' in directory '" + normalizedDir + "' in blob container '" + cloudBlobContainer.getName() + "'", e);
		}
	}

	@Override
	protected boolean deleteRemoteFile(final String directory, final String filename)
	{
		final String normalizedDir = normalizeDir(directory);
		if(LOG.isDebugEnabled())
		{
			LOG.debug("Deleting file '" + filename + "' from directory '" + normalizedDir +"'");
		}

		try
		{
			final CloudBlobContainer cloudBlobContainer = getCloudBlobContainer();
			final CloudBlockBlob blockBlobReference = cloudBlobContainer.getBlockBlobReference(normalizedDir + filename);
			return blockBlobReference.deleteIfExists();
		}
		catch (final URISyntaxException | StorageException | ConnectionFailureException e)
		{
			LOG.error("Failed to delete file '" + filename + "' from directory '" + normalizedDir +"'", e);
			return false;
		}
	}

	@Override
	public void close()
	{
		// Nothing to close
	}

	private CloudBlobContainer getCloudBlobContainer() throws ConnectionFailureException
	{
		try
		{
			final CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(connectionString);
			final CloudBlobClient cloudBlobClient = cloudStorageAccount.createCloudBlobClient();

			final CloudBlobContainer cloudBlobContainer = cloudBlobClient.getContainerReference(blobContainerName);
			if (!cloudBlobContainer.exists())
			{
				throw new ConnectionFailureException("Blob container '" + blobContainerName + "' doesn't exist");
			}

			return cloudBlobContainer;
		}
		catch (InvalidKeyException | URISyntaxException | StorageException e)
		{
			throw new ConnectionFailureException("Can't get blob container '" + blobContainerName + "'", e);
		}
	}

	private String normalizeDir(final String directory)
	{
		final StringBuilder normalized = new StringBuilder();
		if(!StringUtils.isEmpty(directory))
		{
			normalized.append(directory);

			if(normalized.charAt(0) == '/')
			{
				normalized.deleteCharAt(0);
			}

			if(normalized.length() > 0 && normalized.charAt(normalized.length() - 1) != '/')
			{
				normalized.append('/');
			}
		}

		return normalized.toString();
	}

	private String getFileName(final String blobName)
	{
		return FilenameUtils.getName(blobName);
	}
}
