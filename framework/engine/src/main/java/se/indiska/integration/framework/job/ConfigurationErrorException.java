package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationErrorException extends Exception {
    private static final Logger logger = LoggerFactory.getLogger(ConfigurationErrorException.class);

    public ConfigurationErrorException(String message) {
        super(message);
    }

    public ConfigurationErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
