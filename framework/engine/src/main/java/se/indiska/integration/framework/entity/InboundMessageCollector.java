package se.indiska.integration.framework.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * POJO which contains the information for an inbound message collector.
 * Created by heintz on 2017-04-18.
 */
public class InboundMessageCollector {
    private static final Logger logger = LoggerFactory.getLogger(InboundMessageCollector.class);
    private Long id = null;
    private RemoteLocation remoteLocation = null;
    private RemoteServer remoteServer = null;
    private String filenamePattern = null;
    private Map<String, String> contentDatatypeResolver = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RemoteLocation getRemoteLocation() {
        return remoteLocation;
    }

    public void setRemoteLocation(RemoteLocation remoteLocation) {
        this.remoteLocation = remoteLocation;
    }

    public String getFilenamePattern() {
        return filenamePattern;
    }

    public void setFilenamePattern(String filenamePattern) {
        this.filenamePattern = filenamePattern;
    }

    public RemoteServer getRemoteServer() {
        return remoteServer;
    }

    public void setRemoteServer(RemoteServer remoteServer) {
        this.remoteServer = remoteServer;
    }

    public Map<String, String> getContentDatatypeResolver() {
        return contentDatatypeResolver;
    }

    public void setContentDatatypeResolver(Map<String, String> contentDatatypeResolver) {
        this.contentDatatypeResolver = contentDatatypeResolver;
    }
}
