package se.indiska.integration.framework.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POJO which contains the information for a remote_location
 * Created by heintz on 2017-04-18.
 */
public class RemoteLocation {
    private static final Logger logger = LoggerFactory.getLogger(RemoteLocation.class);
    private Long id = null;
    private String directory = null;
    private String description =null;

    public RemoteLocation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
