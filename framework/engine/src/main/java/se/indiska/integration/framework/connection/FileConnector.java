package se.indiska.integration.framework.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.IndioProperties;
import se.indiska.integration.framework.util.Counter;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * "Remote Connector" for local files.
 * Created by heintz on 04/04/17.
 */
public class FileConnector extends RemoteConnector {
    private static final Logger logger = LoggerFactory.getLogger(FileConnector.class);
    private static String rootDirectory = IndioProperties.properties().getProperty("file_root_directory", "/");
    private Set<BufferedWriter> writers = new HashSet<>();

    public FileConnector(String host, String username, String credentials, Map protocolConfig) {
        super(host, username, credentials, protocolConfig);
    }

    @Override
    public void fetchFiles(String directory, String pattern, Counter maxFiles, FileReceiver fileReceiver) throws ConnectionFailureException {
        File dir = new File(rootDirectory, directory);
        File[] files = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.getName().matches(pattern); // && file.lastModified() < (System.currentTimeMillis() - 10000);
            }
        });

        if (logger.isTraceEnabled()) {
            logger.trace("Found " + (files != null ? files.length : "no") + " files in " + dir.getAbsolutePath() + " matching pattern " + pattern);
        }

        try {
            if (files != null) {
                for (File file : files) {
                    if (maxFiles.reduce() <= 0 && file.lastModified() < (System.currentTimeMillis() - 3000)) {
                        break;
                    }
                    FileInputStream fis = new FileInputStream(file);
                    byte[] buf = new byte[1024];
                    int c = 0;
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    while ((c = fis.read(buf)) >= 0) {
                        baos.write(buf, 0, c);
                    }
                    fis.close();
                    fileReceiver.receiveFile("file:///" + file.getAbsolutePath(), file.getName(), baos.toByteArray());
                }
            }
        } catch (FileNotFoundException e) {
            throw new ConnectionFailureException("Cannot read files from file system in dir " + directory, e);
        } catch (IOException e) {
            throw new ConnectionFailureException("Cannot read files from file system in dir " + directory, e);
        } finally {
            fileReceiver.done();
        }
    }

    @Override
    public void putFile(String directory, String filename, String content) throws ConnectionFailureException {
        Path dirPath = Paths.get(rootDirectory, directory);
        try {
            if (!Files.exists(dirPath)) {
                logger.debug("Creating directory " + dirPath.toAbsolutePath().toString());
                Files.createDirectories(dirPath);
            }
            Path path = Paths.get(dirPath.toAbsolutePath().toString(), filename);
            Charset charset = getCharset(filename, Direction.OUTBOUND, StandardCharsets.UTF_8);
            try (BufferedWriter writer = Files.newBufferedWriter(path, charset, StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
                writers.add(writer);
                writer.write(content);
            }
//            Files.write(path, content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        } catch (AccessDeniedException e) {
            logger.error("Cannot create directory " + dirPath.toAbsolutePath());
            throw new ConnectionFailureException("Cannot write file!", e);
        } catch (IOException e) {
            logger.error("Cannot write file!", e);
            throw new ConnectionFailureException("Cannot write file!", e);
        }
    }

    public static String getRootDirectory() {
        return rootDirectory;
    }

    public static void setRootDirectory(String rootDirectory) {
        FileConnector.rootDirectory = rootDirectory;
    }

    @Override
    public boolean deleteRemoteFile(String directory, String filename) {
        File file = new File(new File(rootDirectory, directory), filename);
        if (file.exists()) {
            return file.delete();
        }
        return true;
    }

    @Override
    public void close() {
        for (BufferedWriter writer : writers) {
            try {
                writer.close();
            } catch (IOException e) {
                logger.error("Error while closing file writer!", e);
            }
        }
    }
}
