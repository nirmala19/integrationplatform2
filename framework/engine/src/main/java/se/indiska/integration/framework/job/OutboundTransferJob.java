package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.connection.ConnectionFailureException;
import se.indiska.integration.framework.connection.RemoteConnector;
import se.indiska.integration.framework.connection.RemoteConnectors;
import se.indiska.integration.framework.trace.DataTrace;
import se.indiska.integration.framework.util.Alerter;
import se.indiska.integration.framework.util.InboundFileUtilities;
import se.indiska.integration.framework.util.SedRegexpSubstitute;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * A standard job which transfers files to remote locations based on integration server events.
 * <p>
 * TODO: Need to implement a way to distribute to a directory per store id, where store id is obtained from the
 * filename.
 * Created by heintz on 2017-04-11.
 * <p>
 */
@IdentifiableIndioJob(identifier = "OUTBOUND_MESSAGE_DISTRIBUTOR", eventStep = "OUTBOUND_TRANSFER", description = "Distributes messages to targets")
public class OutboundTransferJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(OutboundTransferJob.class);
    private static int MAX_FILES_PER_EXECUTION = 2000;
    private static int MAX_RUNNING_TIME_IN_MS = 300000;

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        Map<String, RemoteConnector> connectorMap = new HashMap<>();
        long start = System.currentTimeMillis();
        String fromWhere = "FROM\n" +
                "  outbound_distribution od,\n" +
                "  remote_location rl,\n" +
                "  remote_server rs,\n" +
                "  outbound_file_staging ofs,\n" +
                "  outbound_message_distributor omd\n" +
                "WHERE\n" +
                "  omd.remote_location = rl.id\n" +
                "  AND omd.remote_server = rs.id\n" +
                "  AND ofs.id = od.outbound_message_id\n" +
                "  AND NOT od.delivered\n" +
                "  AND omd.id = od.outbound_message_distributor_id\n" +
                "  AND omd.immediate_delivery = TRUE " +
                "  limit " + MAX_FILES_PER_EXECUTION;
        String sql = "SELECT\n" +
                "  rs.protocol,\n" +
                "  rs.id remote_server_id,\n" +
                "  rs.protocol_config,\n" +
                "  omd.remote_location,\n" +
                "  omd.substitute_pattern,\n" +
                "  od.outbound_message_id,\n" +
                "  rs.address,\n" +
                "  rs.password,\n" +
                "  rs.username,\n" +
                "  rl.id remote_location_id,\n" +
                "  rl.directory,\n" +
                "  ofs.filename,\n" +
                "  od.delivered,\n" +
                "  od.event_time,\n" +
                "  ofs.id outbound_id,\n" +
                "  ofs.file_content,\n" +
                "  ofs.message_id,\n" +
                "  od.outbound_message_distributor_id\n" +
                fromWhere;
        String countSql = "SELECT count(1) " + fromWhere;

        String dataType = (String) jobExecutionData.getDataMap().get("datatype");
        String jobId = (String) jobExecutionData.getDataMap().get("jobid");
        String eventStep = (String) jobExecutionData.getDataMap().get("eventstep");
        boolean transmittedFiles = false;
        boolean callbacked = false;
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(countSql);
            ResultSet rset = ps.executeQuery();
            int hits = 0, count = 0;
            if (rset.next()) {
                hits = rset.getInt(1);
            }
            hits = Math.min(MAX_FILES_PER_EXECUTION, hits);

            ps = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
            rset = ps.executeQuery();
            start = System.currentTimeMillis();
            int timerCount = 0;
            while (rset.next() && (System.currentTimeMillis() - MAX_RUNNING_TIME_IN_MS) < start) {
                Long outboundId = rset.getLong("outbound_id");
                String protocol = rset.getString("protocol");
                String address = rset.getString("address");
                String username = rset.getString("username");
                String password = rset.getString("password");
                String directory = rset.getString("directory");
                String filename = rset.getString("filename");
                String messageId = rset.getString("message_id");
                String fileContent = null;
                try {
                    fileContent = InboundFileUtilities.getFileContent(rset.getString("file_content"));
                } catch (IOException e) {
                    logger.error("Unable to get file content for " + rset.getString("file_content"), e);
                }
                String substitutePattern = rset.getString("substitute_pattern");
                String key = protocol + address + username + password;
//                Set<String> addresses = new LinkedHashSet<>();
//                if (address.indexOf("{") >= 0 && address.indexOf("}") > 0) {
//                    String annotation = address.substring(address.indexOf("{") + 1, address.indexOf("}"));
//                    for (MessageAnnotationHelper.Annotation substitute : MessageAnnotationHelper.substitutions(annotation, messageId, connection)) {
//                        addresses.add(substitute.getValue());
//                    }
//                } else {
//                    addresses.add(address);
//                }
                RemoteServer remoteServer = new RemoteServer(rset, connection);

//                for (String networkAddress : addresses) {
                RemoteConnector connector = connectorMap.get(key);
                if (connector == null) {
                    connector = RemoteConnectors.connector(remoteServer.getProtocol(), remoteServer.getAddress(), remoteServer.getUsername(), remoteServer.getPassword(), remoteServer.getProtocolConfig());
                    connectorMap.put(key, connector);
                    if (connector == null) {
                        throw new ConnectionFailureException("Cannot find connector for " + remoteServer.getProtocol() + "://" + remoteServer.getAddress());
                    }
                }
                if (substitutePattern != null) {
                    filename = SedRegexpSubstitute.replace(filename, substitutePattern);
                }

                directory = MessageAnnotationHelper.substituteDirectory(directory);


                String target = protocol + "://" + username + "@" + address + "/" + directory + "/" + filename;

                try {
                    connector.putFile(directory, filename, fileContent);
                    Alerter.clearConnectionError(remoteServer.getProtocol(), remoteServer.getAddress(), Direction.OUTBOUND, connection);
                    traceHandle.getProcessTracer().trace(new DataTrace(messageId, traceHandle.getId(), "outbound_file_staging", outboundId, target, null, ProcessStatus.SUCCESS));
                    transmittedFiles = true;
                    rset.updateBoolean("delivered", true);
                    rset.updateTimestamp("event_time", Timestamp.valueOf(LocalDateTime.now()));
                    rset.updateRow();
                    count++;
                } catch (Exception e) {
                    if (e instanceof SQLException) {
                        throw e;
                    }
                    Alerter.alertConnectionError(remoteServer.getProtocol(), remoteServer.getAddress(), Direction.OUTBOUND, e.getMessage());
                }

                timerCount++;
            }
            if (transmittedFiles) {
                logger.debug("Processed " + count + " out of " + hits + " outbound files: " + new Double(((double) count / (double) hits) * 100).intValue() + "%");
                ProcessStatusHandler.sendWebSocketUpdate(traceHandle.getId(), jobExecutionData.getJobId(), jobExecutionData.getDataType(), jobExecutionData.getEventStep(), ProcessStatus.RUNNING, 0d, ((double) count / (double) hits));
            }
            callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), transmittedFiles ? ProcessStatus.SUCCESS : ProcessStatus.NOOP));
            callbacked = true;
        } catch (SQLException e) {
            String error = "Unable to query or update outbound_distribution";
            logger.error(error, e);
            callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.ERROR, error, ProcessError.DATABASE_PROBLEM));
        } finally {
            for (RemoteConnector connector : connectorMap.values()) {
                connector.close();
            }
            if (!callbacked) {
                callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.ERROR));
            }
        }
    }

}
