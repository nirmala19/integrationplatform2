package se.indiska.integration.framework.connection;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.job.ProcessError;
import se.indiska.integration.framework.util.Counter;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.stream.Collectors;

/**
 * SFTP Connector
 */
public class SFTPConnector extends RemoteConnector {
    private static final Logger logger = LoggerFactory.getLogger(SFTPConnector.class);
    private JSch jsch;
    private Session session = null;
    private ChannelSftp sftpChannel;

    public SFTPConnector(String host, String username, String credentials, Map protocolConfig) {
        super(host, username, credentials, protocolConfig);
        jsch = new JSch();
    }

    public Vector listFiles(String directory) throws JSchException, SftpException {
        return getChannel().ls(directory);
    }

    @Override
    public void fetchFiles(String directory, String pattern, Counter maxFiles, FileReceiver fileReceiver) throws ConnectionFailureException {
        try {
            List<ChannelSftp.LsEntry> entries = (List<ChannelSftp.LsEntry>) getChannel().ls(directory).stream().filter(l -> {
                ChannelSftp.LsEntry lsEntry = (ChannelSftp.LsEntry) l;
                return lsEntry.getFilename().matches(pattern) && maxFiles.reduce() > 0;
            }).collect(Collectors.toList());

            for (ChannelSftp.LsEntry entry : entries) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                getChannel().get(directory + "/" + entry.getFilename(), baos);
                String source = "sftp://" + getHost() + "/" + directory;
                fileReceiver.receiveFile(source, entry.getFilename(), baos.toByteArray());
            }
        } catch (SftpException | JSchException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void putFile(String directory, String filename, String content) throws ConnectionFailureException {
        try {
            String[] pathItems = directory.split("/");
            String path = getChannel().pwd();
            if (directory.startsWith("/")) {
                getChannel().cd("/");
            }
            for (String pathItem : pathItems) {
                if (pathItem.length() > 0) {
                    try {
                        SftpATTRS attrs = getChannel().lstat(pathItem);
                    } catch (SftpException e) {
                        if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
                            getChannel().mkdir(pathItem);
                            // file doesn't exist
                        } else {
                            // something else went wrong
                            throw new ConnectionFailureException("Unable to create directory " + pathItem + " in sftp://" + getHost() + "/" + directory + "/" + filename + ": ", e);
                        }
                    }
                    getChannel().cd(pathItem);
                }
            }

            getChannel().cd(path);
            ByteArrayInputStream bais = new ByteArrayInputStream(content.getBytes(getCharset(filename, Direction.OUTBOUND, StandardCharsets.UTF_8)));
            getChannel().put(bais, directory + "/" + filename);
        } catch (SftpException | JSchException e) {
            throw new ConnectionFailureException("Unable to put file " + directory + "/" + filename + " to " + getUsername() + "@" + getHost(), e);
        }
    }

    @Override
    protected boolean deleteRemoteFile(String directory, String filename) {
        try {
            getChannel().rm(directory + "/" + filename);
            return true;
        } catch (SftpException | JSchException e) {
            logger.error("Unable to delete file sftp://" + getUsername() + "@" + getHost() + "/" + directory + "/" + filename, e);
            return false;
        }
    }

    @Override
    public void close() {
        if (sftpChannel != null) {
            sftpChannel.exit();
        }
        if (session != null) {
            session.disconnect();
        }
    }

    private ChannelSftp getChannel() throws JSchException {
        if (session == null || sftpChannel == null) {
            session = jsch.getSession(getUsername(), getHost());
            session.setConfig("StrictHostKeyChecking", "no");
            String sshKey = (String) getProtocolConfig().get("ssh-key");
            if (sshKey != null) {
                jsch.addIdentity(sshKey);
            } else {
                session.setPassword(getCredentials());
            }
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();

            sftpChannel = (ChannelSftp) channel;
        }
        return sftpChannel;
    }

    public static void main(String[] argv) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Map protocolConfig = mapper.readValue("{ \"ssh-key\" : \"/Users/heintz/.ssh/id_rsa\" }", Map.class);
        SFTPConnector sftpConnector = new SFTPConnector("indio.hk.indiska.se", "andersh", null, protocolConfig);
        Vector v = sftpConnector.listFiles(".");
        System.out.println("List size: "+v.size());
    }
    public static void main2(String[] argv) throws Exception {
        SFTPConnector sftpConnector = new SFTPConnector("shk-oraapp02.hk.indiska.se", "oraias", "P4D4rRwG", new HashMap());
        sftpConnector.fetchFiles("./test_transfer", "POSLog.2013082613.*", new Counter(100), new FileReceiver() {
            @Override
            public void receiveFile(String source, String filename, byte[] content) {
                try {
                    FileOutputStream fos = new FileOutputStream("/tmp/" + filename);
                    fos.write(content);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                logger.debug("Recieved " + filename);
            }

            @Override
            public void receiveFileError(String source, String filename, String error, ProcessError processError) {
                logger.debug("Error recieving " + filename);
            }

            @Override
            public void done() {
                logger.debug("Done!");
            }
        });


        File f = new File("/tmp");
        File[] files = f.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("POSLog");
            }
        });

        for (File file : files) {
            int c = 0;
            byte[] buf = new byte[1024];
            FileInputStream fis = new FileInputStream(file);
            String content = "";
            while ((c = fis.read(buf)) >= 0) {
                content += new String(buf, 0, c);
            }
            sftpConnector.putFile("test_inbound", file.getName(), content);
        }

        for (File file : files) {
            sftpConnector.deleteRemoteFile("test_inbound", file.getName());
        }
        sftpConnector.close();
    }
}
