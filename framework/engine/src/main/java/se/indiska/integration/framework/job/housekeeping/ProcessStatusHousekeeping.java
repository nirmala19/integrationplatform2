package se.indiska.integration.framework.job.housekeeping;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Housekeeping of the process_status table, removing all "NOOP" status records older than 7 days.
 * Created by heintz on 2017-05-09.
 * @deprecated Moved to external script to be able to send all NOOP process status lines to a log file.
 */
@Deprecated
public class ProcessStatusHousekeeping implements Job {
    private static final Logger logger = LoggerFactory.getLogger(ProcessStatusHousekeeping.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
//        String deleteNoopDataTrace = "DELETE FROM process_data_trace WHERE process_id IN (SELECT id FROM process_status WHERE status='NOOP' AND process_status.event_time < NOW() - INTERVAL '14 days')";
//        String sql = "DELETE FROM process_status WHERE status='NOOP' AND process_status.event_time < NOW() - INTERVAL '14 days 10 minute'";
//        String vacuumSql = "VACUUM ANALYZE process_status";
//        boolean paused = true;
//        try (Connection conn = DatasourceProvider.getConnection()) {
//            paused = Settings.isIndioPaused(conn);
//            Settings.setPaused(true, conn);
//            PreparedStatement ps = conn.prepareStatement(deleteNoopDataTrace);
//            ps.executeUpdate();
//            ps.close();
//            ps = conn.prepareStatement(sql);
//            ps.executeUpdate();
//            ps.close();
//            ps = conn.prepareStatement(vacuumSql);
//            ps.executeUpdate();
//            ps.close();
//        } catch (SQLException e) {
//            logger.error("Unable to perform process_status housekeeping!", e);
//        } finally {
//            try {
//                if (!paused) {
//                    Settings.setPaused(false);
//                }
//            } catch (SQLException e) {
//                logger.error("Unable to unpause indio!", e);
//            }
//        }
    }
}
