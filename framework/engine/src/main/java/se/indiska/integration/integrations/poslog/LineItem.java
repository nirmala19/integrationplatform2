package se.indiska.integration.integrations.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by heintz on 27/02/17.
 */
public abstract class LineItem {
    private static final Logger logger = LoggerFactory.getLogger(LineItem.class);
    private Integer sequenceNumber = null;


    public static LineItem lineItem(Element element, File file, String messageId) {
        LineItem item = null;
        Integer sequenceNumber = null;
        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node n = children.item(i);
            if (n instanceof Element) {
//                logger.debug("Checking line item node name " + n.getNodeName()+" in file "+file.getName());
                switch (n.getNodeName()) {
                    case "SequenceNumber":
                        sequenceNumber = new Integer(n.getFirstChild().getNodeValue());
                        break;
                    case "Sale":
                    case "Return":
                        item = new SaleReturn(messageId);
                        item.parse((Element) n, file);
                        break;
                    default:
//                        logger.debug("Unknown line item tag " + n.getNodeName());
                }
            }
        }
        if (item != null) {
            item.setSequenceNumber(sequenceNumber);
        }
        return item;
    }

    abstract void parse(Element element, File file);

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public abstract void store(PreparedStatement ps, String transactionId) throws SQLException;
    public abstract String getInsertSql();

    public abstract String getCreateTableSql();
}
