package se.indiska.integration.framework.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * POJO which contains the information for a job_schedule_trigger.
 * Created by heintz on 2017-04-18.
 */
public class JobScheduleTrigger {
    private static final Logger logger = LoggerFactory.getLogger(JobScheduleTrigger.class);
    private Long id = null;
    private Job job = null;
    private String eventStep = null;
    private String dataType = null;
    private String schedule = null;
    private String jobStatus = null;
    private Date lastUpdated = null;
    private InboundMessageCollector inboundMessageCollector = null;
    private String description;
    private String storeFilenamePattern;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getEventStep() {
        return eventStep;
    }

    public void setEventStep(String eventStep) {
        this.eventStep = eventStep;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public InboundMessageCollector getInboundMessageCollector() {
        return inboundMessageCollector;
    }

    public void setInboundMessageCollector(InboundMessageCollector inboundMessageCollector) {
        this.inboundMessageCollector = inboundMessageCollector;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setStoreFilenamePattern(String storeFilenamePattern) {
        this.storeFilenamePattern = storeFilenamePattern;
    }

    public String getStoreFilenamePattern() {
        return storeFilenamePattern;
    }

}
