package se.indiska.integration.framework.job;

/**
 * Job statuses. Should match content of table JOB_STATUS
 * Created by heintz on 03/04/17.
 */
public enum JobStatus {
    ACTIVE, PAUSED, DISABLED, RUNNING
}
