package se.indiska.integration.connectors;

import com.zaxxer.hikari.HikariDataSource;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.common.Wrapper;
import se.indiska.integration.framework.ExecutorsFactory;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Very simple class to implement an availability monitor routine to check if store POS's BO is online.
 * Created by heintz on 24/03/17.
 */
public class StoreAvailabilityChecker implements Job {
    private static final Logger logger = LoggerFactory.getLogger(StoreAvailabilityChecker.class);
    private String server;
    private String username;
    private String password;
    private String share;
    //    private static HikariDataSource ds = new HikariDataSource();
    private List<Store> storeList = new ArrayList<>();

    public StoreAvailabilityChecker() {

    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        DataSource dataSource = (DataSource) context.getJobDetail().getJobDataMap().get("datasource");
        populateStoreList(dataSource);
        checkStoreBackofficeServers();
    }

    public StoreAvailabilityChecker(String server, String username, String password, String share) {
        this.server = server;
        this.username = username;
        this.password = password;
        this.share = share;
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl("jdbc:postgresql://localhost/indiska2");
        ds.setMinimumIdle(10);
        ds.setMaximumPoolSize(50);
        ds.setUsername("indiska");
        ds.setPassword("indiska");

        populateStoreList(ds);
        checkStoreBackofficeServers();
    }

    private void populateStoreList(DataSource ds) {
        String sql = "select * from store where network_address is not null";
        try (Connection conn = ds.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                storeList.add(new Store(rset.getLong("id"), rset.getString("name"), rset.getString("network_address"), rset.getString("country_code"), rset.getString("timezone")));
            }
            rset.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void checkStoreBackofficeServers() {
        NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, username, password);
        List<Future<Void>> futures = new ArrayList<>();
        Wrapper<Boolean> allGreen = new Wrapper<>(true);
        String wildcard = "POSLog.20170508*";
        Wrapper<Integer> count = new Wrapper<>(0);
        File localDir = new File("/tmp/poslogs/");
        if (!localDir.exists()) {
            localDir.mkdirs();
        }
        logger.debug("Checking " + storeList.size() + " stores");
        for (Store store : storeList) {
            futures.add(ExecutorsFactory.getExecutorService().submit(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    String uri = "smb://" + store.getIpAddress() + "/ExtPgm/Trans/ToHQ/SavePosLog/";
                    if (isReachable(uri)) {
                        File targetDir = new File("/Users/heintz/Projects/Indiska/ICC/testdata/ftp/shop/outbound", store.getId()+"");
                        if (!targetDir.exists()) {
                            targetDir.mkdirs();
                        }
                        SmbFile dir = new SmbFile(uri, auth);

                        int poslogCount = 0, pmlogCount = 0;
                        LocalDateTime now = LocalDateTime.now();
                        now = now.minusMinutes(1);
                        try {
                            SmbFile[] files = dir.listFiles(wildcard);
                            count.set(count.get() + files.length);
                            for (SmbFile f : files) {
                                byte[] buf = new byte[1024];
                                int c = 0;
                                InputStream is = f.getInputStream();
                                File targetFile = new File(targetDir, f.getName());
                                FileOutputStream fos = new FileOutputStream(targetFile);
                                while ((c = is.read(buf)) >= 0) {
                                    fos.write(buf, 0, c);
                                }
                                fos.close();
                                is.close();
                            }
                        } catch (SmbException e) {
                            logger.debug("No files found for " + wildcard + " in store " + store.getName());
                        }
//                        logger.debug("Matching poslogs: " + files.length);
//                        logger.debug("Files left on " + store.getName() + ": " + dir.listFiles().length);
                        allGreen.set(allGreen.get() && true);
                    } else {
//                        Alerter.alert(Alerter.JobEventType.SYSTEM, Alerter.AlertDataType.SYSTEM, Alerter.AlertType.CONNECTION_REFUSED, null, StoreAvailabilityChecker.class.getSimpleName(), "Cannot connect to store " + store.getName() + " (" + store.getIpAddress() + ")");
                        logger.error("Store " + store.getName() + " (" + store.getIpAddress() + ") is not accessible");
                        allGreen.set(allGreen.get() && false);
                    }
                    return null;
                }
            }));
        }
        for (Future<Void> future : futures) {
            try {
                future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        logger.debug("Total number of poslogs matching " + wildcard + ": " + count.get());
        if (!allGreen.get()) {
            logger.debug("Couldn't access all stores!");
//            Alerter.resetAlert(Alerter.JobEventType.SYSTEM, Alerter.AlertDataType.SYSTEM, Alerter.AlertType.CONNECTION_REFUSED, StoreAvailabilityChecker.class.getSimpleName());
        }
    }

    public void list(String directory) throws IOException, ExecutionException, InterruptedException {
        NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, username, password);
        String dirName = directory;
        if (!dirName.endsWith("/")) {
            dirName += "/";
        }


        logger.debug("Checking " + storeList.size() + " backoffice servers");
        String finalDirName = dirName;
        List<Future<Void>> futures = new ArrayList<>();
        for (Store store : storeList) {
            futures.add(ExecutorsFactory.getExecutorService().submit(new Callable<Void>() {

                @Override
                public Void call() throws Exception {
                    String uri = "smb://" + store.getIpAddress() + "/" + share + "/" + finalDirName;
                    if (isReachable(uri)) {
                        SmbFile dir = new SmbFile(uri, auth);
//
//                        int poslogCount = 0, pmlogCount = 0;
//                        LocalDateTime now = LocalDateTime.now();
//                        now = now.minusMinutes(1);
//                        logger.debug("Files left on " + store.getName() + ": " + dir.listFiles().length);
//                        for (SmbFile f : dir.listFiles()) {
//                            if (f.isFile()) {
//                            }
//                            LocalDateTime date =
//                                    LocalDateTime.ofInstant(Instant.ofEpochMilli(f.createTime()), store.getZoneId());
//                            if (date.isBefore(now) && f.isFile()) {
//                                logger.debug(store.getName() + ":\t" + f.getName() + " (" + (f.isDirectory() ? "d" : "f") + ") " + date.toString());
//                            }
//                            if (f.getName().matches("POSLog\\..*")) {
//                                poslogCount++;
//                            } else if (f.getName().matches("PMLog\\..*")) {
//                                pmlogCount++;
//                            }
//                        }
                    } else {
                        logger.error("Server " + store.getIpAddress() + " (" + store.getName() + ") is not reachable!!");
                    }
                    return null;
                }
            }));
        }
        for (Future<Void> future : futures) {
            future.get();
        }
        ExecutorsFactory.getExecutorService().shutdown();
    }

    private boolean isReachable(String uri) {
        int port = 21;
        if (uri.toLowerCase().startsWith("smb:")) {
            port = 139;
        } else if (uri.toLowerCase().startsWith("ftp:")) {
            port = 21;
        } else if (uri.toLowerCase().startsWith("sftp:")) {
            port = 22;
        }
        int hostnameStartPosition = uri.indexOf("://") + "://".length();

        String host = uri.substring(hostnameStartPosition, uri.indexOf("/", hostnameStartPosition + 1));

        try {
            try (Socket soc = new Socket()) {
                soc.connect(new InetSocketAddress(host, port), 1000);
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public static void main(String[] argv) throws Exception {
        long start = System.nanoTime();
        StoreAvailabilityChecker connector = new StoreAvailabilityChecker("172.30.4.30", "Administrator", "flipper", "ExtPgm");

//        connector.list("Trans/ToHQ");
        logger.debug("Time to list content on servers: " + (System.nanoTime() - start) / 1e6d + "ms");
        connector.shutdown();
    }

    private void shutdown() {
        ExecutorsFactory.getExecutorService().shutdown();
    }


    class Store {
        private Long id;
        private String name;
        private String ipAddress;
        private String countryCode;
        private ZoneId zoneId;

        public Store(Long id, String name, String ipAddress, String countryCode, String timezone) {
            this.id = id;
            this.name = name;
            this.ipAddress = ipAddress;
            this.countryCode = countryCode;
            this.zoneId = ZoneId.of(timezone);
        }

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public ZoneId getZoneId() {
            return zoneId;
        }
    }

//    private static String[] servers = {
//            "172.30.4.30", // SWE1
//            "172.30.5.30",// SWE1
//            "172.30.6.30",// SWE1
//            "172.30.7.30",// SWE1
//            "172.30.8.30",// SWE1
//            "172.30.9.30",// SWE1
//            "172.30.11.30",// SWE1
//            "172.30.12.30",// SWE1
//            "172.30.13.30",// SWE1
//            "172.30.15.30",// SWE1
//            "172.30.16.30",// SWE1
//            "172.30.17.30",// SWE1
//            "172.30.18.30",// SWE1
//            "172.30.19.30",// SWE1
//            "172.30.20.30",// SWE1
//            "172.30.21.30",// SWE1
//            "172.30.22.30",// SWE1
//            "172.30.23.30",// SWE1
//            "172.30.24.30", // SWE1
//            "172.30.25.30", //SWE2
//            "172.30.26.30", //SWE2
//            "172.30.28.30", //SWE2
//            "172.30.29.30", //SWE2
//            "172.30.30.30", //SWE2
//            "172.30.31.30", //SWE2
//            "172.30.32.30", //SWE2
//            "172.30.33.30", //SWE2
//            "172.30.36.30", //SWE2
//            "172.30.37.30", //SWE2
//            "172.30.38.30", //SWE2
//            "172.30.39.30", //SWE2
//            "172.30.40.30", //SWE2
//            "172.30.42.30", //SWE2
//            "172.30.43.30", //SWE2
//            "172.30.44.30", //SWE2
//            "172.30.45.30", //SWE2
//            "172.30.46.30", //SWE2
//            "172.30.47.30", //SWE2
//            "172.30.48.30", //SWE2
//            "172.30.49.30", //SWE3
//            "172.30.50.30", //SWE3
//            "172.30.51.30", //SWE3
//            "172.30.53.30", //SWE3
//            "172.30.56.30", //SWE3
//            "172.30.57.30", //SWE3
//            "172.30.58.30", //SWE3
//            "172.30.161.30", //SWE3
//            "172.30.163.30", //SWE3
//            "172.30.164.30", //SWE3
//            "172.30.165.30", //SWE3
//            "172.30.167.30", //SWE3
//            "172.30.168.30", //SWE3
//            "172.30.169.30", //SWE3
//            "172.30.170.30", //SWE3
//            "172.30.171.30", //SWE3
//            "172.30.172.30", //SWE3
//            "172.30.173.30", //SWE3
//            "172.30.174.30", //SWE3
//            "172.30.175.30", //SWE3
//            "172.30.177.30", //SWE3
//            "172.30.178.30", //SWE3
//            "172.30.179.30", //SWE3
//            "172.30.180.30", //SWE3
//            "172.30.181.30", //SWE3
//            "172.30.182.30", //SWE3
//            "172.30.183.30", //SWE3
//            "172.30.184.30", //SWE3
//            "172.30.185.30", //SWE3
//            "172.30.46.30", // Cash
//            "172.30.4.30", // CC
//            "172.30.5.30", // CC
//            "172.30.8.30", // CC
//            "172.30.9.30", // CC
//            "172.30.11.30", // CC
//            "172.30.12.30", // CC
//            "172.30.13.30", // CC
//            "172.30.16.30", // CC
//            "172.30.18.30", // CC
//            "172.30.23.30", // CC
//            "172.30.24.30", // CC
//            "172.30.27.30", // CC
//            "172.30.28.30", // CC
//            "172.30.32.30", // CC
//            "172.30.36.30", // CC
//            "172.30.44.30", // CC
//            "172.30.46.30", // CC
//            "172.30.50.30", // CC
//            "172.30.51.30", // CC
//            "172.30.56.30", // CC
//            "172.30.59.30", // CC
//            "172.30.70.30", // CC
//            "172.30.71.30", // CC
//            "172.30.73.30", // CC
//            "172.30.75.30", // CC
//            "172.30.77.30", // CC
//            "172.30.78.30", // CC
//            "172.30.79.30", // CC
//            "172.30.164.30", // CC
//            "172.30.166.30", // CC
//            "172.30.167.30", // CC
//            "172.30.169.30", // CC
//            "172.30.170.30", // CC
//            "172.30.171.30", // CC
//            "172.30.172.30", // CC
//            "172.30.184.30", // CC
//            "172.30.203.30", // CC
//            "172.30.208.30", // CC
//            "172.30.210.30", // CC
//            "172.30.211.30", // CC
//            "172.30.212.30", // CC
//            "172.30.213.30", // CC
//            "172.30.218.30", // CC
//            "172.30.71.30", // FIN
//            "172.30.73.30", // FIN
//            "172.30.75.30", // FIN
//            "172.30.76.30", // FIN
//            "172.30.77.30", // FIN
//            "172.30.78.30", // FIN
//            "172.30.79.30", // FIN
//            "172.30.80.30", // FIN
//            "172.30.81.30", // FIN
//            "172.30.82.30", // FIN
//            "172.30.81.30", //FIN81
//            "172.30.151.30", //GE
//            "172.30.100.30", //IS
//            "172.30.6.30", //KVITTO
//            "172.30.202.30", // NO
//            "172.30.203.30", // NO
//            "172.30.204.30", // NO
//            "172.30.205.30", // NO
//            "172.30.206.30", // NO
//            "172.30.207.30", // NO
//            "172.30.210.30", // NO
//            "172.30.211.30", // NO
//            "172.30.212.30", // NO
//            "172.30.216.30", // NO
//            "172.30.217.30", // NO
//            "172.30.218.30", // NO
//    };

}
