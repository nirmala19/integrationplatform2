package se.indiska.integration.framework.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.Direction;

public class StoreConnectionException  extends ConnectionFailureException {
    private static final Logger logger = LoggerFactory.getLogger(StoreConnectionException.class);
    private Long storeId = null;
    private Direction direction = null;

    public StoreConnectionException(String message, Long storeId, Direction direction, Throwable cause) {
        super(message, cause);
        this.storeId = storeId;
        this.direction = direction;
    }

    public Long getStoreId() {
        return storeId;
    }

    public Direction getDirection() {
        return direction;
    }
}
