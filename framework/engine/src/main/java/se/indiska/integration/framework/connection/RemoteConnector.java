package se.indiska.integration.framework.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.IndioProperties;
import se.indiska.integration.framework.util.Counter;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract class which defines an interface for a remote file connector.
 * Created by heintz on 04/04/17.
 */
public abstract class RemoteConnector {
    private String host;
    private String username;
    private String credentials;
    private Map protocolConfig = new HashMap();
    private static final Logger logger = LoggerFactory.getLogger(RemoteConnector.class);

    public RemoteConnector(String host, String username, String credentials, Map protocolConfig) {
        this.host = host;
        this.username = username;
        this.credentials = credentials;
        if (protocolConfig != null) {
            this.protocolConfig = protocolConfig;
        }
    }

    /**
     * Fetch file for a specific directory on this host.
     *
     * @param directory    The directory in which we are looking for files.
     * @param pattern      The regular expression to match specific file names.
     * @param maxFiles     The max number of files to retrieve in this call
     * @param fileReceiver File receiver callback.
     * @throws ConnectionFailureException
     */
    public abstract void fetchFiles(String directory, String pattern, Counter maxFiles, FileReceiver fileReceiver) throws ConnectionFailureException;

    /**
     * Put a file to the remote host.
     *
     * @param directory The directory where you want to put the file to.
     * @param filename  The name of the file.
     * @param content   The content of the file.
     * @throws ConnectionFailureException
     */
    public abstract void putFile(String directory, String filename, String content) throws ConnectionFailureException;

    /**
     * Delete a file from remote server.
     *
     * @param directory The directory of the remote system where the file you want to delete is located.
     * @param filename  The name of the file.
     * @return True if the file was successfully deleted.
     */
    protected abstract boolean deleteRemoteFile(String directory, String filename);

    public boolean deleteFile(String directory, String filename) {
        if ("false".equals(IndioProperties.properties().get("shadow.mode"))) {
            return deleteRemoteFile(directory, filename);
        }
        return true;
    }


    protected Charset getCharset(String filename, Direction direction, Charset defaultCharset) {
        Charset result = StandardCharsets.UTF_8;
        if (defaultCharset != null) {
            result = Charset.forName(defaultCharset.name());
        }
        Map protocolConfig = getProtocolConfig();
        if (protocolConfig != null) {
            Object directionConfigO = protocolConfig.get(direction.name());
            if (directionConfigO != null && directionConfigO instanceof Map) {
                Map directionConfig = (Map) directionConfigO;
                Object charsetMapO = directionConfig.get("charsetMap");
                if (charsetMapO != null && charsetMapO instanceof Map) {
                    Map charsetMap = (Map) charsetMapO;
                    for (Object key : charsetMap.keySet()) {
                        if (key != null && key instanceof String) {
                            if (filename.matches((String) key)) {
                                try {
                                    result = Charset.forName((String) charsetMap.get(key));
                                } catch (Exception e) {
                                    logger.warn("Charset " + charsetMap.get(key) + " cannot be used!");
                                }
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Closes the connection to the remote system.
     */
    public abstract void close();


    public String getHost() {
        return host;
    }

    public String getUsername() {
        return username;
    }

    public String getCredentials() {
        return credentials;
    }

    public Map getProtocolConfig() {
        return protocolConfig;
    }

    boolean isReachable(String uri) {
        int port = 21;
        if (uri.toLowerCase().startsWith("smb:")) {
            port = 139;
        } else if (uri.toLowerCase().startsWith("ftp:")) {
            port = 21;
        } else if (uri.toLowerCase().startsWith("sftp:")) {
            port = 22;
        }
        int hostnameStartPosition = uri.indexOf("://") + "://".length();

        String host = uri.substring(hostnameStartPosition, uri.indexOf("/", hostnameStartPosition + 1));

        try {
            try (Socket soc = new Socket()) {
                soc.connect(new InetSocketAddress(host, port), 2500);
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}
