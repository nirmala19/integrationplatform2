package se.indiska.integration.framework.job.store;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by heintz on 2017-05-03.
 */
public class StoreServer {
    private String address;
    private String username;
    private String password;
    private String protocol;
    private Long id;

    public StoreServer(ResultSet rset) throws SQLException {
        this.id = rset.getLong("server_id");
        this.protocol = rset.getString("protocol");
        this.address = rset.getString("address");
        this.username = rset.getString("username");
        this.password = rset.getString("credentials");
    }

    public String getAddress() {
        return address;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getProtocol() {
        return protocol;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StoreServer that = (StoreServer) o;

        if (!getAddress().equals(that.getAddress())) return false;
        if (!getUsername().equals(that.getUsername())) return false;
        if (!getPassword().equals(that.getPassword())) return false;
        if (!getProtocol().equals(that.getProtocol())) return false;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        int result = getAddress().hashCode();
        result = 31 * result + getUsername().hashCode();
        result = 31 * result + getPassword().hashCode();
        result = 31 * result + getProtocol().hashCode();
        result = 31 * result + id.hashCode();
        return result;
    }
}
