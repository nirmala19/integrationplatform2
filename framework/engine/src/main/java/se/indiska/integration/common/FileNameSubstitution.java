package se.indiska.integration.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by heintz on 2017-06-26.
 */
public class FileNameSubstitution {
    private static final Logger logger = LoggerFactory.getLogger(FileNameSubstitution.class);

    public static String substitute(String filename, String substitutePattern) {

        int firstSlashPosition = -1;
        int startPosition = 0;
        int maxTries = 10;

        while (firstSlashPosition < 0 && maxTries-- > 0) {
            firstSlashPosition = substitutePattern.indexOf("/", startPosition);
            if (firstSlashPosition > 0 && substitutePattern.charAt(firstSlashPosition - 1) != '\\') {
                startPosition = firstSlashPosition + 1;
                firstSlashPosition = -1;
            }
        }

        maxTries = 10;
        int secondSlashPosition = -1;
        startPosition = firstSlashPosition + 1;
        while (secondSlashPosition < 0 && maxTries-- > 0) {
            secondSlashPosition = substitutePattern.indexOf("/", startPosition);
            if (secondSlashPosition > firstSlashPosition && substitutePattern.charAt(secondSlashPosition - 1) == '\\') {
                startPosition = secondSlashPosition + 1;
                secondSlashPosition = -1;
            }
        }


        maxTries = 10;
        int thirdSlashPosition = -1;
        startPosition = secondSlashPosition;
        while (thirdSlashPosition < 0 && maxTries-- > 0) {
            thirdSlashPosition = substitutePattern.indexOf("/", startPosition + 1);
            if (thirdSlashPosition > secondSlashPosition && substitutePattern.charAt(thirdSlashPosition - 1) == '\\') {
                startPosition = thirdSlashPosition + 1;
                thirdSlashPosition = -1;
            }
        }

        String matchPattern = substitutePattern.substring(firstSlashPosition + 1, secondSlashPosition);
        String substitution = substitutePattern.substring(secondSlashPosition + 1, thirdSlashPosition);

        Pattern pattern = Pattern.compile(matchPattern);
        Matcher matcher = pattern.matcher(filename);

        MatchResult matchResult = matcher.toMatchResult();

        return matcher.replaceAll(substitution);
    }

    public static void main(String[] argv) throws Exception {
        String filename = "017-12-23-23.029374029374.203948.xml";
        String pattern = "/(.+)/bl$1/";
        String result = FileNameSubstitution.substitute(filename, pattern);
        logger.debug("Result: '" + result + "'");
    }
}
