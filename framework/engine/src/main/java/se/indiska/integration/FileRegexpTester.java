package se.indiska.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by heintz on 2017-04-11.
 */
public class FileRegexpTester {
    private static final Logger logger = LoggerFactory.getLogger(FileRegexpTester.class);

    public static void main(String[] argv) throws Exception {
//        File dir = new File("/Users/heintz/Projects/Indiska/ICC/testdata/ftp/shop/outbound");
//        File[] files = dir.listFiles(new FilenameFilter() {
//            @Override
//            public boolean accept(File dir, String name) {
//                return name.matches("HQ.*?\\.07\\..*");
//            }
//        });
//
//        for (File f : files) {
//            logger.debug(f.getName());
//        }

//        String filename = ;
//        logger.debug("Replace: " + filename.replaceAll(regexp, "\\1"));

        String regex = "HQ.*?\\.01\\.(.+?)\\..*";
        String input = "HQ20170315201915.01.00901.362716";
        Pattern p = Pattern.compile(regex); // java.util.regex.Pattern
        Matcher m = p.matcher(input); // java.util.regex.Matcher
//        logger.debug("" + m.find());
        if(m.find()) { // use while loop for multiple occurrences
            logger.debug(m.group(1));
//            String param2 = m.group(5);

            // process the result...
        }
    }
}
