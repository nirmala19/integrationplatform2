package se.indiska.integration.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple Object wrapper to use when needing to update from a callback or similar where you cannot
 * use normal variables.
 * <p>
 * Created by heintz on 31/03/17.
 */
public class Wrapper<T> {
    private static final Logger logger = LoggerFactory.getLogger(Wrapper.class);
    private T object;

    /**
     * Constructs a new Wrapper
     *
     * @param object The Object that should be wrapped.
     */
    public Wrapper(T object) {
        this.object = object;
    }

    /**
     * Get the wrapped object
     *
     * @return
     */
    public T get() {
        return object;
    }

    /**
     * Set the wrapped object
     *
     * @param object The new value for this wrapper.
     */
    public void set(T object) {
        this.object = object;
    }

    @Override
    public String toString() {
        return object == null ? null : object.toString();
    }
}
