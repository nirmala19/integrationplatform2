package se.indiska.integration.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.indiska.integration.framework.DatasourceProvider;

import java.sql.*;
import java.util.*;

/**
 * Created by heintz on 03/04/17.
 */
@RestController
@RequestMapping("/crud")
public class Crud extends RestService {
    private static final Logger logger = LoggerFactory.getLogger(Crud.class);
    private static Map<String, EntityMapping> entitiesMap = new HashMap<>();
    private static Map<String, String> reverseEntityMap = new HashMap<>();

    static {
        addMapping("jobs", "job", EntityMapping.IdType.STRING, "Jobs", "id");
        addMapping("schedules", "job_schedule_triggers", EntityMapping.IdType.LONG, "Schedule", "job_id", "data_type");
        addMapping("events", "job_event_triggers", EntityMapping.IdType.LONG, "Event-based jobs", "event_step", "data_type");
        addMapping("eventTypes", "job_event_type", EntityMapping.IdType.STRING, "Event types", "id");
        addMapping("jobStatus", "job_status", EntityMapping.IdType.STRING, "Job statuses", "id");
        addMapping("dataTypes", "data_type", EntityMapping.IdType.STRING, "Data types", "id");
        addMapping("remoteServers", "remote_server", EntityMapping.IdType.LONG, "Remote servers", "name");
        addMapping("remoteServer", "remote_server", EntityMapping.IdType.LONG, "Remote server Update", "name");
        addMapping("remoteLocations", "remote_location", EntityMapping.IdType.LONG, "Remote locations", "directory", "description");
        addMapping("inboundMessageCollectors", "inbound_message_collector", EntityMapping.IdType.LONG, "Inbound message collectors", "filename_pattern");
    }

    private static void addMapping(String entity, String table, EntityMapping.IdType idType, String description, String... descriptionColumn) {
        entitiesMap.put(entity, new EntityMapping(table, idType, description, descriptionColumn));
        reverseEntityMap.put(table, entity);
    }

    @RequestMapping(value = "/model", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity getModels() {
        Map<String, Map<String, Object>> result = new HashMap<>();
        try (Connection conn = DatasourceProvider.getConnection()) {
            for (String key : entitiesMap.keySet()) {
                Map<String, Object> item = new HashMap<>();
                item.put("name", entitiesMap.get(key).getDescription());
                item.put("model", modelData(entitiesMap.get(key), conn));
                result.put(key, item);
            }
        } catch (SQLException e) {
            logger.error("Unable to get model ", e);
            return ResponseEntity.status(500).body("Unable to get model");
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/model/{entity}", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity getModel(@PathVariable("entity") String entity) {
        long start = System.nanoTime();
        EntityMapping mapping = entitiesMap.get(entity);
        if (mapping != null) {
            try (Connection conn = DatasourceProvider.getConnection()) {
                return ResponseEntity.ok(modelData(mapping, conn));
            } catch (SQLException e) {
                logger.error("Unable to get model for entity " + entity, e);
                return ResponseEntity.badRequest().body("Unable to get model for entity " + entity);
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    private Map<String, Object> modelData(EntityMapping mapping, Connection conn) throws SQLException {
        DatabaseMetaData metaData = conn.getMetaData();
        ResultSet rset = metaData.getColumns(null, null, mapping.getEntity(), null);
        int count = rset.getMetaData().getColumnCount();
        Map<String, Object> modelData = new LinkedHashMap<>();
        while (rset.next()) {
            String name = rset.getString("COLUMN_NAME");
            String type = rset.getString("TYPE_NAME");
            boolean nullable = !rset.getString("IS_NULLABLE").equalsIgnoreCase("NO");
            Map<String, Object> field = new LinkedHashMap<>();
            field.put("nullable", nullable);
            field.put("type", type);
            field.put("length", rset.getInt("COLUMN_SIZE"));
            modelData.put(name, field);
        }
        modelData.put("descriptiveColumns", mapping.getDescriptionColumns());
        modelData.put("modelDescription", mapping.getDescription());
        additionalInfo(mapping.getEntity(), conn, modelData);
        return modelData;
    }

    @RequestMapping(value = "/{entity}", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity getAll(@PathVariable("entity") String entity) {
        EntityMapping mapping = entitiesMap.get(entity);
        if (mapping != null) {
            try (Connection conn = DatasourceProvider.getConnection()) {
                String sql = "select * from " + mapping.getEntity();
                if (mapping.getDescriptionColumns() != null && mapping.getDescriptionColumns().length > 0) {
                    sql += " order by ";
                    Iterator<String> orderByColsIterator = Arrays.asList(mapping.getDescriptionColumns()).iterator();
                    while (orderByColsIterator.hasNext()) {
                        String col = orderByColsIterator.next();
                        sql += col + " asc";
                        if (orderByColsIterator.hasNext()) {
                            sql += ", ";
                        }
                    }
                }

                PreparedStatement ps = conn.prepareStatement(sql);
                ResultSet rset = ps.executeQuery();
                List<Map> result = new ArrayList<>();
                while (rset.next()) {
                    Map row = new LinkedHashMap<>();
                    int columnCount = rset.getMetaData().getColumnCount();
                    for (int i = 1; i <= columnCount; i++) {
                        row.put(rset.getMetaData().getColumnLabel(i), rset.getObject(i));
                    }
                    result.add(row);
                }
                return ResponseEntity.ok(result);
            } catch (SQLException e) {
                logger.error("Oh oh, exception!", e);
                return ResponseEntity.status(500).body("Unable to get all items " + e.getMessage());
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/{entity}", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity save(@PathVariable("entity") String entity, @RequestBody Map<String, Object> value) {
        EntityMapping mapping = entitiesMap.get(entity);
        logger.debug("Saving entity " + entity);
        if (mapping != null) {
            try (Connection conn = DatasourceProvider.getConnection()) {

                String sql = "select * from " + mapping.getEntity();
                if (value.get("id") != null) {
                    sql += " where id=?";
                } else {
                    sql += " limit 0";
                }
                PreparedStatement ps = conn.prepareStatement(sql);
//                int j = 1;
                if (value.get("id") != null) {
                    ps.setInt(1, (Integer) value.get("id"));
                }
                ResultSet rset = ps.executeQuery();
                int columnCount = rset.getMetaData().getColumnCount();

                List<String> columns = new ArrayList<>();
                for (int i = 1; i <= columnCount; i++) {
                    String columnLabel = rset.getMetaData().getColumnLabel(i);
                    if (!columnLabel.equalsIgnoreCase("id")) {
                        columns.add(columnLabel);
                    }
                }

                if (rset.next()) {
                    // Update
                    sql = "update " + mapping.getEntity() + " set ";
                    Iterator<String> cols = columns.iterator();
                    while (cols.hasNext()) {
                        String col = cols.next();
                        sql += col + "=?";
                        if (cols.hasNext()) {
                            sql += ", ";
                        }
                    }
                    sql += " where id=?";
                    ps = conn.prepareStatement(sql);
                    cols = columns.iterator();
                    int i = 1;
                    while (cols.hasNext()) {
                        String col = cols.next();
                        ps.setObject(i++, value.get(col));
                    }
                    ps.setInt(i, (Integer) value.get("id"));
                    ps.executeUpdate();
                } else {
                    sql = "insert into " + mapping.getEntity() + " (";
                    String qmarks = "";
                    Iterator<String> cols = columns.iterator();
                    while (cols.hasNext()) {
                        String col = cols.next();
                        sql += col;
                        qmarks += "?";
                        if (cols.hasNext()) {
                            sql += ", ";
                            qmarks += ", ";
                        }
                    }
                    sql += ") values (" + qmarks + ")";
                    ps = conn.prepareStatement(sql);
                    cols = columns.iterator();
                    int i = 1;
                    while (cols.hasNext()) {
                        ps.setObject(i++, value.get(cols.next()));
                    }
                    ps.executeUpdate();
                    // Insert
                }
//                List<Map> result = new ArrayList<>();
//                while (rset.next()) {
//                    result.add(row);
//                }
                return ResponseEntity.ok().build();
            } catch (SQLException e) {
                logger.error("Oh oh, exception!", e);
                return ResponseEntity.status(500).body("Unable to get all items " + e.getMessage());
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @RequestMapping(value = "/{entity}/{id}", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity getOne(@PathVariable("entity") String entity, @PathVariable("id") Long id) {
        EntityMapping mapping = entitiesMap.get(entity);
        if (mapping != null) {
            try (Connection conn = DatasourceProvider.getConnection()) {
                String sql = "select * from " + mapping.getEntity() + " where id=?";
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setLong(1, id);
                ResultSet rset = ps.executeQuery();
                Map result = null;
                if (rset.next()) {
                    result = new LinkedHashMap<>();
                    int columnCount = rset.getMetaData().getColumnCount();
                    for (int i = 1; i <= columnCount; i++) {
                        result.put(rset.getMetaData().getColumnLabel(i), rset.getObject(i));
                    }
                }
                return ResponseEntity.ok(result);
            } catch (SQLException e) {
                logger.error("Oh oh, exception!", e);
                return ResponseEntity.badRequest().body("Unable to get all items " + e.getMessage());
            }
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    private void additionalInfo(String table, Connection conn, Map<String, Object> info) throws SQLException {

        String sql = "SELECT\n" +
                "  cols.table_name,\n" +
                "  cols.column_name,\n" +
                "  cols.is_nullable,\n" +
                "  cols.character_maximum_length,\n" +
                "  cols.udt_name,\n" +
                "  keys.foreign_table_name,\n" +
                "  keys.foreign_column_name,\n" +
                "  (\n" +
                "    SELECT pg_catalog.col_description(c.oid, cols.ordinal_position :: INT)\n" +
                "    FROM\n" +
                "      pg_catalog.pg_class c\n" +
                "    WHERE\n" +
                "      c.oid = (SELECT ('\"' || cols.table_name || '\"') :: REGCLASS :: OID)\n" +
                "      AND c.relname = cols.table_name\n" +
                "  ) AS column_comment\n" +
                "FROM information_schema.columns cols\n" +
                "  LEFT JOIN\n" +
                "  (SELECT\n" +
                "     tc.constraint_name,\n" +
                "     tc.table_name,\n" +
                "     kcu.column_name,\n" +
                "     ccu.table_name  AS foreign_table_name,\n" +
                "     ccu.column_name AS foreign_column_name\n" +
                "   FROM\n" +
                "     information_schema.table_constraints AS tc\n" +
                "     JOIN information_schema.key_column_usage AS kcu\n" +
                "       ON tc.constraint_name = kcu.constraint_name\n" +
                "     JOIN information_schema.constraint_column_usage AS ccu\n" +
                "       ON ccu.constraint_name = tc.constraint_name\n" +
                "   WHERE constraint_type = 'FOREIGN KEY' AND tc.table_name = ?) keys\n" +
                "    ON keys.table_name = cols.table_name AND keys.column_name = cols.column_name\n" +
                "WHERE cols.table_name = ?;";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, table);
        ps.setString(2, table);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            Map<String, Object> columnInfo = (Map<String, Object>) info.get(rset.getString("column_name"));
            if (columnInfo != null) {

                columnInfo.put("references_entity", reverseEntityMap.get(rset.getString("foreign_table_name")));
                columnInfo.put("references_table", rset.getString("foreign_table_name"));
                columnInfo.put("references_entity_field", rset.getString("foreign_column_name"));
                columnInfo.put("description", rset.getString("column_comment"));
            }
        }
    }

    private static class EntityMapping {
        enum IdType {LONG, STRING}

        private String entity;
        private IdType idType;
        private String description;
        private String[] descriptionColumns;

        public EntityMapping(String entity, IdType idType, String description, String[] descriptionColumns) {
            this.entity = entity;
            this.idType = idType;
            this.description = description;
            this.descriptionColumns = descriptionColumns;
        }

        public String getEntity() {
            return entity;
        }

        public IdType getIdType() {
            return idType;
        }

        public String getDescription() {
            return description;
        }

        public String[] getDescriptionColumns() {
            return descriptionColumns;
        }
    }
}