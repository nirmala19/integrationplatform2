package se.indiska.integration.framework.job;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import se.indiska.integration.common.Wrapper;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.IndioProperties;
import se.indiska.integration.framework.connection.ConnectionFailureException;
import se.indiska.integration.framework.connection.FileReceiver;
import se.indiska.integration.framework.connection.RemoteConnector;
import se.indiska.integration.framework.connection.RemoteConnectors;
import se.indiska.integration.framework.datatyperesolver.ContentDatatypeResolver;
import se.indiska.integration.framework.datatyperesolver.ContentDatatypeResolverConfig;
import se.indiska.integration.framework.datatyperesolver.ContentDatatypeResolverFactory;
import se.indiska.integration.framework.trace.DataTrace;
import se.indiska.integration.framework.util.Alerter;
import se.indiska.integration.framework.util.Counter;
import se.indiska.integration.framework.util.InboundFileUtilities;

import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.google.common.cache.RemovalCause.EXPLICIT;
import static com.google.common.cache.RemovalCause.REPLACED;

/**
 * Job which periodically (based on it's cron schedule in job_schedule_trigger) retrieves inbound and inserts into
 * the <code>inbound_file_staging</code> table and writes the file to archive directory.
 * Created by heintz on 04/04/17.
 */
@IdentifiableIndioJob(identifier = "INBOUND_MESSAGE_COLLECTOR", eventStep = "INBOUND_TRANSFER", description = "Collects messages from sources")
public class InboundMessageCollectorJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(InboundMessageCollectorJob.class);
    private static final Integer runningCollectorsTimeout = 10;
    //    private static Set<Long> runningCollectors = ConcurrentHashMap.newKeySet();
    private static Cache<Long, Boolean> runningCollectors = CacheBuilder.newBuilder()
            .expireAfterWrite(runningCollectorsTimeout, TimeUnit.MINUTES)
            .removalListener(new RemovalListener<Long, Boolean>() {
                @Override
                public void onRemoval(RemovalNotification<Long, Boolean> removalNotification) {
                    if (removalNotification.getCause() != EXPLICIT && removalNotification.getCause() != REPLACED) {
                        logger.error("Collector with id " + removalNotification.getKey() + " was evicted with cause " + removalNotification.getCause() + ". (Timeout is " + runningCollectorsTimeout + " minutes)");
                    }
                }
            }).maximumSize(200)
            .build();
    private static ObjectMapper mapper = new ObjectMapper();

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        if (logger.isTraceEnabled()) {
            logger.trace("Executing InboundMessageCollectorJob for " + jobExecutionData.getDataType());
        }


        ContentDatatypeResolver contentDatatypeResolver = null;
        Long messageCollectorId = null;
        long start = System.nanoTime();
        Wrapper<Integer> numOfFilesRetrieved = new Wrapper<>(0);
        Wrapper<ProcessError> processError = new Wrapper<>(null);
        boolean wasAlreadyRunning = false;
        ProcessStatus result = ProcessStatus.ERROR;
        List<String> errorMessages = new ArrayList<>();
        Wrapper<Boolean> didReceiveFiles = new Wrapper<>(false);
        Wrapper<Boolean> remainingFiles = new Wrapper<>(false);

        String sql = "SELECT\n" +
                "  imc.id  imc_id,\n" +
                "  rs.id  remote_server_id,\n" +
                "  rs.protocol,\n" +
                "  rs.protocol_config,\n" +
                "  rs.address,\n" +
                "  rs.username,\n" +
                "  rs.password,\n" +
                "  rl.id  remote_location_id,\n" +
                "  rl.directory,\n" +
                "  imc.file_pattern,\n" +
                "  imc.content_datatype_resolver\n" +
                "FROM inbound_message_collector imc\n" +
                "  JOIN remote_location rl ON imc.remote_location = rl.id\n" +
                "  JOIN remote_server rs ON imc.remote_server = rs.id\n" +
                "WHERE imc.id = ?\n" +
                "ORDER BY rs.id, rl.directory";
        Map<String, RemoteServer> remoteServerMap = new HashMap<>();

        try {

            try (Connection connection = DatasourceProvider.getConnection()) {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setInt(1, (Integer) jobExecutionData.getDataMap().get("inbound_message_collector"));
//            logger.debug("Getting " + dataType + " files for imc "+dataMap.getIntValue("inbound_message_collector"));

                ResultSet rset = ps.executeQuery();
                List<String> errors = new ArrayList<>();


                if (rset.next()) {
                    synchronized (runningCollectors) {
                        messageCollectorId = rset.getLong("imc_id");
                        if (runningCollectors.getIfPresent(messageCollectorId) == null) {
                            if (logger.isTraceEnabled()) {
                                logger.trace("Caching message collector id " + messageCollectorId + " for process " + traceHandle.getId());
                            }
                            runningCollectors.put(messageCollectorId, true);
                        } else {
                            if (logger.isTraceEnabled()) {
                                logger.trace("Collector for " + messageCollectorId + " is already running");
                            }
                            callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.NOOP_DUPLICATE_JOB));
                            return;
                        }
                    }


                    String address = rset.getString("address");
                    if (address.indexOf("{") > 0 && address.indexOf("}") > 0) {
                        String annotation = address.substring(address.indexOf("{") + 1, address.indexOf("}"));
                        for (MessageAnnotationHelper.Annotation substitute : MessageAnnotationHelper.substitutions(annotation, null, connection)) {
                            String addr = address.replace("{" + annotation + "}", substitute.getValue());
                            RemoteServer remoteServer = remoteServerMap.get(addr);
                            if (remoteServer == null) {
                                remoteServer = new RemoteServer(rset, connection);
                                remoteServerMap.put(addr, remoteServer);
                                remoteServer.getMetadata().put(annotation + ".key", substitute.getKey());
                                remoteServer.getMetadata().put(annotation + ".value", substitute.getValue());
                            }

                        }
                    } else {
                        RemoteServer remoteServer = new RemoteServer(rset, connection);
                        remoteServerMap.put(address, remoteServer);
                    }

                    ResultSetMetaData metaData = rset.getMetaData();
                    for (int i = 1; i <= metaData.getColumnCount(); i++) {
                        if (metaData.getColumnLabel(i).equals("content_datatype_resolver")) {
                            String contentDatatypeResolverJson = rset.getString("content_datatype_resolver");
                            ContentDatatypeResolverConfig contentDatatypeResolverConfig = null;
                            if (contentDatatypeResolverJson != null && contentDatatypeResolverJson.length() > 3) {
                                try {
                                    contentDatatypeResolverConfig = mapper.readValue(contentDatatypeResolverJson, ContentDatatypeResolverConfig.class);
                                    contentDatatypeResolver = ContentDatatypeResolverFactory.newResolver(contentDatatypeResolverConfig);
                                } catch (IOException | XPathExpressionException e) {
                                    throw new ConfigurationErrorException("Unable to create content datatype resolver!", e);
                                }
                            }
                        }
                    }

                } else {
                    if (logger.isTraceEnabled()) {
                        logger.trace("Couldn't find inbound message collector for " + traceHandle.getId());
                    }
                }
                rset.close();
                ps.close();

            }
            if (logger.isTraceEnabled()) {
                logger.trace("Fetching data from " + remoteServerMap.values().size() + " servers ");
            }
            for (RemoteServer remoteServer : remoteServerMap.values()) {

                RemoteConnector connector = null;

                connector = RemoteConnectors.connector(
                        remoteServer.getProtocol(),
                        remoteServer.getAddress(),
                        remoteServer.getUsername(),
                        remoteServer.getPassword(),
                        remoteServer.getProtocolConfig()
                );

                if (logger.isTraceEnabled()) {
                    logger.trace("Connecting to " + remoteServer.getId() + ":" + remoteServer.getAddress() + " using protocol " + remoteServer.getProtocol());
                }
                RemoteConnector finalConnector = connector;
                for (RemoteDirectory remoteDirectory : remoteServer.getRemoteDirectories().values()) {
                    try {
                        long s = System.nanoTime();
                        Counter maxFiles = new Counter(4000);

                        ContentDatatypeResolver finalContentDatatypeResolver = contentDatatypeResolver;
                        connector.fetchFiles(remoteDirectory.getDirectory(), remoteDirectory.getFilePattern(), maxFiles, new FileReceiver() {
                            private List<InboundFileUtilities.InboundFile> inboundFiles = new ArrayList<>();

                            @Override
                            public void receiveFile(String source, String filename, byte[] content) {
                                String newFilename = filename;
                                if (finalContentDatatypeResolver != null) {
                                    boolean match = false;
                                    try {
                                        match = finalContentDatatypeResolver.match(content);
                                    } catch (IOException | SAXException | XPathExpressionException e) {
                                        logger.error("Unable to match content according to content datatype resolver!", e);
                                    }
                                    if (!match) {
                                        return;
                                    }
                                }
                                File archiveDir = new File(IndioProperties.properties().getProperty("archive_directory") + "/inbound/" + jobExecutionData.getDataType().toLowerCase());
                                String messageUuid = UUID.randomUUID().toString();
                                Long generatedId = null;
                                numOfFilesRetrieved.set(numOfFilesRetrieved.get() + 1);
                                Map<String, String> metadata = new HashMap<>();
                                metadata.putAll(remoteDirectory.getMetadata());
                                metadata.putAll(remoteServer.getMetadata());
                                metadata.put("remote_server", remoteServer.getId().toString());
                                metadata.put("remote_location", remoteDirectory.getId().toString());
                                try {
                                    InboundFileUtilities.InboundFile inboundFile = new InboundFileUtilities.InboundFile(jobExecutionData.getDataType(), source, filename, content, archiveDir, new Date(), messageUuid, metadata, (String) remoteServer.getProtocolConfig().get("charset"));
                                    inboundFiles.add(inboundFile);
                                } catch (IOException e) {
                                    logger.error("Unable to receive file!!", e);
                                }

                            }

                            @Override
                            public void receiveFileError(String source, String filename, String error, ProcessError processError) {
                                String messageUuid = UUID.randomUUID().toString();
                                logger.error("[" + traceHandle.getId() + "]: Failed to retrieve file " + filename + " " + error);
                                traceHandle.getProcessTracer().trace(new DataTrace(messageUuid, traceHandle.getId(), source, null, "inbound_file_staging", (String) null, ProcessStatus.ERROR, error, null, processError));
                            }

                            @Override
                            public void done() {
                                try {
                                    Map<String, Long> updates = null;
                                    try (Connection connection = DatasourceProvider.getConnection()) {
                                        updates = InboundFileUtilities.insertFiles(connection, inboundFiles.toArray(new InboundFileUtilities.InboundFile[inboundFiles.size()]));
                                        List<DataTrace> traces = new ArrayList<>();
                                        Map<String, Map<String, String>> metadatas = new HashMap<>();
                                        for (InboundFileUtilities.InboundFile inboundFile : inboundFiles) {
                                            Long generatedId = updates.get(inboundFile.getMessageUuid());
                                            if (generatedId != null) {
                                                traces.add(new DataTrace(inboundFile.getMessageUuid(), traceHandle.getId(), inboundFile.getSource(), null, "inbound_file_staging", generatedId, ProcessStatus.SUCCESS));
                                                metadatas.put(inboundFile.getMessageUuid(), inboundFile.getMetadata());
                                            }
                                        }
                                        traceHandle.getProcessTracer().trace(traces.toArray(new DataTrace[traces.size()]));
                                        InboundFileUtilities.insertMetadata(metadatas, connection);
                                    }
                                    for (InboundFileUtilities.InboundFile inboundFile : inboundFiles) {
                                        if (!finalConnector.deleteFile(remoteDirectory.getDirectory(), inboundFile.getFileName())) {
                                            Long generatedId = updates.get(inboundFile.getMessageUuid());
                                            traceHandle.getProcessTracer().trace(new DataTrace(inboundFile.getMessageUuid(), traceHandle.getId(), inboundFile.getSource(), null, "inbound_file_staging", generatedId, ProcessStatus.ERROR, "Unable to delete file", null, ProcessError.REMOTE_FILE_DELETE_FAILED));
                                        }
                                    }
                                    didReceiveFiles.set(updates.values().stream().filter(id -> id != null).findFirst().isPresent());
                                } catch (SQLException e) {
                                    logger.error("SQLException", e);
                                } catch (IOException e) {
                                    logger.error("IOException", e);
                                }
                            }
                        });
//                        logger.debug("Time to fetch files " + (System.nanoTime() - s) / 1e6d);
                        boolean notDone = maxFiles.get() <= 0;
                        remainingFiles.set(remainingFiles.get() || notDone);
                        Alerter.clearConnectionError(remoteServer.getProtocol(), remoteServer.getAddress(), Direction.INBOUND);
                    } catch (ConnectionFailureException e) {

                        String error = "Unable to connect to remote server! ";
                        logger.error(error, e);
//                        processError.set(ProcessError.CONNECTION_FAILURE);
                        Alerter.alertConnectionError(remoteServer.getProtocol(), remoteServer.getAddress(), Direction.INBOUND, e.getMessage());
                    } finally {
                        if (connector != null) {
                            connector.close();
                        }
                    }
                }


            }

            result = numOfFilesRetrieved.get() > 0 ? ProcessStatus.SUCCESS : ProcessStatus.NOOP;
//        } catch (SQLException e) {
//            String error = "Unable to execute job! ";
//            logger.error(error, e);
//            errorMessages.add(error + e.getMessage());
//            result = ProcessStatus.ERROR;
//            processError.set(ProcessError.DATABASE_PROBLEM);

            Optional<String> errorMessage = errorMessages.stream().reduce((a, b) -> a + "\n" + b);
            if (numOfFilesRetrieved.get() > 0) {
                long tta = System.nanoTime() - start;
                logger.debug("[" + jobExecutionData.getProcessId() + "] Time to retrieve " + numOfFilesRetrieved + " remote " + jobExecutionData.getDataType() + " files: " + tta / 1e6d + " ms (" + tta / (double) numOfFilesRetrieved.get() / 1e6d + " ms/file)");
            }
            ProcessExecutionResult per = new ProcessExecutionResult(traceHandle.getId(), result, errorMessage.orElse(null), processError.get());
            per.setCompletelyDone(!remainingFiles.get());
            callback.jobComplete(per);
        } finally {
            if (messageCollectorId != null) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Removing message collector semaphore for " + messageCollectorId);
                }

                runningCollectors.invalidate(messageCollectorId);
            }
        }
    }


}
