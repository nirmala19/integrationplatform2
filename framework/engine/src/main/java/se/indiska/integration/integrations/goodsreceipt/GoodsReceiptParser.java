package se.indiska.integration.integrations.goodsreceipt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.job.IdentifiableIndioJob;
import se.indiska.integration.framework.general.DateTimeConverter;
import se.indiska.integration.framework.general.IntegerConverter;
import se.indiska.integration.framework.general.LineParser;
import se.indiska.integration.framework.general.FlatFileLoader;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by heintz on 2017-06-02.
 */
@IdentifiableIndioJob(identifier = "GOODS_RECEIPT_STAGE_LOAD", eventStep = "STAGE_LOAD", description = "Stage load of goods receipt for processing/checking reports")
public class GoodsReceiptParser extends FlatFileLoader {
    private static final Logger logger = LoggerFactory.getLogger(GoodsReceiptParser.class);
    private Map<String, LineParser> lineParserMap = new HashMap<>();

    public GoodsReceiptParser() {
        LineParser line132Parser = new LineParser("goods_receipt", false);
        line132Parser.addInterval("ARRIVED_ID", 0, 9, null, false);
//        line132Parser.addInterval("STORE", 9, 4, new IntegerConverter(), true);
        line132Parser.addInterval("ORDERNR", 19, 9, null, false);
        line132Parser.addInterval("ARRIVED_DATE", 35, 12, new DateTimeConverter("yyyyMMddHHmm"));
        line132Parser.addInterval("TYPE", 47, 4, new IntegerConverter());
        line132Parser.addInterval("ARRIVED_QUANTITY", 51, 10, new IntegerConverter());
        line132Parser.addInterval("UPDATED_DATE", 61, 14, new DateTimeConverter("yyyyMMddHHmmss"));

        addParser("132", line132Parser);
    }
}
