package se.indiska.integration.framework.job;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by heintz on 2017-04-28.
 */
public class StoreConnectionInfo {
    private String countryCode;
    private String timezone;
    private String path;
    private Long storeId;
    private Long connectionDetailsId;

    public StoreConnectionInfo(ResultSet rset) throws SQLException {
        this.countryCode = rset.getString("country_code");
        this.timezone = rset.getString("timezone");
        this.path = rset.getString("path");
        this.storeId = rset.getLong("store_id");
        this.connectionDetailsId = rset.getLong("connection_detail_id_id");
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getPath() {
        return path;
    }

    public Long getStoreId() {
        return storeId;
    }

    public Long getConnectionDetailsId() {
        return connectionDetailsId;
    }

}