package se.indiska.integration.framework.job;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.common.Wrapper;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.ExecutorsFactory;
import se.indiska.integration.framework.IndioProperties;
import se.indiska.integration.framework.connection.ConnectionFailureException;
import se.indiska.integration.framework.connection.FileReceiver;
import se.indiska.integration.framework.connection.RemoteConnector;
import se.indiska.integration.framework.connection.RemoteConnectors;
import se.indiska.integration.framework.job.store.StoreServer;
import se.indiska.integration.framework.trace.DataTrace;
import se.indiska.integration.framework.util.Alerter;
import se.indiska.integration.framework.util.Counter;
import se.indiska.integration.framework.util.InboundFileUtilities;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Callable;

/**
 * Created by heintz on 2017-04-20.
 */
@DisallowConcurrentExecution
@IdentifiableIndioJob(identifier = "STORE_FILE_COLLECTOR", eventStep = "INBOUND_TRANSFER", description = "Collects data from the stores defined in the database table public.store")
public class StoreCollectorJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(StoreCollectorJob.class);
    private static Set<String> runningCollectors = new HashSet<>();
    private static final int MAX_FILES_PER_RUN = 2500;
    private static final int MAX_FILES_PER_STORE = 100;

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        Wrapper<Boolean> atLeastOneStoreFullFetch = new Wrapper<>(false);
        long start = System.nanoTime();
        String id = UUID.randomUUID().toString();
        String sql = "SELECT DISTINCT\n" +
                "  s.id  store_id,\n" +
                "  s.country_code,\n" +
                "  s.source_connection_details connection_detail_id_id,\n" +
                "  ss.id server_id,\n" +
                "  s.timezone,\n" +
                "  ss.address,\n" +
                "  ss.username,\n" +
                "  ss.credentials,\n" +
                "  scd.protocol,\n" +
                "  scd.path\n" +
                "FROM store s\n" +
                "  JOIN store_connection_details scd\n" +
                "    ON s.source_connection_details = scd.id\n" +
                "  JOIN store_server ss\n" +
                "    ON scd.server = ss.id\n" +
                "WHERE s.active = TRUE";
        Counter numOfFilesRetrieved = new Counter();
        Counter storesFetchedFrom = new Counter();
        Wrapper<Boolean> didReceiveFiles = new Wrapper<>(false);
        List<ListenableFuture> storeCollectorFutures = new ArrayList<>();
        String filenamePattern = (String) jobExecutionData.getDataMap().get("filename_pattern");
        Map<StoreServer, Collection<StoreConnectionInfo>> connectionInfoMap = new HashMap<>();

        Counter storeCounter = new Counter();

        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                storeCounter.add();
                StoreServer storeServer = new StoreServer(rset);
                Collection<StoreConnectionInfo> connectionInfos = connectionInfoMap.get(storeServer);
                if (connectionInfos == null) {
                    connectionInfos = new ArrayList<>();
                    connectionInfoMap.put(storeServer, connectionInfos);
                }
                connectionInfos.add(new StoreConnectionInfo(rset));
            }
            rset.close();
        }

        Counter filesForThisRun = new Counter(MAX_FILES_PER_RUN);
        for (StoreServer storeServer : connectionInfoMap.keySet()) {
            if (logger.isTraceEnabled()) {
                logger.trace("Connecting to store " + storeServer.getId());
            }
            RemoteConnector connector = RemoteConnectors.connector(storeServer.getProtocol(), storeServer.getAddress(), storeServer.getUsername(), storeServer.getPassword(), null);

            int maxFiles = Math.min(MAX_FILES_PER_STORE, filesForThisRun.get());

            storeCollectorFutures.add(ExecutorsFactory.getExecutorService().submit(new Callable<List<DataTrace>>() {
                @Override
                public List<DataTrace> call() throws Exception {
                    List<DataTrace> traces = Collections.synchronizedList(new ArrayList<>());
                    Timer timer = new Timer();


                    try { //
                        Map<Long, String> connectionRunId = new HashMap<>();
                        for (StoreConnectionInfo connectionInfo : connectionInfoMap.get(storeServer)) {
                            Counter count = new Counter();
                            try {
                                RemoteConnector finalConnector = connector;
                                if (logger.isTraceEnabled()) {
                                    logger.trace("Fetching files from " + connector.getHost() + "/" + connectionInfo.getPath());
                                }
                                connector.fetchFiles(connectionInfo.getPath(), filenamePattern, filesForThisRun, new FileReceiver() {
                                    private Collection<InboundFileUtilities.InboundFile> batchData = new ArrayList<>();

                                    @Override
                                    public void receiveFile(String source, String filename, byte[] content) {
                                        filesForThisRun.reduce(1);
                                        timer.start("store.receive.file");
                                        if (logger.isTraceEnabled()) {
                                            logger.trace("Retrieved file " + source);
                                        }
                                        String messageUuid = UUID.randomUUID().toString();
                                        File archiveDir = new File(IndioProperties.properties().getProperty("archive_directory") + "/inbound/" + jobExecutionData.getDataType().toLowerCase() + "/" + connectionInfo.getStoreId());

                                        count.add();

                                        Map<String, String> metadata = new HashMap<>();
                                        metadata.put("source_store_id", connectionInfo.getStoreId().toString());
                                        metadata.put("source_store_location_id", connectionInfo.getConnectionDetailsId().toString());
                                        try {
                                            InboundFileUtilities.InboundFile inboundFile = new InboundFileUtilities.InboundFile(jobExecutionData.getDataType(), source, filename, content, archiveDir, new Date(), messageUuid, metadata, "UTF-8");
                                            batchData.add(inboundFile);
                                            timer.end("store.receive.file");
                                        } catch (IOException e) {
                                            logger.error("Unable to receive file!!");
                                        }
                                    }

                                    @Override
                                    public void done() {
                                        String error = null;
                                        ProcessError processError = null;
                                        try {
                                            if (!batchData.isEmpty()) {
                                                timer.start("store.receive.file.done");
                                                long s = System.nanoTime();
                                                Map<String, Long> updates = null;
                                                try (Connection conn = DatasourceProvider.getConnection()) {
                                                    updates = InboundFileUtilities.insertFiles(conn, batchData.toArray(new InboundFileUtilities.InboundFile[batchData.size()]));
                                                    long end = System.nanoTime();
                                                    Map<String, Map<String, String>> metadatas = new HashMap<>();
                                                    for (InboundFileUtilities.InboundFile inboundFile : batchData) {
                                                        Long generatedId = updates.get(inboundFile.getMessageUuid());
                                                        if (generatedId != null) {
                                                            metadatas.put(inboundFile.getMessageUuid(), inboundFile.getMetadata());
                                                        }
                                                    }

                                                    for (InboundFileUtilities.InboundFile inboundFile : batchData) {
                                                        Long generatedId = updates.get(inboundFile.getMessageUuid());
                                                        finalConnector.deleteFile(connectionInfo.getPath(), inboundFile.getFileName());
                                                        /*
                                                         Don't indicate something is wrong if the file wasn't possible to remove,
                                                         at this stage we have successfully retrieved the file and if the file
                                                         isn't changed it'll be retrieved again and nothing will happen. If the file
                                                         has changed it will update the inbound file table and trigger a new processing.

                                                         Code to set "ERROR" status when file wasn't successfully removed was
                                                         disabled due to problem when files were successfully retrieved but this
                                                         error was causing problems in further processing.
                                                          */

//                                                        if (!finalConnector.deleteFile(connectionInfo.getPath(), inboundFile.getFileName())) {
//                                                            traces.add(new DataTrace(inboundFile.getMessageUuid(), traceHandle.getId(), inboundFile.getSource(), null, "inbound_file_staging", generatedId, ProcessStatus.ERROR, "Unable to delete file", null, ProcessError.REMOTE_FILE_DELETE_FAILED));
//                                                        } else if (generatedId != null) {
                                                            traces.add(new DataTrace(inboundFile.getMessageUuid(), traceHandle.getId(), inboundFile.getSource(), null, "inbound_file_staging", generatedId, ProcessStatus.SUCCESS));
//                                                        }
                                                    }
                                                    InboundFileUtilities.insertMetadata(metadatas, conn);
                                                }
                                                didReceiveFiles.set(didReceiveFiles.get() || updates.values().stream().filter(id -> id != null).findFirst().isPresent());
                                                numOfFilesRetrieved.add((int) updates.values().stream().filter(id -> id != null).count());

                                                timer.end("store.receive.file.done");
                                                atLeastOneStoreFullFetch.set(atLeastOneStoreFullFetch.get() || batchData.size() >= MAX_FILES_PER_STORE);
                                            }
                                        } catch (SQLException e) {
                                            error = e.getMessage();
                                            processError = ProcessError.DATABASE_PROBLEM;
                                            e.printStackTrace();
                                            logger.error("SQLE", e);
                                        } catch (IOException e) {
                                            error = e.getMessage();
                                            processError = ProcessError.CONNECTION_FAILURE;
                                            logger.error("IOE", e);
                                        }finally {
                                            storesFetchedFrom.add();
                                            ProcessStatusHandler.sendWebSocketUpdate(traceHandle.getId(), jobExecutionData.getJobId(), jobExecutionData.getDataType(), jobExecutionData.getEventStep(), ProcessStatus.RUNNING, 0d, storeCounter.get() > 0 ? ((double) storesFetchedFrom.get() / storeCounter.get()) : 1);
                                        }
                                    }

                                    @Override
                                    public void receiveFileError(String source, String filename, String error, ProcessError processError) {
                                        String messageUuid = UUID.randomUUID().toString();
                                        logger.error("[" + traceHandle.getId() + "]: Failed to retrieve file " + filename + " " + error);
                                        traceHandle.getProcessTracer().trace(new DataTrace(messageUuid, traceHandle.getId(), source, null, "inbound_file_staging", (String) null, ProcessStatus.ERROR, error, null, processError));
                                    }

                                });
                                Alerter.clearConnectionError(storeServer.getProtocol(), storeServer.getAddress(), Direction.INBOUND);
                            } catch (ConnectionFailureException e) {
                                String error = "Unable to connect to remote server " + storeServer.getAddress()
                                        + " and path " + connectionInfo.getPath() + " using protocol "
                                        + storeServer.getProtocol() + " and username " + storeServer.getUsername();
                                if (logger.isTraceEnabled()) {
                                    logger.error(error, e);
                                }
                                Alerter.alertConnectionError(storeServer.getProtocol(), storeServer.getAddress(), Direction.INBOUND, error);
                            }
                        }
                    } finally {
                        connector.close();
//                        Thread.currentThread().setPriority(priority);
                    }
                    return traces;
                }
            }));
        }
        ListenableFuture future = Futures.allAsList(storeCollectorFutures.toArray(new ListenableFuture[storeCollectorFutures.size()]));
        Futures.addCallback(future, new FutureCallback<List<List<DataTrace>>>() {
            @Override
            public void onSuccess(List<List<DataTrace>> traceListCollection) {
                List<DataTrace> traces = new ArrayList<>();
                for (List<DataTrace> traceList : traceListCollection) {
                    traces.addAll(traceList);
                }


                long tta = System.nanoTime() - start;
                traceHandle.getProcessTracer().trace(traces.toArray(new DataTrace[traces.size()]));
                if (numOfFilesRetrieved.get() > 0) {
                    boolean completelyDone = filesForThisRun.get() > 0 && !atLeastOneStoreFullFetch.get();
                    logger.debug(traceHandle.getId() + ": Retrieved " + numOfFilesRetrieved.get() + " " + jobExecutionData.getDataType() + " files. Completely done: " + completelyDone);
                    callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.SUCCESS, completelyDone));
                } else {
                    callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.NOOP));
                }

            }

            @Override
            public void onFailure(Throwable throwable) {
                List<DataTrace> traces = new ArrayList<>();
                long tta = System.nanoTime() - start;
                traceHandle.getProcessTracer().trace(traces.toArray(new DataTrace[traces.size()]));
//                if (numOfFilesRetrieved.get() > 0) {
                logger.warn("Error collecting " + numOfFilesRetrieved.get() + " " + jobExecutionData.getDataType() + " files in " + (tta / 1e6d) + "ms " + (tta / (numOfFilesRetrieved.get() > 0 ? numOfFilesRetrieved.get() : 1) / 1e6d) + " ms/file");
//                }
                callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.ERROR));

            }
        });
    }
}
