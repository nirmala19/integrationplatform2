package se.indiska.integration.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import se.indiska.integration.framework.IndioProperties;
import se.indiska.integration.framework.IntegrationCore;
import se.indiska.integration.framework.job.JobServiceRunner;
import se.indiska.security.SecurityFilter;

import javax.annotation.PreDestroy;
import java.net.InetAddress;

/**
 * Spring Boot Application starter class for the Integration Engine.
 */
@SpringBootApplication
@ComponentScan({"se.indiska"})
public class IntegrationEngine {
    private static final Logger logger = LoggerFactory.getLogger(IntegrationEngine.class);
    @Autowired
    private IntegrationCore integrationCore;

    @Autowired
    Environment environment;

    @Bean
    IntegrationCore integrationCore() {
        return new IntegrationCore();
    }

    public static void main(String[] args) {
        SpringApplication.run(IntegrationEngine.class, new String[]{});
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return (container -> {
            container.setPort(8080);
        });
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            logger.debug("Started IntegrationEngine on port " + environment.getProperty("local.server.port"));
            IndioProperties.properties();
            JobServiceRunner jobServiceRunner = new JobServiceRunner();
            String localAddress = InetAddress.getLocalHost().getCanonicalHostName() + ":" + environment.getProperty("local.server.port");
            jobServiceRunner.registerJobs(localAddress);
            jobServiceRunner.initializeJobLifecycle();
            integrationCore.initialize();
        };
    }

    @Bean
    public FilterRegistrationBean myFilterBean() {
        final FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
        filterRegBean.setFilter(new SecurityFilter("indio"));
        filterRegBean.addUrlPatterns("/*");
        filterRegBean.setEnabled(Boolean.TRUE);
        filterRegBean.setName("Authentication Filter");
//        filterRegBean.setAsyncSupported(Boolean.TRUE);
        return filterRegBean;
    }


    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*");
            }
        };
    }

    @PreDestroy
    public void onExit() {
        IndioProperties.shutdown();
        integrationCore.shutdown();

    }

}
