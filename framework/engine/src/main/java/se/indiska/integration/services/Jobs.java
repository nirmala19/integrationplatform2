package se.indiska.integration.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.IntegrationCore;
import se.indiska.integration.framework.entity.*;
import se.indiska.integration.framework.job.EventTriggerManager;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by heintz on 03/04/17.
 */
@RestController
@RequestMapping("/jobs")
public class Jobs extends RestService {
    private static final Logger logger = LoggerFactory.getLogger(Jobs.class);
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static ObjectMapper mapper = new ObjectMapper();
    @Autowired
    private Environment environment;


    @RequestMapping(value = "/settings", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity globalSettings() {
        String sql = "SELECT * FROM settings";
        Map<String, String> result = new HashMap<>();
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                result.put(rset.getString("key"), rset.getString("value"));
            }
            rset.close();
            ps.close();
        } catch (SQLException e) {
            logger.error("Unable to get global settings", e);
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/settings", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity globalSettings(@RequestBody Map<String, String> settings) {
        String sql = "INSERT INTO settings (key, value) VALUES (?,?) ON CONFLICT (key) DO UPDATE SET VALUE=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            for (String key : settings.keySet()) {
                ps.setString(1, key);
                ps.setString(2, settings.get(key));
                ps.setString(3, settings.get(key));
                ps.addBatch();
            }
            ps.executeBatch();
            ps.close();
        } catch (SQLException e) {
            logger.error("Unable to get global settings", e);
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

    /**
     * Updates the value of the "paused" attribute in the "settings" table
     * in our database, which will be useful when pausing our system.
     *
     * @param settings Is a Map<String,String> where our key is "paused" and our value is a string which will either be
     *                 "TRUE" or "FALSE"
     *                 depending on if we un-pause or pause the system.
     * @return Message which declares the outcome. Error message if a SQL-exception occurs and an ok-message if
     * the operation is successful.
     **/

    @RequestMapping(value = "/settings", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity uppdateGlobalSettings(@RequestBody Map<String, String> settings) {
        String key = settings.get("key");
        String value = settings.get("value");

        String sql = "UPDATE settings SET value=? WHERE key=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, value);
            ps.setString(2, key);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            logger.error("Unable to get global settings", e);
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/state", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getAllScheduledJobs() {
        Map result = new HashMap<>();
        result.put("execution_queue_length", IntegrationCore.getEventTriggerEngine().getExecutionQueueLength());

        String outboundQueueSql = "SELECT (SELECT count(1)\n" +
                "        FROM outbound_distribution\n" +
                "        WHERE delivered = FALSE) + (SELECT count(1)\n" +
                "                                    FROM outbound_store_distribution\n" +
                "                                    WHERE delivered = FALSE)";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(outboundQueueSql);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                result.put("outbound_distribution_queue_length", rset.getInt(1));
            }
            rset.close();
            ps.close();

        } catch (SQLException e) {
            logger.error("Unable to query database for state of job service", e);
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/inbound", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getAllInboundScheduledJobs() {
        List<JobScheduleTrigger> result = new ArrayList<>();
        try (Connection conn = DatasourceProvider.getConnection()) {
            String getJobEventTriggers = "SELECT * FROM job_schedule_triggers jst, job WHERE event_step=? AND jst.job_id=job.id ORDER BY jst.data_type";
            PreparedStatement ps = conn.prepareStatement(getJobEventTriggers);
            ps.setString(1, "INBOUND_TRANSFER");
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                result.add(jobScheduleTrigger(resultSet, conn));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Unable to get inbound scheduled jobs");
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/inbound/{datatype}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getInboundScheduledJobs(@PathVariable("datatype") String datatype) {
        List<JobScheduleTrigger> result = new ArrayList<>();
        try (Connection conn = DatasourceProvider.getConnection()) {
            String getJobEventTriggers = "SELECT * FROM job_schedule_triggers jst, job WHERE event_step=? AND data_type=? AND jst.job_id=job.id ORDER BY jst.data_type";
            PreparedStatement ps = conn.prepareStatement(getJobEventTriggers);
            ps.setString(1, "INBOUND_TRANSFER");
            ps.setString(2, datatype);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                result.add(jobScheduleTrigger(resultSet, conn));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Unable to get inbound scheduled jobs");
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/processing/{datatype}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getEventTriggeredJob(@PathVariable("datatype") String datatype) {
        List<JobEventTrigger> result = new ArrayList<>();
        try (Connection conn = DatasourceProvider.getConnection()) {
            String getJobEventTriggers = "SELECT * FROM job_event_triggers jst, job WHERE data_type=? AND jst.job_id=job.id";
            PreparedStatement ps = conn.prepareStatement(getJobEventTriggers);
            ps.setString(1, datatype);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                result.add(jobEventTrigger(resultSet, conn));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Unable to get event-based jobs");
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/outbound/{datatype}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity outboundJobs(@PathVariable("datatype") String datatype) throws SQLException, JsonProcessingException {
        String outboundSql = "SELECT omd.id omd_id, *\n" +
                "FROM outbound_message_distributor omd\n" +
                "  JOIN remote_location rl\n" +
                "    ON omd.remote_location = rl.id\n" +
                "  JOIN remote_server rs\n" +
                "    ON omd.remote_server = rs.id\n" +
                "WHERE data_type=?";
        String storeOutboundSql = "SELECT * FROM store_outbound_message_distributor WHERE data_type=?";
        List<MessageDistributor> result = new ArrayList<>();
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(outboundSql);
            ps.setString(1, datatype);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                NormalOutboundDistributor distributor = new NormalOutboundDistributor();
                distributor.setSubstitutePattern(rset.getString("substitute_pattern"));
                distributor.setDataType(rset.getString("data_type"));
                distributor.setId(rset.getLong("omd_id"));
                distributor.setRemoteLocation(remoteLocation(rset, "remote_location"));
                distributor.setRemoteServer(remoteServer(rset, "remote_server"));
                result.add(distributor);
            }
            rset.close();
            ps.close();
            ps = connection.prepareStatement(storeOutboundSql);
            ps.setString(1, datatype);
            rset = ps.executeQuery();
            while (rset.next()) {
                StoreOutboundDistributor storeOutboundDistributor = new StoreOutboundDistributor();
                storeOutboundDistributor.setId(rset.getLong("id"));
                storeOutboundDistributor.setDataType(rset.getString("data_type"));
                storeOutboundDistributor.setFilenameStoreMatchPattern(rset.getString("file_name_store_match"));
                result.add(storeOutboundDistributor);
            }
        }

        MessageDistributor[] outbounds = result.toArray(new MessageDistributor[result.size()]);
        return ResponseEntity.ok(outbounds);
    }


    @RequestMapping(value = "/registered", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getRegisteredJobs() throws IOException {
        return ResponseEntity.ok(EventTriggerManager.getRegisteredJobs());
    }

    @RequestMapping(value = "/activate/{scheduleId}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity enableJob(@PathVariable("scheduleId") Long scheduleId) {
        if (setScheduledJobStatus(scheduleId, true)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(value = "/deactivate/{scheduleId}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity disableJob(@PathVariable("scheduleId") Long scheduleId) {
        if (setScheduledJobStatus(scheduleId, false)) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(value = "/status/{type}/{id}/{status}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity changeStatus(@PathVariable("type") String type, @PathVariable("id") String id, @PathVariable("status") Boolean status) {
        if (type.equalsIgnoreCase("inbound")) {
            if (setScheduledJobStatus(Long.parseLong(id), status)) {
                return ResponseEntity.ok().build();
            }
        } else if (type.equalsIgnoreCase("processing")) {
            if (setEventTriggerStatus(Long.parseLong(id), status)) {
                return ResponseEntity.ok().build();
            }
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(value = "/inbound", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity saveInboundJob(@RequestBody JobScheduleTrigger jobScheduleTrigger) {
        try (Connection conn = DatasourceProvider.getConnection()) {
            saveJobScheduleTrigger(jobScheduleTrigger, conn);
            return ResponseEntity.ok().build();
        } catch (SQLException e) {
            logger.error("Unable to save inbound job!", e);
        } catch (JsonProcessingException e) {
            logger.error("Unable to save inbound job!", e);
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(value = "/outbound", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity saveOutboundJob(@RequestBody MessageDistributor outboundJob) {
        try (Connection conn = DatasourceProvider.getConnection()) {
            if (outboundJob instanceof NormalOutboundDistributor) {
                storeOutboundJob((NormalOutboundDistributor) outboundJob, conn);
            }
            return ResponseEntity.ok().build();
        } catch (SQLException e) {
            logger.error("Unable to save inbound job!", e);
        }
        return ResponseEntity.badRequest().build();
    }

    private void storeOutboundJob(NormalOutboundDistributor outboundJob, Connection conn) throws SQLException {
        outboundJob.setRemoteLocation(saveRemoteLocation(outboundJob.getRemoteLocation(), conn));
        String sql;
        boolean update = false;
        if (outboundJob.getId() == null) {
            sql = "INSERT INTO outbound_message_distributor (remote_location, data_type, immediate_delivery, remote_server, substitute_pattern)" +
                    " VALUES (?,?,TRUE,?,?)";
        } else {
            sql = "UPDATE outbound_message_distributor SET remote_location=?, data_type=?, remote_server=?, substitute_pattern=? WHERE id=?";
            update = true;
        }
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setLong(1, outboundJob.getRemoteLocation().getId());
        ps.setString(2, outboundJob.getDataType());
        ps.setLong(3, outboundJob.getRemoteServer().getId());
        ps.setString(4, outboundJob.getSubstitutePattern() == null || outboundJob.getSubstitutePattern().trim().length() == 0 ? null : outboundJob.getSubstitutePattern());
        if (update) {
            ps.setLong(5, outboundJob.getId());
        }
        ps.executeUpdate();
    }

    @RequestMapping(value = "/inboundJobTypes", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getInboundJobTypes() {
        try (Connection conn = DatasourceProvider.getConnection()) {
            List<Job> jobs = new ArrayList<>();
            String sql = "SELECT * FROM job WHERE event_type=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "INBOUND_TRANSFER");
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                Job job = new Job();
                job.setDescription(rset.getString("description"));
                job.setId(rset.getString("id"));
                job.setEventType(rset.getString("event_type"));
                job.setClassName(rset.getString("class"));
                jobs.add(job);
            }
            return ResponseEntity.ok(jobs);
        } catch (SQLException e) {
            logger.error("Unable return inbound job types!", e);
        }
        return ResponseEntity.badRequest().build();
    }

    @RequestMapping(value = "/datatypes", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getDatatypes() {
        try (Connection conn = DatasourceProvider.getConnection()) {
            List<String> datatype = new ArrayList<>();
            String sql = "SELECT * FROM data_type";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                datatype.add(rset.getString("id"));
            }
            return ResponseEntity.ok(datatype);
        } catch (SQLException e) {
            logger.error("Unable return inbound job types!", e);
        }
        return ResponseEntity.badRequest().build();
    }

    private boolean setScheduledJobStatus(Long id, boolean active) {
        String sql = "UPDATE job_schedule_triggers SET job_status=? WHERE id=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, active ? "ACTIVE" : "DISABLED");
            ps.setLong(2, id);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            logger.debug("Unable to update status of job " + id + " to " + (active ? "ACTIVE" : "DISABLED"), e);
        }
        return false;
    }

    private boolean setEventTriggerStatus(Long id, boolean active) {
        String sql = "UPDATE job_event_triggers SET job_status=? WHERE id=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, active ? "ACTIVE" : "DISABLED");
            ps.setLong(2, id);
            return ps.executeUpdate() > 0;
        } catch (SQLException e) {
            logger.debug("Unable to update status of job " + id + " to " + (active ? "ACTIVE" : "DISABLED"), e);
        }
        return false;
    }


    private RemoteServer remoteServer(ResultSet rset, String idColumn) throws SQLException {
        RemoteServer remoteServer = new RemoteServer();
        remoteServer.setId(rset.getLong(idColumn != null ? idColumn : "id"));
        remoteServer.setName(rset.getString("name"));
        remoteServer.setUsername(rset.getString("username"));
        remoteServer.setPassword(rset.getString("password"));
        remoteServer.setAddress(rset.getString("address"));
        remoteServer.setProtocol(rset.getString("protocol"));
        return remoteServer;
    }

    private RemoteLocation remoteLocation(ResultSet rset, String idColumn) throws SQLException {
        RemoteLocation remoteLocation = new RemoteLocation();
        remoteLocation.setId(rset.getLong(idColumn != null ? idColumn : "id"));
        remoteLocation.setDirectory(rset.getString("directory"));
        remoteLocation.setDescription(rset.getString("description"));
        return remoteLocation;
    }

    private InboundMessageCollector inboundMessageCollector(ResultSet rset, Connection conn) throws SQLException, IOException {
        InboundMessageCollector inboundMessageCollector = new InboundMessageCollector();
        inboundMessageCollector.setId(rset.getLong("id"));
        inboundMessageCollector.setFilenamePattern(rset.getString("file_pattern"));
        String contentResolver = rset.getString("content_datatype_resolver");
        if (contentResolver != null) {
            inboundMessageCollector.setContentDatatypeResolver(mapper.readValue(contentResolver, Map.class));
        }
        Long remoteLocationId = rset.getLong("remote_location");
        Long remoteServerId = rset.getLong("remote_server");
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM remote_location WHERE id=?");
        ps.setLong(1, remoteLocationId);
        ResultSet remoteLocationResultSet = ps.executeQuery();
        if (remoteLocationResultSet.next()) {
            inboundMessageCollector.setRemoteLocation(remoteLocation(remoteLocationResultSet, "id"));
        }
        remoteLocationResultSet.close();
        ps.close();
        ps = conn.prepareStatement("SELECT * FROM remote_server WHERE id=?");
        ps.setLong(1, remoteServerId);
        ResultSet remoteServerResultSet = ps.executeQuery();
        if (remoteServerResultSet.next()) {
            inboundMessageCollector.setRemoteServer(remoteServer(remoteServerResultSet, "id"));
        }
        return inboundMessageCollector;
    }

    private Job job(ResultSet rset) throws SQLException {
        Job job = new Job();
        job.setId(rset.getString("id"));
        job.setClassName(rset.getString("class"));
        job.setEventType(rset.getString("event_type"));
        job.setDescription(rset.getString("description"));
        return job;
    }

    private JobEventTrigger jobEventTrigger(ResultSet rset, Connection conn) throws SQLException {
        JobEventTrigger trigger = new JobEventTrigger();
        trigger.setId(rset.getLong("id"));
        trigger.setEventStep(rset.getString("event_step"));
        trigger.setDataType(rset.getString("data_type"));
        trigger.setJobStatus(rset.getString("job_status"));
        trigger.setLastUpdated(new Date(rset.getTimestamp("last_updated").getTime()));
        String jobId = rset.getString("job_id");
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM job WHERE id=?");
        ps.setString(1, jobId);
        ResultSet jobResultSet = ps.executeQuery();
        if (jobResultSet.next()) {
            trigger.setJob(job(jobResultSet));
        }
        jobResultSet.close();
        ps.close();
        return trigger;
    }

    private void saveJobScheduleTrigger(JobScheduleTrigger trigger, Connection conn) throws SQLException, JsonProcessingException {

        if (trigger.getInboundMessageCollector() != null) {
            trigger.setInboundMessageCollector(saveInboundMessageCollector(trigger.getInboundMessageCollector(), conn));

            if (trigger.getId() != null) {
                String sql = "UPDATE job_schedule_triggers SET schedule=?, last_updated=now(), parameters=?, allow_concurrent=FALSE WHERE id=?";
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, trigger.getSchedule());
                Map params = new HashMap();
                params.put("inbound_message_collector", trigger.getInboundMessageCollector().getId());
                ps.setString(2, mapper.writeValueAsString(params));
                ps.setLong(3, trigger.getId());
                ps.executeUpdate();
            } else {
                String sql = "INSERT INTO job_schedule_triggers (job_id, event_Step, data_type, schedule, job_status, last_updated, parameters, allow_concurrent) VALUES (?,?,?,?,?,now(),?,FALSE)";
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, trigger.getJob().getId());
                ps.setString(2, trigger.getEventStep());
                ps.setString(3, trigger.getDataType());
                ps.setString(4, trigger.getSchedule());
                ps.setString(5, "DISABLED");
                Map params = new HashMap();
                params.put("inbound_message_collector", trigger.getInboundMessageCollector().getId());
                ps.setString(6, mapper.writeValueAsString(params));
                ps.executeUpdate();
            }
        }
    }

    private InboundMessageCollector saveInboundMessageCollector(InboundMessageCollector inboundMessageCollector, Connection conn) throws SQLException, JsonProcessingException {

        inboundMessageCollector.setRemoteLocation(saveRemoteLocation(inboundMessageCollector.getRemoteLocation(), conn));
        boolean update = false;
        String sql = null;
        if (inboundMessageCollector.getId() != null) {
            update = true;
            sql = "UPDATE inbound_message_collector SET remote_server=?, remote_location=?, file_pattern=?, content_datatype_resolver=? WHERE id=?";
        } else {
            sql = "INSERT INTO inbound_message_collector (remote_server, remote_location, file_pattern, content_datatype_resolver) VALUES (?,?,?,?)";
        }
        PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setLong(1, inboundMessageCollector.getRemoteServer().getId());
        ps.setLong(2, inboundMessageCollector.getRemoteLocation().getId());
        ps.setString(3, inboundMessageCollector.getFilenamePattern());
        ps.setString(4, inboundMessageCollector.getContentDatatypeResolver() != null ? mapper.writeValueAsString(inboundMessageCollector.getContentDatatypeResolver()) : null);
        if (update) {
            ps.setLong(5, inboundMessageCollector.getId());
        }
        ps.executeUpdate();
        ResultSet rset = ps.getGeneratedKeys();
        if (rset.next()) {
            inboundMessageCollector.setId(rset.getLong("id"));
        }
        return inboundMessageCollector;
    }

    private RemoteLocation saveRemoteLocation(RemoteLocation remoteLocation, Connection conn) throws SQLException {
        String sql = null;
        boolean update = false;
        if (remoteLocation.getId() != null) {
            sql = "UPDATE remote_location SET directory=?, description=? WHERE id=?";
            update = true;
        } else {
            sql = "INSERT INTO remote_location (directory, description) VALUES (?,?)";
        }

        PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, remoteLocation.getDirectory());
        ps.setString(2, remoteLocation.getDescription());
        if (update) {
            ps.setLong(3, remoteLocation.getId());
        }
        ps.executeUpdate();
        ResultSet rset = ps.getGeneratedKeys();
        if (rset.next()) {
            remoteLocation.setId(rset.getLong("id"));
        }
        return remoteLocation;
    }

    private JobScheduleTrigger jobScheduleTrigger(ResultSet rset, Connection conn) throws SQLException {
        JobScheduleTrigger trigger = new JobScheduleTrigger();
        trigger.setId(rset.getLong("id"));
        trigger.setEventStep(rset.getString("event_step"));
        trigger.setDataType(rset.getString("data_type"));
        trigger.setSchedule(rset.getString("schedule"));
        trigger.setJobStatus(rset.getString("job_status"));
        trigger.setDescription(rset.getString("description"));
        trigger.setLastUpdated(new Date(rset.getTimestamp("last_updated").getTime()));
        String jobId = rset.getString("job_id");
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM job WHERE id=?");
        ps.setString(1, jobId);
        ResultSet jobResultSet = ps.executeQuery();
        if (jobResultSet.next()) {
            trigger.setJob(job(jobResultSet));
        }
        jobResultSet.close();
        ps.close();
        try {
            Map parameters = mapper.readValue(rset.getString("parameters"), Map.class);
            Integer msgCollectorId = (Integer) parameters.get("inbound_message_collector");
            if (msgCollectorId != null) {
                ps = conn.prepareStatement("SELECT * FROM inbound_message_collector WHERE id=?");
                ps.setLong(1, msgCollectorId.longValue());
                ResultSet inboundMessageCollectorResultSet = ps.executeQuery();
                if (inboundMessageCollectorResultSet.next()) {
                    trigger.setInboundMessageCollector(inboundMessageCollector(inboundMessageCollectorResultSet, conn));
                }
                inboundMessageCollectorResultSet.close();
                ps.close();
            } else if (parameters.get("filename_pattern") != null) {
                trigger.setStoreFilenamePattern((String) parameters.get("filename_pattern"));
            }
        } catch (IOException e) {
            logger.error("Unable to parse parameters!!!", e);
        }
        return trigger;
    }
}
