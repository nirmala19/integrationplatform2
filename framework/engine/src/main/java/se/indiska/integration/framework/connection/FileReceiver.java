package se.indiska.integration.framework.connection;

import se.indiska.integration.framework.job.ProcessError;

/**
 * FileReceiver is as the name implies a file receiver interface that is used
 * when fetching files from remote servers using FTP, CIFS/SAMBA, SCP etc.
 * Created by heintz on 04/04/17.
 */
public interface FileReceiver {
    /**
     * Called when a file is received from remote server.
     *
     * @param filename The original name of the file
     * @param content  The content of the file.
     */
    void receiveFile(String source, String filename, byte[] content);

    /**
     * Called when a file receive process failed. Could be error in transfer or permission problems or similar.
     * @param source The source (where did we try to read the file)
     * @param filename The name of the file.
     * @param error A text describing the error
     */
    void receiveFileError(String source, String filename, String error, ProcessError processError);

    /**
     * Must be called when this file receiver is done receiving files. Telling that a process can shut down and commit any batches.
     */
    void done();
}
