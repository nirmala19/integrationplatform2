package se.indiska.integration.framework.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteServerConnectionException extends ConnectionFailureException {
    private static final Logger logger = LoggerFactory.getLogger(RemoteServerConnectionException.class);
    private Long remoteServerId = null;
    private Long remoteDirectoryId = null;

    public RemoteServerConnectionException(String message, Long remoteServerId, Long remoteDirectoryId, Throwable cause) {
        super(message, cause);
        this.remoteDirectoryId = remoteDirectoryId;
        this.remoteServerId = remoteServerId;
    }

    public Long getRemoteServerId() {
        return remoteServerId;
    }

    public Long getRemoteDirectoryId() {
        return remoteDirectoryId;
    }
}
