package se.indiska.integration.framework.entity;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class NormalOutboundDistributor extends MessageDistributor {
    private static final Logger logger = LoggerFactory.getLogger(NormalOutboundDistributor.class);
    private RemoteLocation remoteLocation = null;
    private RemoteServer remoteServer = null;
    private String substitutePattern = null;

    public RemoteLocation getRemoteLocation() {
        return remoteLocation;
    }

    public void setRemoteLocation(RemoteLocation remoteLocation) {
        this.remoteLocation = remoteLocation;
    }

    public RemoteServer getRemoteServer() {
        return remoteServer;
    }

    public void setRemoteServer(RemoteServer remoteServer) {
        this.remoteServer = remoteServer;
    }

    public String getSubstitutePattern() {
        return substitutePattern;
    }

    public void setSubstitutePattern(String substitutePattern) {
        this.substitutePattern = substitutePattern;
    }

}
