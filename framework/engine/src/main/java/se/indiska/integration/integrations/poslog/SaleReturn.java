package se.indiska.integration.integrations.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Created by heintz on 27/02/17.
 */
public class SaleReturn extends LineItem {
    private static final Logger logger = LoggerFactory.getLogger(SaleReturn.class);
    private String itemType = null;
    private String itemId = null;
    private String description = null;
    private String taxIncludedInPriceFlag = null;
    private Double regularSalesUnitPrice = null;
    private Double actualSalesUnitPrice = null;
    private Double extendedAmount = null;
    private Double quantity = null;
    private String webReturn = null;
    private String taxType = null;
    private Double taxAmount = null;
    private Double taxPercent = null;
    private String taxGroupID = null;
    private String disposal = null;
    private String reason = null;
    private boolean isReturn = false;
    private static boolean tableExists = false;
    private String messageId = null;

    public SaleReturn(String messageId) {
        this.messageId = messageId;
    }

    @Override
    void parse(Element element, File file) {
        isReturn = element.getNodeName().equals("Return");
        itemType = element.getAttribute("ItemType");
        NodeList nodeList = element.getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node n = (Node) nodeList.item(i);
            try {
                if (n instanceof Element) {
                    Element e = (Element) n;
                    switch (e.getTagName()) {
                        case "ItemID":
                            itemId = e.getFirstChild().getNodeValue();
                            break;
                        case "Description":
                            description = e.getFirstChild().getNodeValue();
                            break;
                        case "TaxIncludedInPriceFlag":
                            taxIncludedInPriceFlag = e.getFirstChild().getNodeValue();
                            break;
                        case "RegularSalesUnitPrice":
                            regularSalesUnitPrice = new Double(e.getFirstChild().getNodeValue());
                            break;
                        case "ActualSalesUnitPrice":
                            actualSalesUnitPrice = new Double(e.getFirstChild().getNodeValue());
                            break;
                        case "ExtendedAmount":
                            extendedAmount = new Double(e.getFirstChild().getNodeValue());
                            break;
                        case "Quantity":
                            quantity = new Double(e.getFirstChild().getNodeValue());
                            break;
                        case "WebReturn":
                            webReturn = e.getFirstChild().getNodeValue();
                            break;
                        case "Disposal":
                            disposal = e.getAttribute("Method");
                            break;
                        case "Reason":
                            reason = e.getFirstChild().getNodeValue();
                            break;
                        case "Tax":
                            taxType = e.getAttribute("TaxType");
                            NodeList tax = e.getChildNodes();
                            for (int j = 0; j < tax.getLength(); j++) {
                                Node taxNode = tax.item(j);
                                switch (taxNode.getNodeName()) {
                                    case "Amount":
                                        taxAmount = new Double(taxNode.getFirstChild().getNodeValue());
                                        break;
                                    case "Percent":
                                        taxPercent = new Double(taxNode.getFirstChild().getNodeValue());
                                        break;
                                    case "TaxGroupID":
                                        taxGroupID = taxNode.getFirstChild().getNodeValue();
                                        break;
                                }
                            }
                            break;

                    }
                }
            } catch (Exception e) {
                logger.error("Unable to handle data in file " + file.getName(), e);
            }
        }
    }

    @Override
    public void store(PreparedStatement ps, String transactionId) throws SQLException {
        ps.setInt(1, getSequenceNumber());
        ps.setString(2, transactionId);
        ps.setBoolean(3, isReturn);
        ps.setString(4, itemType);
        ps.setString(5, itemId);
        ps.setString(6, description);
        ps.setString(7, taxIncludedInPriceFlag);
        ps.setDouble(8, regularSalesUnitPrice);
        ps.setDouble(9, actualSalesUnitPrice);
        ps.setDouble(10, (isReturn ? -1 : 1) * extendedAmount);
        ps.setDouble(11, (isReturn ? -1 : 1) * quantity);
        ps.setString(12, webReturn);
        ps.setString(13, taxType);
        if (taxAmount == null) {
            ps.setNull(14, Types.FLOAT);
        } else {
            ps.setDouble(14, taxAmount);
        }
        if (taxPercent == null) {
            ps.setNull(15, Types.FLOAT);
        } else {
            ps.setDouble(15, taxPercent);
        }
        ps.setString(16, taxGroupID);
        ps.setString(17, disposal);
        ps.setString(18, reason);
        ps.addBatch();
    }

    @Override
    public String getInsertSql() {
        return "insert into pos_transaction_sales (sequence_number, transaction_id, is_return, item_type, item_id, description, tax_included_in_price_flag, regular_sales_unit_price, " +
                "actual_sales_unit_price, extended_amount, quantity, web_return, tax_type, tax_amount, tax_percent, tax_group_id, disposal, reason, message_id)" +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) on conflict do nothing";
    }

    private void createTable(Connection connection) throws SQLException {
        if (!tableExists) {
            PreparedStatement ps = connection.prepareStatement(getCreateTableSql());
            ps.executeUpdate();
            ps.close();
            tableExists = true;
        }
    }


    @Override
    public String getCreateTableSql() {
        return "create table if not exists pos_transaction_sales (" +
                "sequence_number integer not null," +
                "transaction_id varchar(32) not null," +
                "is_return boolean not null," +
                "item_type varchar(16)," +
                "item_id varchar(24)," +
                "description varchar(24)," +
                "tax_included_in_price_flag varchar(12)," +
                "regular_sales_unit_price decimal(10, 2) not null," +
                "actual_sales_unit_price  decimal(10, 2) not null," +
                "extended_amount decimal(10,2) not null," +
                "quantity decimal(5,2) not null," +
                "web_return varchar(12), " +
                "tax_type varchar(16)," +
                "tax_amount decimal(5,2)," +
                "tax_percent decimal(5,2)," +
                "tax_group_id varchar(16)," +
                "disposal varchar(24)," +
                "reason varchar(8)," +
                "message_id varchar(64) not null," +
                "PRIMARY KEY (transaction_id, sequence_number)" +
                ")";
    }


}
