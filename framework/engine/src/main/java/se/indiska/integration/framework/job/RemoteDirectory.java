package se.indiska.integration.framework.job;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by heintz on 2017-05-03.
 */
class RemoteDirectory {
    private String directory;
    private String filePattern;
    private Map<String, String> metadata = new HashMap<>();
    private Long id = null;

    public RemoteDirectory(String directory, String filePattern) throws SQLException {
        this.directory = directory;
        this.filePattern = filePattern;
    }

    public String getDirectory() {
        return directory;
    }

    public String getFilePattern() {
        return filePattern;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
