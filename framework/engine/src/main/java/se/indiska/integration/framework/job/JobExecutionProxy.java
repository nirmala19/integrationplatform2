package se.indiska.integration.framework.job;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

/**
 * JobExecutionProxy is a class which implements an Indio job that invokes the jobs through
 * the RESTful api.
 * Created by heintz on 2017-06-28.
 */
public class JobExecutionProxy {
    private static final Logger logger = LoggerFactory.getLogger(JobExecutionProxy.class);
    private ObjectMapper mapper = new ObjectMapper();
    private String identifier;
    private String url;
    private JobCompleteCallbackRetriever jobCompleteCallbackRetriever;


    public JobExecutionProxy(String identifier, String url, JobCompleteCallbackRetriever jobCompleteCallbackRetriever) {
        this.identifier = identifier;
        this.url = url;
        this.jobCompleteCallbackRetriever = jobCompleteCallbackRetriever;
    }

    public String identifier() {
        return identifier;
    }

    public String getUrl() {
        return url;
    }

    /**
     * @param jobExecutionData
     * @param traceHandle
     * @param callback
     * @return A ProcessExecutionResult if there is an immediate response. If it's async, return null. Used primarily to
     * return errors.
     * @throws Exception
     */
    public ProcessExecutionResult execute(IndioJob.JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        try {
            MDC.put("processId", traceHandle.getId().toString());
            String body = mapper.writeValueAsString(jobExecutionData);
            boolean successfulCall = false;
//        ExpandingRetryTime retryTime = new ExpandingRetryTime(2000, 3, 10);
            long retryInterval = 1000;
            long retries = 10;
            do {
                try {
                    String executeUrl = url + "/job/execute";
//                logger.debug("Executing job " + jobExecutionData.getJobId() + " by calling " + executeUrl);
                    jobCompleteCallbackRetriever.addCallback(traceHandle.getId(), callback);
                    logger.trace("Calling " + executeUrl);
                    String response = Request.Post(executeUrl)
                            .bodyString(body, ContentType.APPLICATION_JSON)
                            .connectTimeout(5000)
                            .socketTimeout(20000)
                            .execute()
                            .returnContent().asString();
                    successfulCall = true;
                    return null;
                } catch (IOException e) {
                    if (e instanceof HttpResponseException) {
                        logger.error("Unable to trigger job!", e);
                        this.jobCompleteCallbackRetriever.removeCallback(jobExecutionData.getProcessId());
                        return new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.MISSING_JOB);
                    } else {
                        if (--retries > 0) {
                            logger.warn("Unable to execute remote job " + jobExecutionData.getJobId() + ", " + jobExecutionData.getDataType() + ", retrying in " + retryInterval + " ms", e);
                            try {
                                Thread.sleep(retryInterval);
                            } catch (InterruptedException e1) {
                                //Not an error. Just continue.
                            }
                        } else {
                            logger.error("Unable to execute remote job, giving up!", e);
                            this.jobCompleteCallbackRetriever.removeCallback(jobExecutionData.getProcessId());
                            EventTriggerManager.getJobExecutionProxyMap().remove(jobExecutionData.getJobId());
                            return new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.MISSING_JOB);
                        }
                    }
                }
            } while (!successfulCall && retries > 0);
            return new ProcessExecutionResult(jobExecutionData.getProcessId(), ProcessStatus.ERROR, "Callback was not called, this is an error which may cause jobs to be paused indefinitely!", ProcessError.UNDEFINED_ERROR);
        } finally {
            MDC.remove("processId");
        }
    }

    public Collection<Long> getRunningProcessIds() throws IOException {
        Collection<Long> result = new HashSet<>();
        String executeUrl = url + "/job/runningprocesses";
        String response = Request.Get(executeUrl)
                .connectTimeout(2000)
                .socketTimeout(2000)
                .execute()
                .returnContent().asString();
        return Arrays.asList(mapper.readValue(response, Long[].class));

    }

    public interface JobCompleteCallbackRetriever {
        JobCompleteCallback getCallback(Long processId);

        void addCallback(Long processId, JobCompleteCallback proxy);

        void removeCallback(Long processId);
    }

//    private static final Logger logger = LoggerFactory.getLogger(JobExecutionProxy.class);
}
