package se.indiska.integration.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.indiska.integration.framework.DatasourceProvider;

import java.io.IOException;
import java.sql.*;
import java.util.*;

/**
 * Restful web service to retrieve and manipulate integration flows.
 */
@RestController
@RequestMapping("/integrations")
public class IntegrationFlows extends RestService {
    private static final Logger logger = LoggerFactory.getLogger(IntegrationFlows.class);
    private static ObjectMapper mapper = new ObjectMapper();

    public static void main(String[] argv) throws Exception {
        IntegrationFlows flows = new IntegrationFlows();
        mapper.writerWithDefaultPrettyPrinter().writeValue(System.out, flows.getMessageFlow("DISPATCHSYNC"));
    }


    /**
     * Get all integration flows.
     *
     * @return a ResponseEntity wrapping a list of maps containing the matching flows.
     */
    @RequestMapping(value = "/flow", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getFlows() throws JsonProcessingException {
        return ResponseEntity.ok(getMessageFlows());
    }

    /**
     * Get an integration flow based on a data type (i.e. <code>POSLOG</code> or <code>ARTICLE</code>)
     *
     * @param dataType The datatype
     * @return a ResponseEntity wrapping a list of maps containing the matching flows.
     */
    @RequestMapping(value = "/flow/{dataType}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getDatatypeFlow(@PathVariable("dataType") String dataType) {
        return ResponseEntity.ok(getMessageFlow(dataType));
    }

    private String getMessageFlows() throws JsonProcessingException {
        String sql = "SELECT DISTINCT(data_type) FROM job_schedule_triggers where data_type <> 'SYSTEM' order by data_type";
        Map<String, Object> result = new LinkedHashMap<>();
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                String dataType = rset.getString("data_type");
                result.put(dataType, getMessageFlow(dataType));
            }

        } catch (SQLException e) {
            logger.error("Unable to connect to database or get all jobs!", e);
        }
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
    }

    private List<Map<String, Object>> getMessageFlow(String dataType) {
        List<Map<String, Object>> jobs = new ArrayList<>();
        String sql = "SELECT\n" +
                "  jst.id,\n" +
                "  jst.job_id     jobid,\n" +
                "  jst.event_step eventstep,\n" +
                "  jst.data_type  datatype,\n" +
                "  jst.schedule,\n" +
                "  jst.job_status jobstatus,\n" +
                "  jst.parameters,\n" +
                "  j.description,\n" +
                "  'schedule' jobtype\n"+
                "FROM job_schedule_triggers jst\n" +
                "  JOIN job j\n" +
                "    ON j.id = jst.job_id\n" +
                "WHERE data_type = ?";

        try (Connection connection = DatasourceProvider.getConnection()) {

            jobs = getMultipleRows(connection, sql, new Object[]{dataType});
            for (Map<String, Object> job : jobs) {
                String eventStep = (String) job.get("eventstep");
                String paramString = (String) job.get("parameters");
                if (paramString != null) {
                    try {
                        Map params = mapper.readValue(paramString, Map.class);
                        if (params.containsKey("inbound_message_collector")) {
                            // Get remote server info
                            Map<String, Object> inboundMessageCollector = getInboundMessageCollector(connection, (Integer) params.get("inbound_message_collector"));
                            job.put("inboundMessageCollector", inboundMessageCollector);
                        }
                    } catch (IOException e) {
                        logger.error("Unable to json deserialize " + paramString + "!", e);
                    }
                    job.put("nextSteps", eventChain(connection, dataType, eventStep));
                }

            }
        } catch (SQLException e) {
            logger.error("Unable to perform query", e);
        }
        return jobs;
    }

    private List<Map<String, Object>> eventChain(Connection connection, String dataType, String eventStep) throws SQLException {
        List<Map<String, Object>> nextSteps = getNextEventSteps(connection, dataType, eventStep);
        for (Map<String, Object> nextStep : nextSteps) {

            String nextEventStep = (String) nextStep.get("nexteventstep");
            nextStep.put("nextSteps", eventChain(connection, dataType, nextEventStep));
        }
        if (eventStep.equals("OUTBOUND_FILE_LOAD")) {
            nextSteps.addAll(getOutboundSteps(connection, dataType));
        }
        return nextSteps;
    }

    private List<Map<String, Object>> getOutboundSteps(Connection connection, String dataType) throws SQLException {
        String sql = "SELECT\n" +
                "  'OUTBOUND_TRANSFER' eventstep,\n" +
                "  omd.remote_location remotelocationid,\n" +
                "  omd.remote_server   remoteserverid,\n" +
                "  rs.name             remoteservername,\n" +
                "  rs.username         remoteserverusername,\n" +
                "  rs.address          remoteserver,\n" +
                "  rs.protocol,\n" +
                "  rs.protocol_config  protocolconfig,\n" +
                "  rl.directory        remoteserverdirectory,\n" +
                "  rl.description      remoteserverdirectorydescription,\n" +
                "  'outbound_transfer' jobtype\n"+
                "FROM outbound_message_distributor omd\n" +
                "  JOIN remote_server rs\n" +
                "    ON omd.remote_server = rs.id\n" +
                "  JOIN remote_location rl\n" +
                "    ON omd.remote_location = rl.id\n" +
                "WHERE data_type = ?";
        Object[] values = new Object[]{dataType};
        List<Map<String, Object>> outbound = getMultipleRows(connection, sql, values);

        sql = "SELECT\n" +
                "  id,\n" +
                "  'STORE_OUTBOUND_TRANSFER' eventstep,\n" +
                "  data_type                 datatype,\n" +
                "  file_name_store_match     filenamestorematch\n" +
                "FROM store_outbound_message_distributor\n" +
                "WHERE data_type = ?";
        outbound.addAll(getMultipleRows(connection, sql, values));

        return outbound;
    }

    private List<Map<String, Object>> getNextEventSteps(Connection connection, String dataType, String triggerEventStep) throws SQLException {
        String sql = "SELECT\n" +
                "  jet.id         id,\n" +
                "  ? eventstep,\n" +
                "  j.id           jobid,\n" +
                "  j.description,\n" +
                "  jet.job_status jobstatus,\n" +
                "  j.event_type   nexteventstep,\n" +
                "  'event' jobtype\n"+
                "FROM\n" +
                "  job_event_triggers jet\n" +
                "  JOIN job j\n" +
                "    ON jet.job_id = j.id\n" +
                "WHERE data_type = ? AND event_step = ?";
        Object[] values = new Object[]{triggerEventStep, dataType, triggerEventStep};
        return getMultipleRows(connection, sql, values);
    }

    private Map<String, Object> getInboundMessageCollector(Connection connection, Integer id) throws SQLException {
        String sql = "SELECT\n" +
                "  imc.id         id,\n" +
                "  rs.id          remoteServerId,\n" +
                "  rs.name        remoteServerName,\n" +
                "  rs.username    remoteServerUsername,\n" +
                "  rs.address     remoteServer,\n" +
                "  rs.protocol,\n" +
                "  rs.protocol_config protocolConfig,\n" +
                "  rl.directory   remoteServerDirectory,\n" +
                "  rl.description remoteServerDirectoryDescription,\n" +
                "  imc.file_pattern filepattern,\n" +
                "  imc.content_datatype_resolver contentdatatyperesolver,\n" +
                "  rl.id          remoteLocationId,\n" +
                "  'inbound_transfer' jobtype\n"+
                "FROM\n" +
                "  inbound_message_collector imc\n" +
                "  JOIN remote_server rs\n" +
                "    ON rs.id = imc.remote_server\n" +
                "  JOIN remote_location rl\n" +
                "    ON rl.id = imc.remote_location\n" +
                "WHERE imc.id = ?\n";
        Object[] values = new Object[]{id};
        return getSingleRow(connection, sql, values);
    }

    public Map<String, Object> getSingleRow(Connection connection, String sql, Object[] parameters) throws SQLException {
        Map<String, Object> row = new HashMap<>();
        PreparedStatement ps = connection.prepareStatement(sql);
        for (int i = 1; i <= parameters.length; i++) {
            ps.setObject(i, parameters[i - 1]);
        }
        ResultSet rset = ps.executeQuery();
        ResultSetMetaData metaData = rset.getMetaData();
        if (rset.next()) {
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                String label = metaData.getColumnLabel(i);
                row.put(label, rset.getObject(i));
            }
        }
        rset.close();
        ps.close();
        return row;
    }

    public List<Map<String, Object>> getMultipleRows(Connection connection, String sql, Object[] parameters) throws SQLException {
        List<Map<String, Object>> rows = new ArrayList<>();
        PreparedStatement ps = connection.prepareStatement(sql);
        for (int i = 1; i <= parameters.length; i++) {
            ps.setObject(i, parameters[i - 1]);
        }
        ResultSet rset = ps.executeQuery();
        ResultSetMetaData metaData = rset.getMetaData();
        while (rset.next()) {
            Map<String, Object> row = new HashMap<>();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                String label = metaData.getColumnLabel(i);
                row.put(label, rset.getObject(i));
            }
            rows.add(row);
        }
        rset.close();
        ps.close();
        return rows;
    }
}
