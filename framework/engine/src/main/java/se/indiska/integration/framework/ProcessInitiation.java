package se.indiska.integration.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by heintz on 09/03/17.
 */
public class ProcessInitiation {
    private static final Logger logger = LoggerFactory.getLogger(ProcessInitiation.class);
    private Long processId;
    private Long previousStepProcessId;

    public ProcessInitiation(Long processId, Long previousStepProcessId) {
        this.processId = processId;
        this.previousStepProcessId = previousStepProcessId;
    }

    public Long getProcessId() {
        return processId;
    }

    public Long getPreviousStepProcessId() {
        return previousStepProcessId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProcessInitiation that = (ProcessInitiation) o;

        if (!getProcessId().equals(that.getProcessId())) return false;
        return getPreviousStepProcessId() != null ? getPreviousStepProcessId().equals(that.getPreviousStepProcessId()) : that.getPreviousStepProcessId() == null;
    }

    @Override
    public int hashCode() {
        int result = getProcessId().hashCode();
        result = 31 * result + (getPreviousStepProcessId() != null ? getPreviousStepProcessId().hashCode() : 0);
        return result;
    }
}
