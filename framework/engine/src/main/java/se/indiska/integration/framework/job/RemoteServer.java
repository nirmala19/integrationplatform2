package se.indiska.integration.framework.job;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by heintz on 2017-05-03.
 */
class RemoteServer {
    private static final Logger logger = LoggerFactory.getLogger(RemoteServer.class);
    private String protocol;
    private String address;
    private String username;
    private String password;
    private Long id;
    private Map<String, String> metadata = new HashMap<>();
    private Map<String, RemoteDirectory> remoteDirectories = new HashMap<>();
    private Map protocolConfig = new HashMap();
    private static ObjectMapper mapper = new ObjectMapper();

    public RemoteServer(ResultSet rset, Connection conn) throws SQLException, ConfigurationErrorException {
        this.protocol = rset.getString("protocol");
        this.address = rset.getString("address");
        this.username = rset.getString("username");
        this.password = rset.getString("password");
        this.id = rset.getLong("remote_server_id");

        String filePattern = null;
        ResultSetMetaData metaData = rset.getMetaData();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            if (metaData.getColumnLabel(i).equals("file_pattern")) {
                filePattern = rset.getString(i);
            }
        }

        String directory = rset.getString("directory");
        String protocolConfigStr = rset.getString("protocol_config");
        if (protocolConfigStr != null && protocolConfigStr.length() > 2) {
            try {
                this.protocolConfig = mapper.readValue(protocolConfigStr, Map.class);
            } catch (IOException e) {
                logger.error("Unable to read protocol configuration JSON into a Map!", e);
            }
        }
        if (directory.matches(".*?\\{.*?\\}.*?")) {
            String annotation = directory.substring(directory.indexOf("{") + 1, directory.indexOf("}"));
            for (MessageAnnotationHelper.Annotation replacement : MessageAnnotationHelper.substitutions(annotation, null, conn)) {
                String dir = directory.replace("{" + annotation + "}", replacement.getValue());
                RemoteDirectory remoteDirectory = remoteDirectories.get(dir);
                if (remoteDirectory == null) {
                    remoteDirectory = new RemoteDirectory(dir, filePattern);
                    remoteDirectory.setId(rset.getLong("remote_location_id"));
                    remoteDirectories.put(dir, remoteDirectory);
                    String key = annotation;
                    if (key.indexOf(":") >= 0) {
                        key = key.substring(key.indexOf(":") + 1);
                    }
                    remoteDirectory.getMetadata().put(key + ".key", replacement.getKey());
                    remoteDirectory.getMetadata().put(key + ".value", replacement.getValue());
                }
            }
        } else {
            RemoteDirectory remoteDirectory = remoteDirectories.get(directory);
            if (remoteDirectory == null) {
                remoteDirectory = new RemoteDirectory(directory, filePattern);
                remoteDirectory.setId(rset.getLong("remote_location_id"));
                remoteDirectories.put(directory, remoteDirectory);
            }
        }
    }

    public String getProtocol() {
        return protocol;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public String getAddress() {
        return address;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Map<String, RemoteDirectory> getRemoteDirectories() {
        return remoteDirectories;
    }

    public Long getId() {
        return id;
    }

    public Map getProtocolConfig() {
        return protocolConfig;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RemoteServer that = (RemoteServer) o;

        if (!getProtocol().equals(that.getProtocol())) return false;
        if (!getAddress().equals(that.getAddress())) return false;
        if (!getUsername().equals(that.getUsername())) return false;
        return getPassword().equals(that.getPassword());
    }

    @Override
    public int hashCode() {
        int result = getProtocol().hashCode();
        result = 31 * result + getAddress().hashCode();
        result = 31 * result + getUsername().hashCode();
        result = 31 * result + getPassword().hashCode();
        return result;
    }
}
