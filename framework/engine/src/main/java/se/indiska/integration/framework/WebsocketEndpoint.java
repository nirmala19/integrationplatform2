package se.indiska.integration.framework;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.job.JobServiceRunner;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by heintz on 27/03/17.
 */
@ServerEndpoint(value = "/ws", subprotocols = {"app", "auth", "model"})
public class WebsocketEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(WebsocketEndpoint.class);
    private static Map<String, ClientMessageHandler> messageHandlers = new HashMap<>();
    private static BlockingQueue broadcastQueue = new LinkedBlockingQueue(1000);
    private static ObjectMapper mapper = new ObjectMapper();
    private static boolean terminated = false;

    static {
        ExecutorsFactory.getExecutorService().submit(() -> {
            while (!terminated) {
                queueBroadcast();
            }
        });
        JobServiceRunner.getJobServiceRunner().addShutdownListener(() -> terminated = true);
    }

    private static void queueBroadcast() {
        List list = new ArrayList();
        try {
            broadcastQueue.drainTo(list, 20);
            if (list.isEmpty()) {
                Thread.sleep(5);
            } else {
                long start = System.nanoTime();
                broadcastMessage(list.toArray());
//                logger.debug("Time to broadcast " + list.size() + " items: " + (System.nanoTime() - start) / 1e6d + "ms");
            }
        } catch (InterruptedException e) {
            //Do nothing. Just continue and check if terminated==true.
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        ClientMessageHandler messageHandler = new ClientMessageHandler(session);
        session.addMessageHandler(messageHandler);
        messageHandlers.put(session.getId(), messageHandler);
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        if (closeReason.getCloseCode().getCode() > 1001) {
//            logger.info(String.format("Session %s closed because of code: %s, reason: %s", session.getId(), closeReason.getCloseCode(), closeReason.getReasonPhrase()));
        }
        ClientMessageHandler messageHandler = messageHandlers.remove(session.getId());
        if (messageHandler != null) {
            messageHandler.close();
        }
    }

    @OnError
    public void onError(Session session, Throwable thr) {
//        logger.error("Error occurred in websocket session " + session.getId(), thr);
    }

    public static void broadcast(Object object) {
        broadcastQueue.offer(object);
    }

    private static void broadcastMessage(Object... objects) {
        Set<ClientMessageHandler> msgHandlers = new HashSet<>(messageHandlers.values());
        for (ClientMessageHandler messageHandler : msgHandlers) {
            try {
//                for (Object object : objects) {
                messageHandler.writeMessage(mapper.writeValueAsString(objects));
//                }
            } catch (JsonProcessingException e) {
                logger.error("Unable to serialize message to JSON!", e);
            }
        }
    }

}