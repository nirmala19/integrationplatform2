package se.indiska.integration.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by heintz on 04/04/17.
 */
public class NotImplementedException extends RuntimeException {
    private static final Logger logger = LoggerFactory.getLogger(NotImplementedException.class);
}
