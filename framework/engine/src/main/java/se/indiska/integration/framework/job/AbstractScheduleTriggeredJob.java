//package se.indiska.integration.framework.job;
//
//import org.quartz.Job;
//import org.quartz.JobDataMap;
//import org.quartz.JobExecutionContext;
//import org.quartz.JobExecutionException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import se.indiska.integration.framework.IntegrationEngine;
//import se.indiska.integration.framework.Settings;
//import se.indiska.integration.framework.trace.ProcessTrace;
//
//import javax.sql.DataSource;
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
///**
// * Created by heintz on 2017-04-20.
// */
//public abstract class AbstractScheduleTriggeredJob implements Job, IndioJob {
//    private static final Logger logger = LoggerFactory.getLogger(AbstractScheduleTriggeredJob.class);
//    private static Set<RunningJob> runningJobs = new HashSet<>();
//    private static Thread cleanupThread = null;
//    private static boolean stopped = false;
//
//    static {
//        cleanupThread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (!stopped) {
//                    jobCleanup();
//                }
//                logger.info("Terminating job cleanup check thread");
//            }
//        });
//        cleanupThread.start();
//    }
//
//    private static void jobCleanup() {
////        logger.debug("Number of running jobs: "+runningJobs.size());
//        List<RunningJob> jobs = new ArrayList<>(runningJobs);
//        for (RunningJob runningJob : jobs) {
//            if (runningJob.getStartTime() < (System.currentTimeMillis() - 900000)) {
//                logger.debug("Job " + runningJob.getJobIdentitifier() + " has been running longer time than 15 minutes. Marking as not running.");
//                runningJobs.remove(runningJob);
//            }
//        }
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Override
//    public void execute(JobExecutionContext context) throws JobExecutionException {
//        String dataType = (String) context.getJobDetail().getJobDataMap().get("datatype");
//        String jobId = (String) context.getJobDetail().getJobDataMap().get("jobid");
//        String eventStep = (String) context.getJobDetail().getJobDataMap().get("eventstep");
//        JobDataMap dataMap = context.getJobDetail().getJobDataMap();
//        DataSource dataSource = (DataSource) context.getJobDetail().getJobDataMap().get("datasource");
////        logger.debug("Triggered " + this.getClass().getSimpleName() + " job with jobId " + jobId + " and datatype " + dataType);
//        String jobIdentitifier = jobId + dataType;
//        try (Connection connection = IntegrationEngine.getDs().getConnection()) {
//            if (!Settings.isIndioPaused(connection)) {
//                RunningJob runningJob = new RunningJob(jobIdentitifier);
//                if (!runningJobs.contains(runningJob)) {
//                    runningJobs.add(runningJob);
//
//                    ProcessTraceHandle traceHandle = ProcessStatusHandler.newStatus(jobId, eventStep, dataType, ProcessStatus.RUNNING, null, true, null);
////        sendWebSocketUpdate(traceHandle.getId(), dataType, eventStep, ProcessStatus.RUNNING);
//                    try {
//                        JobExecutionData executionData = new JobExecutionData(dataType, jobId, eventStep, dataMap);
//                        execute(executionData, traceHandle, null, new JobCompleteCallback() {
//                            @Override
//                            public void jobComplete(ProcessExecutionResult result) {
//                                traceHandle.getProcessTracer().trace(new ProcessTrace(traceHandle.getId(), result.getResult(), result.getErrorMessage(), result.getProcessError()));
//                                runningJobs.remove(runningJob);
//                            }
//                        });
//                    } catch (Throwable th) {
//                        String error = "Unable to execute job! ";
//                        logger.error(error, th);
//                        traceHandle.getProcessTracer().trace(new ProcessTrace(traceHandle.getId(), ProcessStatus.ERROR, error, ProcessError.UNDEFINED_ERROR));
//                    }
//
//                } else {
//                    logger.debug("Job " + jobId + "/" + dataType + " is already running ");
//                }
//            } else {
//                logger.debug("Not running job " + jobId + "/" + dataType + " - IndIO is paused.");
//            }
//        } catch (SQLException e) {
//            logger.error("Error while checking if indio is paused!");
//        }
//    }
//
//    class RunningJob {
//        private String jobIdentitifier = null;
//        private Long startTime = System.currentTimeMillis();
//
//        public RunningJob(String jobIdentitifier) {
//            this.jobIdentitifier = jobIdentitifier;
//            startTime = System.currentTimeMillis();
//        }
//
//        public Long getStartTime() {
//            return startTime;
//        }
//
//        @Override
//        public boolean equals(Object o) {
//            if (this == o) return true;
//            if (o == null || getClass() != o.getClass()) return false;
//
//            RunningJob that = (RunningJob) o;
//
//            return jobIdentitifier.equals(that.jobIdentitifier);
//        }
//
//        @Override
//        public int hashCode() {
//            return jobIdentitifier.hashCode();
//        }
//
//        public String getJobIdentitifier() {
//            return jobIdentitifier;
//        }
//    }
//}
