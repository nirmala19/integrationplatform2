package se.indiska.integration.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.indiska.integration.framework.DatasourceProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by heintz on 04/04/17.
 */
@RestController
@RequestMapping("/alert")
public class Alerts {
    private static final Logger logger = LoggerFactory.getLogger(Alerts.class);

    @RequestMapping(value = "/alert", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity active() {
        try (Connection conn = DatasourceProvider.getConnection()) {
            String sql = "SELECT\n" +
                    "  event_step,\n" +
                    "  data_type,\n" +
                    "  count(event_step) cnt\n" +
                    "FROM alert\n" +
                    "WHERE acknowledge_id < 0\n" +
                    "GROUP BY event_step, data_type;\n";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            List<Map<String, Object>> result = new ArrayList<>();
            while (rset.next()) {
                result.add(getAlertSummaryRow(rset));
            }
            return ResponseEntity.ok(result);
        } catch (SQLException e) {
            logger.error("Unable to query alert table!", e);
            return ResponseEntity.badRequest().body("Unable to get alerts!");
        }
    }


    @RequestMapping(value = "/servers", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity serverAlerts() {
        try (Connection conn = DatasourceProvider.getConnection()) {
            String sql = "SELECT * FROM remote_server_alert ORDER BY host, event_time desc";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            List<Map<String, Object>> result = new ArrayList<>();
            while (rset.next()) {
                Map<String, Object> row = new HashMap<>();
                row.put("protocol", rset.getString("protocol"));
                row.put("host", rset.getString("host"));
                row.put("direction", rset.getString("direction"));
                row.put("event_time", rset.getTimestamp("event_time"));
                row.put("message", rset.getString("message"));
                row.put("severity", rset.getString("severity"));
                result.add(row);
            }
            return ResponseEntity.ok(result);
        } catch (SQLException e) {
            logger.error("Unable to query alert table!", e);
            return ResponseEntity.badRequest().body("Unable to get alerts!");
        }
    }

    @RequestMapping(value = "/servers/{host}", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity resetAlert(@PathVariable("host") String host) {
        try (Connection conn = DatasourceProvider.getConnection()) {
            String sql = "DELETE FROM remote_server_alert WHERE host=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, host);
            ps.executeUpdate();
            return ResponseEntity.ok().build();
        } catch (SQLException e) {
            logger.error("Unable to query alert table!", e);
            return ResponseEntity.badRequest().body("Unable to get alerts!");
        }
    }

    @RequestMapping(value = "/events/{eventStep}/{dataType}", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity events(@PathVariable("eventStep") String eventStep, @PathVariable("dataType") String dataType) {
        try (Connection conn = DatasourceProvider.getConnection()) {
            String sql = "SELECT *\n" +
                    "FROM alert a\n" +
                    "  JOIN process_data_trace pdt\n" +
                    "  ON pdt.id=a.process_data_trace_id\n" +
                    "JOIN process_error pe\n" +
                    "  ON pe.process_data_trace_id=pdt.id\n" +
                    "WHERE event_step = ? AND data_type = ? AND acknowledge_id < 0";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, eventStep);
            ps.setString(2, dataType);
            ResultSet rset = ps.executeQuery();
            List<Map<String, Object>> result = new ArrayList<>();
            while (rset.next()) {
                result.add(getAlertEventRow(rset));
            }
            return ResponseEntity.ok(result);
        } catch (SQLException e) {
            logger.error("Unable to query alert table!", e);
            return ResponseEntity.badRequest().body("Unable to get alerts!");
        }
    }

    @RequestMapping(value = "/trace/{messageId}", produces = "application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> trace(@PathVariable("messageId") String messageId) {
        String sql = "SELECT\n" +
                "  pdt.id data_trace_id,\n" +
                "  ps.id process_id,\n" +
                "  ps.job_id,\n" +
                "  ps.event_step,\n" +
                "  ps.data_type,\n" +
                "  pdt.source,\n" +
                "  pdt.source_id,\n" +
                "  pdt.target,\n" +
                "  pdt.target_id,\n" +
                "  pdt.message_id,\n" +
                "  pdt.status\n" +
                "FROM process_data_trace pdt\n" +
                "  JOIN process_status ps\n" +
                "    ON ps.id = pdt.process_id\n" +
                "WHERE\n" +
                "  pdt.message_id = ?\n" +
                "ORDER BY pdt.id;";
        List<Map> result = new ArrayList<>();
        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, messageId);
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                result.add(getTraceRow(rset));
            }
        } catch (SQLException e) {
            logger.error("Unable to get message trace!", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unable to get trace " + e.getMessage());
        }
        return ResponseEntity.ok(result);
    }


    @RequestMapping(value = "/acknowledge", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public ResponseEntity<?> acknowledge(@RequestBody Long[] ids) {
        if (ids != null) {
            try (Connection conn = DatasourceProvider.getConnection()) {
                String sql = "UPDATE alert SET active=FALSE WHERE id=?";
                PreparedStatement ps = conn.prepareStatement(sql);
                for (Long id : ids) {
                    ps.setLong(1, id);
                    ps.addBatch();
                }
                ps.executeBatch();
                ps.close();
            } catch (SQLException e) {
                logger.error("Unable to acknowledge alert!");
            }
        }
        return ResponseEntity.ok().build();
    }

    private Map<String, Object> getAlertSummaryRow(ResultSet rset) throws SQLException {
        Map<String, Object> row = new LinkedHashMap<>();
        row.put("event_step", rset.getString("event_step"));
        row.put("data_type", rset.getString("data_type"));
        row.put("count", rset.getString("cnt"));
        return row;
    }

    private Map<String, Object> getAlertItems(ResultSet rset) throws SQLException {
        Map<String, Object> row = new LinkedHashMap<>();
        row.put("id", rset.getLong("id"));
        row.put("stage", rset.getString("stage"));
        row.put("data_type", rset.getString("data_type"));
        row.put("type", rset.getString("type"));
        row.put("time", rset.getTimestamp("event_time"));
        row.put("active", rset.getBoolean("active"));
        return row;
    }

    private Map<String, Object> getAlertEventRow(ResultSet rset) throws SQLException {
        Map<String, Object> row = new LinkedHashMap<>();
        row.put("id", rset.getLong("id"));
        row.put("source", rset.getString("source"));
        row.put("source_id", rset.getString("source_id"));
        row.put("target", rset.getString("target"));
        row.put("target_id", rset.getString("target_id"));
        row.put("message_id", rset.getString("message_id"));
        row.put("error", rset.getString("error"));
        row.put("time", rset.getTimestamp("event_time"));
        return row;
    }

    private Map<String, Object> getTraceRow(ResultSet rset) throws SQLException {
        Map<String, Object> row = new LinkedHashMap<>();
        row.put("data_trace_id", rset.getLong("data_trace_id"));
        row.put("process_id", rset.getLong("process_id"));
        row.put("job_id", rset.getString("job_id"));
        row.put("data_type", rset.getString("data_type"));
        row.put("source", rset.getString("source"));
        row.put("source_id", rset.getString("source_id"));
        row.put("target", rset.getString("target"));
        row.put("target_id", rset.getString("target_id"));
        row.put("status", rset.getString("status"));

        return row;
    }
}
