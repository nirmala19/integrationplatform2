package se.indiska.integration.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

public class RegistrationFailureException extends Exception {
    private static final Logger logger = LoggerFactory.getLogger(RegistrationFailureException.class);

    public RegistrationFailureException(String s, SQLException e) {
        super(s, e);
    }
}
