package se.indiska.integration.framework.connection;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.net.ftp.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.job.ProcessError;
import se.indiska.integration.framework.util.Counter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

/**
 * A connector to retrieve files over FTP. See also <code>RemoteConnector</code>
 */
public class FTPConnector extends RemoteConnector {
    private static final Logger logger = LoggerFactory.getLogger(FTPConnector.class);

    private FTPClient ftp = new FTPClient();

    public FTPConnector(String host, String username, String credentials, Map protocolConfig) {
        super(host, username, credentials, protocolConfig);
        ftp = new FTPClient();
        FTPClientConfig config = new FTPClientConfig();
        ftp.configure(config);
    }

    @Override
    public void fetchFiles(String directory, String pattern, Counter maxFiles, FileReceiver fileReceiver) throws ConnectionFailureException {
        // for example config.setServerTimeZoneId("Pacific/Pitcairn")
//        logger.debug("Fetching files from ftp://" + getHost() + "/" + directory + " using pattern " + pattern);
        if (logger.isTraceEnabled()) {
            logger.trace("Fetching files from ftp://" + getUsername() + ":" + getCredentials() + "@" + this.getHost() + directory + "/" + pattern);
        }

        boolean error = false;
        try {
            int reply;

            connect();

            if (!ftp.changeWorkingDirectory(directory)) {
                throw new ConnectionFailureException("Unable to access directory " + directory + " on ftp host " + getHost() + " using user " + getUsername());
            }


            Calendar aMinuteAgo = new GregorianCalendar();
            aMinuteAgo.add(Calendar.MINUTE, -1);
            FTPFile[] files = ftp.listFiles(".", new FTPFileFilter() {
                @Override
                public boolean accept(FTPFile ftpFile) {
                    if (ftpFile == null) {
                        logger.debug("FTPFile is null!");
                        return false;
                    }
                    boolean match = ftpFile.getName().matches(pattern) && ftpFile.getTimestamp().before(aMinuteAgo) && maxFiles.reduce() > 0;
                    if (logger.isTraceEnabled()) {
                        logger.trace("Checking if file " + ftpFile.getName() + " matches pattern " + pattern + " and created more than a minute ago " + ftpFile.getTimestamp().toString() + " and files left > 0: " + maxFiles.get() + ": " + match);
                    }
                    return match;
                }
            });


            reply = ftp.getReplyCode();

            if (!FTPReply.isPositiveCompletion(reply)) {
                String cause = "<Unknown case>";
                switch (reply) {
                    case FTPReply.NOT_LOGGED_IN:
                        cause = "Could not login";
                }
                ftp.disconnect();
                throw new ConnectionFailureException("Get files from " + getHost() + " in dir " + directory + ": " + cause);
            }

            ftp.setSoTimeout(60000);
            ftp.setDataTimeout(60000);
            ftp.pasv();
            String source = "ftp://" + getHost() + "/" + directory;
            for (FTPFile file : files) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Fetching file " + file.getName());
                }
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ftp.retrieveFile("./" + file.getName(), baos);
                if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
                    fileReceiver.receiveFile(source, file.getName(), baos.toByteArray());
                } else if (ftp.getReplyCode() != FTPReply.FILE_UNAVAILABLE) {
                    String errorMessage = "";
                    for (String e : ftp.getReplyStrings()) {
                        errorMessage += e + "\n";
                    }
                    fileReceiver.receiveFileError(source, file.getName(), errorMessage, ProcessError.CONNECTION_FAILURE);
                }
            }
        } catch (IOException e) {
            error = true;
            logger.error("Unable to fetch files from ftp server " + getHost(), e);
            throw new ConnectionFailureException("Unable to fetch files from FTP server " + getHost(), e);
        } finally {
            fileReceiver.done();
        }
    }

    private boolean connect() throws IOException, ConnectionFailureException {
        if (!ftp.isConnected()) {
            ftp.connect(getHost());
            ftp.user(getUsername());
            ftp.pass(getCredentials());

            if (logger.isTraceEnabled()) {
                logger.trace("Connecting to ftp://" + getHost() + " using username/password: " + getUsername() + "/" + getCredentials());
            }

            FTPClientConfig conf = new FTPClientConfig();
            if (getProtocolConfig().get("SYSTEM_TYPE") != null) {
                conf = new FTPClientConfig((String) getProtocolConfig().get("SYSTEM_TYPE"));
            }
            ftp.configure(conf);

            ftp.enterLocalPassiveMode();
//            ftp.enterRemotePassiveMode();


            // After connection attempt, you should check the reply code to verify
            // success.
            int reply = ftp.getReplyCode();

            if (!FTPReply.isPositiveCompletion(reply)) {
                String cause = "<Unknown case>";
                switch (reply) {
                    case FTPReply.NOT_LOGGED_IN:
                        cause = "Could not login";
                }
                ftp.disconnect();
                throw new ConnectionFailureException("Unable to connect to remote server " + getHost() + " using credentials provided: " + cause + ", " + reply);
            } else {
                return true;
            }
        }
        return true;
    }

    @Override
    public void putFile(String directory, String filename, String content) throws ConnectionFailureException {
        ByteArrayInputStream bais = new ByteArrayInputStream(content.getBytes(getCharset(filename, Direction.OUTBOUND, StandardCharsets.UTF_8)));
        try {
            connect();
            String[] pathItems = directory.split("/");
            String path = ftp.printWorkingDirectory();
            if (directory.startsWith("/")) {
                ftp.changeWorkingDirectory("/");
            }
            for (String pathItem : pathItems) {
                if (pathItem.length() > 0) {
                    if (!ftp.changeWorkingDirectory(pathItem)) {
                        ftp.makeDirectory(pathItem);
                        if (!ftp.changeWorkingDirectory(pathItem)) {
                            throw new ConnectionFailureException("Unable to put file ftp://" + getHost() + "/" + directory + "/" + filename + ": " + ftp.getReplyString());
                        }
                    }
                }
            }

//            ftp.makeDirectory(directory);
//            for (String replyString : ftp.getReplyStrings()) {
//                logger.debug("  - " + replyString);
//            }
            ftp.changeWorkingDirectory(path);
            boolean couldStoreFile;
            synchronized (FTPConnector.class) {
                couldStoreFile = ftp.storeFile(directory + "/" + filename, bais);
                FTPFile[] fs = ftp.listFiles(directory + "/" + filename);
                couldStoreFile = couldStoreFile && fs.length == 1 && fs[0].getSize() > 0;
            }

            if (!couldStoreFile) {
                throw new ConnectionFailureException("Unable to put file ftp://" + getHost() + "/" + directory + "/" + filename + ": " + ftp.getReplyString());
            }
            bais.close();
        } catch (IOException e) {
            throw new ConnectionFailureException("Unable to put file ftp://" + getHost() + "/" + directory + "/" + filename, e);
        }
    }

    @Override
    public boolean deleteRemoteFile(String directory, String filename) {
        try {
            boolean couldDelete = ftp.deleteFile(filename);
            return couldDelete;
        } catch (IOException e) {
            logger.error("Unable to delete file " + directory + "/" + filename + " on ftp server " + getHost(), e);
            return false;
        }
    }


    @Override
    public void close() {
        if (ftp.isConnected()) {
            try {
                ftp.disconnect();
            } catch (IOException ioe) {
                // do nothing
            }
        }
    }

    public static void main(String[] argv) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Map protocolConfig = mapper.readValue("{ \"SYSTEM_TYPE\":\"UNIX\" }", Map.class);
//        Map protocolConfig = new HashMap();
        FTPConnector ftpConnector = new FTPConnector("spider.noventus.com", "uindiska", "mvQ72A832w9R3BL", null);
        ftpConnector.fetchFiles("/out/DispatchSync", ".*?\\.xml", new Counter(100), new FileReceiver() {
            @Override
            public void receiveFile(String source, String filename, byte[] content) {
                logger.debug("Received file " + source + " - " + filename);
            }

            @Override
            public void receiveFileError(String source, String filename, String error, ProcessError processError) {
                logger.debug("Error receiving file " + source + " - " + filename + " - " + error);
            }

            @Override
            public void done() {
                logger.debug("DONE!");
            }
        });
    }
}
