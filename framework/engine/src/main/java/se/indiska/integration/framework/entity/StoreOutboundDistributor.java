package se.indiska.integration.framework.entity;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class StoreOutboundDistributor extends MessageDistributor {
    private static final Logger logger = LoggerFactory.getLogger(StoreOutboundDistributor.class);
    private String filenameStoreMatchPattern = null;

    public String getFilenameStoreMatchPattern() {
        return filenameStoreMatchPattern;
    }

    public void setFilenameStoreMatchPattern(String filenameStoreMatchPattern) {
        this.filenameStoreMatchPattern = filenameStoreMatchPattern;
    }

}
