package se.indiska.integration.integrations.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by heintz on 27/02/17.
 */
public class POSLogParser {
    private static final Logger logger = LoggerFactory.getLogger(POSLogParser.class);

    public void parse(File poslogFile, DataSource dataSource) throws ParserConfigurationException, IOException, SAXException, SQLException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(poslogFile);
        Element root = document.getDocumentElement();
        NodeList nodeList = root.getChildNodes();
        int count = 0;
        int modulo = 100;
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node n = nodeList.item(i);
            if (n instanceof Element) {
                if ("Transaction".equals(n.getNodeName())) {
                    long start = System.nanoTime();
                    Transaction transaction = new Transaction(null);
                    transaction.parse((Element) n, poslogFile);
                    transaction.store(dataSource);
                }
            }
        }
    }

}
