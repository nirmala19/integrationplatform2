package se.indiska.integration.framework.job;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.ExecutorsFactory;
import se.indiska.integration.framework.IntegrationCore;
import se.indiska.integration.framework.Settings;
import se.indiska.integration.framework.trace.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by heintz on 06/03/17.
 */
@DisallowConcurrentExecution
public class EventTriggerManager {

    private static final Logger logger = LoggerFactory.getLogger(EventTriggerManager.class);
    private BlockingQueue<ProcessQueueItem> executionQueue = new ArrayBlockingQueue<>(10000, true);
    private ProcessExecution processExecution;
    private static ObjectMapper mapper = new ObjectMapper();

    private static Map<String, ConcurrentExecution> concurrentExecutionWatch = new ConcurrentHashMap<>();
    private static Set<Long> runningExecutions = ConcurrentHashMap.newKeySet();
    private static Map<String, JobExecutionProxy> jobExecutionProxyMap = new HashMap<>();
    private Map<String, String> jobClassMap = new HashMap<>();

    public EventTriggerManager() {
        processExecution = new ProcessExecution();
        IntegrationCore.addShutdownListener(() -> processExecution.stop());
    }

    public static Map<String, JobExecutionProxy> getJobExecutionProxyMap() {
        return jobExecutionProxyMap;
    }

    public static void registerJobProxy(JobExecutionProxy jobExecutionProxy) {
        jobExecutionProxyMap.put(jobExecutionProxy.identifier(), jobExecutionProxy);
    }

    public static Map<String, Collection<Long>> getRegisteredJobs() throws IOException {
        Map<String, Collection<Long>> response = new HashMap<>();
        for (String jobId : jobExecutionProxyMap.keySet()) {
            JobExecutionProxy proxy = jobExecutionProxyMap.get(jobId);
            response.put(jobId, proxy.getRunningProcessIds());
        }
        return response;
    }

//    private DataProcessTracer getTracer() {
//        return IntegrationCore.getProcessTracer();
//    }


    public synchronized void execute() throws JobExecutionException {
        if (logger.isTraceEnabled()) {
            logger.trace("Executing EventTriggerManager.execute()");
        }
        long start = System.nanoTime();


        String jobSql = "SELECT DISTINCT id, class FROM job";


        try (Connection connection = DatasourceProvider.getConnection()) {
            synchronized (EventTriggerManager.class) {
                jobClassMap.clear();
                PreparedStatement ps = connection.prepareStatement(jobSql);
                ResultSet rset = ps.executeQuery();
                while (rset.next()) {
                    String jobId = rset.getString("id");
                    String jobClass = rset.getString("class");
                    jobClassMap.put(jobId, jobClass);
                }
                rset.close();
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        String sql = "SELECT ps.status, ps.id, ps.id process_id, ps.event_step, ps.job_id, ps.data_type, ps.parameters, ps.allow_concurrent FROM process_status ps, job WHERE ps.status=? AND ps.job_id=job.id AND job.job_status='ACTIVE' ORDER BY ps.id LIMIT 2500";
        boolean foundToDo = false;
        try (Connection connection = DatasourceProvider.getConnection()) {
            if (!Settings.isIndioPaused(connection)) {
                start = System.nanoTime();
                long s2 = System.nanoTime();
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setQueryTimeout(3);
                ps.setString(1, "NEW");
                ResultSet rset = ps.executeQuery();
                double tta = (System.nanoTime() - start) / 1e6d;
                if (tta > 1500) {
                    logger.warn("Time to execute 'get all jobs that have been triggered' took " + tta + "ms");
                }
                while (rset.next()) {
                    foundToDo = true;
                    Long processId = rset.getLong("process_id");
                    String jobId = rset.getString("job_id");
                    String dataType = rset.getString("data_type");
                    String eventStep = rset.getString("event_step");
                    String parameters = rset.getString("parameters");
                    Boolean allowConcurrent = rset.getBoolean("allow_concurrent");
                    Map parameterMap = new HashMap();
                    if (parameters != null) {
                        parameterMap = mapper.readValue(parameters, Map.class);
                    }


                    enqueueJob(processId, jobId, dataType, eventStep, parameterMap, allowConcurrent);
                }
                rset.close();
                ps.close();
            }
        } catch (SQLException e) {
            logger.error("SQL Error!", e);
        } catch (Throwable th) {
            logger.error("SHIT!", th);
        }
        double tta = (System.nanoTime() - start) / 1e6d;
        if (foundToDo && tta > 1500) {
            logger.warn("Time to trigger event jobs reached warning threshold of 1500ms: " + tta + "ms");
        }
    }


    private boolean enqueueJob(Long processId, String jobId, String dataType, String eventStep, Map parameterMap, boolean allowsConcurrent) throws InterruptedException, SQLException {
        String jobIdentifier = jobId + dataType;
        MDC.put("processId", processId != null ? processId.toString() : "N/A");
        try {
            synchronized (EventTriggerManager.class) {
                // if process already running or if the same job/datatype is already running and
                //

                if (executionQueue.contains(processId) || (concurrentExecutionWatch.containsKey(jobIdentifier) && !allowsConcurrent)) {
                    logger.trace("Cannot run process " + processId + " (" + jobIdentifier + "), job currently running");
                    if (!checkRunningJobs(jobIdentifier)) {
                        logger.warn("Job not really running, this should be fixed: " + jobIdentifier);
                        concurrentExecutionWatch.remove(jobIdentifier);
                        executionQueue.remove(processId);

                    }
                    return false;
                } else {
                    IndioJob.JobExecutionData jobExecutionData = new IndioJob.JobExecutionData(processId, dataType, jobId, eventStep, parameterMap, allowsConcurrent);
                    ProcessQueueItem queueItem = new ProcessQueueItem(jobId, jobExecutionData, processId);
                    concurrentExecutionWatch.put(jobIdentifier, new ConcurrentExecution(queueItem));
                    ProcessStatusHandler.statusUpdate(processId, ProcessStatus.INITIATED);
                    executionQueue.put(queueItem);
                    return true;
                }
                // allowsConcurrent =>
            }
        } finally {
            MDC.remove("processId");
        }
    }


    public static Map<String, ConcurrentExecution> getConcurrentExecutionWatch() {
        return concurrentExecutionWatch;
    }

    public static Set<Long> getRunningExecutions() {
        return runningExecutions;
    }

    private boolean checkRunningJobs(String jobIdDataType) throws SQLException {
        String sql = "SELECT id\n" +
                "FROM process_status\n" +
                "WHERE\n" +
                "  job_id || data_type = ?\n" +
                "  AND (\n" +
                "    status = 'RUNNING'\n" +
                "    OR\n" +
                "    status = 'NEW'\n" +
                "    OR\n" +
                "    status = 'INITIATED'\n" +
                "  )";
        boolean exists = false;
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, jobIdDataType);
            ResultSet rset = ps.executeQuery();
            exists = rset.next();
            ps.close();
        }
        return exists;
    }

    private void processExecution() {

        try {
            List<Trace> traces = Collections.synchronizedList(new ArrayList<>());
            ProcessQueueItem processItem = executionQueue.poll(5000, TimeUnit.MILLISECONDS);
            if (processItem == null) {
                return;
            }
            Long processId = processItem.getProcessId();
            MDC.put("processId", processId.toString());
            ProcessTraceHandle processTraceHandle = new ProcessTraceHandle(processId, new DataProcessTracer() {
                @Override
                public void trace(Trace... trace) {
                    traces.addAll(Arrays.asList(trace));
                }
            });
            if (processItem != null) {
                ListenableFuture future = ExecutorsFactory.getExecutorService().submit(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        String jobIdentifier = processItem.getExecutionData().getJobId() + processItem.getExecutionData().getDataType();
                        runningExecutions.add(processId);
                        JobExecutionProxy job = jobExecutionProxyMap.get(processItem.getJobId());
                        if (job != null) {
                            ProcessExecutionResult processExecutionResult = job.execute(processItem.getExecutionData(), processTraceHandle, new JobCompleteCallback() {
                                @Override
                                public void jobComplete(ProcessExecutionResult result) {
                                    runningExecutions.remove(processId);
                                    concurrentExecutionWatch.remove(jobIdentifier);
                                    if (!result.isCompletelyDone()) {
                                        ProcessStatusHandler.newJob(
                                                processItem.getExecutionData().getJobId(),
                                                processItem.getExecutionData().getEventStep(),
                                                processItem.getExecutionData().getDataType(),
                                                processItem.getExecutionData().getDataMap(),
                                                processItem.getExecutionData().allowsConcurrent()
                                        );

                                    }
                                }

                                @Override
                                public Collection<Long> runningProcessIds() throws IOException {
                                    return job.getRunningProcessIds();
                                }
                            });
                            if (processExecutionResult != null) {
                                ProcessTracerFactory.getProcessTracer().trace(new ProcessTrace(processExecutionResult.getProcessId(),
                                        processExecutionResult.getResult(),
                                        processExecutionResult.getErrorMessage(),
                                        processExecutionResult.getProcessError()));
                            }
                        } else {
                            ProcessTracerFactory.getProcessTracer().trace(new ProcessTrace(processId, ProcessStatus.MISSING_JOB));
                            logger.warn("Job " + jobIdentifier + " is not initialized yet!");
                            runningExecutions.remove(processId);
                            concurrentExecutionWatch.remove(jobIdentifier);
                        }
                        return null;
                    }
                });

                ListenableFuture timeoutFuture = Futures.withTimeout(future, 300, TimeUnit.SECONDS, ExecutorsFactory.getScheduledExecutorService());
                Futures.addCallback(timeoutFuture, new FutureCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Do nothing. Everything should be OK!
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        StringWriter writer = new StringWriter();
                        throwable.printStackTrace(new PrintWriter(writer));


                        processTraceHandle.getProcessTracer().trace(new ProcessTrace(processItem.getProcessId(), ProcessStatus.ERROR, "Failed to execute job " + processItem.getExecutionData().getJobId() + " for " + processItem.getExecutionData().getDataType() + "\nError:\n" + writer.toString(), ProcessError.UNDEFINED_ERROR));
                        DataProcessTracer tracer = new DataProcessTracerImpl();
                        tracer.trace(traces.toArray(new Trace[traces.size()]));
//                        processItem.getJob().terminate();
                        logger.error("Error!", throwable);
                    }
                });


            } else {
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            logger.warn("Interrupted while waiting for polling queue. Shouldn't interfere with processing.");
        } finally {
            MDC.remove("processId");
        }
    }

    class ProcessExecution {

        private boolean terminated = false;
        private ListenableFuture future;

        public ProcessExecution() {

            future = ExecutorsFactory.getExecutorService().submit(
                    new Runnable() {
                        @Override
                        public void run() {
                            while (!terminated) {
                                try {
                                    processExecution();
                                } catch (Throwable e) {
                                    logger.error("Error while processing job execution!", e);
                                    try {
                                        Thread.sleep(2000);
                                    } catch (InterruptedException e1) {
                                        // Do nothing
                                    }
                                }
                            }
                        }
                    }
            );
        }

        public void stop() {
            this.terminated = true;
            future.cancel(true);
        }
    }

    class ProcessQueueItem {
        private IndioJob.JobExecutionData executionData;
        private String jobId;
        private Long processId;

        public ProcessQueueItem(String jobId, IndioJob.JobExecutionData executionData, Long processId) {
            this.jobId = jobId;
            this.executionData = executionData;
            this.processId = processId;
        }

        public IndioJob.JobExecutionData getExecutionData() {
            return executionData;
        }

        public Long getProcessId() {
            return processId;
        }

        public String getJobId() {
            return jobId;
        }
    }

    class ConcurrentExecution {
        private ProcessQueueItem processInfo;
        private Long startTimeInMillis;

        public ConcurrentExecution(ProcessQueueItem queueItem) {
            this.processInfo = queueItem;
            this.startTimeInMillis = System.currentTimeMillis();
        }

        public ProcessQueueItem getProcessInfo() {
            return processInfo;
        }

        public Long getTimeRunning() {
            return System.currentTimeMillis() - this.startTimeInMillis;
        }

    }

    public int getExecutionQueueLength() {
        return executionQueue.size();
    }
}
