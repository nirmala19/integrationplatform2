package se.indiska.integration;

import com.zaxxer.hikari.HikariDataSource;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFilenameFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.ExecutorsFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heintz on 2017-04-10.
 */
public class CheckPosLogs {
    private static final Logger logger = LoggerFactory.getLogger(CheckPosLogs.class);

    public static void main(String[] argv) throws Exception {

//        String filename = "HQ20170518000038940.07.0000000900";
//
//        logger.debug("Match: " + filename.matches("HQ20170518.*?\\.06\\..*"));
//
//        if (true) {
//            return;
//        }


        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl("jdbc:postgresql://localhost/indiska2");
        ds.setMinimumIdle(10);
        ds.setMaximumPoolSize(50);
        ds.setUsername("indiska");
        ds.setPassword("indiska");

        List<Store> stores = new ArrayList<>();
        String sql = "SELECT * FROM store WHERE network_address IS NOT NULL";
        try (Connection connection = ds.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                stores.add(new Store(rset.getLong("id"), rset.getString("network_address")));
            }
        }

//        Store s = new Store(666l, "shk-ftp02.hk.indiska.se");
//        stores.add(s);

        for (Store store : stores) {
            ExecutorsFactory.getExecutorService().submit(new Runnable() {
                @Override
                public void run() {
                    logger.debug("Store " + store.id);
                    try {
                        long start = System.nanoTime();
                        NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(null, "administrator", "flipper");
//                        String share = "filetransfer";
                        String uri = "smb://" + store.ip + "/ExtPgm/Trans/ToHQ/";
                        SmbFile dir = new SmbFile(uri, auth);

                        int poslogCount = 0, pmlogCount = 0;
                        LocalDateTime now = LocalDateTime.now();
                        now = now.minusMinutes(30);

//                        SmbFile[] files = dir.listFiles("PMLog*");
                        SmbFile[] files = dir.listFiles(new SmbFilenameFilter() {
                            @Override
                            public boolean accept(SmbFile smbFile, String name) throws SmbException {

                                return !smbFile.isDirectory() && name.matches("POSLog\\.20171031.*");
                            }
                        });

                        for (SmbFile file : files) {
                            logger.debug(" - " + file.getName());
//                            InputStream is = file.getInputStream();
//                            File localDir = new File("/Users/heintz/Projects/Indiska/ICC/pmlogs09/" + store.id);
//                            if (!localDir.exists()) {
//                                localDir.mkdirs();
//                            }
//                            File localFile = new File(localDir, file.getName());
//                            FileOutputStream fos = new FileOutputStream(localFile);
//                            byte[] buf = new byte[1024];
//                            int c = 0;
//                            while ((c = is.read(buf)) >= 0) {
//                                fos.write(buf, 0, c);
//                            }
//                            fos.close();
//                            is.close();
                        }

//                        logger.debug("Time to get " + files.length + " POSlogs for 2017-03-03 from " + store.id + ": " + (System.nanoTime() - start) / 1e6d + "ms");
                    } catch (Exception e) {
                        logger.debug("Unable to get poslogs from " + store.id + " (" + store.ip + ")", e);
                    }
                }
            });

        }
    }

    static class Store {
        String ip = null;
        Long id = null;

        public Store(Long id, String ip) {
            this.ip = ip;
            this.id = id;
        }
    }
}
