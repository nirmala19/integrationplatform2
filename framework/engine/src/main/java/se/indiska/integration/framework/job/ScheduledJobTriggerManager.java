package se.indiska.integration.framework.job;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.Settings;

import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.impl.matchers.GroupMatcher.groupEquals;

/**
 * ScheduledJobTriggerManager handles all scheduled jobs by inserting a new process into <code>process_status</code>
 * table. There the EventTriggerManager takes the created event and starts the job.
 * <p>
 * The ScheduledJobTriggerManager uses the Quartz framework and cron expressions for scheduling jobs.
 * <p>
 * Created by heintz on 03/04/17.
 */
@DisallowConcurrentExecution
public class ScheduledJobTriggerManager implements Job {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledJobTriggerManager.class);
    private static final String TRIGGER_GROUP = ScheduledJobTriggerManager.class.getSimpleName();
    private static ObjectMapper mapper = new ObjectMapper();
    private static boolean paused = false;

    @Override
    public synchronized void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        // Get all information for jobs scheduled
        String sql = "SELECT jst.id trigger_id, event_step, job_id, class, data_type, schedule, j.job_status, jst.job_status schedule_status, last_updated, parameters, allow_concurrent FROM job_schedule_triggers jst, job j WHERE jst.job_id=j.id";
        Scheduler scheduler = jobExecutionContext.getScheduler();
        try (Connection conn = DatasourceProvider.getConnection()) {
            // Continously update the paused variable based on value in settings table
            paused = Settings.isIndioPaused(conn);

            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            Set<TriggerKey> triggerKeys = getAllTriggers(scheduler);
            while (rset.next()) {
                Long triggerId = rset.getLong("trigger_id");
                String jobId = rset.getString("job_id");
                String uniqJobId = triggerId + "_" + jobId;
                String dataType = rset.getString("data_type");
                String eventStep = rset.getString("event_step");
                String jobClass = rset.getString("class");
                String schedule = rset.getString("schedule");
                String jobStatus = rset.getString("job_status");
                String parameters = rset.getString("parameters");
                String scheduleStatus = rset.getString("schedule_status");
                Boolean allowConcurrent = rset.getBoolean("allow_concurrent");
                Timestamp lastUpdatedTimestamp = rset.getTimestamp("last_updated");

                TriggerKey triggerKey = new TriggerKey(uniqJobId, TRIGGER_GROUP);
                JobKey jobKey = new JobKey(uniqJobId, TRIGGER_GROUP);
                triggerKeys.remove(triggerKey);
                Trigger trigger = scheduler.getTrigger(triggerKey);

                Map<String, Object> jobParameters = new HashMap<>();
                if (parameters != null) {
                    try {
                        jobParameters = mapper.readValue(parameters, Map.class);
                    } catch (IOException e) {
                        logger.error("Unable to parse job parameters " + parameters, e);
                    }
                }

                // Check if the job is already triggered/running
                if (trigger != null) {
                    /**
                     * There is a trigger active for this job. Reschedule the job if the job_status is ACTIVE,
                     * pause it if its job_status is PAUSED or unschedule if the job_status is DISABLED.
                     */
                    if (JobStatus.ACTIVE.name().equals(jobStatus) && JobStatus.ACTIVE.name().equals(scheduleStatus)) {
                        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
                        jobDetail.getJobDataMap().putAll(jobParameters);
                        Long lastupdated = (Long) jobDetail.getJobDataMap().get("lastupdated");
                        if (lastupdated.longValue() != lastUpdatedTimestamp.getTime()) {
                            logger.debug("Rescheduling job " + uniqJobId);
                            try {
                                JobDataMap dataMap = new JobDataMap();
                                dataMap.put("datatype", dataType);
                                dataMap.put("eventstep", eventStep);
                                dataMap.put("allowconcurrent", allowConcurrent);
                                dataMap.put("jobid", jobId);
                                dataMap.put("triggerid", triggerId);
                                dataMap.putAll(jobParameters);
                                dataMap.put("lastupdated", lastUpdatedTimestamp.getTime());
                                CronScheduleBuilder cronSchedule = CronScheduleBuilder.cronSchedule(schedule);
                                JobDetail newJob = newJob(ScheduledJob.class)
                                        .withIdentity(uniqJobId, TRIGGER_GROUP)
                                        .usingJobData(dataMap)
                                        .build();
                                CronTrigger newTrigger = newTrigger()
                                        .withIdentity(uniqJobId, TRIGGER_GROUP)
                                        .startNow()
                                        .withSchedule(cronSchedule)
                                        .build();
                                logger.debug("Scheduling job " + uniqJobId);
                                scheduler.unscheduleJob(triggerKey);
                                scheduler.deleteJob(jobKey);
                                scheduler.scheduleJob(newJob, newTrigger);
                            } catch (Throwable th) {
                                logger.error("Error while rescheduling job " + uniqJobId, th);
                                th.printStackTrace();
                            }
                        }
                        if (scheduler.getTriggerState(triggerKey) == Trigger.TriggerState.PAUSED) {
                            logger.debug("Resuming job " + uniqJobId);
                            scheduler.resumeTrigger(triggerKey);
                        }
                    } else if ((JobStatus.PAUSED.name().equals(jobStatus) || JobStatus.PAUSED.name().equals(scheduleStatus)) && scheduler.getTriggerState(triggerKey) != Trigger.TriggerState.PAUSED) {
                        logger.debug("Pause trigger for job " + uniqJobId);
                        scheduler.pauseTrigger(triggerKey);
                    } else if (JobStatus.DISABLED.name().equals(jobStatus) || JobStatus.DISABLED.name().equals(scheduleStatus)) {
                        logger.debug("Disabled job " + uniqJobId);
                        scheduler.unscheduleJob(triggerKey);
                        scheduler.deleteJob(jobKey);
                    }
                } else {
                    /**
                     * There is not trigger scheduled for this job. If its job_status is ACTIVE, schedule it.
                     */
                    if (JobStatus.ACTIVE.name().equals(jobStatus) && JobStatus.ACTIVE.name().equals(scheduleStatus)) {
//                        ProcessStatusHandler.newJob(jobId, eventStep, dataType, jobParameters, allowConcurrent);
                        JobDataMap dataMap = new JobDataMap();
                        dataMap.put("datatype", dataType);
                        dataMap.put("eventstep", eventStep);
                        dataMap.put("allowconcurrent", allowConcurrent);
                        dataMap.put("jobid", jobId);
                        dataMap.put("triggerid", triggerId);
                        dataMap.putAll(jobParameters);
                        dataMap.put("lastupdated", lastUpdatedTimestamp.getTime());
                        CronScheduleBuilder cronSchedule = CronScheduleBuilder.cronSchedule(schedule);
                        JobDetail newJob = newJob(ScheduledJob.class)
                                .withIdentity(uniqJobId, TRIGGER_GROUP)
                                .usingJobData(dataMap)
                                .build();
                        CronTrigger newTrigger = newTrigger()
                                .withIdentity(uniqJobId, TRIGGER_GROUP)
                                .startNow()
                                .withSchedule(cronSchedule)
                                .build();
                        logger.debug("Scheduling job " + uniqJobId);
                        scheduler.scheduleJob(newJob, newTrigger);

                    }
                }
            }
            for (TriggerKey triggerKey : triggerKeys) {
                scheduler.unscheduleJob(triggerKey);
                scheduler.deleteJob(new JobKey(triggerKey.getName(), TRIGGER_GROUP));
                logger.debug("Deleting job " + triggerKey.getName());
            }
        } catch (SQLException e) {
            logger.error("Unable to get schedule triggers!", e);
        } catch (SchedulerException e) {
            logger.debug("Unable to schedule job!", e);
        }
    }

    /**
     * Get all Quartz triggers.
     *
     * @param scheduler
     * @return
     * @throws SchedulerException
     */
    private Set<TriggerKey> getAllTriggers(Scheduler scheduler) throws SchedulerException {
        Set<TriggerKey> triggerKeys = new HashSet<>();
        for (TriggerKey triggerKey : scheduler.getTriggerKeys(groupEquals(TRIGGER_GROUP))) {
            triggerKeys.add(triggerKey);
        }
        return triggerKeys;
    }

    /**
     * The actual running job. The job only creates a new job using <code>ProcessStatusHandler</code>
     * which in practice inserts a new row in <code>process_status</code> table with the NEW status and
     * the data provided with this job.
     */
    @DisallowConcurrentExecution
    public static class ScheduledJob implements Job {

        @Override
        public void execute(JobExecutionContext context) throws JobExecutionException {
            if (!paused) {
                String dataType = (String) context.getJobDetail().getJobDataMap().get("datatype");
                String jobId = (String) context.getJobDetail().getJobDataMap().get("jobid");
                String eventStep = (String) context.getJobDetail().getJobDataMap().get("eventstep");
                Boolean allowConcurrent = (Boolean) context.getJobDetail().getJobDataMap().get("allowconcurrent");
                JobDataMap dataMap = context.getJobDetail().getJobDataMap();


                ProcessTraceHandle processTraceHandle = ProcessStatusHandler.newJob(
                        jobId,
                        eventStep,
                        dataType,
                        dataMap,
                        allowConcurrent
                );
            }
        }
    }
}
