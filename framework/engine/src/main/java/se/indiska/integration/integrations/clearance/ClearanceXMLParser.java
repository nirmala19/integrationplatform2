package se.indiska.integration.integrations.clearance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by heintz on 21/02/17.
 */
public class ClearanceXMLParser extends DefaultHandler {
    private static final Logger logger = LoggerFactory.getLogger(ClearanceXMLParser.class);
    private static Map<String, Column> tagColumnNameMapping = new HashMap<>();
    private String currentTag = null;
    private String currentValue = null;

    static {
        tagColumnNameMapping.put("location", new Column("store", Column.Datatype.Numeric));
        tagColumnNameMapping.put("loc_type", new Column("store_type", Column.Datatype.String));
        tagColumnNameMapping.put("clearance_id", new Column("clearance_id", Column.Datatype.String));
        tagColumnNameMapping.put("item", new Column("item", Column.Datatype.String));
        tagColumnNameMapping.put("ribdate:year", new Column("year", Column.Datatype.Numeric));
        tagColumnNameMapping.put("ribdate:month", new Column("month", Column.Datatype.Numeric));
        tagColumnNameMapping.put("ribdate:day", new Column("day", Column.Datatype.Numeric));
        tagColumnNameMapping.put("ribdate:hour", new Column("hour", Column.Datatype.Numeric));
        tagColumnNameMapping.put("ribdate:minute", new Column("minute", Column.Datatype.Numeric));
        tagColumnNameMapping.put("ribdate:second", new Column("second", Column.Datatype.Numeric));
        tagColumnNameMapping.put("selling_unit_retail", new Column("selling_unit_retail", Column.Datatype.Numeric));
        tagColumnNameMapping.put("selling_uom", new Column("selling_uom", Column.Datatype.String));
        tagColumnNameMapping.put("selling_currency", new Column("selling_currency", Column.Datatype.String));
        tagColumnNameMapping.put("reset_clearance_id", new Column("reset_clearance_id", Column.Datatype.Numeric));
    }

    public static void main(String[] argv) throws Exception {
//        File file1 = new File("src/test/resources/oracle/clearance/promo_1.2.xml");
        File file1 = new File("/Users/heintz/Projects/Indiska/ICC/testdata/promos/complex/RIB.multiple_AND_or_OR.xml");
        logger.debug("File " + file1.getAbsolutePath() + " exists: " + file1.exists());
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file1);


        NodeList messageDataElements = document.getElementsByTagName("messageData");
        if (messageDataElements.getLength() == 1) {
            Element messageDataElement = (Element) messageDataElements.item(0);
            ByteArrayInputStream bais = new ByteArrayInputStream(messageDataElement.getTextContent().getBytes());
            Document payload = builder.parse(bais);
            prettyPrintXml(payload);

//            SAXParserFactory parserFactor = SAXParserFactory.newInstance();
//            SAXParser parser = parserFactor.newSAXParser();
//            ClearanceXMLParser handler = new ClearanceXMLParser();
//            parser.parse(bais, handler);

        } else {
            logger.error("Not only one messageData elements!");
        }

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentTag = qName;
        currentValue = null;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        currentValue = null;
        currentTag = null;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        currentValue = new String(ch, start, length);
        Column column = tagColumnNameMapping.get(currentTag);
        if (column != null) {
            String fnutt = "'";
            if (column.getDatatype() == Column.Datatype.Numeric) {
                fnutt = "";
            }
            logger.debug(column.getName() + ":\t" + fnutt + currentValue + fnutt);
        }
    }

    private static void prettyPrintXml(Node node) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(node);
        transformer.transform(source, result);
        String xmlString = result.getWriter().toString();
        System.out.println(xmlString);
    }

    private static class Column {
        public enum Datatype {String, Numeric}

        private String name;
        private Datatype datatype;

        public Column(String name, Datatype datatype) {
            this.name = name;
            this.datatype = datatype;
        }

        public String getName() {
            return name;
        }

        public Datatype getDatatype() {
            return datatype;
        }
    }
}
