package se.indiska.integration.services;

import org.glassfish.jersey.servlet.ServletContainer;
import org.glassfish.jersey.servlet.WebConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by heintz on 27/03/17.
 */
@WebServlet(initParams = {
        @WebInitParam(name = "jersey.config.server.provider.packages", value = "se.indiska.integration.services")
}, loadOnStartup = 1, urlPatterns = "/rest/*")
public class RestContainer extends ServletContainer {
    private static final Logger logger = LoggerFactory.getLogger(RestContainer.class);

    @Override
    protected void init(WebConfig webConfig) throws ServletException {
        super.init(webConfig);
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            super.service(request, response);
        } catch (Exception e) {
            logger.error("Unable to perform request for " + request.getRequestURL().toString(), e);
        }
    }
}