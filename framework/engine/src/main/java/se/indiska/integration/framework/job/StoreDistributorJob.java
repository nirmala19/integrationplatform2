package se.indiska.integration.framework.job;

import org.quartz.DisallowConcurrentExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.common.Wrapper;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.ExecutorsFactory;
import se.indiska.integration.framework.connection.ConnectionFailureException;
import se.indiska.integration.framework.connection.RemoteConnector;
import se.indiska.integration.framework.connection.RemoteConnectors;
import se.indiska.integration.framework.job.store.StoreServer;
import se.indiska.integration.framework.trace.DataTrace;
import se.indiska.integration.framework.trace.ProcessTrace;
import se.indiska.integration.framework.util.Alerter;
import se.indiska.integration.framework.util.InboundFileUtilities;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by heintz on 2017-04-28.
 */
@DisallowConcurrentExecution
@IdentifiableIndioJob(identifier = "STORE_FILE_DISTRIBUTOR", eventStep = "OUTBOUND_TRANSFER", description = "Distributes messages to stores (multiple targets)")
public class StoreDistributorJob implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(StoreDistributorJob.class);

    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        String sql = "SELECT *\n" +
                "FROM outbound_store_distribution osd,\n" +
                "  outbound_file_staging ofs\n" +
                "WHERE\n" +
                "  osd.outbound_message_id = ofs.id\n" +
                "  AND osd.store_id = ?\n" +
                "  AND osd.delivered = FALSE";

        String getStoreConnectionInfo = "SELECT DISTINCT\n" +
                "  s.id  store_id,\n" +
                "  s.country_code,\n" +
                "  s.timezone,\n" +
                "  s.target_connection_details connection_detail_id_id,\n" +
                "  ss.id server_id,\n" +
                "  ss.address,\n" +
                "  ss.username,\n" +
                "  ss.credentials,\n" +
                "  scd.protocol,\n" +
                "  scd.path\n" +
                "FROM store s\n" +
                "  JOIN store_connection_details scd\n" +
                "    ON s.target_connection_details = scd.id\n" +
                "  JOIN store_server ss\n" +
                "    ON scd.server = ss.id\n" +
                "WHERE s.active = TRUE";

        List<Future> storeDistributionFutures = new ArrayList<>();
        Map<Long, RemoteConnector> connectorMap = new HashMap<>();
        Wrapper<Integer> filesDelivered = new Wrapper<>(0);

        Map<StoreServer, Collection<StoreConnectionInfo>> connectionInfoMap = new HashMap<>();
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(getStoreConnectionInfo);
            ResultSet rset = ps.executeQuery();

            while (rset.next()) {
                StoreServer storeServer = new StoreServer(rset);
                Collection<StoreConnectionInfo> connectionInfos = connectionInfoMap.get(storeServer);
                if (connectionInfos == null) {
                    connectionInfos = new ArrayList<>();
                    connectionInfoMap.put(storeServer, connectionInfos);
                }
                connectionInfos.add(new StoreConnectionInfo(rset));
            }
            rset.close();
            ps.close();
        }
        for (StoreServer storeServer : connectionInfoMap.keySet()) {
            RemoteConnector connector = RemoteConnectors.connector(storeServer.getProtocol(), storeServer.getAddress(), storeServer.getUsername(), storeServer.getPassword(), null);
            for (StoreConnectionInfo connectionInfo : connectionInfoMap.get(storeServer)) {
                storeDistributionFutures.add(ExecutorsFactory.getExecutorService().submit(new Runnable() {
                    @Override
                    public void run() {
                        try (Connection connection = DatasourceProvider.getConnection()) {
                            PreparedStatement ps = connection.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
                            ps.setLong(1, connectionInfo.getStoreId());
                            ResultSet rset = ps.executeQuery();
                            while (rset.next()) {
                                Long storeId = rset.getLong("store_id");
                                String messageId = rset.getString("message_id");
                                Long outboundId = rset.getLong("outbound_message_id");
                                String filename = rset.getString("filename");
                                if (connector != null && connectionInfo != null) {
                                    try {
//                                        logger.debug("Putting file " + filename);
                                        String fileContent = null;
                                        try {
                                            fileContent = InboundFileUtilities.getFileContent(rset.getString("file_content"));
                                        } catch (IOException e) {
                                            logger.error("Unable to get file content for " + rset.getString("file_content"), e);
                                        }

                                        connector.putFile(connectionInfo.getPath(), filename, fileContent);
                                        traceHandle.getProcessTracer().trace(
                                                new DataTrace(messageId,
                                                        traceHandle.getId(),
                                                        "outbound_file_staging",
                                                        outboundId,
                                                        storeServer.getProtocol() + "://" + storeServer.getAddress() + "/" + connectionInfo.getPath() + "/" + filename,
                                                        null, ProcessStatus.SUCCESS));
                                        rset.updateBoolean("delivered", true);
                                        rset.updateTimestamp("delivered_time", new Timestamp(System.currentTimeMillis()));
                                        rset.updateRow();
                                        filesDelivered.set(filesDelivered.get() + 1);
                                        Alerter.clearConnectionError(storeServer.getProtocol(), storeServer.getAddress(), Direction.OUTBOUND, connection);
                                    } catch (ConnectionFailureException e) {
                                        Alerter.alertConnectionError(storeServer.getProtocol(), storeServer.getAddress(), Direction.OUTBOUND, e.getMessage());
                                        // End processing now since it's unlikely that the store will get back online before the next iteration in the loop
                                        // and this job is executed on a frequent basis.
                                        return;
                                    }
                                } else {
                                    logger.error("There are no active store connection details for store id " + storeId);
                                }
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }));
            }
        }

        for (Future future : storeDistributionFutures) {
            try {
                future.get();
            } catch (InterruptedException e) {
                logger.warn("Interrupted while waiting for store distributor. Doing nothing.");
            } catch (ExecutionException e) {
                String error = "Error while waiting for store distributor to complete!";
                logger.error(error, e);
                traceHandle.getProcessTracer().trace(new ProcessTrace(traceHandle.getId(), ProcessStatus.ERROR, error + ": " + e.getMessage(), ProcessError.UNDEFINED_ERROR));
            }
        }
        callback.jobComplete(filesDelivered.get() > 0 ? new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.SUCCESS) : new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.NOOP));
    }

}
