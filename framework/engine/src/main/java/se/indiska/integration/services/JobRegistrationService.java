package se.indiska.integration.services;

import com.google.common.cache.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * JobRegistrationService is used by the integrations to register themselves in the <code>engine</code>.
 * <p>
 * This class requires some refactoring to break apart registration/deregistration (the latter not implemented)
 * from the management of the callbacks.
 * <p>
 * Created by heintz on 2017-06-28.
 */
@RestController
@RequestMapping("/registry")
public class JobRegistrationService {
    private static final Logger logger = LoggerFactory.getLogger(JobRegistrationService.class);
    @Autowired
    private Environment environment;

    private static Cache<Long, JobCompleteCallback> jobCompleteCallbackBuffer =
            CacheBuilder.newBuilder()
                    .maximumSize(100000)
                    .expireAfterWrite(5, TimeUnit.MINUTES)
                    .removalListener(new RemovalListener<Long, JobCompleteCallback>() {
                        @Override
                        public void onRemoval(RemovalNotification<Long, JobCompleteCallback> removalNotification) {
                            if (removalNotification.getCause() == RemovalCause.EXPIRED) {
                                try {
                                    if (removalNotification.getValue().runningProcessIds().contains(removalNotification.getKey())) {
                                        logger.debug("Process " + removalNotification.getKey() + " timed out but is still running, putting back into buffer");
                                        jobCompleteCallbackBuffer.put(removalNotification.getKey(), removalNotification.getValue());
                                    } else {
                                        logger.debug("Process " + removalNotification.getKey() + " timed out and is not running on remote job service! Setting status to TIMEOUT");
                                        ProcessExecutionResult result = new ProcessExecutionResult(removalNotification.getKey(), ProcessStatus.TIMEOUT);
                                        removalNotification.getValue().jobComplete(result);
                                    }
                                } catch (IOException e) {
                                    ProcessExecutionResult result = new ProcessExecutionResult(removalNotification.getKey(), ProcessStatus.MISSING_JOB);
                                    removalNotification.getValue().jobComplete(result);
                                }
                            }
                        }
                    })
                    .build();

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity register(@RequestBody JobRegistration registration) {
        JobRegistrationResult result = null;
        try {
            String localAddress = InetAddress.getLocalHost().getCanonicalHostName() + ":" + environment.getProperty("local.server.port");
            String jobCompleteCallbackAddress = "http://" + localAddress + "/registry/jobcomplete";

            JobExecutionProxy proxy = EventTriggerManager.getJobExecutionProxyMap().get(registration.getIdentifier());
            if (proxy != null && proxy.getUrl().equals(registration.getAddress())) {
                return ResponseEntity.ok(new JobRegistrationResult(JobRegistrationResult.Status.OK, jobCompleteCallbackAddress));
            }


            addJobToDatabase(registration);


            logger.info("Successfully registered " + registration.getIdentifier() + " (" + registration.getAddress() + ")");
            JobExecutionProxy jobExecutionProxy = new JobExecutionProxy(registration.getIdentifier(), registration.getAddress(), new JobExecutionProxy.JobCompleteCallbackRetriever() {
                @Override
                public JobCompleteCallback getCallback(Long processId) {
                    return jobCompleteCallbackBuffer.getIfPresent(processId);
                }

                @Override
                public void addCallback(Long processId, JobCompleteCallback callback) {
                    jobCompleteCallbackBuffer.put(processId, callback);
                }

                @Override
                public void removeCallback(Long processId) {
                    jobCompleteCallbackBuffer.invalidate(processId);
                }
            });

            EventTriggerManager.registerJobProxy(jobExecutionProxy);
            try (Connection conn = DatasourceProvider.getConnection()) {
                String sql = "UPDATE process_status SET status=? WHERE status IN (?, ?, ?) AND job_id=?";
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.setString(1, "NEW");
                ps.setString(2, ProcessStatus.MISSING_JOB.name());
                ps.setString(3, ProcessStatus.INITIATED.name());
                ps.setString(4, ProcessStatus.RUNNING.name());
                ps.setString(5, registration.getIdentifier());
                ps.executeUpdate();
                ps.close();
            } catch (SQLException e) {
                logger.error("Unable to reset previous MISSING_JOB process runs.", e);
            }
            result = new JobRegistrationResult(JobRegistrationResult.Status.OK, jobCompleteCallbackAddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (RegistrationFailureException e) {
            logger.error("Unable to register job due to registration problems!", e);
        }


        return ResponseEntity.ok(result);
    }

    private void addJobToDatabase(JobRegistration jobRegistration) throws RegistrationFailureException {
        if (jobRegistration.getIdentifier() == null) {
            return;
        }
        String sql = "INSERT INTO job (id, class, event_type, description) VALUES (?,?,?,?) " +
                "ON CONFLICT (id) DO UPDATE SET description=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, jobRegistration.getIdentifier());
            ps.setString(2, "Distributed execution");
            ps.setString(3, jobRegistration.getEventStep());
            ps.setString(4, jobRegistration.getDescription());
            ps.setString(5, jobRegistration.getDescription());
            ps.executeUpdate();
            ps.close();
            if (jobRegistration.getJobType() == IdentifiableIndioJob.JobType.SCHEDULED) {
                sql = "INSERT INTO job_schedule_triggers (job_id, event_step, data_type, schedule, job_status, last_updated) VALUES (?,?,?,?,?, now()) " +
                        "ON CONFLICT DO NOTHING ";
                ps = connection.prepareStatement(sql);
                ps.setString(1, jobRegistration.getIdentifier());
                ps.setString(2, jobRegistration.getEventStep());
                ps.setString(3, jobRegistration.getDataType());
                ps.setString(4, jobRegistration.getSchedule());
                ps.setString(5, "ACTIVE");
                ps.executeUpdate();
                ps.close();
            } else if (jobRegistration.getJobType() == IdentifiableIndioJob.JobType.EVENT_BASED) {
                sql = "INSERT INTO job_event_triggers (job_id, event_step, data_type, job_status, last_updated) " +
                        "VALUES (?,?,?,?,?) ON CONFLICT  DO NOTHING ";
                ps = connection.prepareStatement(sql);
                ps.setString(1, jobRegistration.getIdentifier());
                ps.setString(2, jobRegistration.getTriggerEventStep());
                ps.setString(3, jobRegistration.getTriggerDataType());
                ps.setString(4, "ACTIVE");
                ps.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
                ps.executeUpdate();
                ps.close();
            }
        } catch (SQLException e) {
            throw new RegistrationFailureException("Unable to register job " + jobRegistration.getIdentifier(), e);
        }
    }


    @RequestMapping(value = "/registeredJobs", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getRegisteredJobs() {
        Map<String, JobExecutionProxy> jobExecutionProxyMap = EventTriggerManager.getJobExecutionProxyMap();
        Map<String, Map> result = new HashMap<>();

        for (JobExecutionProxy jobExecutionProxy : jobExecutionProxyMap.values()) {
            Map<String, Object> item = new HashMap<>();
            item.put("url", jobExecutionProxy.getUrl());
//            try {
//                item.put("runningProcessIds", jobExecutionProxy.getRunningProcessIds());
//            } catch (IOException e) {
//                logger.warn("Unable to get running process ID's from " + jobExecutionProxy.identifier());
//            }
            result.put(jobExecutionProxy.identifier(), item);
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getRegistrations() {
        return ResponseEntity.ok("{'status':'ok'}");
    }

    @RequestMapping(value = "/registration/{jobId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getRegistrations(@PathVariable("jobId") String jobId) {

        return ResponseEntity.ok("{'status':'ok'}");
    }


    @RequestMapping(value = "/jobcomplete", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity completeCallback(@RequestBody ProcessExecutionResult processExecutionResult) {
        JobCompleteCallback callback = jobCompleteCallbackBuffer.getIfPresent(processExecutionResult.getProcessId());
        if (callback != null) {
            callback.jobComplete(processExecutionResult);
        }
        jobCompleteCallbackBuffer.invalidate(processExecutionResult.getProcessId());
        return ResponseEntity.ok().build();
    }

}
