package se.indiska.integration.framework.entity;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "distributionType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = NormalOutboundDistributor.class, name = "OUTBOUND_DISTRIBUTE"),
        @JsonSubTypes.Type(value = StoreOutboundDistributor.class, name = "STORE_DISTRIBUTE")
})
public abstract class MessageDistributor {
    private static final Logger logger = LoggerFactory.getLogger(MessageDistributor.class);
    private Long id = null;
    private String dataType = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

//    public abstract String getDistributionType();
}
