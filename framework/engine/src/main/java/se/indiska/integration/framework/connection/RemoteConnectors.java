package se.indiska.integration.framework.connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Utility class to get a RemoteConnector by it's properties (protocol).
 * <p>
 * Created by heintz on 04/04/17.
 */
public class RemoteConnectors {
    private static final Logger logger = LoggerFactory.getLogger(RemoteConnectors.class);

    public static RemoteConnector connector(String protocol, String hostname, String username, String credentials, Map protocolConfig) {
        if (protocol.equalsIgnoreCase("ftp")) {
            return new FTPConnector(hostname, username, credentials, protocolConfig);
        } else if (protocol.equalsIgnoreCase("file")) {
            return new FileConnector(hostname, username, credentials, protocolConfig);
        } else if (protocol.equalsIgnoreCase("smb")) {
            return new CIFSConnector(hostname, username, credentials, protocolConfig);
        } else if (protocol.equalsIgnoreCase("sftp")) {
            return new SFTPConnector(hostname, username, credentials, protocolConfig);
        } else if(protocol.equalsIgnoreCase("azure")) {
            return new AzureConnector(hostname, username, credentials, protocolConfig);
        }
        return null;
    }
}
