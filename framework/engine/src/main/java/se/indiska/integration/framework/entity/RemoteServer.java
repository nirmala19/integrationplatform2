package se.indiska.integration.framework.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POJO which contains the information for a remote_server.
 * Created by heintz on 2017-04-18.
 */
public class RemoteServer {
    private static final Logger logger = LoggerFactory.getLogger(RemoteServer.class);
    private Long id = null;
    private String name = null;
    private String username = null;
    private String password = null;
    private String address = null;
    private String protocol;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getProtocol() {
        return protocol;
    }
}
