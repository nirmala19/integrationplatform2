package se.indiska.integration.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.Direction;
import se.indiska.integration.framework.util.InboundFileUtilities;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Date;

@RestController
@RequestMapping("/messages")
public class MessageService {
    private static final Logger logger = LoggerFactory.getLogger(MessageService.class);

    @RequestMapping(value = "/query", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity queryFiles(@RequestBody MessageQueryRequest request) {
        if (request.getDirection() == Direction.OUTBOUND) {
            return queryOutboundFiles(request);
        }
        String sql = "SELECT ifs.id, ifs.event_time, ifs.file_type, ifs.source_directory, ifs.archive_directory, ifs.filename, ifs.file_content, ifs.message_id, mm.key, mm.value\n" +
                "FROM\n" +
                "  inbound_file_staging ifs\n" +
                "  LEFT OUTER JOIN message_metadata mm\n" +
                "    ON ifs.message_id = mm.message_id\n" +
                "       AND (mm.key = 'remote_server' OR mm.key = 'source_store_id')\n" +
                "WHERE\n";

        String whereStatement = "";
        if (request.getDataType() != null) {
            whereStatement += "  ifs.file_type = ?\n AND ";
        }
        if (request.getStore() != null) {
            whereStatement += " mm.key='source_store_id' AND mm.value=?\n AND ";
        }
        if (request.getRemoteServer() != null) {
            whereStatement += " mm.key='remote_server' AND mm.value=? AND ";
        }
        if (request.getFilename() != null) {
            whereStatement += " ifs.filename like ? AND ";
        }
        whereStatement += "  ifs.event_time BETWEEN ? AND ?\n";

        sql += whereStatement;
        boolean freetext = false;
        if (request.getFreetext() != null && request.getFreetext().length() > 0) {
            sql += " and ifs.file_content like ?";
            freetext = true;
        }
        sql += "ORDER BY ifs.event_time DESC\n" +
                "LIMIT ?";
        if (request.getPage() != null) {
            sql += " OFFSET ?";
        }

        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            int i = 1;
            if (request.getDataType() != null) {
                ps.setString(i++, request.getDataType());
            }
            if (request.getStore() != null) {
                ps.setString(i++, request.getStore());
            }
            if (request.getRemoteServer() != null) {
                ps.setString(i++, request.getRemoteServer());
            }
            if (request.getFilename() != null) {
                ps.setString(i++, "%" + request.getFilename() + "%");
            }
            Timestamp from = request.getFromDate() != null ? new java.sql.Timestamp(request.getFromDate().getTime()) : new java.sql.Timestamp(0l);
            Timestamp to = request.getToDate() != null ? new java.sql.Timestamp(request.getToDate().getTime()) : new java.sql.Timestamp(System.currentTimeMillis() + 1000000);
            ps.setTimestamp(i++, from);
            ps.setTimestamp(i++, to);
            if (freetext) {
                ps.setString(i++, "%" + request.getFreetext() + "%");
            }
            Integer pageSize = request.getPage() != null ? request.getPageSize() : 25;
            ps.setInt(i++, pageSize);
            if (request.getPage() != null) {
                ps.setInt(i++, request.getPage() * pageSize);
            }
            long start = System.nanoTime();
            ResultSet rset = ps.executeQuery();
            List result = new ArrayList<>();
            while (rset.next()) {
                Map row = new HashMap();
                row.put("id", rset.getLong("id"));
                row.put("time", rset.getTimestamp("event_time"));
                row.put("dataType", rset.getString("file_type"));
                row.put("sourceDirectory", rset.getString("source_directory"));
                row.put("archiveDirectory", rset.getString("archive_directory"));
                row.put("filename", rset.getString("filename"));
                try {
                    row.put("content", InboundFileUtilities.getFileContent(rset.getString("file_content")));
                } catch (IOException e) {
                    logger.error("Unable to get file content for " + rset.getString("file_content"), e);
                }
                row.put("messageId", rset.getString("message_id"));
                String sourceServerType = rset.getString("key");
                row.put("sourceServerType", sourceServerType != null && sourceServerType.equals("source_store_id") ? "store" : "regular");
                String sourceServerId = rset.getString("value");
                row.put("sourceServerId", sourceServerId != null ? Integer.parseInt(sourceServerId) : null);
                result.add(row);
            }
            rset.close();
            ps.close();
            return ResponseEntity.ok(result);
        } catch (SQLException e) {
            logger.error("Unable to query inbound_file_staging!", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    private ResponseEntity queryOutboundFiles(MessageQueryRequest request) {
        StoreService storeService = new StoreService();
        if (request.getStore() != null) {
            return storeService.getOutboundMessages(
                    Long.parseLong(request.getStore()),
                    request.getPage().longValue(),
                    request.getPage() * request.getPageSize().longValue(),
                    request.getDataType(),
                    request.getFromDate() != null ? request.getFromDate().getTime() : null,
                    request.getToDate() != null ? request.getToDate().getTime() : null);
        }
        return null;
    }

    @RequestMapping(value = "/datatypes", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getDataTypes() {
        String sql = "SELECT id FROM data_type ORDER BY id";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            List<String> result = new ArrayList<>();
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {

                result.add(rset.getString(1));
            }
            return ResponseEntity.ok(result);
        } catch (SQLException e) {
            logger.error("Unable to get all datatypes", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }


    @RequestMapping(value = "/messagepath/{messageId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity messagePath(@PathVariable String messageId) {
        String sql = "SELECT *\n" +
                "FROM process_data_trace\n" +
                "WHERE message_id = ?\n" +
                "ORDER BY event_time, id";
        try (Connection connection = DatasourceProvider.getConnection()) {
            List result = new ArrayList();
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, messageId);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                Map row = new HashMap();
                row.put("processId", rset.getLong("process_id"));
                row.put("source", rset.getString("source"));
                row.put("sourceId", rset.getString("source_id"));
                row.put("target", rset.getString("target"));
                row.put("targetId", rset.getString("target_id"));
                row.put("status", rset.getString("status"));
                row.put("time", rset.getTimestamp("event_time"));
                result.add(row);
            }
            rset.close();
            ps.close();
            return ResponseEntity.ok(result);
        } catch (SQLException e) {
            logger.error("Unable to query inbound_file_staging!", e);

        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @RequestMapping(value = "/targets/{messageId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity messageTargets(@PathVariable String messageId) {
        String normalSql = "SELECT *\n" +
                "FROM outbound_file_staging ofs\n" +
                "  JOIN outbound_message_distributor omd\n" +
                "    ON ofs.file_type = omd.data_type\n" +
                "       AND ofs.message_id = ?\n" +
                "  JOIN remote_location rl\n" +
                "    ON rl.id = omd.remote_location\n" +
                "  JOIN remote_server rs\n" +
                "    ON rs.id = omd.remote_server\n" +
                "  JOIN outbound_distribution od\n" +
                "    ON od.outbound_message_id = ofs.id";

        String storeSql = "SELECT *\n" +
                "FROM outbound_file_staging ofs\n" +
                "  JOIN store_outbound_message_distributor omd\n" +
                "    ON ofs.file_type = omd.data_type\n" +
                "       AND ofs.message_id = ?\n" +
                "  JOIN outbound_store_distribution osd\n" +
                "    ON osd.outbound_message_id = ofs.id";

        try (Connection connection = DatasourceProvider.getConnection()) {
            List result = new ArrayList();
            PreparedStatement ps = connection.prepareStatement(normalSql);
            ps.setString(1, messageId);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                Map row = new HashMap();
                row.put("processId", rset.getLong("process_id"));
                row.put("source", rset.getString("source"));
                row.put("sourceId", rset.getString("source_id"));
                row.put("target", rset.getString("target"));
                row.put("targetId", rset.getString("target_id"));
                row.put("status", rset.getString("status"));
                row.put("time", rset.getTimestamp("event_time"));
                result.add(row);
            }
            rset.close();
            ps.close();
            return ResponseEntity.ok(result);
        } catch (SQLException e) {
            logger.error("Unable to query inbound_file_staging!", e);

        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    static class MessageQueryRequest {
        private Date fromDate = null;
        private Date toDate = null;
        private String dataType = null;
        private String freetext = null;
        private Integer page = null;
        private String store = null;
        private String remoteServer = null;
        private Direction direction = null;
        private Integer pageSize = null;
        private String filename = null;


        public Date getFromDate() {
            return fromDate;
        }

        public void setFromDate(Date fromDate) {
            this.fromDate = fromDate;
        }

        public Date getToDate() {
            return toDate;
        }

        public void setToDate(Date toDate) {
            this.toDate = toDate;
        }

        public String getDataType() {
            return dataType;
        }

        public void setDataType(String dataType) {
            this.dataType = dataType;
        }

        public String getFreetext() {
            return freetext;
        }

        public void setFreetext(String freetext) {
            this.freetext = freetext;
        }

        public Integer getPage() {
            return page;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public String getStore() {
            return store;
        }

        public void setStore(String store) {
            this.store = store;
        }

        public String getRemoteServer() {
            return remoteServer;
        }

        public void setRemoteServer(String remoteServer) {
            this.remoteServer = remoteServer;
        }

        public Direction getDirection() {
            return direction;
        }

        public void setDirection(Direction direction) {
            this.direction = direction;
        }

        public Integer getPageSize() {
            return pageSize;
        }

        public void setPageSize(Integer pageSize) {
            this.pageSize = pageSize;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }
    }
}
