package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Helper class to handle Annotations. Annotations are {}-delimited code words in strings which may be used
 * to replace with one or multiple values. For example the {lookup:XYZ} will query table message_annotation_lookup
 * for an SQL expression with id='ZYX'. That SQL expression will be executed and a list of <code>Annotation</code>
 * objects will be returned, one object for each row in the result of the sql expression.
 * <p>
 * An annotation can have two values, <code>k</code> and <code>v</code> (key and value) and the SQL Expression
 * provided in message_annotation_lookup must provide a value for both (might be null though).
 * <p>
 * If the prefix is <code>metadata</code> instead of <code>lookup</code>, the table message_metadata is queried
 * and annotations for key/value of message_metadata table is returned where key matches XYZ and message_id matches
 * messageId.
 * <p>
 * Created by heintz on 2017-04-25.
 */
public class MessageAnnotationHelper {
    private static final Logger logger = LoggerFactory.getLogger(MessageAnnotationHelper.class);

    public static List<Annotation> substitutions(String annotation, String messageId, Connection conn) throws SQLException {
        List<Annotation> result = new ArrayList<>();
        if (annotation.startsWith("lookup:")) {
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM message_annotation_lookup WHERE id=?");
            ps.setString(1, annotation.substring(annotation.indexOf(":") + 1));
            String sql = null;
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                sql = resultSet.getString("sql_expression");
            }
            resultSet.close();
            ps.close();
            if (sql != null) {
                ps = conn.prepareStatement(sql);
                resultSet = ps.executeQuery();
                while (resultSet.next()) {
                    result.add(new Annotation(resultSet.getString("k"), resultSet.getString("v")));
                }
                resultSet.close();
                ps.close();
            }
        } else if (annotation.startsWith("metadata:")) {
            String sql = "SELECT * FROM message_metadata WHERE message_id=? AND key=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, messageId);
            ps.setString(2, annotation.substring(annotation.indexOf(":") + 1));
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                result.add(new Annotation(rset.getString("key"), rset.getString("value")));
            }
        }
        return result;
    }

    public static String substituteDirectory(String directory) {
        String result = directory;
        if (result.indexOf("{") >= 0 && result.indexOf("}") > 0) {
            String substition = result.substring(result.indexOf("{") + 1, result.indexOf("}"));
            if (substition.toLowerCase().startsWith("date:")) {
                String format = substition.substring("date:".length());
                SimpleDateFormat sdf = new SimpleDateFormat(format);
                String date = sdf.format(new Date());
                result = result.replaceAll("\\{" + substition + "\\}", date);
            }
        }
        return result;
    }

    static class Annotation {
        private String key;
        private String value;

        public Annotation(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }
}
