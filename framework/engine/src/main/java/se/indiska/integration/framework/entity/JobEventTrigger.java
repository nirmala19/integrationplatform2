package se.indiska.integration.framework.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class JobEventTrigger {
    private static final Logger logger = LoggerFactory.getLogger(JobEventTrigger.class);
    private Long id = null;
    private Job job = null;
    private String eventStep = null;
    private String dataType = null;
    private String jobStatus = null;
    private Date lastUpdated = null;

    public JobEventTrigger() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getEventStep() {
        return eventStep;
    }

    public void setEventStep(String eventStep) {
        this.eventStep = eventStep;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
