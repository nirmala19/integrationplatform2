package se.indiska.integration.integrations.poslog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heintz on 27/02/17.
 */
public class Transaction {
    private static final Logger logger = LoggerFactory.getLogger(Transaction.class);
    private String store = null;
    private String transactionId = null;
    private String workStationId = null;
    private String sequenceNumber = null;
    private String operatorId = null;
    private String receiptNumber = null;
    private String currencyCode = null;
    private String dateTime = null;
    private String customerId = null;
    private String messageId = null;
    private static boolean tableExists = false;

    private List<LineItem> lineItems = new ArrayList<>();

    public Transaction(String messageId) {
        this.messageId = messageId;
    }

    public void parse(Element element, File file) {
        NodeList children = element.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node n = children.item(i);
            if ("TransactionID".equals(n.getNodeName())) {
                transactionId = n.getFirstChild().getNodeValue();
            } else if ("WorkstationID".equals(n.getNodeName())) {
                workStationId = n.getFirstChild().getNodeValue();
            } else if ("RetailStoreID".equals(n.getNodeName())) {
                store = n.getFirstChild().getNodeValue();
            } else if ("SequenceNumber".equals(n.getNodeName())) {
                sequenceNumber = n.getFirstChild().getNodeValue();
            } else if ("OperatorID".equals(n.getNodeName())) {
                operatorId = n.getFirstChild().getNodeValue();
            } else if ("CurrencyCode".equals(n.getNodeName())) {
                currencyCode = n.getFirstChild().getNodeValue();
            } else if ("ReceiptNumber".equals(n.getNodeName())) {
                receiptNumber = n.getFirstChild().getNodeValue();
            } else if ("PosLogDateTime".equals(n.getNodeName())) {
                dateTime = n.getFirstChild().getNodeValue();
            } else if ("RetailTransaction".equals(n.getNodeName())) {
                NodeList lines = n.getChildNodes();
                for (int j = 0; j < lines.getLength(); j++) {
                    Node transationLine = lines.item(j);
                    if ("LineItem".equals(transationLine.getNodeName())) {
                        Element lineElement = (Element) lines.item(j);
                        LineItem lineItem = LineItem.lineItem(lineElement, file, this.messageId);
                        if (lineItem != null) {
                            lineItems.add(lineItem);
                        }
//                        else {
//                            logger.debug("Line item is null " + lineElement+" in file "+file.getAbsolutePath());
//                        }
                    } else if ("LoyaltyAccount".equals(transationLine.getNodeName())) {
                        NodeList loyaltyAccount = transationLine.getChildNodes();
                        for (int k = 0; k < loyaltyAccount.getLength(); k++) {
                            Node loyaltyAccountNode = loyaltyAccount.item(k);
                            if ("CustomerID".equals(loyaltyAccountNode.getNodeName())) {
                                customerId = loyaltyAccountNode.getFirstChild().getNodeValue();
                            }
                        }
                    }
                }
            }
        }

//        logger.debug("Line items for " + transactionId + ": " + lineItems.size());
    }

    public void store(DataSource dataSource) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            createTable(connection);
            String sql = "INSERT INTO pos_transaction (transaction_id, store, workstation_id, sequence_number, operator_id, currency_code, receipt_number, event_time, customer_id) " +
                    "VALUES(?,?,?,?,?,?,?,?,?) on conflict do nothing";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, transactionId);
            ps.setString(2, store);
            ps.setString(3, workStationId);
            ps.setString(4, sequenceNumber);
            ps.setString(5, operatorId);
            ps.setString(6, currencyCode);
            ps.setString(7, receiptNumber);
            if (dateTime.length() > 23) {
                ps.setDate(8, new java.sql.Date(ZonedDateTime.parse(dateTime).toInstant().toEpochMilli()));
            } else {
                ps.setDate(8, new java.sql.Date(LocalDateTime.parse(dateTime).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()));
            }
            ps.setString(9, customerId);
            ps.executeUpdate();
            ps.close();
            if (lineItems.size() > 0) {
                ps = connection.prepareStatement(lineItems.get(0).getInsertSql());
                for (LineItem lineItem : lineItems) {
                    lineItem.store(ps, transactionId);
                }
                ps.executeBatch();
                ps.close();
            }
        }
    }

    private void createTable(Connection connection) throws SQLException {
        if (!tableExists) {
            PreparedStatement ps = connection.prepareStatement(getCreateTableSql());
            ps.executeUpdate();
            ps.close();
            tableExists = true;
        }
    }

    public String getCreateTableSql() {
        return "create table if not exists pos_transaction (" +
                "transaction_id varchar(128) primary key," +
                "store varchar(16) not null," +
                "workstation_id varchar(8) not null," +
                "sequence_number varchar(8) not null," +
                "operator_id varchar(16) not null," +
                "currency_code varchar(8) not null," +
                "receipt_number varchar(16) not null," +
                "event_time datetime not null," +
                "customer_id varchar(32)," +
                "message_id varchar(64) not null);";
    }

    public String getTransactionId() {
        return transactionId;
    }
}
