package se.indiska.integration.framework;

import com.google.common.base.Predicate;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.util.SystemMonitor;

import javax.sql.DataSource;
import java.lang.annotation.Annotation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by heintz on 01/03/17.
 */
public class IntegrationCore {
    private static final Logger logger = LoggerFactory.getLogger(IntegrationCore.class);
    private static DataSource ds = null;
    private static Scheduler scheduler = null;
    private static EventTriggerManager eventTriggerEngine = null;

    private static ScheduledFuture eventTriggerFuture = null;
    private static Map<String, Class> deployedJobIds = new HashMap<>();
    private static Set<ShutdownListener> shutdownListeners = new HashSet<>();
    private static SystemMonitor systemMonitor = null;

    private void scheduleEventTriggers(Scheduler scheduler) {
//        try {
        eventTriggerEngine = new EventTriggerManager();
        eventTriggerFuture = ExecutorsFactory.getScheduledExecutorService().scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                try {
                    eventTriggerEngine.execute();
                } catch (JobExecutionException e) {
                    logger.error("Error while executing Event Trigger Engine!");
                }
            }
        }, 15000, 5000, TimeUnit.MILLISECONDS);
    }

    private void initializeSystemMonitor() {
        systemMonitor = new SystemMonitor();
    }

    private void scheduleJobScheduleUpdater(Scheduler scheduler) {
        JobDataMap dataMap = new JobDataMap();
        logger.debug("Scheduling job trigger manager");
        JobDetail job = newJob(ScheduledJobTriggerManager.class)
                .withIdentity("JOB_TRIGGER_MANAGER", "SYSTEM")
                .usingJobData(dataMap)
                .build();

        Trigger trigger = newTrigger()
                .withIdentity("JOB_TRIGGER_MANAGER", "SYSTEM")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInSeconds(3)
                        .repeatForever())
                .build();
        try {
            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            logger.error("Unable to start Job Trigger Manager!", e);
        }
    }

//    private void scheduleHouseKeeping(Scheduler scheduler) {
//        logger.debug("Scheduling housekeeping");
//        JobDetail job = newJob(ProcessStatusHousekeeping.class)
//                .withIdentity("PROCESS_HOUSEKEEPING", "SYSTEM")
//                .build();
//
//        Trigger trigger = newTrigger()
//                .withIdentity("PROCESS_HOUSEKEEPING", "SYSTEM")
//                .startNow()
//                .withSchedule(simpleSchedule()
//                        .withIntervalInHours(3)
//                        .repeatForever())
//                .build();
//        try {
//            scheduler.scheduleJob(job, trigger);
//        } catch (SchedulerException e) {
//            logger.error("Unable to start Job Trigger Manager!", e);
//        }
//    }


    public static EventTriggerManager getEventTriggerEngine() {
        return eventTriggerEngine;
    }

    public void initialize() throws SchedulerException {
        Reflections reflections = new Reflections("se.indiska");
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(IdentifiableIndioJob.class);
        for (Class cls : annotated) {
            if (IndioJob.class.isAssignableFrom(cls)) {
                Set<Annotation> annotations = ReflectionUtils.getAnnotations(cls, new Predicate<Annotation>() {
                    @Override
                    public boolean apply(Annotation annotation) {
                        logger.debug("Checking if " + annotation.getClass().getName() + " equals " + IdentifiableIndioJob.class.getName() + ": " + annotation.annotationType().equals(IdentifiableIndioJob.class));
                        return annotation.annotationType().equals(IdentifiableIndioJob.class);
                    }
                });
                if (annotations.isEmpty()) {
                    logger.error("Error in class lookup! Class " + cls.getName() + " should be annotated with " + IdentifiableIndioJob.class.getName() + "!");
                } else {
                    IdentifiableIndioJob annotation = (IdentifiableIndioJob) annotations.iterator().next();
                    logger.debug("Annotated identifier for " + cls.getName() + " is " + annotation.identifier());
                }
            } else {
                logger.error("Class " + cls.getName() + " must implement interface " + IndioJob.class.getName());
            }
        }
        scheduler = StdSchedulerFactory.getDefaultScheduler();

        restartPreviousJobs();

        // and start it off
        scheduler.start();
        scheduleJobScheduleUpdater(scheduler);
        scheduleEventTriggers(scheduler);
        initializeSystemMonitor();
//        scheduleHouseKeeping(scheduler);

    }

    private void restartPreviousJobs() {
        try (Connection conn = DatasourceProvider.getConnection()) {
            String sql = "UPDATE process_status SET status=? WHERE status=? OR status=?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "NEW");
            ps.setString(2, ProcessStatus.INITIATED.name());
            ps.setString(3, ProcessStatus.RUNNING.name());
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            logger.error("Unable to restart previously prematurely terminated processes");
        }
    }

    public void shutdown() {
        logger.info("Shutting down IntegrationCore");
        ExecutorsFactory.shutdown();
        eventTriggerFuture.cancel(true);
        try {
            logger.debug("  - Scheduler");
            scheduler.shutdown(true);
        } catch (SchedulerException e) {
            logger.error("Unable to shutdown scheduler!", e);
        }
        for (ShutdownListener shutdownListener : shutdownListeners) {
            shutdownListener.shutdown();
        }
    }

    public static void addShutdownListener(ShutdownListener listener) {
        shutdownListeners.add(listener);
    }
}
