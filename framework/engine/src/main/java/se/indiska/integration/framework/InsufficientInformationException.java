package se.indiska.integration.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by heintz on 28/02/17.
 */
public class InsufficientInformationException extends RuntimeException {
    private static final Logger logger = LoggerFactory.getLogger(InsufficientInformationException.class);

    public InsufficientInformationException() {
    }

    public InsufficientInformationException(String message) {
        super(message);
    }

    public InsufficientInformationException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsufficientInformationException(Throwable cause) {
        super(cause);
    }

    public InsufficientInformationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
