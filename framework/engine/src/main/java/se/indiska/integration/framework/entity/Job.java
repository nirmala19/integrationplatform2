package se.indiska.integration.framework.entity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * POJO which contains the information for a job.
 * Created by heintz on 2017-04-18.
 */
public class Job {
    private static final Logger logger = LoggerFactory.getLogger(Job.class);
    private String id = null;
    private String className = null;
    private String eventType = null;
    private String description = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
