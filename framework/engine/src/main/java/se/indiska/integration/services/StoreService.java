package se.indiska.integration.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.util.InboundFileUtilities;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Date;


/**
 * Created by heintz on 2017-04-27.
 */
@RestController
@RequestMapping("/stores")
public class StoreService {
    private static final Logger logger = LoggerFactory.getLogger(StoreService.class);

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getAllStores() {
        logger.debug("Get all stores...");


        String sql = "SELECT\n" +
                "  s.id,\n" +
                "  s.name,\n" +
                "  s.country_code,\n" +
                "  s.timezone,\n" +
                "  s.active,\n" +
                "  source_ss.address source_address,\n" +
                "  source_ss.username source_username,\n" +
                "  source_ss.credentials source_credentials,\n" +
                "  target_ss.address target_address,\n" +
                "  target_ss.username target_username,\n" +
                "  target_ss.credentials target_credentials,\n" +
                "  scd_source.protocol source_protocol,\n" +
                "  scd_source.path source_path,\n" +
                "  scd_target.protocol target_protocol,\n" +
                "  scd_target.path target_path\n" +
                "FROM\n" +
                "  store s\n" +
                "  LEFT OUTER JOIN store_connection_details scd_source\n" +
                "    ON s.source_connection_details = scd_source.id\n" +
                "  LEFT OUTER JOIN store_server source_ss\n" +
                "    ON scd_source.server = source_ss.id\n" +
                "  LEFT OUTER JOIN store_connection_details scd_target\n" +
                "    ON s.target_connection_details = scd_target.id\n" +
                "  LEFT OUTER JOIN store_server target_ss\n" +
                "    ON scd_target.server = target_ss.id\n" +
                "ORDER BY s.id";
        List<Map> result = new ArrayList<>();
        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                Map<String, Object> row = new LinkedHashMap<>();
                row.put("id", rset.getLong("id"));
                row.put("name", rset.getString("name"));
                row.put("country_code", rset.getString("country_code"));
                row.put("timezone", rset.getString("timezone"));
                row.put("active", rset.getBoolean("active"));
                row.put("sourceAddress", rset.getString("source_address"));
                row.put("sourceUsername", rset.getString("source_username"));
                row.put("sourceCredentials", rset.getString("source_credentials"));
                row.put("targetAddress", rset.getString("target_address"));
                row.put("targetUsername", rset.getString("target_username"));
                row.put("targetCredentials", rset.getString("target_credentials"));
                row.put("source_protocol", rset.getString("source_protocol"));
                row.put("source_path", rset.getString("source_path"));
                row.put("target_protocol", rset.getString("target_protocol"));
                row.put("target_path", rset.getString("target_path"));
                result.add(row);
            }
            rset.close();
            ps.close();
        } catch (SQLException e) {
            logger.error("Unable to query all stores");
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/{storeId}/getStore", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getStore(@PathVariable("storeId") String storeId) {


        logger.debug("In i getStore metod" + storeId);
        String sql = "SELECT\n" +
                "  s.id,\n" +
                "  s.name,\n" +
                "  s.country_code,\n" +
                "  s.timezone,\n" +
                "  s.active,\n" +
                "  ss.address,\n" +
                "  ss.username,\n" +
                "  ss.credentials,\n" +
                "  scd_source.protocol source_protocol,\n" +
                "  scd_source.path source_path,\n" +
                "  scd_target.protocol target_protocol,\n" +
                "  scd_target.path target_path\n" +
                "FROM\n" +
                "  store s\n" +
                "  LEFT OUTER JOIN store_connection_details scd_source\n" +
                "    ON s.source_connection_details = scd_source.id\n" +
                "  LEFT OUTER JOIN store_server ss\n" +
                "    ON scd.server = ss.id\n" +
                "  LEFT OUTER JOIN store_connection_details scd_target\n" +
                "    ON s.target_connection_details = scd_target.id\n" +
                "   WHERE s.id = ? ";

        List<Map> result = new ArrayList<>();
        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(storeId));
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                Map<String, Object> row = new LinkedHashMap<>();
                row.put("id", rset.getLong("id"));
                row.put("name", rset.getString("name"));
                row.put("country_code", rset.getString("country_code"));
                row.put("timezone", rset.getString("timezone"));
                row.put("active", rset.getBoolean("active"));
                row.put("address", rset.getString("address"));
                row.put("username", rset.getString("username"));
                row.put("credentials", rset.getString("credentials"));
                row.put("source_protocol", rset.getString("source_protocol"));
                row.put("source_path", rset.getString("source_path"));
                row.put("target_protocol", rset.getString("target_protocol"));
                row.put("target_path", rset.getString("target_path"));
                result.add(row);
                logger.debug("THIS IS OUR RESULT FROM GETSTORE: " + result.get(0).toString());
            }

            rset.close();
            ps.close();
        } catch (SQLException e) {
            logger.error("SQL EXCEPTION IS:   " + e.toString());
            logger.error("Unable to query A STORE");
        }
        logger.debug("Klar med getStore");
        return ResponseEntity.ok(result);
    }


    @RequestMapping(value = "/{storeId}/inboundmessages/{start}/{offset}/{filetype}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getInboundMessages(@PathVariable("storeId") String storeId, @PathVariable("start") Long start, @PathVariable("offset") Long offset, @PathVariable(value = "filetype", required = false) String filetype) {
        return inboundMessages(storeId, start, offset, filetype, null, null);
    }

    @RequestMapping(value = "/{storeId}/inboundmessages/{start}/{offset}/{filetype}/{from}/{to}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getInboundMessagesTimeConstrained(
            @PathVariable("storeId") String storeId,
            @PathVariable("start") Long start,
            @PathVariable("offset") Long offset,
            @PathVariable(value = "filetype", required = false) String filetype,
            @PathVariable("from") Long fromTime,
            @PathVariable("to") Long toTime) {
        return inboundMessages(storeId, start, offset, filetype, fromTime, toTime);
    }

    private ResponseEntity inboundMessages(String storeId, Long start, Long offset, String filetype, Long from, Long to) {
        Long count = -1l;
        String fileTypes = "SELECT DISTINCT file_type FROM inbound_file_staging";
        ArrayList<String> FT = new ArrayList<String>();

        String sql;

        start = start == null ? 0 : start;
        offset = offset == null ? 100 : offset;

        if (filetype.equalsIgnoreCase("All")) {
            filetype = "%";
        }


        sql = "SELECT\n" +
                "  ifs.id,\n" +
                "  ifs.event_time,\n" +
                "  ifs.file_type,\n" +
                "  ifs.archive_directory,\n" +
                "  ifs.filename,\n" +
                "  ifs.file_content,\n" +
                "  ifs.message_id,\n" +
                "  pdt.status = 'SUCCESS' retrieved\n" +
                "FROM inbound_file_staging ifs\n" +
                "  JOIN message_metadata mm\n" +
                "    ON ifs.message_id = mm.message_id\n" +
                "       AND mm.key = 'source_store_id'\n" +
                "       AND mm.value = ?\n" +
                "  JOIN process_data_trace pdt\n" +
                "    ON pdt.message_id = ifs.message_id\n" +
                "       AND pdt.target = 'inbound_file_staging'\n" +
                "WHERE ifs.file_type ILIKE ?";
        String orderStatement = "ORDER BY ifs.event_time DESC LIMIT ? OFFSET ?";

        if (from != null && from > 0) {
            sql += " AND ifs.event_time>=?";
        }
        if (to != null && to > 0) {
            sql += " AND ifs.event_time<=?";
        }
        sql += " " + orderStatement;

        List<Map> result = new ArrayList<>();
        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, storeId);
            ps.setString(i++, filetype);
            if (from != null && from > 0) {
                ps.setTimestamp(i++, new Timestamp(from));
            }
            if (to != null && to > 0) {
                ps.setTimestamp(i++, new Timestamp(to));
            }
            ps.setLong(i++, offset);
            ps.setLong(i++, start);
            logger.debug("Store: " + storeId + ", start=" + start + ", offset=" + offset + ", filetype = " + filetype);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                Map<String, Object> row = new LinkedHashMap<>();
                row.put("id", rset.getLong("id"));
                row.put("event_time", new Date(rset.getTimestamp("event_time").getTime()));
                row.put("data_type", rset.getString("file_type"));
                row.put("archive_directory", rset.getString("archive_directory"));
                row.put("filename", rset.getString("filename"));
                try {
                    row.put("file_content", InboundFileUtilities.getFileContent(rset.getString("file_content")));
                } catch (IOException e) {
                    logger.error("Unable to get file content for " + rset.getString("file_content"), e);
                }
                row.put("message_id", rset.getString("message_id"));
                row.put("success", rset.getBoolean("retrieved"));
                result.add(row);
            }
            rset.close();
            ps.close();

            ps = conn.prepareStatement(fileTypes);
            rset = ps.executeQuery();
            FT.add("All");
            while (rset.next()) {
                FT.add(rset.getString(1));
            }


        } catch (SQLException e) {
            logger.error("Unable to query for messages starting at " + start + " with length " + offset + " for store " + storeId, e);
        }
        Map finalResult = new HashMap();
        finalResult.put("items", result);
        finalResult.put("count", count);
        finalResult.put("filetypes", FT);


        return ResponseEntity.ok(finalResult);
    }

    @RequestMapping(value = "/{storeId}/outboundmessages/{start}/{offset}/{filetype}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getOutboundMessages(@PathVariable("storeId") Long storeId,
                                              @PathVariable("start") Long start,
                                              @PathVariable("offset") Long offset,
                                              @PathVariable("filetype") String filetype) {
        return outboundMessages(storeId, start, offset, filetype, null, null);
    }

    @RequestMapping(value = "/{storeId}/outboundmessages/{start}/{offset}/{filetype}/{from}/{to}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getOutboundMessages(@PathVariable("storeId") Long storeId,
                                              @PathVariable("start") Long start,
                                              @PathVariable("offset") Long offset,
                                              @PathVariable("filetype") String filetype,
                                              @PathVariable("from") Long fromTime,
                                              @PathVariable("to") Long toTime) {
        return outboundMessages(storeId, start, offset, filetype, fromTime, toTime);
    }

    private ResponseEntity outboundMessages(Long storeId, Long start, Long offset, String filetype, Long fromDate, Long toDate) {
        Long count = -1l;
        String countSql = "SELECT\n" +
                "  count(1)\n" +
                "FROM outbound_store_distribution osd\n" +
                "JOIN outbound_file_staging ofs\n" +
                "  ON ofs.id=osd.outbound_message_id\n" +
                "AND osd.store_id=?";

        if (filetype.equalsIgnoreCase("All")) {
            filetype = "%";
        }
        String fileTypes = "SELECT\n" +
                "  DISTINCT(ofs.file_type)\n" +
                "FROM outbound_store_distribution osd\n" +
                "  JOIN outbound_file_staging ofs\n" +
                "    ON ofs.id = osd.outbound_message_id\n" +
                "WHERE osd.delivered = TRUE\n" +
                "ORDER BY ofs.file_type";
        ArrayList<String> outboundMessages = new ArrayList<String>();

        String sql = "SELECT\n" +
                "  ofs.id,\n" +
                "  osd.event_time,\n" +
                "  ofs.file_type,\n" +
                "  ofs.filename,\n" +
                "  ofs.file_content,\n" +
                "  ofs.message_id,\n" +
                "  osd.delivered\n" +
                "FROM outbound_store_distribution osd\n" +
                "JOIN outbound_file_staging ofs\n" +
                "  ON ofs.id=osd.outbound_message_id\n" +
                "AND osd.store_id=?\n" +
                "WHERE osd.delivered=TRUE\n" +
                "AND ofs.file_type ILIKE ? ";

        if (fromDate != null && fromDate > 0) {
            sql += " AND osd.event_time>=? ";
        }
        if (toDate != null && toDate > 0) {
            sql += " AND osd.event_time<=? ";
        }
        sql += "ORDER BY osd.delivered_time DESC, ofs.id DESC LIMIT ? OFFSET ?";
        List<Map> result = new ArrayList<>();
        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            int i = 1;
            ps.setLong(i++, storeId);
            ps.setString(i++, filetype);
            if (fromDate != null && fromDate > 0) {
                ps.setTimestamp(i++, new Timestamp(fromDate));
            }
            if (toDate != null && toDate > 0) {
                ps.setTimestamp(i++, new Timestamp(toDate));
            }
            ps.setLong(i++, offset);
            ps.setLong(i++, start);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                Map<String, Object> row = new LinkedHashMap<>();
                row.put("id", rset.getLong("id"));
                row.put("event_time", new Date(rset.getTimestamp("event_time").getTime()));
                row.put("data_type", rset.getString("file_type"));
                row.put("archive_directory", null);
                row.put("filename", rset.getString("filename"));
                try {
                    row.put("file_content", InboundFileUtilities.getFileContent(rset.getString("file_content")));
                } catch (IOException e) {
                    logger.error("Unable to get file content for " + rset.getString("file_content"), e);
                }
                row.put("message_id", rset.getString("message_id"));
                row.put("success", rset.getString("delivered"));
                result.add(row);
            }
            rset.close();
            ps.close();

            ps = conn.prepareStatement(countSql);
            ps.setLong(1, storeId);
            rset = ps.executeQuery();
            if (rset.next()) {
                count = rset.getLong(1);
            }
            ps = conn.prepareStatement(fileTypes);
            rset = ps.executeQuery();
            outboundMessages.add("All");
            while (rset.next()) {
                outboundMessages.add(rset.getString(1));
            }
        } catch (SQLException e) {
            logger.error("Unable to query for messages starting at " + start + " with length " + offset + " for store " + storeId, e);
        }
        Map finalResult = new HashMap();
        finalResult.put("items", result);
        finalResult.put("count", count);
        finalResult.put("filetypes", outboundMessages);
        return ResponseEntity.ok(finalResult);
    }

    @RequestMapping(value = "/inboundmessage/{messageId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getInboundMessage(@PathVariable("messageId") Long messageId) {
        String sql = "SELECT * FROM inbound_file_staging ifs, process_data_trace pdt WHERE ifs.id=? AND ifs.id=cast(pdt.target_id AS BIGINT) AND pdt.target='inbound_file_staging'";
        Map result = new LinkedHashMap();
        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, messageId);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {

                result.put("id", rset.getLong("id"));
                result.put("event_time", new Date(rset.getTimestamp("event_time").getTime()));
                result.put("data_type", rset.getString("file_type"));
                result.put("archive_directory", rset.getString("archive_directory"));
                result.put("filename", rset.getString("filename"));
                try {
                    result.put("file_content", InboundFileUtilities.getFileContent(rset.getString("file_content")));
                } catch (IOException e) {
                    logger.error("Unable to get file content for " + rset.getString("file_content"), e);
                }
                result.put("status", rset.getString("status"));
                result.put("message_id", rset.getString("message_id"));
            }
            rset.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Response.serverError().build();
        }
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/outboundmessage/{messageId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getOutboundMessage(@PathVariable("messageId") Long messageId) {
        logger.debug("Getting outbound message for msg id: " + messageId);
        String sql = "SELECT * FROM outbound_file_staging ifs, process_data_trace pdt WHERE ifs.id=? AND ifs.id=cast(pdt.target_id AS BIGINT) AND pdt.target='outbound_file_staging'";
        Map result = new LinkedHashMap();
        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setLong(1, messageId);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {

                result.put("id", rset.getLong("id"));
                result.put("event_time", new Date(rset.getTimestamp("event_time").getTime()));
                result.put("data_type", rset.getString("file_type"));
                result.put("filename", rset.getString("filename"));
                try {
                    result.put("file_content", InboundFileUtilities.getFileContent(rset.getString("file_content")));
                } catch (IOException e) {
                    logger.error("Unable to get file content for " + rset.getString("file_content"), e);
                }
                result.put("status", rset.getString("status"));
                result.put("message_id", rset.getString("message_id"));
            }
            rset.close();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
            Response.serverError().build();
        }
        return ResponseEntity.ok(result);
    }


    /**
     * This is a really simple store update function which support only the most basic case (same IP for get/put,
     * standard
     * Extenda path's and username/password).
     *
     * @param settings
     * @return
     */
    @RequestMapping(value = "/{storeId}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity upsertStore(@RequestBody Map<String, String> settings) {
        logger.debug("Upsert store " + settings.get("id"));
        String upsertStore = "INSERT INTO store (id, name, network_address, country_code, timezone, active, source_connection_details, server, target_connection_details) VALUES (?,?,?,?,?,?,?,?,?) " +
                "ON CONFLICT (id) DO UPDATE SET NAME=?, network_address=?, country_code=?, TIMEZONE=?, active=?, source_connection_details=?, SERVER=?, target_connection_details=?";
        String upsertStoreServer = "INSERT INTO store_server (id, address, username, credentials) VALUES (?,?,?,?) " +
                "ON CONFLICT (id) DO UPDATE SET address=?";
        String upsertConnectionDetails = "INSERT INTO store_connection_details (id, protocol, path, server) VALUES (?,?,?,?) " +
                "ON CONFLICT (id) DO UPDATE SET protocol=?, path=?, SERVER=?";

        Long id = new Long(settings.get("id"));
        Long storeServerId = id + 20000;
        Long outboundConnectionId = id * 100;
        Long inboundConnectionId = id + 10000;
        String address = settings.get("address");
        String name = settings.get("name");
        boolean active = settings.get("active") != null && settings.get("active").equals("true");
        String username = "administrator";
        String password = "flipper";
        String outPath = "/ExtPgm/Trans/ToHQ";
        String inPath = "/ExtPgm/Trans/FromHQ";
        String protocol = "smb";
        String countryCode = settings.get("country_code");
        String timezone = "Europe/Stockholm";
        switch (countryCode) {
            case "NO":
                timezone = "Europe/Oslo";
                break;
            case "FI":
                timezone = "Europe/Helsinki";
                break;
            case "DE":
                timezone = "Europe/Berlin";
                break;
            case "IS":
                timezone = "Atlantic/Reykjavik";
                break;
        }


        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(upsertStoreServer);
            ps.setLong(1, storeServerId);
            ps.setString(2, address);
            ps.setString(3, username);
            ps.setString(4, password);
            ps.setString(5, address);
            ps.executeUpdate();

            ps = conn.prepareStatement(upsertConnectionDetails);
            ps.setLong(1, outboundConnectionId);
            ps.setString(2, protocol);
            ps.setString(3, outPath);
            ps.setLong(4, storeServerId);
            ps.setString(5, protocol);
            ps.setString(6, outPath);
            ps.setLong(7, storeServerId);
            ps.executeUpdate();

            ps = conn.prepareStatement(upsertConnectionDetails);
            ps.setLong(1, inboundConnectionId);
            ps.setString(2, protocol);
            ps.setString(3, inPath);
            ps.setLong(4, storeServerId);
            ps.setString(5, protocol);
            ps.setString(6, inPath);
            ps.setLong(7, storeServerId);
            ps.executeUpdate();

            ps = conn.prepareStatement(upsertStore);
            ps.setLong(1, id);
            ps.setString(2, name);
            ps.setString(3, address);
            ps.setString(4, countryCode);
            ps.setString(5, timezone);
            ps.setBoolean(6, active);
            ps.setLong(7, outboundConnectionId);
            ps.setLong(8, id);
            ps.setLong(9, inboundConnectionId);
            ps.setString(10, name);
            ps.setString(11, address);
            ps.setString(12, countryCode);
            ps.setString(13, timezone);
            ps.setBoolean(14, active);
            ps.setLong(15, outboundConnectionId);
            ps.setLong(16, id);
            ps.setLong(17, inboundConnectionId);
            ps.executeUpdate();

        } catch (SQLException e) {
            logger.error("Unable to upsert store!");
        }

        return ResponseEntity.ok().build();
    }


    @RequestMapping(value = "/addStore/", method = RequestMethod.PUT, produces = "application/json")
    @ResponseBody
    public ResponseEntity addStore(@RequestBody Map<String, String> settings) {
        /**
         * NEEDS TO BE REWORKED NOW THAT store is defined in store_connection_details instead of store table.!
         */
        if (true) {
            return null;
        }

        logger.debug("Entering addStore method and scd is: + " + settings.get("scd"));

        String ins_store = "INSERT INTO store (id,name,network_address,country_code,timezone,active,server,source_connection_details,target_connection_details) VALUES (?,?,?,?,?,?,?,?,?)";
        String ins_store_server = "INSERT INTO store_server (id,address,username,credentials) VALUES (?,?,?,?)";
        String ins_store_connection_in = "INSERT INTO store_connection_details (id,protocol,path) VALUES (?,?,?)";
        String ins_store_connection_out = "INSERT INTO store_connection_details (id,protocol,path) VALUES (?,?,?)";

        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(ins_store);
            ps.setInt(1, Integer.parseInt(settings.get("id")));
            ps.setString(2, settings.get("name"));
            ps.setString(3, settings.get("address"));
            ps.setString(4, settings.get("country_code"));
            ps.setString(5, settings.get("timezone"));
            ps.setBoolean(6, Boolean.valueOf(settings.get("active")));
            ps.setInt(7, Integer.parseInt(settings.get("server")));
            ps.setInt(8, Integer.parseInt(settings.get("scd")));
            ps.setInt(9, Integer.parseInt(settings.get("tcd")));


            PreparedStatement ps2 = conn.prepareStatement(ins_store_server);
            ps2.setInt(1, Integer.parseInt(settings.get("server"))); //NOT Implemented in FRONT!!!
            ps2.setString(2, settings.get("address"));
            ps2.setString(3, settings.get("username"));
            ps2.setString(4, settings.get("credentials"));


            PreparedStatement ps3 = conn.prepareStatement(ins_store_connection_in);
            ps3.setInt(1, Integer.parseInt(settings.get("scd")));  //NOT Implemented in FRONT!!!
            ps3.setString(2, settings.get("source_protocol"));
            ps3.setString(3, settings.get("source_path"));


            PreparedStatement ps4 = conn.prepareStatement(ins_store_connection_out);
            ps4.setInt(1, Integer.parseInt(settings.get("tcd")));  //NOT Implemented in FRONT!!!
            ps4.setString(2, settings.get("target_protocol"));
            ps4.setString(3, settings.get("target_path"));


            ps.executeUpdate();
            ps2.executeUpdate();
            ps3.executeUpdate();
            ps4.executeUpdate();


            ps.close();
            ps2.close();
            ps3.close();
            ps4.close();


        } catch (SQLException e) {
            logger.debug("CANNOT ADD STORE!!!!!");
            logger.debug("SQL EXCEPTION IS: ", e);

        }

        logger.debug("ADD STORE FINISHED!");
        return ResponseEntity.ok().build();
    }

}
