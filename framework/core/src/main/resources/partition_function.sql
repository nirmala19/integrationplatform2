CREATE OR REPLACE FUNCTION partition_function()
  RETURNS TRIGGER
LANGUAGE plpgsql
AS $$
DECLARE
  _tablename           TEXT;
  _original_table_name TEXT;
  _startdate           TIMESTAMP;
  _enddate             TIMESTAMP;
  _chk                 TEXT;
BEGIN
  --Takes the current inbound "time" value and determines when midnight is for the given date
  _original_table_name := TG_TABLE_NAME;
  _startdate := date_trunc('month', NEW.event_time);
  _tablename := _original_table_name || '_' || to_char(_startdate, 'YYYYMM');

  RAISE NOTICE 'Table name %', _tablename;

  -- Check if the partition needed for the current record exists
  PERFORM 1
  FROM pg_catalog.pg_class c
    JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
  WHERE c.relkind = 'r'
        AND c.relname = _tablename
        AND n.nspname = TG_TABLE_SCHEMA;

  -- If the partition needed does not yet exist, then we create it:
  -- Note that || is string concatenation (joining two strings to make one)
  IF NOT FOUND
  THEN
    _enddate:=_startdate :: TIMESTAMP + INTERVAL '1 month';
    EXECUTE 'CREATE TABLE ' || TG_TABLE_SCHEMA || '.' || quote_ident(_tablename) || ' (LIKE ' || _original_table_name ||
    ' INCLUDING ALL) INHERITS (' || _original_table_name
    || ')';
    --   EXECUTE 'ALTER TABLE '|| tg_table_schema || '.' || quote_ident(_tablename) ||' ADD CHECK ( "time" >= EXTRACT(EPOCH FROM DATE ' || quote_literal(_startdate) || ')
    --   AND "time" < EXTRACT(EPOCH FROM DATE ' || quote_literal(_enddate) || ')
    --   )

    _chk := ('ALTER TABLE ' || tg_table_schema || '.' || quote_ident(_tablename) ||
             ' ADD CHECK (event_time >= ' || quote_literal(_startdate) || ')');
    RAISE NOTICE 'Add check sql: %', _chk;
    EXECUTE _chk;
    --     EXECUTE 'ALTER TABLE ' || tg_table_schema || '.' || quote_ident(_tablename) ||
    --             'ADD CHECK (event_time < date_trunc(''month'', ' || quote_literal(_enddate) || '))';
  END IF;

  -- Insert the current record into the correct partition, which we are sure will now exist.
  EXECUTE 'INSERT INTO ' || TG_TABLE_SCHEMA || '.' || quote_ident(_tablename) || ' VALUES ($1.*)'
  USING NEW;
  RETURN NULL;
END;
$$;
