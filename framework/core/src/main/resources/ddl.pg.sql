--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE IF EXISTS indiska;
--
-- Name: indiska; Type: DATABASE; Schema: -; Owner: indiska
--

CREATE DATABASE indiska WITH TEMPLATE = template0 ENCODING = 'UTF8';


ALTER DATABASE indiska OWNER TO indiska;

\connect indiska

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: stage; Type: SCHEMA; Schema: -; Owner: indiska
--

CREATE SCHEMA stage;


ALTER SCHEMA stage OWNER TO indiska;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: outbound_distribution_update(); Type: FUNCTION; Schema: public; Owner: indiska
--

CREATE FUNCTION outbound_distribution_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  --
  -- Create a row in emp_audit to reflect the operation performed on emp,
  -- make use of the special variable TG_OP to work out the operation.
  --
  IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE')
  THEN
    INSERT INTO outbound_distribution (outbound_message_id, outbound_message_distributor_id, event_time, delivered)
      SELECT
        NEW.id,
        id,
        now(),
        FALSE
      FROM outbound_message_distributor omd
      WHERE
        omd.data_type = NEW.file_type
        AND omd.distribute_to_store = FALSE
    ON CONFLICT DO NOTHING;

    INSERT INTO outbound_store_distribution
      SELECT
        NEW.id,
        now(),
        FALSE,
        CASE WHEN (mm.value IS NOT NULL)
          THEN cast(mm.value AS BIGINT)
        ELSE cast(regexp_replace(NEW.filename, somd.file_name_store_match, E'\\1') AS BIGINT) END store_id
      FROM
        store_outbound_message_distributor somd
        LEFT OUTER JOIN message_metadata mm
          ON mm.message_id = NEW.message_id
        , store s
      WHERE (mm.key = 'STORE.ID.key'
             OR mm.key IS NULL)
            AND s.id = CASE WHEN (mm.value IS NOT NULL)
        THEN cast(mm.value AS BIGINT)
                       ELSE cast(regexp_replace(NEW.filename, somd.file_name_store_match, E'\\1') AS BIGINT) END
            AND s.active = TRUE
            AND somd.data_type = NEW.file_type
    ON CONFLICT DO NOTHING;
  END IF;

  RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$$;


ALTER FUNCTION public.outbound_distribution_update() OWNER TO indiska;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: alert; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE alert (
    id bigint NOT NULL,
    event_step character varying(128) NOT NULL,
    data_type character varying(128) NOT NULL,
    process_id bigint NOT NULL,
    process_data_trace_id bigint NOT NULL,
    acknowledge_id bigint NOT NULL,
    message_id character varying(64),
    event_time timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE alert OWNER TO indiska;

--
-- Name: alert_acknowledgement; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE alert_acknowledgement (
    id bigint NOT NULL,
    achknowledged_by character varying(64) NOT NULL,
    event_time timestamp without time zone DEFAULT now()
);


ALTER TABLE alert_acknowledgement OWNER TO indiska;

--
-- Name: alert_acknowledgement_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE alert_acknowledgement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alert_acknowledgement_id_seq OWNER TO indiska;

--
-- Name: alert_acknowledgement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE alert_acknowledgement_id_seq OWNED BY alert_acknowledgement.id;


--
-- Name: alert_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE alert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE alert_id_seq OWNER TO indiska;

--
-- Name: alert_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE alert_id_seq OWNED BY alert.id;


--
-- Name: balancelist; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE balancelist (
    item character varying(32) NOT NULL,
    date timestamp without time zone NOT NULL,
    warehouse character varying(16) NOT NULL,
    quantity integer NOT NULL,
    account00qty integer NOT NULL,
    account20qty integer NOT NULL
);


ALTER TABLE balancelist OWNER TO indiska;

--
-- Name: clearance_current_campaign; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE clearance_current_campaign (
    campaign_id bigint NOT NULL,
    store_id bigint NOT NULL,
    item_id character varying(128) NOT NULL,
    campaign_price bigint NOT NULL,
    from_date timestamp without time zone NOT NULL,
    to_date timestamp without time zone NOT NULL,
    change_date timestamp without time zone NOT NULL
);


ALTER TABLE clearance_current_campaign OWNER TO indiska;

--
-- Name: data_type; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE data_type (
    id character varying(128) NOT NULL
);


ALTER TABLE data_type OWNER TO indiska;

--
-- Name: file_type_identifier; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE file_type_identifier (
    data_type character varying(128) NOT NULL,
    pattern character varying(256) NOT NULL
);


ALTER TABLE file_type_identifier OWNER TO indiska;

--
-- Name: inbound_message_collector; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE inbound_message_collector (
    id bigint NOT NULL,
    remote_location bigint NOT NULL,
    remote_server bigint NOT NULL,
    file_pattern character varying(256),
    substitute_pattern character varying(256)
);


ALTER TABLE inbound_message_collector OWNER TO indiska;

--
-- Name: inbound_file_retrieval_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE inbound_file_retrieval_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inbound_file_retrieval_id_seq OWNER TO indiska;

--
-- Name: inbound_file_retrieval_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE inbound_file_retrieval_id_seq OWNED BY inbound_message_collector.id;


--
-- Name: inbound_file_staging; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE inbound_file_staging (
    id bigint NOT NULL,
    event_time timestamp without time zone NOT NULL,
    file_type character varying(256) NOT NULL,
    source_directory character varying(1024) NOT NULL,
    archive_directory character varying(1024),
    filename character varying(512) NOT NULL,
    file_content text,
    checksum character varying(64),
    message_id character varying(64) NOT NULL
);


ALTER TABLE inbound_file_staging OWNER TO indiska;

--
-- Name: inbound_file_staging_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE inbound_file_staging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE inbound_file_staging_id_seq OWNER TO indiska;

--
-- Name: inbound_file_staging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE inbound_file_staging_id_seq OWNED BY inbound_file_staging.id;


--
-- Name: job; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE job (
    id character varying(128) NOT NULL,
    class character varying(128) NOT NULL,
    event_type character varying(128) NOT NULL,
    description character varying(256),
    job_status character varying(24) DEFAULT 'ACTIVE'::character varying NOT NULL
);


ALTER TABLE job OWNER TO indiska;

--
-- Name: COLUMN job.id; Type: COMMENT; Schema: public; Owner: indiska
--

COMMENT ON COLUMN job.id IS 'Job ID';


--
-- Name: job_event_triggers; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE job_event_triggers (
    id bigint NOT NULL,
    job_id character varying(128) NOT NULL,
    event_step character varying(128) NOT NULL,
    data_type character varying(128) NOT NULL,
    job_status character varying(128),
    last_updated timestamp without time zone DEFAULT now() NOT NULL,
    allow_concurrent boolean DEFAULT true NOT NULL
);


ALTER TABLE job_event_triggers OWNER TO indiska;

--
-- Name: COLUMN job_event_triggers.id; Type: COMMENT; Schema: public; Owner: indiska
--

COMMENT ON COLUMN job_event_triggers.id IS 'Job ID';


--
-- Name: job_event_triggers_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE job_event_triggers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE job_event_triggers_id_seq OWNER TO indiska;

--
-- Name: job_event_triggers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE job_event_triggers_id_seq OWNED BY job_event_triggers.id;


--
-- Name: job_event_type; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE job_event_type (
    id character varying(128) NOT NULL,
    description character varying(256)
);


ALTER TABLE job_event_type OWNER TO indiska;

--
-- Name: job_schedule_triggers; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE job_schedule_triggers (
    id bigint DEFAULT nextval('job_event_triggers_id_seq'::regclass) NOT NULL,
    job_id character varying(128) NOT NULL,
    event_step character varying(128) NOT NULL,
    data_type character varying(128) NOT NULL,
    schedule character varying(64) NOT NULL,
    job_status character varying(128),
    last_updated timestamp without time zone NOT NULL,
    parameters text,
    allow_concurrent boolean DEFAULT true NOT NULL
);


ALTER TABLE job_schedule_triggers OWNER TO indiska;

--
-- Name: COLUMN job_schedule_triggers.job_id; Type: COMMENT; Schema: public; Owner: indiska
--

COMMENT ON COLUMN job_schedule_triggers.job_id IS 'Job ID';


--
-- Name: job_status; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE job_status (
    id character varying(128) NOT NULL
);


ALTER TABLE job_status OWNER TO indiska;

--
-- Name: message_annotation_lookup; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE message_annotation_lookup (
    id character varying(128) NOT NULL,
    sql_expression character varying(1024)
);


ALTER TABLE message_annotation_lookup OWNER TO indiska;

--
-- Name: message_metadata; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE message_metadata (
    message_id character varying(64) NOT NULL,
    key character varying(128) NOT NULL,
    value character varying(128),
    event_time timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE message_metadata OWNER TO indiska;

--
-- Name: outbound_distribution; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE outbound_distribution (
    outbound_message_id bigint NOT NULL,
    event_time timestamp without time zone DEFAULT now() NOT NULL,
    delivered boolean DEFAULT false NOT NULL,
    outbound_message_distributor_id bigint NOT NULL
);


ALTER TABLE outbound_distribution OWNER TO indiska;

--
-- Name: outbound_file_staging; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE outbound_file_staging (
    id bigint NOT NULL,
    event_time timestamp without time zone NOT NULL,
    filename character varying(512) NOT NULL,
    file_type character varying(256) NOT NULL,
    file_content text NOT NULL,
    source_identifier bigint,
    message_id character varying(64) NOT NULL
);


ALTER TABLE outbound_file_staging OWNER TO indiska;

--
-- Name: outbound_file_staging_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE outbound_file_staging_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE outbound_file_staging_id_seq OWNER TO indiska;

--
-- Name: outbound_file_staging_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE outbound_file_staging_id_seq OWNED BY outbound_file_staging.id;


--
-- Name: outbound_message_distributor; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE outbound_message_distributor (
    id bigint NOT NULL,
    remote_location bigint NOT NULL,
    data_type character varying(128) NOT NULL,
    immediate_delivery boolean NOT NULL,
    remote_server bigint NOT NULL,
    distribute_to_store boolean DEFAULT false
);


ALTER TABLE outbound_message_distributor OWNER TO indiska;

--
-- Name: outbound_message_distributor_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE outbound_message_distributor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE outbound_message_distributor_id_seq OWNER TO indiska;

--
-- Name: outbound_message_distributor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE outbound_message_distributor_id_seq OWNED BY outbound_message_distributor.id;


--
-- Name: outbound_store_distribution; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE outbound_store_distribution (
    outbound_message_id bigint NOT NULL,
    event_time timestamp without time zone DEFAULT now() NOT NULL,
    delivered boolean DEFAULT false NOT NULL,
    store_id bigint NOT NULL,
    delivered_time timestamp without time zone
);


ALTER TABLE outbound_store_distribution OWNER TO indiska;

--
-- Name: process_data_trace; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE process_data_trace (
    id bigint NOT NULL,
    process_id bigint NOT NULL,
    source character varying(128) NOT NULL,
    source_id bigint,
    target character varying(128) NOT NULL,
    target_id bigint,
    status character varying(32) NOT NULL,
    message_id character varying(64),
    event_time timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE process_data_trace OWNER TO indiska;

--
-- Name: process_data_trace_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE process_data_trace_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE process_data_trace_id_seq OWNER TO indiska;

--
-- Name: process_data_trace_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE process_data_trace_id_seq OWNED BY process_data_trace.id;


--
-- Name: process_error; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE process_error (
    id bigint NOT NULL,
    process_status_id bigint NOT NULL,
    error text,
    process_data_trace_id bigint,
    event_time timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE process_error OWNER TO indiska;

--
-- Name: process_errors_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE process_errors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE process_errors_id_seq OWNER TO indiska;

--
-- Name: process_errors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE process_errors_id_seq OWNED BY process_error.id;


--
-- Name: process_status; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE process_status (
    id bigint NOT NULL,
    event_time timestamp without time zone NOT NULL,
    job_id character varying(128) NOT NULL,
    data_type character varying(128) NOT NULL,
    status character varying(24) NOT NULL,
    process_initiator_id bigint,
    event_step character varying(128),
    parameters character varying(256),
    allow_concurrent boolean DEFAULT true NOT NULL
);


ALTER TABLE process_status OWNER TO indiska;

--
-- Name: process_status_history; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE process_status_history (
    process_status_id bigint NOT NULL,
    from_status character varying(128) DEFAULT NULL::character varying,
    to_status character varying(128) NOT NULL,
    event_time timestamp without time zone NOT NULL,
    change_initiator character varying(128) DEFAULT NULL::character varying
);


ALTER TABLE process_status_history OWNER TO indiska;

--
-- Name: process_status_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE process_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE process_status_id_seq OWNER TO indiska;

--
-- Name: process_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE process_status_id_seq OWNED BY process_status.id;


--
-- Name: remote_location; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE remote_location (
    id bigint NOT NULL,
    directory character varying(256) NOT NULL,
    description character varying(512),
    group_id bigint
);


ALTER TABLE remote_location OWNER TO indiska;

--
-- Name: remote_location_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE remote_location_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE remote_location_id_seq OWNER TO indiska;

--
-- Name: remote_location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE remote_location_id_seq OWNED BY remote_location.id;


--
-- Name: remote_server; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE remote_server (
    id bigint NOT NULL,
    name character varying(64) NOT NULL,
    username character varying(64) NOT NULL,
    address character varying(256) NOT NULL,
    password character varying(256),
    group_id bigint,
    protocol character varying(16)
);


ALTER TABLE remote_server OWNER TO indiska;

--
-- Name: remote_server_group; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE remote_server_group (
    id bigint NOT NULL,
    name character varying(128)
);


ALTER TABLE remote_server_group OWNER TO indiska;

--
-- Name: remote_server_group_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE remote_server_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE remote_server_group_id_seq OWNER TO indiska;

--
-- Name: remote_server_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE remote_server_group_id_seq OWNED BY remote_server_group.id;


--
-- Name: remote_server_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE remote_server_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE remote_server_id_seq OWNER TO indiska;

--
-- Name: remote_server_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE remote_server_id_seq OWNED BY remote_server.id;


--
-- Name: settings; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE settings (
    key character varying(64) NOT NULL,
    value character varying(256)
);


ALTER TABLE settings OWNER TO indiska;

--
-- Name: store; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE store (
    id bigint NOT NULL,
    name character varying(128) NOT NULL,
    network_address character varying(256),
    country_code character varying(4),
    timezone character varying(64) DEFAULT 'Europe/Stockholm'::character varying NOT NULL,
    active boolean DEFAULT false NOT NULL,
    source_connection_details bigint,
    server bigint,
    target_connection_details bigint
);


ALTER TABLE store OWNER TO indiska;

--
-- Name: store_connection_details; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE store_connection_details (
    id bigint NOT NULL,
    protocol character varying(16) NOT NULL,
    path character varying(256) NOT NULL
);


ALTER TABLE store_connection_details OWNER TO indiska;

--
-- Name: store_connection_details_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE store_connection_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_connection_details_id_seq OWNER TO indiska;

--
-- Name: store_connection_details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE store_connection_details_id_seq OWNED BY store_connection_details.id;


--
-- Name: store_outbound_message_distributor; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE store_outbound_message_distributor (
    id bigint NOT NULL,
    data_type character varying(128) NOT NULL,
    file_name_store_match character varying(128)
);


ALTER TABLE store_outbound_message_distributor OWNER TO indiska;

--
-- Name: store_outbound_message_distributor_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE store_outbound_message_distributor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_outbound_message_distributor_id_seq OWNER TO indiska;

--
-- Name: store_outbound_message_distributor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE store_outbound_message_distributor_id_seq OWNED BY store_outbound_message_distributor.id;


--
-- Name: store_server; Type: TABLE; Schema: public; Owner: indiska
--

CREATE TABLE store_server (
    id bigint NOT NULL,
    address character varying(128) NOT NULL,
    username character varying(128) NOT NULL,
    credentials character varying(128) NOT NULL
);


ALTER TABLE store_server OWNER TO indiska;

--
-- Name: store_server_id_seq; Type: SEQUENCE; Schema: public; Owner: indiska
--

CREATE SEQUENCE store_server_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE store_server_id_seq OWNER TO indiska;

--
-- Name: store_server_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: indiska
--

ALTER SEQUENCE store_server_id_seq OWNED BY store_server.id;


SET search_path = stage, pg_catalog;

--
-- Name: store_currentcampaign; Type: TABLE; Schema: stage; Owner: indiska
--

CREATE TABLE store_currentcampaign (
    store integer NOT NULL,
    item bigint NOT NULL,
    campaign bigint NOT NULL,
    fromdate timestamp without time zone NOT NULL,
    todate timestamp without time zone NOT NULL,
    campaign_price numeric NOT NULL,
    changedate timestamp without time zone NOT NULL,
    shop_id integer NOT NULL
);


ALTER TABLE store_currentcampaign OWNER TO indiska;

--
-- Name: pos_transaction; Type: TABLE; Schema: stage; Owner: indiska
--

CREATE TABLE pos_transaction (
    transaction_id character varying(64) NOT NULL,
    store integer NOT NULL,
    workstation_id integer NOT NULL,
    sequence_number integer NOT NULL,
    operator_id integer NOT NULL,
    currency_code character varying(8) NOT NULL,
    receipt_number integer NOT NULL,
    event_time timestamp without time zone NOT NULL,
    customer_id character varying(64),
    message_id character varying(64) NOT NULL,
    training_mode boolean,
    cancel_flag boolean,
    transaction_status character varying(64)
);


ALTER TABLE pos_transaction OWNER TO indiska;

--
-- Name: pos_transaction_sales; Type: TABLE; Schema: stage; Owner: indiska
--

CREATE TABLE pos_transaction_sales (
    sequence_number integer NOT NULL,
    transaction_id character varying(64) NOT NULL,
    is_return boolean NOT NULL,
    item_type character varying(16),
    item_id character varying(64),
    description character varying(64),
    tax_included_in_price_flag character varying(12),
    regular_sales_unit_price numeric(18,2) NOT NULL,
    actual_sales_unit_price numeric(18,2) NOT NULL,
    extended_amount numeric(18,2) NOT NULL,
    quantity numeric(18,2) NOT NULL,
    web_return character varying(12),
    tax_type character varying(16),
    tax_amount numeric(18,2),
    tax_percent numeric(18,2),
    tax_group_id character varying(16),
    disposal character varying(24),
    reason character varying(8),
    message_id character varying(64) NOT NULL,
    "time" timestamp without time zone DEFAULT now(),
    new_price numeric(18,2),
    previous_price numeric(18,2),
    cancel_flag boolean
);


ALTER TABLE pos_transaction_sales OWNER TO indiska;

--
-- Name: transaction_025; Type: TABLE; Schema: stage; Owner: indiska
--

CREATE TABLE transaction_025 (
    transactiontype character varying(3),
    action character varying(1),
    store_id integer NOT NULL,
    sequence_number integer,
    commodity character varying(13) NOT NULL,
    artgroup integer NOT NULL,
    receipttext character varying(45),
    quantcode integer,
    bonuspoint integer,
    guaranteecode integer,
    commoditytype integer,
    salespriced numeric(8,0),
    vippriced numeric(8,0),
    pricegroupd numeric(4,0),
    changedate timestamp without time zone NOT NULL,
    lastchangedby integer,
    transdate timestamp without time zone,
    advertweek integer,
    orderlevel integer,
    type integer,
    category integer,
    material character varying(40),
    materialconstr integer,
    pattern integer,
    sizetype integer,
    sizelist character varying(20),
    weight character varying(20),
    country integer,
    prodcountry integer,
    storesolid integer,
    storeorderunit integer,
    pricelabeltype integer,
    pricelsupplier integer,
    pricelabelqty integer,
    description character varying(30),
    text character varying(20),
    box integer,
    unity character varying(4),
    startdate character varying(8),
    nroflabels integer,
    colliq integer,
    season integer,
    orderunit integer,
    assortment integer,
    duty integer,
    currency integer,
    fillcode character varying(5),
    prop1 character varying(20),
    prop2 character varying(20),
    prop3 character varying(20),
    prop4 character varying(20),
    prop5 character varying(20),
    prop6 character varying(20),
    trademark integer,
    supplier integer,
    costpricesuppd numeric(8,0),
    costpriced numeric(8,0),
    grossmarginpcd numeric(4,0),
    comppriced numeric(8,0),
    priceorigd numeric(8,0),
    ordercode integer,
    suppliernrd character varying(20),
    orderpack integer,
    consumerpack integer,
    comsupplier integer,
    suppldescription character varying(40),
    articlecode character varying(20),
    prewash integer,
    washcode integer,
    pickstat integer,
    shoporderpack integer,
    packing integer
);


ALTER TABLE transaction_025 OWNER TO indiska;

--
-- Name: transaction_026; Type: TABLE; Schema: stage; Owner: indiska
--

CREATE TABLE transaction_026 (
    transactiontype character varying(3),
    action character varying(1),
    store_id integer NOT NULL,
    sequence_number integer,
    article character varying(13) NOT NULL,
    commodity character varying(13) NOT NULL,
    salespriced numeric(8,0),
    description character varying(20),
    colour integer NOT NULL,
    costpriced numeric(8,0),
    grossmarginpcd numeric(4,0),
    orderunit integer,
    costpricesuppd numeric(8,0),
    priceorigd numeric(8,0),
    bnr integer,
    size integer,
    suppliernr character varying(13)
);


ALTER TABLE transaction_026 OWNER TO indiska;

--
-- Name: transaction_040; Type: TABLE; Schema: stage; Owner: indiska
--

CREATE TABLE transaction_040 (
    transactiontype character varying(3),
    action character varying(1),
    store_id integer NOT NULL,
    sequence_number integer,
    article character varying(13) NOT NULL,
    barcode character varying(13)
);


ALTER TABLE transaction_040 OWNER TO indiska;

--
-- Name: transaction_132; Type: TABLE; Schema: stage; Owner: indiska
--

CREATE TABLE transaction_132 (
    transactiontype character varying(3),
    action character varying(1),
    store_id integer,
    sequence_number integer,
    arrived_id character varying(9),
    ordernr character varying(9),
    arrived_date timestamp without time zone,
    type integer,
    arrived_quantity integer,
    updated_date timestamp without time zone
);


ALTER TABLE transaction_132 OWNER TO indiska;

SET search_path = public, pg_catalog;

--
-- Name: alert id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert ALTER COLUMN id SET DEFAULT nextval('alert_id_seq'::regclass);


--
-- Name: alert_acknowledgement id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert_acknowledgement ALTER COLUMN id SET DEFAULT nextval('alert_acknowledgement_id_seq'::regclass);


--
-- Name: inbound_file_staging id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY inbound_file_staging ALTER COLUMN id SET DEFAULT nextval('inbound_file_staging_id_seq'::regclass);


--
-- Name: inbound_message_collector id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY inbound_message_collector ALTER COLUMN id SET DEFAULT nextval('inbound_file_retrieval_id_seq'::regclass);


--
-- Name: job_event_triggers id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_event_triggers ALTER COLUMN id SET DEFAULT nextval('job_event_triggers_id_seq'::regclass);


--
-- Name: outbound_file_staging id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_file_staging ALTER COLUMN id SET DEFAULT nextval('outbound_file_staging_id_seq'::regclass);


--
-- Name: outbound_message_distributor id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_message_distributor ALTER COLUMN id SET DEFAULT nextval('outbound_message_distributor_id_seq'::regclass);


--
-- Name: process_data_trace id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_data_trace ALTER COLUMN id SET DEFAULT nextval('process_data_trace_id_seq'::regclass);


--
-- Name: process_error id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_error ALTER COLUMN id SET DEFAULT nextval('process_errors_id_seq'::regclass);


--
-- Name: process_status id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_status ALTER COLUMN id SET DEFAULT nextval('process_status_id_seq'::regclass);


--
-- Name: remote_location id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY remote_location ALTER COLUMN id SET DEFAULT nextval('remote_location_id_seq'::regclass);


--
-- Name: remote_server id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY remote_server ALTER COLUMN id SET DEFAULT nextval('remote_server_id_seq'::regclass);


--
-- Name: remote_server_group id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY remote_server_group ALTER COLUMN id SET DEFAULT nextval('remote_server_group_id_seq'::regclass);


--
-- Name: store_connection_details id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY store_connection_details ALTER COLUMN id SET DEFAULT nextval('store_connection_details_id_seq'::regclass);


--
-- Name: store_outbound_message_distributor id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY store_outbound_message_distributor ALTER COLUMN id SET DEFAULT nextval('store_outbound_message_distributor_id_seq'::regclass);


--
-- Name: store_server id; Type: DEFAULT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY store_server ALTER COLUMN id SET DEFAULT nextval('store_server_id_seq'::regclass);


--
-- Data for Name: alert; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Data for Name: alert_acknowledgement; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO alert_acknowledgement VALUES (-1, 'NOT ACKNOWLEDGED', '2017-05-03 09:44:11.707864');


--
-- Name: alert_acknowledgement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('alert_acknowledgement_id_seq', 1, false);


--
-- Name: alert_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('alert_id_seq', 508746, true);


--
-- Data for Name: balancelist; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Data for Name: clearance_current_campaign; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Data for Name: data_type; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO data_type VALUES ('POSLOG');
INSERT INTO data_type VALUES ('CLEARANCE');
INSERT INTO data_type VALUES ('ARTICLE');
INSERT INTO data_type VALUES ('SYSTEM');
INSERT INTO data_type VALUES ('BALANCELIST');
INSERT INTO data_type VALUES ('DISPATCHSYNC');
INSERT INTO data_type VALUES ('ORDERDELIVERYREPORT');
INSERT INTO data_type VALUES ('BALANCELISTALL');
INSERT INTO data_type VALUES ('BALANCELISTECOM');
INSERT INTO data_type VALUES ('PURCHASEORDERLINERECEIVE');
INSERT INTO data_type VALUES ('PURCHASEORDERLINERECEIVEREPORT');
INSERT INTO data_type VALUES ('ORDERDELIVERYREPORTECOM');
INSERT INTO data_type VALUES ('ORDERCREATE');
INSERT INTO data_type VALUES ('PARTSYNCEXTRA');
INSERT INTO data_type VALUES ('APEXDISPATCHSYNC');
INSERT INTO data_type VALUES ('PMLOG');
INSERT INTO data_type VALUES ('SHOPORDER');
INSERT INTO data_type VALUES ('STORESTOCKCOUNT');
INSERT INTO data_type VALUES ('GOODSRECEIPT');
INSERT INTO data_type VALUES ('ARTICLEHIERARCHY');
INSERT INTO data_type VALUES ('PRICECHANGE');
INSERT INTO data_type VALUES ('PROMOTION');


--
-- Data for Name: file_type_identifier; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO file_type_identifier VALUES ('POSLOG', 'POSLog.*');
INSERT INTO file_type_identifier VALUES ('PMLOG', 'PMLog.*');
INSERT INTO file_type_identifier VALUES ('ARTICLE', 'HQ.*?\.01\..*');
INSERT INTO file_type_identifier VALUES ('ARTICLEHIERARCHY', 'HQ.*?\.02\..*');
INSERT INTO file_type_identifier VALUES ('GOODSRECEIPT', 'HQ.*?\.06\..*');
INSERT INTO file_type_identifier VALUES ('SHOPORDER', 'HQ.*?\.07\..*');
INSERT INTO file_type_identifier VALUES ('STORESTOCKCOUNT', 'HQ.*?\.08\..*');
INSERT INTO file_type_identifier VALUES ('PRICECHANGE', 'HQ.*?\.03\..*');
INSERT INTO file_type_identifier VALUES ('PROMOTION', 'HQ.*?\.04\..*');


--
-- Name: inbound_file_retrieval_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('inbound_file_retrieval_id_seq', 1, false);


--
-- Data for Name: inbound_file_staging; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Name: inbound_file_staging_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('inbound_file_staging_id_seq', 14093086, true);


--
-- Data for Name: inbound_message_collector; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO inbound_message_collector VALUES (7, 19, 1, NULL, NULL);
INSERT INTO inbound_message_collector VALUES (27, 12, 2, 'HQ.*?\.03\..*', NULL);
INSERT INTO inbound_message_collector VALUES (29, 12, 2, 'HQ.*?\.04\..*', NULL);
INSERT INTO inbound_message_collector VALUES (23, 12, 2, 'HQ.*?\.01\..*', NULL);
INSERT INTO inbound_message_collector VALUES (24, 12, 2, 'HQ.*?\.02\..*', NULL);
INSERT INTO inbound_message_collector VALUES (1, 3, 1, '.*?\.xml', '/(.+)/S$1/');
INSERT INTO inbound_message_collector VALUES (2, 2, 1, '.*?\.xml', '/(.+)/ds$1/');
INSERT INTO inbound_message_collector VALUES (3, 19, 1, '.*?\.xml', '/(.+)/od$1/');
INSERT INTO inbound_message_collector VALUES (4, 3, 1, '.*?\.xml', '/(.+)/p$1/');
INSERT INTO inbound_message_collector VALUES (5, 18, 1, '.*?\.xml', '/(.+)/bl$1/');
INSERT INTO inbound_message_collector VALUES (28, 20, 1, '.*?\.xml', '/(.+)/o$1/');


--
-- Data for Name: job; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO job VALUES ('POSLOG_STAGE_LOAD', 'se.indiska.integration.integrations.poslog.StaxPosLogParser', 'STAGE_LOAD', 'Parses and stores poslogs into data staging', 'ACTIVE');
INSERT INTO job VALUES ('CLEARANCE_STAGE_LOAD', 'se.indiska.integration.integrations.clearance.ClearanceFixedParser', 'STAGE_LOAD', 'Loads clearances into staging tables', 'ACTIVE');
INSERT INTO job VALUES ('ARTICLE_STAGE_LOAD', 'se.indiska.integration.integrations.article.ArticleParser', 'STAGE_LOAD', 'Parses and stores articles into data staging', 'ACTIVE');
INSERT INTO job VALUES ('INBOUND_MESSAGE_COLLECTOR', 'se.indiska.integration.framework.job.InboundMessageCollectorJob', 'INBOUND_TRANSFER', 'Collects inbound messages', 'ACTIVE');
INSERT INTO job VALUES ('INBOUND_OUTBOUND_BOUNCER', 'se.indiska.integration.integrations.general.InboundOutboundBouncer', 'OUTBOUND_FILE_LOAD', 'Bounces files from inbound to outbound.', 'ACTIVE');
INSERT INTO job VALUES ('OUTBOUND_MESSAGE_DISTRIBUTOR', 'se.indiska.integration.framework.job.OutboundTransferByEventJob', 'OUTBOUND_TRANSFER', 'Transmits outbound messages to targets', 'ACTIVE');
INSERT INTO job VALUES ('STORE_FILE_COLLECTOR', 'se.indiska.integration.framework.job.StoreCollectorJob', 'INBOUND_TRANSFER', 'Collects messages from file-based POS''s (extenda)', 'ACTIVE');
INSERT INTO job VALUES ('STORE_FILE_DISTRIBUTOR', 'se.indiska.integration.framework.job.StoreDistributorJob', 'OUTBOUND_TRANSFER', 'Distributes files to file-based POS''s (extenda)', 'ACTIVE');
INSERT INTO job VALUES ('POSLOG_ELASTICSEARCH_LOAD', 'se.indiska.integration.integrations.poslog.ESLoader', 'ODS_LOAD', 'Loads POSLogs into elasticsearch for analysis/query/reporting', 'ACTIVE');
INSERT INTO job VALUES ('GOODS_RECEIPT_STAGE_LOAD', 'se.indiska.integration.integrations.goodsreceipt.GoodsReceiptParser', 'STAGE_LOAD', 'Loads Goods Receipt files from inbound to stage table', 'ACTIVE');


--
-- Data for Name: job_event_triggers; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO job_event_triggers VALUES (16, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'POSLOG', 'ACTIVE', '2017-04-11 10:55:53.903126', true);
INSERT INTO job_event_triggers VALUES (3, 'CLEARANCE_STAGE_LOAD', 'INBOUND_TRANSFER', 'CLEARANCE', 'DISABLED', '2017-04-03 11:48:24.091469', true);
INSERT INTO job_event_triggers VALUES (4, 'ARTICLE_STAGE_LOAD', 'INBOUND_TRANSFER', 'ARTICLE', 'DISABLED', '2017-04-03 11:48:24.091469', true);
INSERT INTO job_event_triggers VALUES (21, 'POSLOG_ELASTICSEARCH_LOAD', 'STAGE_LOAD', 'POSLOG', 'DISABLED', '2017-05-02 13:29:55.225', true);
INSERT INTO job_event_triggers VALUES (17, 'POSLOG_STAGE_LOAD', 'INBOUND_TRANSFER', 'POSLOG', 'ACTIVE', '2017-04-24 17:02:27.164', true);
INSERT INTO job_event_triggers VALUES (5, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'DISPATCHSYNC', 'DISABLED', '2017-04-04 16:15:09.978', true);
INSERT INTO job_event_triggers VALUES (6, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'BALANCELIST', 'DISABLED', '2017-04-06 17:11:45.593', true);
INSERT INTO job_event_triggers VALUES (7, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'ORDERDELIVERYREPORT', 'DISABLED', '2017-04-06 17:11:45.593', true);
INSERT INTO job_event_triggers VALUES (8, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'BALANCELISTALL', 'DISABLED', '2017-04-06 17:11:45.593', true);
INSERT INTO job_event_triggers VALUES (9, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'BALANCELISTECOM', 'DISABLED', '2017-04-06 17:11:45.593', true);
INSERT INTO job_event_triggers VALUES (10, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'ORDERDELIVERYREPORTECOM', 'DISABLED', '2017-04-06 17:11:45.593', true);
INSERT INTO job_event_triggers VALUES (11, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'ORDERCREATE', 'DISABLED', '2017-04-11 10:26:17.754292', true);
INSERT INTO job_event_triggers VALUES (12, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'PARTSYNCEXTRA', 'DISABLED', '2017-04-11 10:46:25.48985', true);
INSERT INTO job_event_triggers VALUES (13, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'PURCHASEORDERLINERECEIVE', 'DISABLED', '2017-04-11 10:49:15.798279', true);
INSERT INTO job_event_triggers VALUES (14, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'PURCHASEORDERLINERECEIVEREPORT', 'DISABLED', '2017-04-11 10:51:15.798', true);
INSERT INTO job_event_triggers VALUES (15, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'APEXDISPATCHSYNC', 'DISABLED', '2017-04-11 10:55:53.903126', true);
INSERT INTO job_event_triggers VALUES (18, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'ARTICLE', 'DISABLED', '2017-04-25 16:04:45.418', true);
INSERT INTO job_event_triggers VALUES (19, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'GOODSRECEIPT', 'DISABLED', '2017-04-25 18:01:25.737', true);
INSERT INTO job_event_triggers VALUES (20, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'ARTICLEHIERARCHY', 'DISABLED', '2017-04-25 16:04:45.418', true);
INSERT INTO job_event_triggers VALUES (22, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'PMLOG', 'DISABLED', '2017-04-11 10:55:53.903126', true);
INSERT INTO job_event_triggers VALUES (23, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'CLEARANCE', 'DISABLED', '2017-04-03 11:48:24.091469', true);
INSERT INTO job_event_triggers VALUES (24, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'STORESTOCKCOUNT', 'DISABLED', '2017-04-25 18:01:25.737', true);
INSERT INTO job_event_triggers VALUES (25, 'INBOUND_OUTBOUND_BOUNCER', 'INBOUND_TRANSFER', 'SHOPORDER', 'DISABLED', '2017-04-25 18:01:25.737', true);
INSERT INTO job_event_triggers VALUES (26, 'GOODS_RECEIPT_STAGE_LOAD', 'INBOUND_TRANSFER', 'GOODSRECEIPT', 'DISABLED', '2017-06-02 14:44:54.738', true);


--
-- Name: job_event_triggers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('job_event_triggers_id_seq', 2, true);


--
-- Data for Name: job_event_type; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO job_event_type VALUES ('INBOUND_FILE_LOAD', 'When a message is loaded into a staging table');
INSERT INTO job_event_type VALUES ('OUTBOUND_FILE_LOAD', 'When a message is loaded into the outbound staging table');
INSERT INTO job_event_type VALUES ('SYSTEM', 'System checks and similar internal jobs');
INSERT INTO job_event_type VALUES ('INBOUND_TRANSFER', 'Retrieving a message from a source system');
INSERT INTO job_event_type VALUES ('OUTBOUND_TRANSFER', 'Sending a message to a target system');
INSERT INTO job_event_type VALUES ('STAGE_LOAD', 'Loading into staging area');
INSERT INTO job_event_type VALUES ('ODS_LOAD', 'Loaded data into ODS Area');
INSERT INTO job_event_type VALUES ('ODS_DATA_INDEXED', 'ODS Data indexed in indexing engine');


--
-- Data for Name: job_schedule_triggers; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO job_schedule_triggers VALUES (29, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'PRICECHANGE', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"inbound_message_collector": 27}', true);
INSERT INTO job_schedule_triggers VALUES (15, 'OUTBOUND_MESSAGE_DISTRIBUTOR', 'OUTBOUND_TRANSFER', 'SYSTEM', '0/5 * * * * ?', 'DISABLED', '2017-04-11 14:24:02.296', NULL, true);
INSERT INTO job_schedule_triggers VALUES (10, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'ORDERCREATE', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:19:18.052', '{"inbound_message_collector": 8}', true);
INSERT INTO job_schedule_triggers VALUES (5, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'ORDERDELIVERYREPORT', '0/15 * * * * ?', 'DISABLED', '2017-04-07 10:53:43.307', '{"inbound_message_collector": 3}', true);
INSERT INTO job_schedule_triggers VALUES (9, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'ORDERDELIVERYREPORTECOM', '0/15 * * * * ?', 'DISABLED', '2017-04-07 10:53:43.307', '{"inbound_message_collector": 28}', true);
INSERT INTO job_schedule_triggers VALUES (11, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'PARTSYNCEXTRA', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"inbound_message_collector": 9}', true);
INSERT INTO job_schedule_triggers VALUES (12, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'PURCHASEORDERLINERECEIVE', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"inbound_message_collector": 10}', true);
INSERT INTO job_schedule_triggers VALUES (22, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'ARTICLE', '0/15 * * * * ?', 'DISABLED', '2017-04-25 14:17:18.563', '{"inbound_message_collector": 23}', true);
INSERT INTO job_schedule_triggers VALUES (27, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'ARTICLEHIERARCHY', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"inbound_message_collector": 24}', true);
INSERT INTO job_schedule_triggers VALUES (31, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'CLEARANCE', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"inbound_message_collector": 29}', true);
INSERT INTO job_schedule_triggers VALUES (13, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'PURCHASEORDERLINERECEIVEREPORT', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"inbound_message_collector": 11}', true);
INSERT INTO job_schedule_triggers VALUES (18, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'SHOPORDER', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"inbound_message_collector": 15}', true);
INSERT INTO job_schedule_triggers VALUES (14, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'APEXDISPATCHSYNC', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"inbound_message_collector": 12}', true);
INSERT INTO job_schedule_triggers VALUES (3, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'BALANCELIST', '0/15 * * * * ?', 'DISABLED', '2017-04-04 09:51:32.676', '{"inbound_message_collector": 1}', true);
INSERT INTO job_schedule_triggers VALUES (30, 'STORE_FILE_DISTRIBUTOR', 'OUTBOUND_TRANSFER', 'SYSTEM', '0/15 * * * * ?', 'DISABLED', '2017-04-28 13:41:35.403', NULL, false);
INSERT INTO job_schedule_triggers VALUES (6, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'BALANCELISTALL', '0/15 * * * * ?', 'DISABLED', '2017-04-07 11:02:17.773', '{"inbound_message_collector": 4}', true);
INSERT INTO job_schedule_triggers VALUES (21, 'STORE_FILE_COLLECTOR', 'INBOUND_TRANSFER', 'POSLOG', '0/10 * * * * ?', 'ACTIVE', '2017-04-11 10:46:37.34', '{"filename_pattern": "POSLog.*" }', false);
INSERT INTO job_schedule_triggers VALUES (7, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'BALANCELISTECOM', '0/15 * * * * ?', 'DISABLED', '2017-04-07 11:02:17.773', '{"inbound_message_collector": 5}', true);
INSERT INTO job_schedule_triggers VALUES (4, 'INBOUND_MESSAGE_COLLECTOR', 'INBOUND_TRANSFER', 'DISPATCHSYNC', '0/15 * * * * ?', 'DISABLED', '2017-04-04 14:51:10.042', '{"inbound_message_collector": 2}', true);
INSERT INTO job_schedule_triggers VALUES (23, 'STORE_FILE_COLLECTOR', 'INBOUND_TRANSFER', 'GOODSRECEIPT', '0/5 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"filename_pattern": "HQ.*?\\.06\\..*" }', false);
INSERT INTO job_schedule_triggers VALUES (24, 'STORE_FILE_COLLECTOR', 'INBOUND_TRANSFER', 'STORESTOCKCOUNT', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"filename_pattern": "HQ.*?\\.08\\..*" }', false);
INSERT INTO job_schedule_triggers VALUES (25, 'STORE_FILE_COLLECTOR', 'INBOUND_TRANSFER', 'SHOPORDER', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"filename_pattern": "HQ.*?\\.07\\..*" }', false);
INSERT INTO job_schedule_triggers VALUES (26, 'STORE_FILE_COLLECTOR', 'INBOUND_TRANSFER', 'PMLOG', '0/15 * * * * ?', 'DISABLED', '2017-04-11 10:46:37.34', '{"filename_pattern": "PMLog.*" }', false);


--
-- Data for Name: job_status; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO job_status VALUES ('ACTIVE');
INSERT INTO job_status VALUES ('DISABLED');
INSERT INTO job_status VALUES ('PAUSED');
INSERT INTO job_status VALUES ('RUNNING');


--
-- Data for Name: message_annotation_lookup; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO message_annotation_lookup VALUES ('STORE.ID', 'select store.id k, store.id v from store, store_server rs where store.server=rs.id and store.active=TRUE');
INSERT INTO message_annotation_lookup VALUES ('STORE.ADDRESS', 'select distinct store.id k, ss.address v from store, store_server ss where store.server=ss.id and store.active=TRUE');


--
-- Data for Name: message_metadata; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Data for Name: outbound_distribution; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Data for Name: outbound_file_staging; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Name: outbound_file_staging_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('outbound_file_staging_id_seq', 9972708, true);


--
-- Data for Name: outbound_message_distributor; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO outbound_message_distributor VALUES (1, 4, 'BALANCELIST', true, 1, false);
INSERT INTO outbound_message_distributor VALUES (2, 5, 'DISPATCHSYNC', true, 1, false);
INSERT INTO outbound_message_distributor VALUES (3, 6, 'ORDERDELIVERYREPORT', true, 1, false);
INSERT INTO outbound_message_distributor VALUES (4, 7, 'BALANCELIST', false, 1, false);
INSERT INTO outbound_message_distributor VALUES (5, 9, 'POSLOG', true, 1, false);
INSERT INTO outbound_message_distributor VALUES (7, 0, 'ARTICLE', true, 0, true);


--
-- Name: outbound_message_distributor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('outbound_message_distributor_id_seq', 1, false);


--
-- Data for Name: outbound_store_distribution; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Data for Name: process_data_trace; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Name: process_data_trace_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('process_data_trace_id_seq', 30121247, true);


--
-- Data for Name: process_error; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Name: process_errors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('process_errors_id_seq', 103467, true);


--
-- Data for Name: process_status; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Data for Name: process_status_history; Type: TABLE DATA; Schema: public; Owner: indiska
--



--
-- Name: process_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('process_status_id_seq', 6453106, true);


--
-- Data for Name: remote_location; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO remote_location VALUES (1, '/epix/out/BalanceList', 'Epix BalanceList for test', 1);
INSERT INTO remote_location VALUES (3, '/epix/out/all', 'A directory for test purposes for loading files in bulk', 1);
INSERT INTO remote_location VALUES (4, '/oracle/inbound/balancelist', 'Target directory for balancelists to oracle', 2);
INSERT INTO remote_location VALUES (5, '/oracle/inbound/dispatchsync', 'Target directory for dispatch syncs to oracle', 2);
INSERT INTO remote_location VALUES (6, '/oracle/inbound/orderdeliveryreport', 'Target directory for orderdeliveryreports to oracle', 2);
INSERT INTO remote_location VALUES (7, '/oracle/inbound/zipped', 'Test target for zipped files', 2);
INSERT INTO remote_location VALUES (9, '/oracle/inbound/poslogs', 'Target directory for poslogs', 2);
INSERT INTO remote_location VALUES (10, '/crm/inbound/poslogs', 'Target directory for CRM files', 4);
INSERT INTO remote_location VALUES (11, '/ExtPgm/Trans/ToHQ', 'Source directory for POS files', 3);
INSERT INTO remote_location VALUES (8, '/shop/outbound', 'Test source for POSLogs', 5);
INSERT INTO remote_location VALUES (13, '/shop/inbound/', 'Destination folder for files to shops', 5);
INSERT INTO remote_location VALUES (14, '/oracle/outbound/all', 'Outgoing files from Oracle', 2);
INSERT INTO remote_location VALUES (15, '/shop/inbound/1', 'Destination folder for store 1', 5);
INSERT INTO remote_location VALUES (16, '/shop/inbound/2', 'Destination folder for store 2', 5);
INSERT INTO remote_location VALUES (0, '/shop/inbound/', 'Remote location for stores', 5);
INSERT INTO remote_location VALUES (12, '/oracle/outbound/{lookup:STORE.ID}', 'Files from Oracle to Stores', 2);
INSERT INTO remote_location VALUES (17, '/oracle/outbound/', 'Files from Oracle to Stores (for testing, single dir for alls tores)', 2);
INSERT INTO remote_location VALUES (18, '/epix/out/BalanceListEcom', 'Epix out BalanceLists for Ecom', 1);
INSERT INTO remote_location VALUES (20, '/epix/out/OrderDeliveryReportEcom', 'Order delivery reports for ecom', 1);
INSERT INTO remote_location VALUES (2, '/out/DispatchSync', 'Epix DispatchSync', 1);
INSERT INTO remote_location VALUES (19, '/out/OrderDeliveryReport', 'Order delivery reports', 1);


--
-- Name: remote_location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('remote_location_id_seq', 1, false);


--
-- Data for Name: remote_server; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO remote_server VALUES (20, 'Skellefteå POS Backoffice', 'administrator', '172.30.56.30', 'flipper', 3, 'smb');
INSERT INTO remote_server VALUES (3, 'Localhost Test Shop', 'sharing', 'localhost', 'sharing', 5, 'ftp');
INSERT INTO remote_server VALUES (21, 'Falun POS Backoffice', 'administrator', '172.30.17.30', 'flipper', 3, 'smb');
INSERT INTO remote_server VALUES (4, 'Localhost Test Shop 2', '', 'STORE_ADDRESS', NULL, 5, NULL);
INSERT INTO remote_server VALUES (2, 'Localhost Oracle', 'sharing', 'localhost', 'sharing', 2, 'file');
INSERT INTO remote_server VALUES (1, 'Localhost Epix', 'sharing', 'localhost', 'sharing', 1, 'file');
INSERT INTO remote_server VALUES (0, 'Placeholder Remote Server for Extenda Stores', 'sharing', '{STORE.ADDRESS}', 'sharing', 5, 'file');


--
-- Data for Name: remote_server_group; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO remote_server_group VALUES (1, 'Epix');
INSERT INTO remote_server_group VALUES (2, 'Oracle');
INSERT INTO remote_server_group VALUES (3, 'Store');
INSERT INTO remote_server_group VALUES (4, 'Abalon');
INSERT INTO remote_server_group VALUES (5, 'Localhost Test Store');


--
-- Name: remote_server_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('remote_server_group_id_seq', 1, false);


--
-- Name: remote_server_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('remote_server_id_seq', 1, false);


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO settings VALUES ('websocket_file_print', 'TRUE');
INSERT INTO settings VALUES ('paused', 'FALSE');


--
-- Data for Name: store; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO store VALUES (82, 'Valkeå Uleåborg', '172.30.82.30', 'FI', 'Europe/Helsinki', true, 82222, 45, 82111);
INSERT INTO store VALUES (4100, 'Iceland', '172.30.100.30', 'IS', 'Atlantic/Reykjavik', true, 4100222, 1, 4100111);
INSERT INTO store VALUES (5101, 'Hamburger Meile', '172.30.151.30', 'DE', 'Europe/Berlin', true, 5101222, 3, 5101111);
INSERT INTO store VALUES (567, 'Nyköping', '172.30.167.30', 'SE', 'Europe/Stockholm', true, 567222, 6, 567111);
INSERT INTO store VALUES (569, 'Gävle', '172.30.169.30', 'SE', 'Europe/Stockholm', true, 569222, 7, 569111);
INSERT INTO store VALUES (577, 'Valbo', '172.30.177.30', 'SE', 'Europe/Stockholm', true, 577222, 11, 577111);
INSERT INTO store VALUES (579, 'Samarkand, Växjö', '172.30.179.30', 'SE', 'Europe/Stockholm', true, 579222, 12, 579111);
INSERT INTO store VALUES (402, 'Ski', '172.30.202.30', 'NO', 'Europe/Oslo', true, 402222, 17, 402111);
INSERT INTO store VALUES (403, 'Storgate', '172.30.203.30', 'NO', 'Europe/Oslo', true, 403222, 17, 403111);
INSERT INTO store VALUES (404, 'Strömmen', '172.30.204.30', 'NO', 'Europe/Oslo', true, 404222, 18, 404111);
INSERT INTO store VALUES (405, 'Grönland Bazaar', '172.30.205.30', 'NO', 'Europe/Oslo', true, 405222, 18, 405111);
INSERT INTO store VALUES (910, 'E-com EU', NULL, 'EU', 'Europe/Stockholm', false, 1, 48, 4);
INSERT INTO store VALUES (406, 'Kristiansand', '172.30.206.30', 'NO', 'Europe/Oslo', true, 406222, 19, 406111);
INSERT INTO store VALUES (407, 'Drammen', '172.30.207.30', 'NO', 'Europe/Oslo', true, 407222, 19, 407111);
INSERT INTO store VALUES (410, 'Trondheim', '172.30.210.30', 'NO', 'Europe/Oslo', true, 410222, 20, 410111);
INSERT INTO store VALUES (412, 'Bergen', '172.30.212.30', 'NO', 'Europe/Oslo', true, 412222, 21, 412111);
INSERT INTO store VALUES (416, 'Gjövik', '172.30.216.30', 'NO', 'Europe/Oslo', true, 416222, 22, 416111);
INSERT INTO store VALUES (417, 'Tromsö', '172.30.217.30', 'NO', 'Europe/Oslo', true, 417222, 22, 417111);
INSERT INTO store VALUES (33, 'Fältöversten', '172.30.33.30', 'SE', 'Europe/Stockholm', true, 33222, 28, 33111);
INSERT INTO store VALUES (45, 'Söderhallarna', '172.30.45.30', 'SE', 'Europe/Stockholm', true, 45222, 33, 45111);
INSERT INTO store VALUES (5, 'Postgatan Gbg', '172.30.5.30', 'SE', 'Europe/Stockholm', true, 5222, 36, 5111);
INSERT INTO store VALUES (50, 'Kupolen', '172.30.50.30', 'SE', 'Europe/Stockholm', true, 50222, 36, 50111);
INSERT INTO store VALUES (6, 'Västerås', '172.30.6.30', 'SE', 'Europe/Stockholm', true, 6222, 39, 6111);
INSERT INTO store VALUES (78, 'Kauppakeskus Sello', '172.30.78.30', 'FI', 'Europe/Helsinki', true, 78222, 43, 78111);
INSERT INTO store VALUES (79, 'Kamppi', '172.30.79.30', 'FI', 'Europe/Helsinki', true, 79222, 43, 79111);
INSERT INTO store VALUES (901, 'E-com Norway', NULL, 'NO', 'Europe/Stockholm', true, 901222, 47, 901111);
INSERT INTO store VALUES (81, 'Mylly Åbo', '172.30.81.30', 'FI', 'Europe/Helsinki', true, 81222, 45, 81111);
INSERT INTO store VALUES (80, 'Björneborg', '172.30.80.30', 'FI', 'Europe/Helsinki', true, 80222, 44, 80111);
INSERT INTO store VALUES (902, 'E-com Finland', NULL, 'FI', 'Europe/Stockholm', true, 902222, 48, 902111);
INSERT INTO store VALUES (57, 'Överby/Trollhättan', '172.30.57.30', 'SE', 'Europe/Stockholm', true, 57222, 38, 57111);
INSERT INTO store VALUES (58, 'Nordby/Strömstad', '172.30.58.30', 'SE', 'Europe/Stockholm', true, 58222, 39, 58111);
INSERT INTO store VALUES (7, 'Karlstad', '172.30.7.30', 'SE', 'Europe/Stockholm', true, 7222, 40, 7111);
INSERT INTO store VALUES (11, 'Kungsgatan Gbg', '172.30.11.30', 'SE', 'Europe/Stockholm', true, 11222, 1, 11111);
INSERT INTO store VALUES (75, 'Tampere', '172.30.75.30', 'FI', 'Europe/Helsinki', true, 75222, 41, 75111);
INSERT INTO store VALUES (12, 'Odengatan', '172.30.12.30', 'SE', 'Europe/Stockholm', true, 12222, 2, 12111);
INSERT INTO store VALUES (900, 'E-com Sweden', NULL, 'SE', 'Europe/Stockholm', true, 900222, 47, 900111);
INSERT INTO store VALUES (13, 'Jönköping', '172.30.13.30', 'SE', 'Europe/Stockholm', true, 13222, 2, 13111);
INSERT INTO store VALUES (15, 'Luleå', '172.30.15.30', 'SE', 'Europe/Stockholm', true, 15222, 3, 15111);
INSERT INTO store VALUES (8, 'Malmö', '172.30.8.30', 'SE', 'Europe/Stockholm', true, 8222, 44, 8111);
INSERT INTO store VALUES (561, 'Nacka Forum', '172.30.161.30', 'SE', 'Europe/Stockholm', true, 561222, 4, 561111);
INSERT INTO store VALUES (9, 'Täby', '172.30.9.30', 'SE', 'Europe/Stockholm', true, 9222, 46, 9111);
INSERT INTO store VALUES (563, 'Kista', '172.30.163.30', 'SE', 'Europe/Stockholm', true, 563222, 5, 563111);
INSERT INTO store VALUES (414, 'Ålesund', NULL, 'NO', 'Europe/Oslo', false, 1, 46, 4);
INSERT INTO store VALUES (9990, 'Heintz Test Shop 1', NULL, 'SE', 'Europe/Stockholm', false, 2, 9994, 5);
INSERT INTO store VALUES (568, 'Birsta', '172.30.168.30', 'SE', 'Europe/Stockholm', true, 568222, 7, 568111);
INSERT INTO store VALUES (571, 'Gränby', '172.30.171.30', 'SE', 'Europe/Stockholm', true, 571222, 9, 571111);
INSERT INTO store VALUES (16, 'Uppsala', '172.30.16.30', 'SE', 'Europe/Stockholm', true, 16222, 4, 16111);
INSERT INTO store VALUES (564, 'Partille', '172.30.164.30', 'SE', 'Europe/Stockholm', true, 564222, 5, 564111);
INSERT INTO store VALUES (565, 'Kungsmässan', '172.30.165.30', 'SE', 'Europe/Stockholm', true, 565222, 6, 565111);
INSERT INTO store VALUES (17, 'Falun', '172.30.17.30', 'SE', 'Europe/Stockholm', true, 17222, 8, 17111);
INSERT INTO store VALUES (18, 'Drottningatan', '172.30.18.30', 'SE', 'Europe/Stockholm', true, 18222, 13, 18111);
INSERT INTO store VALUES (580, 'Varberg', '172.30.180.30', 'SE', 'Europe/Stockholm', true, 580222, 13, 580111);
INSERT INTO store VALUES (584, 'Avion', '172.30.184.30', 'SE', 'Europe/Stockholm', true, 584222, 15, 584111);
INSERT INTO store VALUES (19, 'Kristianstad', '172.30.19.30', 'SE', 'Europe/Stockholm', true, 19222, 16, 19111);
INSERT INTO store VALUES (21, 'Eskilstuna', '172.30.21.30', 'SE', 'Europe/Stockholm', true, 21222, 20, 21111);
INSERT INTO store VALUES (22, 'Östersund', '172.30.22.30', 'SE', 'Europe/Stockholm', true, 22222, 23, 22111);
INSERT INTO store VALUES (23, 'Väla centrum', '172.30.23.30', 'SE', 'Europe/Stockholm', true, 23222, 24, 23111);
INSERT INTO store VALUES (24, 'Västra Frölunda', '172.30.24.30', 'SE', 'Europe/Stockholm', true, 24222, 24, 24111);
INSERT INTO store VALUES (25, 'Kalmar', '172.30.25.30', 'SE', 'Europe/Stockholm', true, 25222, 25, 25111);
INSERT INTO store VALUES (26, 'Farsta centrum', '172.30.26.30', 'SE', 'Europe/Stockholm', true, 26222, 25, 26111);
INSERT INTO store VALUES (28, 'Umeå', '172.30.28.30', 'SE', 'Europe/Stockholm', true, 28222, 26, 28111);
INSERT INTO store VALUES (29, 'Hudiksvall', '172.30.29.30', 'SE', 'Europe/Stockholm', true, 29222, 26, 29111);
INSERT INTO store VALUES (30, 'Globen', '172.30.30.30', 'SE', 'Europe/Stockholm', true, 30222, 27, 30111);
INSERT INTO store VALUES (31, 'Piteå', '172.30.31.30', 'SE', 'Europe/Stockholm', true, 31222, 27, 31111);
INSERT INTO store VALUES (36, 'Norrköping', '172.30.36.30', 'SE', 'Europe/Stockholm', true, 36222, 29, 36111);
INSERT INTO store VALUES (48, 'Lund', '172.30.48.30', 'SE', 'Europe/Stockholm', true, 48222, 35, 48111);
INSERT INTO store VALUES (53, 'Lund Nova', '172.30.53.30', 'SE', 'Europe/Stockholm', true, 53222, 37, 53111);
INSERT INTO store VALUES (71, 'Itä Keskus Helsingfors', '172.30.71.30', 'FI', 'Europe/Helsinki', true, 71222, 40, 71111);
INSERT INTO store VALUES (73, 'Åbo Hansa', '172.30.73.30', 'FI', 'Europe/Helsinki', true, 73222, 41, 73111);
INSERT INTO store VALUES (76, 'Vasa', '172.30.76.30', 'FI', 'Europe/Helsinki', true, 76222, 42, 76111);
INSERT INTO store VALUES (77, 'Forum Helsinki', '172.30.77.30', 'FI', 'Europe/Helsinki', true, 77222, 42, 77111);
INSERT INTO store VALUES (9991, 'Heintz Test Shop 2', NULL, 'SE', 'Europe/Stockholm', false, 2, 9994, 5);
INSERT INTO store VALUES (573, 'Liljeholmstorget', '172.30.173.30', 'SE', 'Europe/Stockholm', true, 573222, 10, 573111);
INSERT INTO store VALUES (575, 'Bergvik, Karlstad', '172.30.175.30', 'SE', 'Europe/Stockholm', true, 575222, 11, 575111);
INSERT INTO store VALUES (578, 'Storknallen, Borås', '172.30.178.30', 'SE', 'Europe/Stockholm', true, 578222, 12, 578111);
INSERT INTO store VALUES (581, 'Erikslund, Västerås', '172.30.181.30', 'SE', 'Europe/Stockholm', true, 581222, 14, 581111);
INSERT INTO store VALUES (582, 'Solna', '172.30.182.30', 'SE', 'Europe/Stockholm', true, 582222, 14, 582111);
INSERT INTO store VALUES (583, 'Mall of Scandinavia', '172.30.183.30', 'SE', 'Europe/Stockholm', true, 583222, 15, 583111);
INSERT INTO store VALUES (585, 'Center Syd', '172.30.185.30', 'SE', 'Europe/Stockholm', true, 585222, 16, 585111);
INSERT INTO store VALUES (411, 'Sandvika Storsenter', '172.30.211.30', 'NO', 'Europe/Oslo', true, 411222, 21, 411111);
INSERT INTO store VALUES (418, 'Bodö', '172.30.218.30', 'NO', 'Europe/Oslo', true, 418222, 23, 418111);
INSERT INTO store VALUES (32, 'Linköping', '172.30.32.30', 'SE', 'Europe/Stockholm', true, 32222, 28, 32111);
INSERT INTO store VALUES (37, 'Västermalmsgallerian', '172.30.37.30', 'SE', 'Europe/Stockholm', true, 37222, 29, 37111);
INSERT INTO store VALUES (38, 'Skövde', '172.30.38.30', 'SE', 'Europe/Stockholm', true, 38222, 30, 38111);
INSERT INTO store VALUES (39, 'Halmstad', '172.30.39.30', 'SE', 'Europe/Stockholm', true, 39222, 30, 39111);
INSERT INTO store VALUES (42, 'Karlskrona', '172.30.42.30', 'SE', 'Europe/Stockholm', true, 42222, 32, 42111);
INSERT INTO store VALUES (43, 'Visby', '172.30.43.30', 'SE', 'Europe/Stockholm', true, 43222, 32, 43111);
INSERT INTO store VALUES (44, 'Örebro', '172.30.44.30', 'SE', 'Europe/Stockholm', true, 44222, 33, 44111);
INSERT INTO store VALUES (47, 'Borås', '172.30.47.30', 'SE', 'Europe/Stockholm', true, 47222, 34, 47111);
INSERT INTO store VALUES (49, 'Vällingby', '172.30.49.30', 'SE', 'Europe/Stockholm', true, 49222, 35, 49111);
INSERT INTO store VALUES (56, 'Skellefteå', '172.30.56.30', 'SE', 'Europe/Stockholm', true, 56222, 38, 56111);
INSERT INTO store VALUES (570, 'Marieberg', '172.30.170.30', 'SE', 'Europe/Stockholm', true, 570222, 8, 570111);
INSERT INTO store VALUES (572, 'Mobilia Malmö', '172.30.172.30', 'SE', 'Europe/Stockholm', true, 572222, 9, 572111);
INSERT INTO store VALUES (574, 'Emporia, Malmö', '172.30.174.30', 'SE', 'Europe/Stockholm', true, 574222, 10, 574111);
INSERT INTO store VALUES (4, 'Växjö', '172.30.4.30', 'SE', 'Europe/Stockholm', true, 4222, 31, 4111);
INSERT INTO store VALUES (40, 'Södertälje', '172.30.40.30', 'SE', 'Europe/Stockholm', true, 40222, 31, 40111);
INSERT INTO store VALUES (46, 'Hötorget', '172.30.46.30', 'SE', 'Europe/Stockholm', true, 46222, 34, 46111);
INSERT INTO store VALUES (51, 'Ringen', '172.30.51.30', 'SE', 'Europe/Stockholm', true, 51222, 37, 51111);


--
-- Data for Name: store_connection_details; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO store_connection_details VALUES (1, 'smb', '/ExtPgm/Trans/ToHQ');
INSERT INTO store_connection_details VALUES (900111, 'file', '/shop/inbound/900');
INSERT INTO store_connection_details VALUES (900222, 'file', '/shop/outbound/900');
INSERT INTO store_connection_details VALUES (2, 'ftp', '/shop/outbound/1');
INSERT INTO store_connection_details VALUES (3, 'ftp', '/shop/outbound/2');
INSERT INTO store_connection_details VALUES (4, 'smb', '/ExtPgm/Trans/FromHQ');
INSERT INTO store_connection_details VALUES (5, 'ftp', '/shop/inbound/1');
INSERT INTO store_connection_details VALUES (6, 'ftp', '/shop/inbound/2');
INSERT INTO store_connection_details VALUES (17111, 'file', '/shop/inbound/17');
INSERT INTO store_connection_details VALUES (17222, 'file', '/shop/outbound/17');
INSERT INTO store_connection_details VALUES (582111, 'file', '/shop/inbound/582');
INSERT INTO store_connection_details VALUES (582222, 'file', '/shop/outbound/582');
INSERT INTO store_connection_details VALUES (42111, 'file', '/shop/inbound/42');
INSERT INTO store_connection_details VALUES (42222, 'file', '/shop/outbound/42');
INSERT INTO store_connection_details VALUES (47111, 'file', '/shop/inbound/47');
INSERT INTO store_connection_details VALUES (47222, 'file', '/shop/outbound/47');
INSERT INTO store_connection_details VALUES (57111, 'file', '/shop/inbound/57');
INSERT INTO store_connection_details VALUES (57222, 'file', '/shop/outbound/57');
INSERT INTO store_connection_details VALUES (32111, 'file', '/shop/inbound/32');
INSERT INTO store_connection_details VALUES (32222, 'file', '/shop/outbound/32');
INSERT INTO store_connection_details VALUES (38111, 'file', '/shop/inbound/38');
INSERT INTO store_connection_details VALUES (38222, 'file', '/shop/outbound/38');
INSERT INTO store_connection_details VALUES (5111, 'file', '/shop/inbound/5');
INSERT INTO store_connection_details VALUES (45111, 'file', '/shop/inbound/45');
INSERT INTO store_connection_details VALUES (45222, 'file', '/shop/outbound/45');
INSERT INTO store_connection_details VALUES (561111, 'file', '/shop/inbound/561');
INSERT INTO store_connection_details VALUES (561222, 'file', '/shop/outbound/561');
INSERT INTO store_connection_details VALUES (49111, 'file', '/shop/inbound/49');
INSERT INTO store_connection_details VALUES (49222, 'file', '/shop/outbound/49');
INSERT INTO store_connection_details VALUES (579111, 'file', '/shop/inbound/579');
INSERT INTO store_connection_details VALUES (579222, 'file', '/shop/outbound/579');
INSERT INTO store_connection_details VALUES (406111, 'file', '/shop/inbound/406');
INSERT INTO store_connection_details VALUES (406222, 'file', '/shop/outbound/406');
INSERT INTO store_connection_details VALUES (407111, 'file', '/shop/inbound/407');
INSERT INTO store_connection_details VALUES (407222, 'file', '/shop/outbound/407');
INSERT INTO store_connection_details VALUES (402111, 'file', '/shop/inbound/402');
INSERT INTO store_connection_details VALUES (402222, 'file', '/shop/outbound/402');
INSERT INTO store_connection_details VALUES (19111, 'file', '/shop/inbound/19');
INSERT INTO store_connection_details VALUES (19222, 'file', '/shop/outbound/19');
INSERT INTO store_connection_details VALUES (418111, 'file', '/shop/inbound/418');
INSERT INTO store_connection_details VALUES (418222, 'file', '/shop/outbound/418');
INSERT INTO store_connection_details VALUES (411111, 'file', '/shop/inbound/411');
INSERT INTO store_connection_details VALUES (411222, 'file', '/shop/outbound/411');
INSERT INTO store_connection_details VALUES (48111, 'file', '/shop/inbound/48');
INSERT INTO store_connection_details VALUES (48222, 'file', '/shop/outbound/48');
INSERT INTO store_connection_details VALUES (405111, 'file', '/shop/inbound/405');
INSERT INTO store_connection_details VALUES (405222, 'file', '/shop/outbound/405');
INSERT INTO store_connection_details VALUES (76111, 'file', '/shop/inbound/76');
INSERT INTO store_connection_details VALUES (76222, 'file', '/shop/outbound/76');
INSERT INTO store_connection_details VALUES (580111, 'file', '/shop/inbound/580');
INSERT INTO store_connection_details VALUES (580222, 'file', '/shop/outbound/580');
INSERT INTO store_connection_details VALUES (404111, 'file', '/shop/inbound/404');
INSERT INTO store_connection_details VALUES (404222, 'file', '/shop/outbound/404');
INSERT INTO store_connection_details VALUES (43111, 'file', '/shop/inbound/43');
INSERT INTO store_connection_details VALUES (43222, 'file', '/shop/outbound/43');
INSERT INTO store_connection_details VALUES (410111, 'file', '/shop/inbound/410');
INSERT INTO store_connection_details VALUES (410222, 'file', '/shop/outbound/410');
INSERT INTO store_connection_details VALUES (44111, 'file', '/shop/inbound/44');
INSERT INTO store_connection_details VALUES (44222, 'file', '/shop/outbound/44');
INSERT INTO store_connection_details VALUES (79111, 'file', '/shop/inbound/79');
INSERT INTO store_connection_details VALUES (79222, 'file', '/shop/outbound/79');
INSERT INTO store_connection_details VALUES (567111, 'file', '/shop/inbound/567');
INSERT INTO store_connection_details VALUES (567222, 'file', '/shop/outbound/567');
INSERT INTO store_connection_details VALUES (73111, 'file', '/shop/inbound/73');
INSERT INTO store_connection_details VALUES (73222, 'file', '/shop/outbound/73');
INSERT INTO store_connection_details VALUES (82111, 'file', '/shop/inbound/82');
INSERT INTO store_connection_details VALUES (82222, 'file', '/shop/outbound/82');
INSERT INTO store_connection_details VALUES (36111, 'file', '/shop/inbound/36');
INSERT INTO store_connection_details VALUES (36222, 'file', '/shop/outbound/36');
INSERT INTO store_connection_details VALUES (9111, 'file', '/shop/inbound/9');
INSERT INTO store_connection_details VALUES (9222, 'file', '/shop/outbound/9');
INSERT INTO store_connection_details VALUES (12111, 'file', '/shop/inbound/12');
INSERT INTO store_connection_details VALUES (12222, 'file', '/shop/outbound/12');
INSERT INTO store_connection_details VALUES (24111, 'file', '/shop/inbound/24');
INSERT INTO store_connection_details VALUES (24222, 'file', '/shop/outbound/24');
INSERT INTO store_connection_details VALUES (53111, 'file', '/shop/inbound/53');
INSERT INTO store_connection_details VALUES (53222, 'file', '/shop/outbound/53');
INSERT INTO store_connection_details VALUES (584111, 'file', '/shop/inbound/584');
INSERT INTO store_connection_details VALUES (584222, 'file', '/shop/outbound/584');
INSERT INTO store_connection_details VALUES (23111, 'file', '/shop/inbound/23');
INSERT INTO store_connection_details VALUES (23222, 'file', '/shop/outbound/23');
INSERT INTO store_connection_details VALUES (30111, 'file', '/shop/inbound/30');
INSERT INTO store_connection_details VALUES (30222, 'file', '/shop/outbound/30');
INSERT INTO store_connection_details VALUES (80111, 'file', '/shop/inbound/80');
INSERT INTO store_connection_details VALUES (80222, 'file', '/shop/outbound/80');
INSERT INTO store_connection_details VALUES (585111, 'file', '/shop/inbound/585');
INSERT INTO store_connection_details VALUES (585222, 'file', '/shop/outbound/585');
INSERT INTO store_connection_details VALUES (25111, 'file', '/shop/inbound/25');
INSERT INTO store_connection_details VALUES (25222, 'file', '/shop/outbound/25');
INSERT INTO store_connection_details VALUES (75111, 'file', '/shop/inbound/75');
INSERT INTO store_connection_details VALUES (75222, 'file', '/shop/outbound/75');
INSERT INTO store_connection_details VALUES (31111, 'file', '/shop/inbound/31');
INSERT INTO store_connection_details VALUES (31222, 'file', '/shop/outbound/31');
INSERT INTO store_connection_details VALUES (565111, 'file', '/shop/inbound/565');
INSERT INTO store_connection_details VALUES (565222, 'file', '/shop/outbound/565');
INSERT INTO store_connection_details VALUES (564111, 'file', '/shop/inbound/564');
INSERT INTO store_connection_details VALUES (564222, 'file', '/shop/outbound/564');
INSERT INTO store_connection_details VALUES (71111, 'file', '/shop/inbound/71');
INSERT INTO store_connection_details VALUES (71222, 'file', '/shop/outbound/71');
INSERT INTO store_connection_details VALUES (77111, 'file', '/shop/inbound/77');
INSERT INTO store_connection_details VALUES (77222, 'file', '/shop/outbound/77');
INSERT INTO store_connection_details VALUES (78111, 'file', '/shop/inbound/78');
INSERT INTO store_connection_details VALUES (78222, 'file', '/shop/outbound/78');
INSERT INTO store_connection_details VALUES (4100111, 'file', '/shop/inbound/4100');
INSERT INTO store_connection_details VALUES (4100222, 'file', '/shop/outbound/4100');
INSERT INTO store_connection_details VALUES (29111, 'file', '/shop/inbound/29');
INSERT INTO store_connection_details VALUES (29222, 'file', '/shop/outbound/29');
INSERT INTO store_connection_details VALUES (412111, 'file', '/shop/inbound/412');
INSERT INTO store_connection_details VALUES (412222, 'file', '/shop/outbound/412');
INSERT INTO store_connection_details VALUES (40111, 'file', '/shop/inbound/40');
INSERT INTO store_connection_details VALUES (40222, 'file', '/shop/outbound/40');
INSERT INTO store_connection_details VALUES (51111, 'file', '/shop/inbound/51');
INSERT INTO store_connection_details VALUES (51222, 'file', '/shop/outbound/51');
INSERT INTO store_connection_details VALUES (18111, 'file', '/shop/inbound/18');
INSERT INTO store_connection_details VALUES (18222, 'file', '/shop/outbound/18');
INSERT INTO store_connection_details VALUES (11111, 'file', '/shop/inbound/11');
INSERT INTO store_connection_details VALUES (11222, 'file', '/shop/outbound/11');
INSERT INTO store_connection_details VALUES (46111, 'file', '/shop/inbound/46');
INSERT INTO store_connection_details VALUES (46222, 'file', '/shop/outbound/46');
INSERT INTO store_connection_details VALUES (572111, 'file', '/shop/inbound/572');
INSERT INTO store_connection_details VALUES (572222, 'file', '/shop/outbound/572');
INSERT INTO store_connection_details VALUES (5101111, 'file', '/shop/inbound/5101');
INSERT INTO store_connection_details VALUES (5101222, 'file', '/shop/outbound/5101');
INSERT INTO store_connection_details VALUES (50111, 'file', '/shop/inbound/50');
INSERT INTO store_connection_details VALUES (50222, 'file', '/shop/outbound/50');
INSERT INTO store_connection_details VALUES (6111, 'file', '/shop/inbound/6');
INSERT INTO store_connection_details VALUES (6222, 'file', '/shop/outbound/6');
INSERT INTO store_connection_details VALUES (569111, 'file', '/shop/inbound/569');
INSERT INTO store_connection_details VALUES (569222, 'file', '/shop/outbound/569');
INSERT INTO store_connection_details VALUES (577111, 'file', '/shop/inbound/577');
INSERT INTO store_connection_details VALUES (577222, 'file', '/shop/outbound/577');
INSERT INTO store_connection_details VALUES (81111, 'file', '/shop/inbound/81');
INSERT INTO store_connection_details VALUES (81222, 'file', '/shop/outbound/81');
INSERT INTO store_connection_details VALUES (570111, 'file', '/shop/inbound/570');
INSERT INTO store_connection_details VALUES (570222, 'file', '/shop/outbound/570');
INSERT INTO store_connection_details VALUES (7111, 'file', '/shop/inbound/7');
INSERT INTO store_connection_details VALUES (7222, 'file', '/shop/outbound/7');
INSERT INTO store_connection_details VALUES (15111, 'file', '/shop/inbound/15');
INSERT INTO store_connection_details VALUES (15222, 'file', '/shop/outbound/15');
INSERT INTO store_connection_details VALUES (26111, 'file', '/shop/inbound/26');
INSERT INTO store_connection_details VALUES (26222, 'file', '/shop/outbound/26');
INSERT INTO store_connection_details VALUES (28111, 'file', '/shop/inbound/28');
INSERT INTO store_connection_details VALUES (28222, 'file', '/shop/outbound/28');
INSERT INTO store_connection_details VALUES (21111, 'file', '/shop/inbound/21');
INSERT INTO store_connection_details VALUES (21222, 'file', '/shop/outbound/21');
INSERT INTO store_connection_details VALUES (902111, 'file', '/shop/inbound/902');
INSERT INTO store_connection_details VALUES (902222, 'file', '/shop/outbound/902');
INSERT INTO store_connection_details VALUES (574111, 'file', '/shop/inbound/574');
INSERT INTO store_connection_details VALUES (574222, 'file', '/shop/outbound/574');
INSERT INTO store_connection_details VALUES (573111, 'file', '/shop/inbound/573');
INSERT INTO store_connection_details VALUES (573222, 'file', '/shop/outbound/573');
INSERT INTO store_connection_details VALUES (58111, 'file', '/shop/inbound/58');
INSERT INTO store_connection_details VALUES (58222, 'file', '/shop/outbound/58');
INSERT INTO store_connection_details VALUES (22111, 'file', '/shop/inbound/22');
INSERT INTO store_connection_details VALUES (22222, 'file', '/shop/outbound/22');
INSERT INTO store_connection_details VALUES (39111, 'file', '/shop/inbound/39');
INSERT INTO store_connection_details VALUES (39222, 'file', '/shop/outbound/39');
INSERT INTO store_connection_details VALUES (56111, 'file', '/shop/inbound/56');
INSERT INTO store_connection_details VALUES (56222, 'file', '/shop/outbound/56');
INSERT INTO store_connection_details VALUES (581111, 'file', '/shop/inbound/581');
INSERT INTO store_connection_details VALUES (581222, 'file', '/shop/outbound/581');
INSERT INTO store_connection_details VALUES (16111, 'file', '/shop/inbound/16');
INSERT INTO store_connection_details VALUES (16222, 'file', '/shop/outbound/16');
INSERT INTO store_connection_details VALUES (563111, 'file', '/shop/inbound/563');
INSERT INTO store_connection_details VALUES (563222, 'file', '/shop/outbound/563');
INSERT INTO store_connection_details VALUES (4111, 'file', '/shop/inbound/4');
INSERT INTO store_connection_details VALUES (4222, 'file', '/shop/outbound/4');
INSERT INTO store_connection_details VALUES (575111, 'file', '/shop/inbound/575');
INSERT INTO store_connection_details VALUES (575222, 'file', '/shop/outbound/575');
INSERT INTO store_connection_details VALUES (5222, 'file', '/shop/outbound/5');
INSERT INTO store_connection_details VALUES (8111, 'file', '/shop/inbound/8');
INSERT INTO store_connection_details VALUES (8222, 'file', '/shop/outbound/8');
INSERT INTO store_connection_details VALUES (571111, 'file', '/shop/inbound/571');
INSERT INTO store_connection_details VALUES (571222, 'file', '/shop/outbound/571');
INSERT INTO store_connection_details VALUES (417111, 'file', '/shop/inbound/417');
INSERT INTO store_connection_details VALUES (417222, 'file', '/shop/outbound/417');
INSERT INTO store_connection_details VALUES (578111, 'file', '/shop/inbound/578');
INSERT INTO store_connection_details VALUES (578222, 'file', '/shop/outbound/578');
INSERT INTO store_connection_details VALUES (568111, 'file', '/shop/inbound/568');
INSERT INTO store_connection_details VALUES (568222, 'file', '/shop/outbound/568');
INSERT INTO store_connection_details VALUES (901111, 'file', '/shop/inbound/901');
INSERT INTO store_connection_details VALUES (901222, 'file', '/shop/outbound/901');
INSERT INTO store_connection_details VALUES (583111, 'file', '/shop/inbound/583');
INSERT INTO store_connection_details VALUES (583222, 'file', '/shop/outbound/583');
INSERT INTO store_connection_details VALUES (416111, 'file', '/shop/inbound/416');
INSERT INTO store_connection_details VALUES (416222, 'file', '/shop/outbound/416');
INSERT INTO store_connection_details VALUES (403111, 'file', '/shop/inbound/403');
INSERT INTO store_connection_details VALUES (403222, 'file', '/shop/outbound/403');
INSERT INTO store_connection_details VALUES (37111, 'file', '/shop/inbound/37');
INSERT INTO store_connection_details VALUES (37222, 'file', '/shop/outbound/37');
INSERT INTO store_connection_details VALUES (13111, 'file', '/shop/inbound/13');
INSERT INTO store_connection_details VALUES (13222, 'file', '/shop/outbound/13');
INSERT INTO store_connection_details VALUES (33111, 'file', '/shop/inbound/33');
INSERT INTO store_connection_details VALUES (33222, 'file', '/shop/outbound/33');


--
-- Name: store_connection_details_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('store_connection_details_id_seq', 1, false);


--
-- Data for Name: store_outbound_message_distributor; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO store_outbound_message_distributor VALUES (2, 'PRICECHANGE', 'HQ.*?\.03\.0*(\d+)\..*$');
INSERT INTO store_outbound_message_distributor VALUES (1, 'ARTICLE', 'HQ.*?\.01\.0*(\d+)\..*$');
INSERT INTO store_outbound_message_distributor VALUES (3, 'ARTICLEHIERARCHY', 'HQ.*?\.02\.0*(\d+)\..*$');


--
-- Name: store_outbound_message_distributor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('store_outbound_message_distributor_id_seq', 1, false);


--
-- Data for Name: store_server; Type: TABLE DATA; Schema: public; Owner: indiska
--

INSERT INTO store_server VALUES (1, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (2, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (3, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (4, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (5, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (6, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (7, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (8, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (9, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (10, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (11, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (12, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (13, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (14, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (15, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (16, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (17, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (18, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (19, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (20, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (21, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (22, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (23, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (24, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (25, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (26, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (27, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (28, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (29, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (30, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (31, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (32, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (33, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (34, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (35, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (36, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (37, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (38, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (39, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (40, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (41, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (42, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (43, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (44, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (45, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (46, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (47, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (48, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (49, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (50, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (51, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (52, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (53, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (54, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (55, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (56, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (57, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (58, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (59, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (60, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (61, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (62, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (63, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (64, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (65, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (66, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (67, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (68, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (69, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (70, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (71, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (72, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (73, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (74, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (75, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (76, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (77, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (78, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (79, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (80, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (81, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (82, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (83, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (84, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (85, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (86, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (87, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (88, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (89, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (90, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (91, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (92, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (9993, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (9994, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (93, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (94, 'localhost', 'sharing', 'sharing');
INSERT INTO store_server VALUES (95, 'localhost', 'sharing', 'sharing');


--
-- Name: store_server_id_seq; Type: SEQUENCE SET; Schema: public; Owner: indiska
--

SELECT pg_catalog.setval('store_server_id_seq', 92, true);


SET search_path = stage, pg_catalog;

--
-- Data for Name: currentcampaign; Type: TABLE DATA; Schema: stage; Owner: indiska
--



--
-- Data for Name: pos_transaction; Type: TABLE DATA; Schema: stage; Owner: indiska
--



--
-- Data for Name: pos_transaction_sales; Type: TABLE DATA; Schema: stage; Owner: indiska
--



--
-- Data for Name: transaction_025; Type: TABLE DATA; Schema: stage; Owner: indiska
--



--
-- Data for Name: transaction_026; Type: TABLE DATA; Schema: stage; Owner: indiska
--



--
-- Data for Name: transaction_040; Type: TABLE DATA; Schema: stage; Owner: indiska
--



--
-- Data for Name: transaction_132; Type: TABLE DATA; Schema: stage; Owner: indiska
--



SET search_path = public, pg_catalog;

--
-- Name: alert_acknowledgement alert_acknowledgement_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert_acknowledgement
    ADD CONSTRAINT alert_acknowledgement_pkey PRIMARY KEY (id);


--
-- Name: alert alert_id_key; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_id_key UNIQUE (id);


--
-- Name: alert alert_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_pkey PRIMARY KEY (event_step, data_type, process_id, process_data_trace_id, acknowledge_id);


--
-- Name: balancelist balancelist_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY balancelist
    ADD CONSTRAINT balancelist_pkey PRIMARY KEY (item, date);


--
-- Name: data_type data_type_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY data_type
    ADD CONSTRAINT data_type_pkey PRIMARY KEY (id);


--
-- Name: file_type_identifier file_type_identifier_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY file_type_identifier
    ADD CONSTRAINT file_type_identifier_pkey PRIMARY KEY (data_type);


--
-- Name: inbound_message_collector inbound_file_retrieval_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY inbound_message_collector
    ADD CONSTRAINT inbound_file_retrieval_pkey PRIMARY KEY (id);


--
-- Name: inbound_file_staging inbound_file_staging_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY inbound_file_staging
    ADD CONSTRAINT inbound_file_staging_pkey PRIMARY KEY (id);


--
-- Name: job_event_triggers job_event_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_event_triggers
    ADD CONSTRAINT job_event_triggers_pkey PRIMARY KEY (id);


--
-- Name: job_event_type job_event_type_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_event_type
    ADD CONSTRAINT job_event_type_pkey PRIMARY KEY (id);


--
-- Name: job job_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_pkey PRIMARY KEY (id);


--
-- Name: job_schedule_triggers job_schedule_triggers_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_schedule_triggers
    ADD CONSTRAINT job_schedule_triggers_pkey PRIMARY KEY (id);


--
-- Name: job_status job_status_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_status
    ADD CONSTRAINT job_status_pkey PRIMARY KEY (id);


--
-- Name: message_annotation_lookup message_annotation_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY message_annotation_lookup
    ADD CONSTRAINT message_annotation_pkey PRIMARY KEY (id);


--
-- Name: message_metadata message_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY message_metadata
    ADD CONSTRAINT message_metadata_pkey PRIMARY KEY (message_id, key);


--
-- Name: outbound_distribution outbound_distribution_outbound_message_id_outbound_message_dist; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_distribution
    ADD CONSTRAINT outbound_distribution_outbound_message_id_outbound_message_dist PRIMARY KEY (outbound_message_id, outbound_message_distributor_id);


--
-- Name: outbound_file_staging outbound_file_staging__uk; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_file_staging
    ADD CONSTRAINT outbound_file_staging__uk UNIQUE (filename, file_type);


--
-- Name: outbound_file_staging outbound_file_staging_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_file_staging
    ADD CONSTRAINT outbound_file_staging_pkey PRIMARY KEY (id);


--
-- Name: outbound_message_distributor outbound_message_distributor_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_message_distributor
    ADD CONSTRAINT outbound_message_distributor_pkey PRIMARY KEY (id);


--
-- Name: outbound_store_distribution outbound_store_distribution_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_store_distribution
    ADD CONSTRAINT outbound_store_distribution_pkey PRIMARY KEY (outbound_message_id, store_id);


--
-- Name: process_data_trace process_data_trace_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_data_trace
    ADD CONSTRAINT process_data_trace_pkey PRIMARY KEY (id);


--
-- Name: process_error process_errors_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_error
    ADD CONSTRAINT process_errors_pkey PRIMARY KEY (id);


--
-- Name: process_status process_status_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_status
    ADD CONSTRAINT process_status_pkey PRIMARY KEY (id);


--
-- Name: remote_location remote_location_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY remote_location
    ADD CONSTRAINT remote_location_pkey PRIMARY KEY (id);


--
-- Name: remote_server_group remote_server_group_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY remote_server_group
    ADD CONSTRAINT remote_server_group_pkey PRIMARY KEY (id);


--
-- Name: remote_server remote_server_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY remote_server
    ADD CONSTRAINT remote_server_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (key);


--
-- Name: store_connection_details store_connection_details_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY store_connection_details
    ADD CONSTRAINT store_connection_details_pkey PRIMARY KEY (id);


--
-- Name: store_outbound_message_distributor store_outbound_message_distributor_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY store_outbound_message_distributor
    ADD CONSTRAINT store_outbound_message_distributor_pkey PRIMARY KEY (id);


--
-- Name: store store_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY store
    ADD CONSTRAINT store_pkey PRIMARY KEY (id);


--
-- Name: store_server store_server_pkey; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY store_server
    ADD CONSTRAINT store_server_pkey PRIMARY KEY (id);


--
-- Name: inbound_file_staging type_filename_checksum_uniq; Type: CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY inbound_file_staging
    ADD CONSTRAINT type_filename_checksum_uniq UNIQUE (file_type, filename, checksum);


SET search_path = stage, pg_catalog;

--
-- Name: pos_transaction pos_transaction_pkey; Type: CONSTRAINT; Schema: stage; Owner: indiska
--

ALTER TABLE ONLY pos_transaction
    ADD CONSTRAINT pos_transaction_pkey PRIMARY KEY (transaction_id);


--
-- Name: pos_transaction_sales pos_transaction_sales_pkey; Type: CONSTRAINT; Schema: stage; Owner: indiska
--

ALTER TABLE ONLY pos_transaction_sales
    ADD CONSTRAINT pos_transaction_sales_pkey PRIMARY KEY (transaction_id, sequence_number);


--
-- Name: transaction_025 transaction_025_pkey; Type: CONSTRAINT; Schema: stage; Owner: indiska
--

ALTER TABLE ONLY transaction_025
    ADD CONSTRAINT transaction_025_pkey PRIMARY KEY (store_id, commodity, artgroup, changedate);


--
-- Name: transaction_026 transaction_026_pkey; Type: CONSTRAINT; Schema: stage; Owner: indiska
--

ALTER TABLE ONLY transaction_026
    ADD CONSTRAINT transaction_026_pkey PRIMARY KEY (store_id, article, commodity, colour);


--
-- Name: transaction_040 transaction_040_pkey; Type: CONSTRAINT; Schema: stage; Owner: indiska
--

ALTER TABLE ONLY transaction_040
    ADD CONSTRAINT transaction_040_pkey PRIMARY KEY (store_id, article);


SET search_path = public, pg_catalog;

--
-- Name: inbound_file_staging_message_id_uindex; Type: INDEX; Schema: public; Owner: indiska
--

CREATE UNIQUE INDEX inbound_file_staging_message_id_uindex ON inbound_file_staging USING btree (message_id);


--
-- Name: outbound_distribution_delivered_index; Type: INDEX; Schema: public; Owner: indiska
--

CREATE INDEX outbound_distribution_delivered_index ON outbound_distribution USING btree (delivered);


--
-- Name: outbound_distribution_outbound_distributor_id_index; Type: INDEX; Schema: public; Owner: indiska
--

CREATE INDEX outbound_distribution_outbound_distributor_id_index ON outbound_distribution USING btree (outbound_message_distributor_id);


--
-- Name: outbound_message_distributor_immidiate_delivery_index; Type: INDEX; Schema: public; Owner: indiska
--

CREATE INDEX outbound_message_distributor_immidiate_delivery_index ON outbound_message_distributor USING btree (immediate_delivery);


--
-- Name: process_data_trace_status_index; Type: INDEX; Schema: public; Owner: indiska
--

CREATE INDEX process_data_trace_status_index ON process_data_trace USING btree (status);


--
-- Name: process_error_process_data_trace_id_index; Type: INDEX; Schema: public; Owner: indiska
--

CREATE INDEX process_error_process_data_trace_id_index ON process_error USING btree (process_data_trace_id);


--
-- Name: process_status_event_step_index; Type: INDEX; Schema: public; Owner: indiska
--

CREATE INDEX process_status_event_step_index ON process_status USING btree (job_id);


--
-- Name: process_status_process_initiator_id_index; Type: INDEX; Schema: public; Owner: indiska
--

CREATE INDEX process_status_process_initiator_id_index ON process_status USING btree (process_initiator_id);


--
-- Name: process_status_status_index; Type: INDEX; Schema: public; Owner: indiska
--

CREATE INDEX process_status_status_index ON process_status USING btree (status);


SET search_path = stage, pg_catalog;

--
-- Name: currentcampaign_campaign_index; Type: INDEX; Schema: stage; Owner: indiska
--

CREATE INDEX currentcampaign_campaign_index ON store_currentcampaign USING btree (campaign);


--
-- Name: currentcampaign_fromdate_todate_index; Type: INDEX; Schema: stage; Owner: indiska
--

CREATE INDEX currentcampaign_fromdate_todate_index ON store_currentcampaign USING btree (fromdate, todate);


--
-- Name: currentcampaign_item_index; Type: INDEX; Schema: stage; Owner: indiska
--

CREATE INDEX currentcampaign_item_index ON store_currentcampaign USING btree (item);


--
-- Name: currentcampaign_store_index; Type: INDEX; Schema: stage; Owner: indiska
--

CREATE INDEX currentcampaign_store_index ON store_currentcampaign USING btree (store);


--
-- Name: transaction_025_store_id_index; Type: INDEX; Schema: stage; Owner: indiska
--

CREATE INDEX transaction_025_store_id_index ON transaction_025 USING btree (store_id);


--
-- Name: transaction_026_store_id_index; Type: INDEX; Schema: stage; Owner: indiska
--

CREATE INDEX transaction_026_store_id_index ON transaction_026 USING btree (store_id);


--
-- Name: transaction_040_store_id_index; Type: INDEX; Schema: stage; Owner: indiska
--

CREATE INDEX transaction_040_store_id_index ON transaction_040 USING btree (store_id);


--
-- Name: transaction_132_store_id_index; Type: INDEX; Schema: stage; Owner: indiska
--

CREATE INDEX transaction_132_store_id_index ON transaction_132 USING btree (store_id);


SET search_path = public, pg_catalog;

--
-- Name: outbound_file_staging outbound_distribution_update; Type: TRIGGER; Schema: public; Owner: indiska
--

CREATE TRIGGER outbound_distribution_update AFTER INSERT OR UPDATE ON outbound_file_staging FOR EACH ROW EXECUTE PROCEDURE outbound_distribution_update();


--
-- Name: alert alert_acknowledge_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_acknowledge_id_fkey FOREIGN KEY (acknowledge_id) REFERENCES alert_acknowledgement(id);


--
-- Name: alert alert_data_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_data_type_fkey FOREIGN KEY (data_type) REFERENCES data_type(id);


--
-- Name: alert alert_event_step_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_event_step_fkey FOREIGN KEY (event_step) REFERENCES job_event_type(id);


--
-- Name: alert alert_process_data_trace_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_process_data_trace_id_fkey FOREIGN KEY (process_data_trace_id) REFERENCES process_data_trace(id);


--
-- Name: alert alert_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY alert
    ADD CONSTRAINT alert_process_id_fkey FOREIGN KEY (process_id) REFERENCES process_status(id);


--
-- Name: file_type_identifier file_type_identifier_data_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY file_type_identifier
    ADD CONSTRAINT file_type_identifier_data_type_fkey FOREIGN KEY (data_type) REFERENCES data_type(id);


--
-- Name: inbound_message_collector inbound_file_retrieval_remote_location_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY inbound_message_collector
    ADD CONSTRAINT inbound_file_retrieval_remote_location_fkey FOREIGN KEY (remote_location) REFERENCES remote_location(id);


--
-- Name: inbound_message_collector inbound_message_collector_remote_server_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY inbound_message_collector
    ADD CONSTRAINT inbound_message_collector_remote_server_fk FOREIGN KEY (remote_server) REFERENCES remote_server(id);


--
-- Name: job_event_triggers job_event_triggers_data_type_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_event_triggers
    ADD CONSTRAINT job_event_triggers_data_type_fk FOREIGN KEY (data_type) REFERENCES data_type(id);


--
-- Name: job_event_triggers job_event_triggers_event_step_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_event_triggers
    ADD CONSTRAINT job_event_triggers_event_step_fkey FOREIGN KEY (event_step) REFERENCES job_event_type(id);


--
-- Name: job_event_triggers job_event_triggers_job_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_event_triggers
    ADD CONSTRAINT job_event_triggers_job_id_fkey FOREIGN KEY (job_id) REFERENCES job(id);


--
-- Name: job_event_triggers job_event_triggers_status_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_event_triggers
    ADD CONSTRAINT job_event_triggers_status_fk FOREIGN KEY (job_status) REFERENCES job_status(id);


--
-- Name: job job_event_type_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_event_type_fk FOREIGN KEY (event_type) REFERENCES job_event_type(id);


--
-- Name: job job_job_status_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job
    ADD CONSTRAINT job_job_status_id_fk FOREIGN KEY (job_status) REFERENCES job_status(id);


--
-- Name: job_schedule_triggers job_schedule_triggers_data_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_schedule_triggers
    ADD CONSTRAINT job_schedule_triggers_data_type_fkey FOREIGN KEY (data_type) REFERENCES data_type(id);


--
-- Name: job_schedule_triggers job_schedule_triggers_event_step_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_schedule_triggers
    ADD CONSTRAINT job_schedule_triggers_event_step_fkey FOREIGN KEY (event_step) REFERENCES job_event_type(id);


--
-- Name: job_schedule_triggers job_schedule_triggers_job_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_schedule_triggers
    ADD CONSTRAINT job_schedule_triggers_job_id_fkey FOREIGN KEY (job_id) REFERENCES job(id);


--
-- Name: job_schedule_triggers job_schedule_triggers_job_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY job_schedule_triggers
    ADD CONSTRAINT job_schedule_triggers_job_status_fkey FOREIGN KEY (job_status) REFERENCES job_status(id);


--
-- Name: outbound_distribution outbound_distribution_oubound_message_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_distribution
    ADD CONSTRAINT outbound_distribution_oubound_message_id_fkey FOREIGN KEY (outbound_message_id) REFERENCES outbound_file_staging(id);


--
-- Name: outbound_message_distributor outbound_message_distributor_data_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_message_distributor
    ADD CONSTRAINT outbound_message_distributor_data_type_fkey FOREIGN KEY (data_type) REFERENCES data_type(id);


--
-- Name: outbound_message_distributor outbound_message_distributor_remote_location_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_message_distributor
    ADD CONSTRAINT outbound_message_distributor_remote_location_fkey FOREIGN KEY (remote_location) REFERENCES remote_location(id);


--
-- Name: outbound_message_distributor outbound_message_distributor_remote_server_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_message_distributor
    ADD CONSTRAINT outbound_message_distributor_remote_server_id_fk FOREIGN KEY (remote_server) REFERENCES remote_server(id);


--
-- Name: outbound_store_distribution outbound_store_distribution_outbound_message_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_store_distribution
    ADD CONSTRAINT outbound_store_distribution_outbound_message_id_fkey FOREIGN KEY (outbound_message_id) REFERENCES outbound_file_staging(id);


--
-- Name: outbound_store_distribution outbound_store_distribution_store_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY outbound_store_distribution
    ADD CONSTRAINT outbound_store_distribution_store_id_fkey FOREIGN KEY (store_id) REFERENCES store(id);


--
-- Name: process_data_trace process_data_trace_process_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_data_trace
    ADD CONSTRAINT process_data_trace_process_id_fkey FOREIGN KEY (process_id) REFERENCES process_status(id) ON DELETE CASCADE;


--
-- Name: process_error process_error_process_status_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_error
    ADD CONSTRAINT process_error_process_status_id_fk FOREIGN KEY (process_status_id) REFERENCES process_status(id) ON DELETE CASCADE;


--
-- Name: process_status process_status_data_type_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_status
    ADD CONSTRAINT process_status_data_type_fk FOREIGN KEY (data_type) REFERENCES data_type(id);


--
-- Name: process_status process_status_event_step_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_status
    ADD CONSTRAINT process_status_event_step_fk FOREIGN KEY (event_step) REFERENCES job_event_type(id);


--
-- Name: process_status_history process_status_history_process_status_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_status_history
    ADD CONSTRAINT process_status_history_process_status_id_fk FOREIGN KEY (process_status_id) REFERENCES process_status(id) ON DELETE CASCADE;


--
-- Name: process_status process_status_job_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_status
    ADD CONSTRAINT process_status_job_id_fk FOREIGN KEY (job_id) REFERENCES job(id);


--
-- Name: process_status process_status_process_initiator_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY process_status
    ADD CONSTRAINT process_status_process_initiator_id_fkey FOREIGN KEY (process_initiator_id) REFERENCES process_status(id);


--
-- Name: remote_location remote_location_remote_server_group_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY remote_location
    ADD CONSTRAINT remote_location_remote_server_group_id_fk FOREIGN KEY (group_id) REFERENCES remote_server_group(id);


--
-- Name: remote_server remote_server_remote_server_group_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY remote_server
    ADD CONSTRAINT remote_server_remote_server_group_id_fk FOREIGN KEY (group_id) REFERENCES remote_server_group(id);


--
-- Name: store_outbound_message_distributor store_outbound_message_distributor_data_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: indiska
--

ALTER TABLE ONLY store_outbound_message_distributor
    ADD CONSTRAINT store_outbound_message_distributor_data_type_fkey FOREIGN KEY (data_type) REFERENCES data_type(id);


--
-- PostgreSQL database dump complete
--

