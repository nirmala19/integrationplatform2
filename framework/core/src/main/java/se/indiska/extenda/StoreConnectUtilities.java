package se.indiska.extenda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Utility connection class for getting stores and getting database connections to store backoffice databases.
 * Created by heintz on 2017-08-18.
 */
public class StoreConnectUtilities {
    private static final Logger logger = LoggerFactory.getLogger(StoreConnectUtilities.class);

    public static List<Integer> getStoreIds() {
        List<Integer> result = new ArrayList<>();
        String sql = "SELECT * FROM store s WHERE s.active=TRUE ORDER BY id";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                result.add(rset.getInt("id"));
            }
        } catch (SQLException e) {
            logger.error("Unable to fetch store ID's!", e);
        }
        return result;
    }

    public static List<Store> getStores() {
        List<Store> stores = new ArrayList<>();
        String storeSql = "SELECT * FROM store WHERE active=TRUE ORDER BY id";
        try (Connection localDbConnection = DatasourceProvider.getConnection()) {
            PreparedStatement localGetStorePs = localDbConnection.prepareStatement(storeSql);
            ResultSet localGetStoreRset = localGetStorePs.executeQuery();
            while (localGetStoreRset.next()) {
                stores.add(getStore(localGetStoreRset));
            }
        } catch (SQLException e) {
            logger.error("Unable to query store!", e);
        }
        return stores;
    }

    public static List<Store> getStores(Integer[] ids) {
        List<Store> stores = new ArrayList<>();
        String qs = "";
        Iterator<Integer> idIterator = Arrays.asList(ids).iterator();
        while (idIterator.hasNext()) {
            idIterator.next();
            qs += "?";
            if (idIterator.hasNext()) {
                qs += ",";
            }
        }
        String storeSql = "SELECT * FROM store WHERE active=TRUE AND id IN (" + qs + ") ORDER BY id";
        logger.debug("sql: "+storeSql);
        try (Connection localDbConnection = DatasourceProvider.getConnection()) {
            PreparedStatement localGetStorePs = localDbConnection.prepareStatement(storeSql);
            for (int i = 1; i <= ids.length; i++) {
                localGetStorePs.setInt(i, ids[i - 1]);
            }
            ResultSet localGetStoreRset = localGetStorePs.executeQuery();
            while (localGetStoreRset.next()) {
                stores.add(getStore(localGetStoreRset));
            }
        } catch (SQLException e) {
            logger.error("Unable to query store!", e);
        }
        return stores;
    }

    private static Store getStore(ResultSet localGetStoreRset) throws SQLException {
        return new Store(localGetStoreRset.getLong("id"),
                localGetStoreRset.getString("network_address"),
                localGetStoreRset.getString("name"),
                localGetStoreRset.getString("country_code"));
    }

    /**
     * Get the name of a store database.
     *
     * @param store The store to get the database name for.
     * @return The database name.
     */
    public static String getStoreDatabase(Store store) {
        String dbName = "" + store.id;
        while (dbName.length() < 4) {
            dbName = "0" + dbName;
        }
        dbName = "IND" + dbName;
        return dbName;
    }

    /**
     * Get a connection to a store's database. IMPORTANT: This connection is not pooled and it
     * might impact backoffice database performance if connected too frequently. Additionally,
     * there's noone doing your cleaning, meaning YOU MUST ENSURE THIS CONNECTION IS CLOSED WHEN
     * YOU'RE DONE!
     *
     * @param store The store you want a connection to.
     * @return A java.sql.Connection to the specific database.
     * @throws SQLException If unable to connect to the store database.
     */
    public static Connection getStoreConnection(Store store) throws SQLException {
        DriverManager.setLoginTimeout(5);
        return DriverManager.getConnection("jdbc:jtds:sqlserver://" + store.ip + ":1433/" + getStoreDatabase(store), "sysadm", "");
    }
}
