package se.indiska.extenda;

/**
 * A data structure to represent a Shop/Store when communicating with the store backoffices.
 * Created by heintz on 2017-08-18.
 */
public class Store {
    public String ip = null;
    public Long id = null;
    public String name = null;
    public String countryCode = null;

    public Store(Long id, String ip, String name, String countryCode) {
        this.ip = ip;
        this.id = id;
        this.name = name;
        this.countryCode = countryCode;
    }
    public Store(Long id, String ip) {
        this.ip = ip;
        this.id = id;
    }
}
