package se.indiska.extenda;

import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.ExecutorsFactory;
import se.indiska.integration.framework.util.Counter;

import java.sql.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Utility class to import campaigns from Extenda POS's to a local database table.
 * Created by heintz on 2017-06-08.
 */
public class ArticleDataImporter {
    private static final Logger logger = LoggerFactory.getLogger(ArticleDataImporter.class);

    public static void main(String[] argv) throws Exception {
        ArticleDataImporter dataImporter = new ArticleDataImporter();
        dataImporter.importArticles();
    }

    public void importArticles() throws SQLException, ClassNotFoundException, ExecutionException, InterruptedException {
        HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl("jdbc:postgresql://localhost/indiska2");
        ds.setMinimumIdle(10);
        ds.setMaximumPoolSize(50);
        ds.setUsername("indiska");
        ds.setPassword("indiska");

        Set<String> truncateLocalTable = new HashSet<>();
        truncateLocalTable.add("TRUNCATE TABLE stage.store_article");
        truncateLocalTable.add("TRUNCATE TABLE stage.store_commodity");
        truncateLocalTable.add("TRUNCATE TABLE stage.store_commoditycolor");
        truncateLocalTable.add("TRUNCATE TABLE stage.store_commodityprice");


        List<se.indiska.extenda.Store> stores = StoreConnectUtilities.getStores();

        try (Connection localDbConnection = ds.getConnection()) {
//            for (String localTruncatSql : truncateLocalTable) {
//                PreparedStatement truncatePs = localDbConnection.prepareStatement(localTruncatSql);
//                truncatePs.executeUpdate();
//                truncatePs.close();
//            }
        }

        Class.forName("net.sourceforge.jtds.jdbc.Driver");

        Set<Future> futureSet = new HashSet<>();

        Counter counter = new Counter();
        for (se.indiska.extenda.Store store : stores) {
            if (store.id == 22 || store.id == 33 || store.id == 582) {
                futureSet.add(ExecutorsFactory.getExecutorService().submit(new Runnable() {
                    @Override
                    public void run() {
                        try (Connection localDbConnection = ds.getConnection()) {
                            counter.add(importArticleInformation(store, localDbConnection).get());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }));
            }
        }

        for (Future f : futureSet) {
            f.get();
        }
        logger.debug("All completed!");
    }

    private Counter importArticleInformation(Store store, Connection localDbConnection) throws SQLException {
        Counter counter = new Counter();
        String dbName = StoreConnectUtilities.getStoreDatabase(store);

        String insertArticle = "INSERT INTO stage.store_article (store_id, item, commodity, color, size, description) VALUES (?,?,?,?,?,?) ON CONFLICT DO NOTHING ";
        PreparedStatement insertArticlePs = localDbConnection.prepareStatement(insertArticle);
        String insertCommodity = "INSERT INTO stage.store_commodity (store_id, commodity, artgroup, receipttext) VALUES (?,?,?,?)  ON CONFLICT DO NOTHING";
        PreparedStatement insertCommodityPs = localDbConnection.prepareStatement(insertCommodity);
        String insertCommodityColor = "INSERT INTO stage.store_commoditycolor (store_id, commodity, color, salespriced) VALUES (?,?,?,?)  ON CONFLICT DO NOTHING";
        PreparedStatement insertCommodityColorPs = localDbConnection.prepareStatement(insertCommodityColor);
        String insertCommodityPrice = "INSERT INTO stage.store_commodityprice (store_id, commodity, color, salesprice) VALUES (?,?,?,?)  ON CONFLICT DO NOTHING";
        PreparedStatement insertCommodityPricePs = localDbConnection.prepareStatement(insertCommodityPrice);
//
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:jtds:sqlserver://" + store.ip + ":1433/" + dbName, "sysadm", "");
            String sql = "SELECT article, commodity, colour, size, description FROM article";
            PreparedStatement remotePs = connection.prepareStatement(sql);

            logger.debug("Getting article");
            ResultSet remoteRset = remotePs.executeQuery();

            while (remoteRset.next()) {
                insertArticlePs.setLong(1, store.id);
                insertArticlePs.setLong(2, remoteRset.getLong(1));
                insertArticlePs.setLong(3, remoteRset.getLong(2));
                insertArticlePs.setLong(4, remoteRset.getLong(3));
                insertArticlePs.setLong(5, remoteRset.getLong(4));
                insertArticlePs.setString(6, remoteRset.getString(5));
                insertArticlePs.addBatch();
                counter.add();
                if (counter.get() > 5000) {
                    insertArticlePs.executeBatch();
                    System.out.print(".");
                    counter.reset();
                }
            }
            remoteRset.close();
            insertArticlePs.executeBatch();
            counter.reset();
            System.out.println();
            insertArticlePs.close();

            sql = "SELECT c.COMMODITY, c.ARTGROUP, c.RECEIPTTEXT FROM COMMODITY c";
            remotePs = connection.prepareStatement(sql);

            logger.debug("Getting commodity");
            remoteRset = remotePs.executeQuery();

            while (remoteRset.next()) {
                insertCommodityPs.setLong(1, store.id);
                insertCommodityPs.setLong(2, remoteRset.getLong(1));
                insertCommodityPs.setLong(3, remoteRset.getLong(2));
                insertCommodityPs.setString(4, remoteRset.getString(3));
                insertCommodityPs.addBatch();
                counter.add();
                if (counter.get() > 2000) {
                    insertCommodityPs.executeBatch();
                    System.out.print(".");
                    counter.reset();
                }
            }
            remoteRset.close();
            insertCommodityPs.executeBatch();
            counter.reset();
            System.out.println();
            insertCommodityPs.close();

            sql = "SELECT c.COMMODITY, c.COLOUR, c.SALESPRICED FROM COMMODITYCOLOUR c";
            remotePs = connection.prepareStatement(sql);

            logger.debug("Getting commoditycolour");
            remoteRset = remotePs.executeQuery();

            while (remoteRset.next()) {
                insertCommodityColorPs.setLong(1, store.id);
                insertCommodityColorPs.setLong(2, remoteRset.getLong(1));
                insertCommodityColorPs.setLong(3, remoteRset.getLong(2));
                insertCommodityColorPs.setDouble(4, remoteRset.getDouble(3));
                insertCommodityColorPs.addBatch();
                counter.add();
                if (counter.get() > 2000) {
                    insertCommodityColorPs.executeBatch();
                    System.out.print(".");
                    counter.reset();
                }
            }
            remoteRset.close();
            insertCommodityColorPs.executeBatch();
            counter.reset();
            System.out.println();
            insertCommodityColorPs.close();

            sql = "SELECT c.COMMODITY, c.colour, c.SALESPRICE FROM COMMODITYSHOP c";
            remotePs = connection.prepareStatement(sql);

            logger.debug("Getting COMMODITYSHOP");
            remoteRset = remotePs.executeQuery();

            while (remoteRset.next()) {
                insertCommodityPricePs.setLong(1, store.id);
                insertCommodityPricePs.setLong(2, remoteRset.getLong(1));
                insertCommodityPricePs.setLong(3, remoteRset.getLong(2));
                insertCommodityPricePs.setDouble(4, remoteRset.getDouble(3));
                insertCommodityPricePs.addBatch();
                counter.add();
                if (counter.get() > 2000) {
                    insertCommodityPricePs.executeBatch();
                    System.out.print(".");
                    counter.reset();
                }
            }
            remoteRset.close();
            insertCommodityPricePs.executeBatch();
            counter.reset();
            System.out.println();
            insertCommodityPricePs.close();

        } catch (Exception e) {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            logger.error("Cannot connect to database!", e);
        } finally {
            System.out.println("Store " + store.id + " completed");
//            disconnect(session);
        }
        return counter;
    }
}
