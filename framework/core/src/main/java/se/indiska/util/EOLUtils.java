package se.indiska.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class EOLUtils {
    private static final Logger logger = LoggerFactory.getLogger(EOLUtils.class);
    /**
     * Unix-style end-of-line marker (LF)
     */
    private static final String EOL_UNIX = "\n";

    /**
     * Windows-style end-of-line marker (CRLF)
     */
    private static final String EOL_WINDOWS = "\r\n";

    /**
     * "Old Mac"-style end-of-line marker (CR)
     */
    private static final String EOL_OLD_MAC = "\r";

    /**
     * Default end-of-line marker on current system
     */
    private static final String EOL_SYSTEM_DEFAULT = System.getProperty("line.separator");

    /**
     * The support end-of-line marker modes
     */
    public static enum Mode {
        /**
         * Unix-style end-of-line marker ("\n")
         */
        LF,

        /**
         * Windows-style end-of-line marker ("\r\n")
         */
        CRLF,

        /**
         * "Old Mac"-style end-of-line marker ("\r")
         */
        CR
    }

    /**
     * The default end-of-line marker mode for the current system
     */
    public static final Mode SYSTEM_DEFAULT = (EOL_SYSTEM_DEFAULT.equals(EOL_UNIX) ? Mode.LF : (EOL_SYSTEM_DEFAULT
            .equals(EOL_WINDOWS) ? Mode.CRLF : (EOL_SYSTEM_DEFAULT.equals(EOL_OLD_MAC) ? Mode.CR : null)));

    static {
        // Just in case...
        if (SYSTEM_DEFAULT == null) {
            throw new IllegalStateException("Could not determine system default end-of-line marker");
        }
    }

    /**
     * Determines the end-of-line {@link Mode} of a text file.
     *
     * @param textContent the file content to investigate
     * @return the end-of-line {@link Mode} of the given file, or {@code null} if it could not be determined
     * @throws Exception
     */
    public static Mode determineEOL(String textContent)
            throws Exception {
        ByteArrayInputStream bais = new ByteArrayInputStream(textContent.getBytes());
        BufferedInputStream bufferIn = new BufferedInputStream(bais);
        try {
            int prev = -1;
            int ch;
            while ((ch = bufferIn.read()) != -1) {
                if (ch == '\n') {
                    if (prev == '\r') {
                        return Mode.CRLF;
                    } else {
                        return Mode.LF;
                    }
                } else if (prev == '\r') {
                    return Mode.CR;
                }
                prev = ch;
            }
            throw new Exception("Could not determine end-of-line marker mode");
        } catch (IOException ioe) {
            throw new Exception("Could not determine end-of-line marker mode", ioe);
        }
    }

    /**
     * Checks whether the given text file has Windows-style (CRLF) line endings.
     *
     * @param textContent the string content to investigate
     * @return
     * @throws Exception
     */
    public static boolean hasWindowsEOL(String textContent)
            throws Exception {
        return Mode.CRLF.equals(determineEOL(textContent));
    }

    /**
     * Checks whether the given text  has Unix-style (LF) line endings.
     *
     * @param textContent the file to investigate
     * @return
     * @throws Exception
     */
    public static boolean hasUnixEOL(String textContent)
            throws Exception {
        return Mode.LF.equals(determineEOL(textContent));
    }

    /**
     * Checks whether the given text has "Old Mac"-style (CR) line endings.
     *
     * @param textContent the string to investigate
     * @return
     * @throws Exception
     */
    public static boolean hasOldMacEOL(String textContent)
            throws Exception {
        return Mode.CR.equals(determineEOL(textContent));
    }

    /**
     * Checks whether the given text has line endings that conform to the system default mode (e.g. LF on Unix).
     *
     * @param textContent the file to investigate
     * @return
     * @throws Exception
     */
    public static boolean hasSystemDefaultEOL(String textContent)
            throws Exception {
        return SYSTEM_DEFAULT.equals(determineEOL(textContent));
    }

    /**
     * Convert the line endings in the given file to Unix-style (LF).
     *
     * @param textContent the String to process
     * @throws IOException
     */
    public static String convertToUnixEOL(String textContent, String charset)
            throws IOException {
        return convertLineEndings(textContent, EOL_UNIX, charset);
    }

    /**
     * Convert the line endings in the given file to Windows-style (CRLF).
     *
     * @param textContent the text to process
     * @throws IOException
     */
    public static String convertToWindowsEOL(String textContent, String charset)
            throws IOException {
        return convertLineEndings(textContent, EOL_WINDOWS, charset);
    }

    /**
     * Convert the line endings in the given file to "Old Mac"-style (CR).
     *
     * @param textContent the text to process
     * @throws IOException
     */
    public static String convertToOldMacEOL(String textContent, String charset)
            throws IOException {
        return convertLineEndings(textContent, EOL_OLD_MAC, charset);
    }

    /**
     * Convert the line endings in the given file to the system default mode.
     *
     * @param textContent the text to process
     * @throws IOException
     */
    public static String convertToSystemEOL(String textContent, String charset)
            throws IOException {
        return convertLineEndings(textContent, EOL_SYSTEM_DEFAULT, charset);
    }

    /**
     * Line endings conversion method.
     *
     * @param textContent the text to process
     * @param eol         the end-of-line marker to use (as a {@link String})
     * @throws IOException
     */
    private static String convertLineEndings(String textContent, String eol, String charset)
            throws IOException {
        BufferedReader bufferIn = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedWriter bufferOut = null;

        // Get a stream to read from the file un-normalized file
        ByteArrayInputStream bais = new ByteArrayInputStream(textContent.getBytes());
        DataInputStream dataIn = new DataInputStream(bais);
        bufferIn = new BufferedReader(new InputStreamReader(dataIn));

        // Get a stream to write to the normalized file
        bufferOut = new BufferedWriter(new OutputStreamWriter(baos));

        // For each line in the un-normalized file
        String line;
        while ((line = bufferIn.readLine()) != null) {
            // Write the original line plus the operating-system dependent newline
            bufferOut.write(line);
            bufferOut.write(eol); // write EOL marker
        }

        // Close buffered reader & writer:
        bufferIn.close();
        bufferOut.close();
        return baos.toString(charset);
    }

}
