package se.indiska.integration.framework;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.HikariPoolMXBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.util.Counter;

import javax.sql.DataSource;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by heintz on 2017-06-28.
 */
public class DatasourceProvider {
    private static final Logger logger = LoggerFactory.getLogger(DatasourceProvider.class);
    private static HikariDataSource dataSource;
    private static Counter counter = new Counter();
    private static final int MAX_POOL_SIZE = 50;
    private static HikariPoolMXBean mxBean = null;

    private static synchronized DataSource getDataSource() {
        if (dataSource == null) {
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl(IndioProperties.properties().getProperty("db.url"));
            config.setUsername(IndioProperties.properties().getProperty("db.user"));
            config.setPassword(IndioProperties.properties().getProperty("db.password"));
            config.setMinimumIdle(2);
            config.setMaximumPoolSize(MAX_POOL_SIZE);
//            config.setConnectionTimeout(60000);

            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            config.addDataSourceProperty("leakDetectionThreshold", "20");

            dataSource = new HikariDataSource(config);

            Method[] methods = dataSource.getClass().getMethods();
            for (Method m : methods) {
                logger.debug(" - " + m.getName());
            }
            mxBean = dataSource.getHikariPoolMXBean();

        }
        return dataSource;
    }

    public static Connection getConnection() throws SQLException {
        Connection conn = getDataSource().getConnection(); //new ConnectionWrapper(getDataSource().getConnection());
        ObjectMapper mapper = new ObjectMapper();
        if (mxBean != null) {
            if (mxBean.getActiveConnections() > (MAX_POOL_SIZE- 10)){
                logger.debug("Active connections: " + mxBean.getActiveConnections());
            }
        }
//
//        try {
//            logger.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(dataSource.getHealthCheckRegistry()));
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
        return conn;
    }

}
