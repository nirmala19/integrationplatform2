package se.indiska.integration.framework.trace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.job.ProcessError;
import se.indiska.integration.framework.job.ProcessStatus;

/**
 * Created by heintz on 06/04/17.
 */
public abstract class Trace {
    private static final Logger logger = LoggerFactory.getLogger(Trace.class);
    private Long processId;
    private ProcessStatus processStatus;
    private String error = null;
    private ProcessError processError = null;


    public Trace(Long processId, ProcessStatus processStatus) {
        this.processId = processId;
        this.processStatus = processStatus;
    }

    public Trace(Long processId, ProcessStatus processStatus, String error, ProcessError processError) {
        this.processId = processId;
        this.processStatus = processStatus;
        this.error = error;
        this.processError = processError;
    }

    public Long getProcessId() {
        return processId;
    }

    public ProcessStatus getProcessStatus() {
        return processStatus;
    }

    public String getError() {
        return error;
    }

    protected void setError(String error) {
        this.error = error;
    }

    public ProcessError getProcessError() {
        return processError;
    }

    protected void setProcessError(ProcessError processError) {
        this.processError = processError;
    }
}
