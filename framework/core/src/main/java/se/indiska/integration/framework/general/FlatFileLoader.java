package se.indiska.integration.framework.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.util.InboundFileUtilities;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * FlatFileLoader is a base abstract class which is used as a Job to parse and store
 * data from an Extenda flat file format to a stage database table.
 * <p>
 * Created by heintz on 14/03/17.
 */
public abstract class FlatFileLoader implements IndioJob {
    private static final Logger logger = LoggerFactory.getLogger(FlatFileLoader.class);
    private static Set<String> tablesCreated = new HashSet<>();
    private Map<String, LineParser> lineParserMap = new HashMap<>();


    protected FlatFileLoader() {

    }

    /**
     * Add a parser for a transaction type.
     *
     * @param transaction The transaction code (i.e. 025, 050, 040, 053, etc).
     * @param lineParser  The parser implementation for that transaction line type.
     */
    protected void addParser(String transaction, LineParser lineParser) {
        lineParserMap.put(transaction, lineParser);
    }

    /**
     * @see IndioJob
     */
    @Override
    public void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception {
        execute(traceHandle);
        callback.jobComplete(new ProcessExecutionResult(traceHandle.getId(), ProcessStatus.SUCCESS));
    }

    public void execute(ProcessTraceHandle traceHandle) throws Exception {
        String sql = "SELECT * FROM public.inbound_file_staging ifs, " +
                "public.process_data_trace pdt, " +
                "public.process_status ps\n" +
                "WHERE ps.process_initiator_id=pdt.process_id\n" +
                "AND ifs.id=cast(pdt.target_id AS BIGINT)\n" +
                "AND ps.id=?";
        Map<String, List<Map<String, Object>>> data = new LinkedHashMap<>();
        long result = 0;
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1, traceHandle.getId());


            int successFileCount = 0;

            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                BufferedReader br = new BufferedReader(InboundFileUtilities.getFileContentReader(rset.getString("file_content")));
                String line = null;
                boolean lineFound = false;
                while ((line = br.readLine()) != null) {
                    if (line.length() > 3) {
                        String transaction = line.substring(0, 3);
                        LineParser lineParser = lineParserMap.get(transaction);
                        if (lineParser != null) {
                            List<Map<String, Object>> lines = data.computeIfAbsent(transaction, k -> new ArrayList<>());
                            lines.add(lineParser.parse(line));
                            if (lines.size() >= 2500) {
                                result += insertBatch(lines);
                                lines.clear();
                            }
                        }
                    }
                }
            }
        }
        for (String transaction : data.keySet()) {
            List<Map<String, Object>> lines = data.get(transaction);
            if (lines.size() > 0) {
                result += insertBatch(lines);
                lines.clear();
            }
        }
    }

    /**
     * Creates the table(s) based on the LineParser implementation.
     */
    private void createTables() {
        List<String> sqlStmts = new ArrayList<>();
        for (String transaction : lineParserMap.keySet()) {
            LineParser lineParser = lineParserMap.get(transaction);

            sqlStmts.add(lineParser.getInputCreateTable("STAGE." + lineParser.getTableName()));
            sqlStmts.addAll(lineParser.getInputCreateIndices("STAGE." + lineParser.getTableName()));
        }


        try (Connection connection = DatasourceProvider.getConnection()) {
            for (String sql : sqlStmts) {
                PreparedStatement ps = connection.prepareStatement(sql);
                ps.executeUpdate();
                ps.close();
            }

            for (String transaction : lineParserMap.keySet()) {
                tablesCreated.add(transaction);
            }
        } catch (SQLException e) {
            logger.error("Unable to connect, query or update database", e);
        }


    }

    /**
     * Insert a batch of entries into the stage table.
     *
     * @param lines The lines parsed by the line parser
     * @return The number of entries stored or updated.
     * @throws SQLException If connection or sql statement fails.
     */
    private int insertBatch(List<Map<String, Object>> lines) throws SQLException {
        int result = 0;
        try (Connection connection = DatasourceProvider.getConnection()) {
            Map<String, Object> line = lines.get(0);
            String transactionType = line.get("TRANSACTIONTYPE").toString();
            if (!tablesCreated.contains(transactionType)) {
                createTables();
            }
            String sql = "insert into stage." + lineParserMap.get(transactionType).getTableName() + " (";

            Iterator<String> keyIterator = line.keySet().iterator();
            String params = "";
            while (keyIterator.hasNext()) {
                String key = keyIterator.next();
                sql += key;
                params += "?";
                if (keyIterator.hasNext()) {
                    sql += ", ";
                    params += ",";
                }
            }
            sql += ") VALUES (" + params + ") on conflict do nothing";

            PreparedStatement ps = connection.prepareStatement(sql);
            for (Map<String, Object> item : lines) {
                int i = 1;
                for (String key : item.keySet()) {
                    Object value = item.get(key);
                    if (value instanceof String) {
                        value = ((String) value).trim();
                        if (((String) value).length() == 0) {
                            value = null;
                        }
                    }
                    ps.setObject(i++, value);
                }
                result++;
                ps.addBatch();
            }
            ps.executeBatch();
            ps.close();
            logger.debug("inserted " + lines.size() + " items into " + lineParserMap.get(transactionType).getTableName());
        }
        return result;
    }
}
