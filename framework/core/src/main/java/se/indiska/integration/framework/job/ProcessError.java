package se.indiska.integration.framework.job;

/**
 * Created by heintz on 2017-04-20.
 */
public enum ProcessError {
    CONNECTION_FAILURE, DATABASE_PROBLEM, INVALID_FILE_CONTENT, REMOTE_FILE_DELETE_FAILED, PERMISSION_DENIED, REMOTE_FILE_READ_FAILED, UNDEFINED_ERROR
}
