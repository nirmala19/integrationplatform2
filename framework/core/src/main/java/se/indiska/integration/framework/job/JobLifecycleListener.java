package se.indiska.integration.framework.job;

/**
 * Implementations of this interface are called when the job platform is initialized and is shutting down.
 * Created by heintz on 2017-07-07.
 */
public interface JobLifecycleListener {
    void onJobInitialization();

    void onJobShutdown();
}
