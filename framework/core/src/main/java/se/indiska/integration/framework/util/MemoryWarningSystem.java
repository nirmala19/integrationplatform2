package se.indiska.integration.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.management.Notification;
import javax.management.NotificationEmitter;
import javax.management.NotificationListener;
import java.lang.management.*;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;

/**
 * MemoryWarningSystem is a class which, after it's initated calls any listener if the
 * memory collection threshold is exceeded and memory is over the defined threshold.
 * <p>
 * Default threshold is 80%.
 */
public class MemoryWarningSystem {
    private static final Logger logger = LoggerFactory.getLogger(MemoryWarningSystem.class);
    private static double percentage;
    private final Collection<Listener> listeners =
            new ArrayList<>();


    public interface Listener {
        void availeblMemoryLowWarning(long usedMemory, long maxMemory);
    }

    public MemoryWarningSystem() {
        MemoryMXBean mbean = ManagementFactory.getMemoryMXBean();
        NotificationEmitter emitter = (NotificationEmitter) mbean;

        emitter.addNotificationListener(new NotificationListener() {
            @Override
            public void handleNotification(Notification notification, Object handback) {
                if (notification.getType().equals(MemoryNotificationInfo.MEMORY_COLLECTION_THRESHOLD_EXCEEDED)) {
                    long maxMemory = tenuredGenPool.getUsage().getMax();
                    long usedMemory = tenuredGenPool.getUsage().getUsed();
                    for (Listener listener : listeners) {
                        listener.availeblMemoryLowWarning(usedMemory, maxMemory);
                    }
                }
            }
        }, null, null);
    }

    public boolean addListener(Listener listener) {
        return listeners.add(listener);
    }

    public boolean removeListener(Listener listener) {
        return listeners.remove(listener);
    }

    private static final MemoryPoolMXBean tenuredGenPool =
            findTenuredGenPool();
    static {
        setPercentageUsageThreshold(0.8);
    }

    /**
     * Set the threshold in percent as a <pre>0 &lt;= number &gt; 1.0</pre>
     *
     * @param percentage
     */
    public static void setPercentageUsageThreshold(double percentage) {
        if (percentage <= 0.0 || percentage > 1.0) {
            throw new IllegalArgumentException("Percentage not in range");
        }
        MemoryWarningSystem.percentage = percentage;
        long maxMemory = tenuredGenPool.getUsage().getMax();
        long warningThreshold = (long) (maxMemory * percentage);
        tenuredGenPool.setCollectionUsageThreshold((int) Math.floor(warningThreshold));
    }

    /**
     * Tenured Space Pool can be determined by it being of type
     * HEAP and by it being possible to set the usage threshold.
     */
    private static MemoryPoolMXBean findTenuredGenPool() {
        for (MemoryPoolMXBean pool :
                ManagementFactory.getMemoryPoolMXBeans()) {
            // I don't know whether this approach is better, or whether
            // we should rather check for the pool name "Tenured Gen"?
            if (pool.getType() == MemoryType.HEAP &&
                    pool.isUsageThresholdSupported()) {
                return pool;
            }
        }
        throw new AssertionError("Could not find tenured space");
    }

    /**
     * Print memory status
     */
    public void printMemoryStatus() {
        MemoryUsage memoryUsage = tenuredGenPool.getCollectionUsage();
        double availableMem = memoryUsage.getMax() / 1024d / 1024d;
        double usedMem = memoryUsage.getUsed() / 1024d / 1024d;
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        nf.setMinimumIntegerDigits(1);
        logger.info("################# Memory control status #################");
        logger.info("### Available memory:          " + nf.format(availableMem) + "M");
        logger.info("### Used memory:               " + nf.format(usedMem) + "M");
        NumberFormat pf = NumberFormat.getPercentInstance();
        logger.info("### Percent used:              " + pf.format((double) memoryUsage.getUsed() / (double) memoryUsage.getMax()));
        logger.info("### Alert and pause threshold: " + pf.format(percentage));
    }


}
