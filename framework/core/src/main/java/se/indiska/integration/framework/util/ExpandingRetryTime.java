package se.indiska.integration.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by heintz on 2017-06-29.
 */
public class ExpandingRetryTime {
    private static final Logger logger = LoggerFactory.getLogger(ExpandingRetryTime.class);
    private final int maxRetries;
    private Double wait = 10000d;
    private Double currentWait = wait;
    private double expansion = 2;
    private long iteration = 0;

    public ExpandingRetryTime(int wait, double expansion, int maxRetries) {
        this.wait = new Double(wait);
        this.currentWait = this.wait;
        this.expansion = expansion;
        this.maxRetries = maxRetries;
    }

    public static void main(String[] argv) {
        ExpandingRetryTime expandingWindowRetryTime = new ExpandingRetryTime(2, 3, 10);
        long lastWait = 0;
        for (int i = 1; i < 10; i++) {
            long wait = expandingWindowRetryTime.getNextWaitTime();
            logger.debug(i + ": " + wait + " (" + (wait - lastWait));
            lastWait = wait;
        }
    }

    /**
     * @return
     */
    public long getNextWaitTime() {
        currentWait = Math.pow(iteration, expansion) * this.wait + this.wait;
        iteration++;
        return currentWait.intValue();
    }

    public long getCurrentWaitTime() {
        return currentWait.intValue();
    }

    public boolean reachedMaxRetries() {
        return this.iteration >= this.maxRetries;
    }

    public long getIteration() {
        return iteration;
    }
}
