package se.indiska.integration.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.*;
import java.util.Properties;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by heintz on 2017-05-15.
 */
public class IndioProperties extends Properties {
    private static final Logger logger = LoggerFactory.getLogger(IndioProperties.class);
    private static IndioProperties properties = null;
    private static String dir = "/etc/indio";
    private static String filename = "indio.conf";
    private static File propertyFile = new File(dir, filename);
    private static Thread thread;
    private static boolean stopped = false;
    private static boolean initialized = false;

    private IndioProperties() {
        super();
        try {
            logger.debug("Initializing properties from file " + propertyFile.getAbsolutePath() + " - checking for changes every 2.5 seconds");
            reloadProps();
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path nioPath = FileSystems.getDefault().getPath(dir);
            nioPath.register(watchService, ENTRY_CREATE, ENTRY_MODIFY);

            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!stopped) {
                        try {
                            checkFileChangeEvents(watchService.take());
                        } catch (InterruptedException e) {
                            // Do nothing
                        }
                    }
                }
            });
            thread.start();
        } catch (IOException e) {
            logger.error("Unable to load properties from file " + propertyFile.getAbsolutePath() + "!", e);
        }
    }

    private void checkFileChangeEvents(WatchKey watchKey) {
        for (WatchEvent<?> event : watchKey.pollEvents()) {
            WatchEvent.Kind kind = event.kind();
            if (kind == OVERFLOW) {
                return;
            }
            Path updatedPath = ((WatchEvent<Path>) event).context();
            logger.debug("Updated path " + updatedPath.toAbsolutePath().toString());
            try {
                if (updatedPath.getFileName().toString().equals(filename)) {
                    reloadProps();
                }
            } catch (IOException e) {
                logger.error("Unable to reload properties in file " + dir, e);
            }
        }
    }

    private synchronized void reloadProps() throws IOException {
        this.clear();
        this.load(new FileReader(propertyFile));
    }

    public static IndioProperties properties() {
        if (properties == null) {
            properties = new IndioProperties();
        }
        return properties;
    }

    public static void shutdown() {
        logger.debug("Shutting down property file change listener");
        stopped = true;
        thread.interrupt();
    }
}
