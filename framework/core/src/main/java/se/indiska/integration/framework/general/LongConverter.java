package se.indiska.integration.framework.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts String to Bigint (Long).
 * Created by heintz on 2017-05-05.
 */
public class LongConverter implements DatatypeConverter<Long> {
    private static final Logger logger = LoggerFactory.getLogger(LongConverter.class);

    @Override
    public Long convert(String value) {
        return Long.parseLong(value.trim());
    }

    @Override
    public DataType dataType() {
        return DataType.BIGINT;
    }
}