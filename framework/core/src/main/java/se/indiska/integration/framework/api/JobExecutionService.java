package se.indiska.integration.framework.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.indiska.integration.framework.ExecutorsFactory;
import se.indiska.integration.framework.job.*;
import se.indiska.integration.framework.trace.DataProcessTracer;
import se.indiska.integration.framework.trace.ProcessTrace;
import se.indiska.integration.framework.trace.ProcessTracerFactory;
import se.indiska.integration.framework.trace.Trace;
import se.indiska.integration.framework.util.ExpandingRetryTime;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.*;

/**
 * JobExecutionService is a rest service in the core package, which means all war components
 * which depends in this package will start this rest service (through JobRestInterface).
 * <p>
 * The Execution service accepts calls to execute any job in the current package.
 * <p>
 * Created by heintz on 2017-06-28.
 */
@RestController
@RequestMapping("/job")
public class JobExecutionService {
    private static final Logger logger = LoggerFactory.getLogger(JobExecutionService.class);
    private static Set<Long> executingProcessIds = new HashSet<>();
    private static boolean stopped = false;
    private static Map<Long, ListenableFuture<?>> runningFutures = new HashMap<>();


    /**
     * Called by the <code>engine</code> when a service should be invoked.
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/execute", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity execute(@RequestBody IndioJob.JobExecutionData request) {
        if (stopped) {
            return ResponseEntity.status(HttpStatus.GONE).build();
        }
        if (logger.isTraceEnabled()) {
            logger.debug(request.getProcessId() + ": Executing job " + request.getJobId() + "/" + request.getDataType());
        }
        List<Trace> traces = Collections.synchronizedList(new ArrayList<>());
        ProcessTraceHandle processTraceHandle = new ProcessTraceHandle(request.getProcessId(), new DataProcessTracer() {
            @Override
            public void trace(Trace... trace) {
                traces.addAll(Arrays.asList(trace));
            }
        });
        ProcessStatusHandler.statusUpdate(request.getProcessId(), ProcessStatus.RUNNING);
        ListenableFuture<?> future = ExecutorsFactory.getExecutorService().submit(new Runnable() {
            @Override
            public void run() {
                Long processId = request.getProcessId();
                executingProcessIds.add(processId);
                MDC.put("processId", processId.toString());
                try {//(Connection connection = DatasourceProvider.getConnection()) {
                    IndioJob job = getInstance(JobServiceRunner.getJobClassMap().get(request.getJobId()));
                    job.execute(request, processTraceHandle, new JobCompleteCallback() {
                        @Override
                        public void jobComplete(ProcessExecutionResult result) {
                            traces.add(new ProcessTrace(processTraceHandle.getId(), result.getResult(), result.getErrorMessage(), result.getProcessError()));
                            ProcessTracerFactory.getProcessTracer().trace(traces.toArray(new Trace[traces.size()]));
                            callback(request.getJobId(), result);
                        }

                        @Override
                        public Collection<Long> runningProcessIds() {
                            return executingProcessIds;
                        }
                    });
//                    processTraceHandle.getProcessTracer().trace(new ProcessTrace(processId, ProcessStatus.SUCCESS));
                } catch (Exception e) {
                    processTraceHandle.getProcessTracer().trace(new ProcessTrace(processId, ProcessStatus.ERROR, e.getMessage(), ProcessError.UNDEFINED_ERROR));
                    ProcessTracerFactory.getProcessTracer().trace(traces.toArray(new Trace[traces.size()]));
                    logger.error("Error while processing job " + processId + ": " + e, e);

                    callback(request.getJobId(), new ProcessExecutionResult(processId, ProcessStatus.ERROR, e.getMessage(), ProcessError.UNDEFINED_ERROR));
                } finally {
                    executingProcessIds.remove(processId);
                    MDC.remove("processId");
                }
            }
        });
        runningFutures.put(request.getProcessId(), future);
        Futures.addCallback(future, new FutureCallback<Object>() {
            @Override
            public void onSuccess(Object result) {
                runningFutures.remove(request.getProcessId());
            }

            @Override
            public void onFailure(Throwable t) {
                logger.error("Error executing process id " + request.getProcessId(), t);
                runningFutures.remove(request.getProcessId());
            }
        });


        return ResponseEntity.ok("ok!");
    }

    /**
     * Get the ID's of the running processes.
     *
     * @return
     */
    @RequestMapping(value = "/runningprocesses", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getExecutingProcessIds() {
        return ResponseEntity.ok(executingProcessIds);
    }

    /**
     * Terminate a process if it's running. If/when the process terminates, the callback should remove itself from
     * runningFutures.
     *
     * @return ResponseEntity with NOT_FOUND if the process/Future is not found running otherwise OK.
     */
    @RequestMapping(value = "/terminateprocess/{processId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity terminateProcess(@PathVariable("processId") Long processId) {
        ListenableFuture<?> future = runningFutures.get(processId);
        if (future == null) {
            executingProcessIds.remove(processId);
            return ResponseEntity.notFound().build();
        }

        future.cancel(true);

        return ResponseEntity.ok().build();
    }

    /**
     * Get a heartbeat to ensure that this service is up and running. Used by the engine to check that a job
     * is alive and kicking.
     *
     * @return
     */
    @RequestMapping(value = "/heartbeat", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity heartbeat() {
        Map<String, Object> response = new HashMap<>();
        response.put("status", "ok");
        response.put("runningProcesses", executingProcessIds);
        return ResponseEntity.ok(response);
    }

    /**
     * Used to callback to <code>engine</code> to communicate the result of an execution.
     * Calls the <code>engine</code> using http.
     *
     * @param jobId  The Job ID
     * @param result The Job execution result.
     */
    private void callback(String jobId, ProcessExecutionResult result) {
        ObjectMapper mapper = new ObjectMapper();
        ExecutorsFactory.getExecutorService().submit(new Runnable() {
            @Override
            public void run() {
                ExpandingRetryTime retryTime = new ExpandingRetryTime(2000, 3, 12);
                boolean successfulCall = false;

                do {
                    try {
                        String processResult = mapper.writeValueAsString(result);
                        if (logger.isTraceEnabled()) {
                            logger.trace(result.getProcessId() + ": Calling callback url " + JobServiceRunner.getCallbackUrl(jobId));
                        }
                        HttpResponse httpResponse = Request.Post(JobServiceRunner.getCallbackUrl(jobId))
                                .bodyString(processResult, ContentType.APPLICATION_JSON)
                                .connectTimeout(5000)
                                .socketTimeout(5000)
                                .execute().returnResponse();
                        successfulCall = true;
                    } catch (IOException e) {
                        long waitTime = retryTime.getNextWaitTime();
                        if (!retryTime.reachedMaxRetries()) {
                            logger.warn("Unable to send callback to indio server, retrying in " + waitTime + " ms");
                            try {
                                Thread.sleep(waitTime);
                            } catch (InterruptedException e1) {
                                //Not an error. Just continue.
                            }
                        } else {
                            logger.warn("Unable to send callback to indio server, giving up!", e);
                        }
                    }
                } while (!successfulCall && !retryTime.reachedMaxRetries());
            }
        });
    }

    @PreDestroy
    public void shutdownJob() {
        long start = System.currentTimeMillis();
        logger.info("Shutting down job execution, waiting for " + executingProcessIds.size() + " running executions to finish (though max 30 secs)");
        while (executingProcessIds.size() > 0) {
            try {
                Thread.sleep(500);
                if (System.currentTimeMillis() - start > 30000) {
                    logger.info("Wait threshold exceeded, shutting down anyway!");
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        logger.info("JobExecutionService shutdown complete");
    }

    /**
     * Get an object instance from a job class. Hides any
     *
     * @param jobClass
     * @return
     */
    private IndioJob getInstance(Class jobClass) {
        try {
            return (IndioJob) jobClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

}
