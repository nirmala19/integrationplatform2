package se.indiska.integration.framework.job;

/**
 * Created by heintz on 28/02/17.
 */
public enum ProcessStatus {
    NEW, UPDATED, PROCESSING, RUNNING, SUCCESS, NOOP, ERROR, TIMEOUT, INITIATED, MISSING_JOB, TRANSMITTED, NOOP_DUPLICATE_JOB;

    public static boolean isTerminalStatus(ProcessStatus status) {
        return status == SUCCESS || status == ERROR;
    }
}
