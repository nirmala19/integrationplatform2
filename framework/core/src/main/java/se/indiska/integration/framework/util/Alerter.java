package se.indiska.integration.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.Direction;

import javax.mail.Session;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Properties;

/**
 * Created by heintz on 30/03/17.
 */
public class Alerter {
    private static final Logger logger = LoggerFactory.getLogger(Alerter.class);

    /**
     * Should match the content of the table alert_type
     */
    public enum AlertType {
        CONNECTION_REFUSED
    }

    /**
     * Should match the content of the table data_type.
     */
    public enum AlertDataType {
        POSLOG, CLEARANCE, ARTICLE, SYSTEM
    }

    /**
     * Should match the content of the table job_event_type
     */
    public enum JobEventType {
        INBOUND_FILE_LOAD, OUTBOUND_FILE_LOAD, SYSTEM, INBOUND_TRANSFER, OUTBOUND_TRANSFER
    }

    /**
     * Publishes an alert and adds to alert history. If there is already an active alert matching the input,
     * the alert is added to alert_history only.
     *
     * @param stage     In which stage did the error occur?
     * @param dataType  What kind of data type does the alert stems from. Ex. POSLOG, CLEARANCE or SYSTEM. See table
     *                  data_type.
     * @param alertType What kind of alert is this, Connection refused,
     * @param source
     * @param error
     */
    public static void alert(JobEventType stage, AlertDataType dataType, AlertType alertType, String messageId, String source, String error) {
        String alertInsert = "INSERT INTO alert (stage, data_type, type, active, message_id, time) VALUES (?,?,?,?,?,now()) ON CONFLICT (stage, data_type, type, active) DO UPDATE SET type=? RETURNING ID";
        String alertHistoryInsert = "INSERT INTO alert_event (alert_id, source, error, event_time) VALUES(?,?,?,now())";
        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(alertInsert, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, stage.name());
            ps.setString(2, dataType.name());
            ps.setString(3, alertType.name());
            ps.setBoolean(4, true);
            ps.setString(5, alertType.name());
            ps.setString(6, messageId);
            ps.executeUpdate();
            ResultSet rset = ps.getGeneratedKeys();
            rset.next();
            Long alertId = rset.getLong("id");
            rset.close();
            ps.close();

            ps = conn.prepareStatement(alertHistoryInsert);
            ps.setLong(1, alertId);
            ps.setString(2, source);
            ps.setString(3, error);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void alert(Long executionId, Connection connection) throws SQLException {
        logger.debug("Alerting for executionId " + executionId);
        String sql = "INSERT INTO alert (event_step, data_type, process_id, process_data_trace_id, acknowledge_id, message_id)\n" +
                "SELECT ps.event_step, ps.data_type, ps.id, pdt.id, -1, pdt.message_id\n" +
                "FROM process_status ps, process_data_trace pdt\n" +
                "WHERE ps.id=?\n" +
                "AND pdt.process_id=ps.id\n" +
                "AND pdt.status='ERROR'\n" +
                "ON CONFLICT DO NOTHING";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setLong(1, executionId);
        ps.executeUpdate();
        ps.close();
    }

    public static void alertConnectionFailed() {

    }


    public static void resetAlert(JobEventType stage, AlertDataType dataType, AlertType alertType, String source) {
        String updateSql = "UPDATE alert SET active=FALSE WHERE stage=? AND data_type=? AND type=? AND active=TRUE";
        String resetEvent = "INSERT INTO alert_event (alert_id, source, error, event_time) VALUES (?,?,?,now())";
        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(updateSql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, stage.name());
            ps.setString(2, dataType.name());
            ps.setString(3, alertType.name());
            ps.executeUpdate();
            ResultSet rset = ps.getGeneratedKeys();
            Long alertId = null;
            if (rset.next()) {
                alertId = rset.getLong("id");
            }
            rset.close();
            ps.close();
            if (alertId != null) {
                ps = conn.prepareStatement(resetEvent);
                ps.setLong(1, alertId);
                ps.setString(2, source);
                ps.setString(3, "ALERT CLEARED");
                ps.executeUpdate();
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static void sendAlertMessage(String subject, String errorMessage) {
        subject = "INDIO: " + subject;
        String[] emailAddresses = new String[]{"anders.heintz@indiska.se", "servicedesk@indiska.se","Nookaraju.kandregula@indiska.se","nookaraju.kandregula@aspiresys.com"};
        for (int i = 0; i < emailAddresses.length; i++) {
            emailAddresses[i] = emailAddresses[i].toLowerCase().trim();
        }

        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", "localhost");
        Session session = Session.getDefaultInstance(properties);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("Indio Alert <no-reply@indiska.se>");
        message.setTo(emailAddresses);
        message.setSubject(subject);
        message.setText(errorMessage);
        MailSenderProvider.getMailSender().send(message);
    }

    public static void alertConnectionError(String protocol, String host, Direction direction, String message) {
        String existingError = "SELECT severity, event_time FROM remote_server_alert WHERE protocol=? AND host=? AND direction=?";
        String warnSql = "INSERT INTO remote_server_alert (protocol, host, direction, event_time, message, severity) VALUES (?,?,?,now(),?,'WARN') ON CONFLICT DO NOTHING ";
        String errSql = "UPDATE remote_server_alert SET severity='ERROR', event_time=now(), message=? WHERE protocol=? AND host=? AND direction=?";
        try (Connection connection = DatasourceProvider.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(existingError);
            ps.setString(1, protocol);
            ps.setString(2, host);
            ps.setString(3, direction.name());
            String severity = null;
            boolean warningThresholdPassed = false;
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                severity = rset.getString("severity");
                Timestamp time = rset.getTimestamp("event_time");
                warningThresholdPassed = time.toLocalDateTime().isBefore(LocalDateTime.now().minusMinutes(30));
            }
            ps.close();
            if (severity == null) {
                ps = connection.prepareStatement(warnSql);
                ps.setString(1, protocol);
                ps.setString(2, host);
                ps.setString(3, direction.name());
                ps.setString(4, message);
                ps.executeUpdate();
                ps.close();
            } else if (severity.equals("WARN") && warningThresholdPassed) {
                ps = connection.prepareStatement(errSql);
                ps.setString(1, message);
                ps.setString(2, protocol);
                ps.setString(3, host);
                ps.setString(4, direction.name());
                ps.executeUpdate();
                ps.close();
                sendAlertMessage("Error connecting to " + protocol + "://" + host, "Server " + host + " have not been reachable for the past 30 minutes or more. Access failed using\n\nThe server is now in an error state!\n\nConnection error message is:\n" + message);
            }
        } catch (SQLException e) {
            logger.error("Unable to alert store connection error: " + message, e);
        }
    }

    private static boolean isStoreHost(String host, Connection connection) throws SQLException {
        String sql = "SELECT count(1) FROM store_server ss WHERE ss.address=?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ResultSet resultSet = ps.executeQuery();
        if (resultSet.next()) {
            return resultSet.getInt(1) > 0;
        }
        return false;
    }

    public static void clearConnectionError(String protocol, String host, Direction direction) {
        try (Connection connection = DatasourceProvider.getConnection()) {
            clearConnectionError(protocol, host, direction, connection);
        } catch (SQLException e) {
            logger.error("Clear connection errors for " + protocol + "://" + host + " for direction " + direction.name(), e);
        }
    }

    public static void clearConnectionError(String protocol, String host, Direction direction, Connection connection) throws SQLException {
        String sql = "DELETE FROM remote_server_alert WHERE protocol=? AND host=? AND direction=? AND severity=?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, protocol);
        ps.setString(2, host);
        ps.setString(3, direction.name());
        ps.setString(4, "WARN");
        if (ps.executeUpdate() == 0) {
            ps.close();
            ps = connection.prepareStatement(sql);
            ps.setString(1, protocol);
            ps.setString(2, host);
            ps.setString(3, direction.name());
            ps.setString(4, "ERROR");
            if (ps.executeUpdate() > 0) {
                sendAlertMessage("Connection to " + host + " restored", direction.name() + " access to " + protocol + "://" + host + " has been restored");
            }
        }
        ps.close();
    }

}
