package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by heintz on 2017-04-20.
 */
public class ProcessResult {
    private static final Logger logger = LoggerFactory.getLogger(ProcessResult.class);
    private ProcessStatus processStatus;
    private EventStep eventStep;
    public ProcessResult(ProcessStatus processStatus, EventStep eventStep) {
        this.processStatus = processStatus;
        this.eventStep = eventStep;
    }

    public ProcessStatus getProcessStatus() {
        return processStatus;
    }

    public EventStep getEventStep() {
        return eventStep;
    }

    public enum EventStep {
        INBOUND_FILE_LOAD, OUTBOUND_FILE_LOAD, SYSTEM, INBOUND_TRANSFER, OUTBOUND_TRANSFER, ODS_LOAD, STAGE_LOAD, NULL
    }

}
