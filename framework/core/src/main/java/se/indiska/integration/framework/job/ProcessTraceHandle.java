package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.trace.DataProcessTracer;

/**
 * Created by heintz on 06/04/17.
 */
public class ProcessTraceHandle {
    private static final Logger logger = LoggerFactory.getLogger(ProcessTraceHandle.class);
    private Long id;
    private DataProcessTracer processTracer;

    public ProcessTraceHandle(Long id, DataProcessTracer processTracer) {
        this.id = id;
        this.processTracer = processTracer;
    }

    public Long getId() {
        return id;
    }

    public DataProcessTracer getProcessTracer() {
        return processTracer;
    }

}
