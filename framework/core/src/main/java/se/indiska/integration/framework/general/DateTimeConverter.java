package se.indiska.integration.framework.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Converts a date & time string based on a specified pattern to a Timestamp.
 * Created by heintz on 23/02/17.
 */
public class DateTimeConverter implements DatatypeConverter<Timestamp> {
    private static final Logger logger = LoggerFactory.getLogger(DateTimeConverter.class);
    private DateTimeFormatter formatter;

    public DateTimeConverter(String format) {
        formatter = DateTimeFormatter.ofPattern(format);
    }

    @Override
    public Timestamp convert(String value) {
        return Timestamp.from(LocalDateTime.parse(value.trim(), formatter).toInstant(ZoneOffset.UTC));
    }

    @Override
    public DataType dataType() {
        return DataType.TIMESTAMP;
    }
}
