package se.indiska.integration.framework.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.IndioProperties;
import se.indiska.integration.framework.job.ProcessStatus;
import se.indiska.integration.framework.job.ProcessTraceHandle;
import se.indiska.integration.framework.trace.DataTrace;

import java.io.*;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by heintz on 2017-04-21.
 */
public class InboundFileUtilities {
    private static final String FILE_SCHEMA = "file:///";
    private static final Logger logger = LoggerFactory.getLogger(InboundFileUtilities.class);
    private static SimpleDateFormat archiveDatePathFormat = new SimpleDateFormat("yyyy/MM/dd");
    private static File quarantineDirectory = new File("/Users/heintz/Projects/Indiska/ICC/quarantine");
    private static ObjectMapper mapper = new ObjectMapper();

    public static void addToFileQuarantine(String dataType, String source, String filename, byte[] content, Date date, Exception e) {
        File dir = new File(quarantineDirectory, dataType);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        File dateDir = new File(dir, dateFormat.format(date));
        if (!dateDir.exists()) {
            dateDir.mkdirs();
        }

        File metadataFile = new File(dateDir, filename + ".metadata");

        StringWriter sout = new StringWriter();
        PrintWriter printWriter = new PrintWriter(sout);
        e.printStackTrace(printWriter);
        printWriter.close();

        Map metadata = new LinkedHashMap();
        metadata.put("filename", filename);
        metadata.put("time", dateTimeFormat.format(date));
        metadata.put("source", source);
        metadata.put("error", sout.toString());
        try {
            mapper.writerWithDefaultPrettyPrinter().writeValue(metadataFile, metadata);
        } catch (IOException e1) {
            logger.error("Unable to write quarantine metadata to file " + metadataFile.getAbsolutePath(), e1);
        }
        File file = new File(dateDir, filename);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(content);
            fos.close();
        } catch (IOException e1) {
            logger.error("Unable to write quarantine file to " + file.getAbsolutePath(), e1);
        }
    }

    public static List<InboundFile> getInboundFiles(Connection connection, Long processId) throws SQLException, IOException {
        String sql = "SELECT\n" +
                "  ifs.id messageId,\n" +
                "  now(),\n" +
                "  filename,\n" +
                "  file_type,\n" +
                "  archive_directory,\n" +
                "  source_directory,\n" +
                "  file_content,\n" +
                "  target_id,\n" +
                "  ifs.message_id,\n" +
                "  ifs.event_time\n" +
                "FROM process_status ps\n" +
                "  JOIN process_data_trace pdt\n" +
                "    ON pdt.process_id = ps.process_initiator_id\n" +
                "  JOIN inbound_file_staging ifs\n" +
                "    ON ifs.id = cast(pdt.target_id AS BIGINT)\n" +
                "WHERE ps.id = ?";
        List<InboundFile> result = new ArrayList<>();
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setLong(1, processId);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            InboundFile inboundFile = new InboundFile(rset.getLong("messageId"),
                    rset.getString("file_type"),
                    rset.getString("source_directory"),
                    rset.getString("filename"),
                    getFileContent(rset.getString("file_content")),
                    new File(rset.getString("archive_directory")),
                    rset.getTimestamp("event_time"),
                    rset.getString("message_id"), null);
            result.add(inboundFile);
        }
        rset.close();
        ps.close();
        return result;
    }

    public static int addOutboundFile(Connection connection, ProcessTraceHandle traceHandle, OutboundFile... outboundFiles) throws SQLException {
        String sql = "INSERT INTO outbound_file_staging (event_time, filename, file_type, file_content, source_identifier, message_id) VALUES (now(),?,?,?,?,?) ON CONFLICT (filename, file_type) DO UPDATE SET file_content=?";
        PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        for (OutboundFile outboundFile : outboundFiles) {
            try {
                File outboundArchiveFile = null;
                if (outboundFile.isSavedOnDisk()) {
                    File archiveDir = new File(IndioProperties.properties().getProperty("archive_directory") + "/outbound/" + outboundFile.getFileType());
                    outboundArchiveFile = writeFileToArchive(archiveDir, new Date(), outboundFile.getFilename(), outboundFile.getFileContent());
                }
                ps.setString(1, outboundFile.getFilename());
                ps.setString(2, outboundFile.getFileType());
                if (outboundArchiveFile != null) {
                    ps.setString(3, FILE_SCHEMA + outboundArchiveFile.getAbsolutePath());
                } else {
                    ps.setString(3, outboundFile.getFileContent());
                }
                ps.setLong(4, outboundFile.getSourceId());
                ps.setString(5, outboundFile.getMessageUuid());
                ps.setString(6, outboundFile.getFileContent());
                ps.addBatch();
            } catch (IOException e) {
                logger.error("Unable to archive outbound file with message id " + outboundFile.getMessageUuid(), e);
            }
        }
        ps.executeBatch();
        ResultSet generatedKeysResultSet = ps.getGeneratedKeys();
        int count = 0;
        while (generatedKeysResultSet.next()) {
            Long generatedId = generatedKeysResultSet.getLong("id");
            Long sourceId = generatedKeysResultSet.getLong("source_identifier");
            String messageId = generatedKeysResultSet.getString("message_id");
            traceHandle.getProcessTracer().trace(new DataTrace(messageId, traceHandle.getId(), "inbound_file_staging", sourceId + "", "outbound_file_staging", generatedId + "", ProcessStatus.SUCCESS));
            count++;
        }

        return count;
    }

    public static Map<String, Long> insertFiles(Connection conn, InboundFile... inboundFiles) throws SQLException, IOException {
        String sql = "INSERT INTO inbound_file_staging (event_time, file_type, source_directory, archive_directory, filename, file_content, checksum, message_id) " +
                "VALUES (?,?,?,?,?,?,?,?) " +
                "ON CONFLICT (file_type, filename, checksum) DO NOTHING RETURNING ID, MESSAGE_ID"; //UPDATE set insert_time=?, source_directory=?, archive_directory=?, status=?, file_content=?";
        PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        Timestamp timestamp = new Timestamp(new java.util.Date().getTime());
        String md5sum = null;
        long start = System.nanoTime();
        for (InboundFile inboundFile : inboundFiles) {

            File archiveFile = writeFileToArchive(inboundFile.getArchiveDirectory(), inboundFile.getEventTime(), inboundFile.getFileName(), inboundFile.getFileContent());

            if (inboundFile.getFileContent().startsWith(FILE_SCHEMA)) {
                inboundFile.fileContent = FILE_SCHEMA + archiveFile.getAbsolutePath();
            }
            try {
                if (inboundFile.getFileContent() != null) {
                    md5sum = InboundFileUtilities.md5sum(inboundFile.getFileContent());
                }
            } catch (NoSuchAlgorithmException e) {
                logger.error("Couldn't find MD5 message digest algorithm! (something is seriously wrong here if this happens since MD5 is builtin in Java)");
            }
            long md5time = System.nanoTime() - start;
            int i = 1;
            ps.setTimestamp(i++, timestamp);
            ps.setString(i++, inboundFile.getDataType());
            ps.setString(i++, inboundFile.getSource());
            ps.setString(i++, archiveFile.getParentFile().getAbsolutePath());
            ps.setString(i++, inboundFile.getFileName());
            ps.setString(i++, inboundFile.getFileContent().startsWith(FILE_SCHEMA) ? FILE_SCHEMA + archiveFile.getAbsolutePath() : inboundFile.getFileContent());
            ps.setString(i++, md5sum);
            ps.setString(i++, inboundFile.getMessageUuid());
            ps.addBatch();
            Map map = new HashMap();
            Map value = new HashMap();
            map.put("file_insert", value);
            value.put("file", inboundFile.getFileName());
            value.put("type", inboundFile.getDataType());
//            WebsocketEndpoint.broadcast(value);
        }
        ps.executeBatch();

        ResultSet rs = ps.getGeneratedKeys();
        Map<String, Long> result = new HashMap<>();
        while (rs.next()) {
            Long generatedId = rs.getLong("id");
            String messageId = rs.getString("message_id");
            result.put(messageId, generatedId);
        }
        ps.close();
        return result;
    }

    protected static String md5sum(File file) throws IOException, NoSuchAlgorithmException {
        return md5sum(FILE_SCHEMA + file.getAbsolutePath());
    }

    protected static String md5sum(String fileContent) throws NoSuchAlgorithmException, IOException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.reset();
        if (fileContent.startsWith(FILE_SCHEMA)) {
            URL url = new URL(fileContent);
            try (InputStream is = url.openStream();
                 DigestInputStream dis = new DigestInputStream(is, m)) {
                int c = 0;
                byte[] buf = new byte[2048];
                while ((c = dis.read(buf)) >= 0) {
                }
            }
        } else {
            m.update(fileContent.getBytes());
        }
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        return bigInt.toString(32);
    }

    static File writeFileToArchive(File archiveDirectory, java.util.Date loadedTime, String filename, String content) throws IOException {
        File f = null;
        if (archiveDirectory != null && content != null) {

            File destinationDirectory = new File(archiveDirectory + "/" + archiveDatePathFormat.format(loadedTime));
            if (!destinationDirectory.exists()) {
                destinationDirectory.mkdirs();
            }
            f = new File(destinationDirectory, filename);
            int retry = 0;
            while (f.exists()) {

                try {
                    if (md5sum(content).equals(md5sum(f))) {
                        return f;
                    }
                } catch (NoSuchAlgorithmException e) {
                    logger.error("Unable to calculate MD5 checksum!", e);
                }
                retry++;
                f = new File(destinationDirectory, filename + "." + retry);
            }
            if (SystemMonitor.spaceAvailable(f, Math.round(content.length() * 1.4d), true)) {
                if (!content.startsWith(FILE_SCHEMA)) {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        logger.debug("Error creating new file " + f.getAbsolutePath() + "!");
                        throw e;
                    }
                    FileWriter writer = new FileWriter(f);
                    writer.write(content);
                    writer.close();
                } else {
                    URL url = new URL(content);
                    FileUtils.moveFile(new File(url.getFile()), f);
                }
            } else {
                logger.warn("Not enough disk space, throwing exception");
                throw new IOException("Cannot store file " + f.getAbsolutePath() + " not enough disk space (required " + Math.round(content.length() * 1.4d));
            }
        }
        if (archiveDirectory == null) {
            logger.warn("Archive directory is null!");
        }
        if (f == null) {
            throw new IOException("Couldn't write file to store, file is null!");
        }

        return f;
    }

    public static void addMessageMetadata(Connection connection, InboundFile inboundFile) throws SQLException {
        String sql = "SELECT * FROM message_metadata WHERE message_id=?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, inboundFile.getMessageUuid());
        ResultSet rset = ps.executeQuery();
        inboundFile.metadata = new HashMap<>();
        while (rset.next()) {
            inboundFile.metadata.put(rset.getString("key"), rset.getString("value"));
        }
        rset.close();
        ps.close();
    }

    public static void insertMetadata(Map<String, Map<String, String>> metadatas, Connection conn) throws SQLException {
        String sql = "INSERT INTO message_metadata (message_id, key, value) VALUES (?,?,?) ON CONFLICT (message_id, key) DO UPDATE SET value=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        for (String messageUuid : metadatas.keySet()) {
            Map<String, String> metadata = metadatas.get(messageUuid);
            for (String key : metadata.keySet()) {
                ps.setString(1, messageUuid);
                ps.setString(2, key);
                ps.setString(3, metadata.get(key));
                ps.setString(4, metadata.get(key));
                ps.addBatch();
            }
        }
        ps.executeBatch();
    }

    public static class InboundFile {
        private Long messageId = null;
        private String dataType;
        private String source;
        private String fileName;
        private String fileContent;
        private File archiveDirectory;
        private java.util.Date eventTime;
        private String messageUuid;
        private Map<String, String> metadata;
        private long size = 0;
        private String remotePath;

        public InboundFile(String dataType, String source, String fileName, byte[] fileContent, File archiveDirectory, Date eventTime, String messageUuid, Map<String, String> metadata, String charset) throws IOException {
            this.dataType = dataType;
            this.source = source;
            this.fileName = fileName;
            this.archiveDirectory = archiveDirectory;
            this.eventTime = eventTime;
            this.messageUuid = messageUuid;
            this.metadata = metadata;
            this.size = fileContent != null ? fileContent.length : 0;

//            if (fileContent != null && SystemMonitor.enoughAvailableSpace(archiveDirectory, this.size)) {
//                SystemMonitor.pauseAndAlert("There is not space available to store file " + archiveDirectory.getAbsolutePath() + "/" + fileName + ".");
//                throw new IOException("There is not space available to store file " + archiveDirectory.getAbsolutePath() + "/" + fileName + ".");
//            }

            if (fileContent != null && fileContent.length > 1024 * 100) {
                File tempFile = File.createTempFile(dataType, fileName);
//                if (!SystemMonitor.enoughAvailableSpace(tempFile, this.size)) {
//                    throw new IOException("There is not space available to store file " + tempFile.getAbsolutePath() + "/" + fileName + ".");
//                }
                FileOutputStream fos = new FileOutputStream(tempFile);
                fos.write(new String(fileContent, Charset.forName(charset != null ? charset : "UTF-8")).getBytes());
                fos.close();
                this.fileContent = FILE_SCHEMA + tempFile.getAbsolutePath();
            } else {
//                THIS NEEDS CHANGING TO ACCOMMODATE OTHER ENCODINGS!
                this.fileContent = new String(fileContent == null ? new byte[0] : fileContent, Charset.forName(charset != null ? charset : "UTF-8"));
            }


        }

        InboundFile(Long messageId, String dataType, String source, String fileName, String fileContent, File archiveDirectory, Date eventTime, String messageUuid, Map<String, String> metadata) {
            this.dataType = dataType;
            this.source = source;
            this.fileName = fileName;
            this.archiveDirectory = archiveDirectory;
            this.eventTime = eventTime;
            this.messageUuid = messageUuid;
            this.metadata = metadata;
            this.size = fileContent != null ? fileContent.length() : 0;
            this.fileContent = fileContent;
            this.messageId = messageId;
        }

        public String getDataType() {
            return dataType;
        }

        public String getSource() {
            return source;
        }

        public String getFileName() {
            return fileName;
        }

        public String getFileContent() throws IOException {
            return this.fileContent;
        }

        File getArchiveDirectory() {
            return archiveDirectory;
        }

        Date getEventTime() {
            return eventTime;
        }

        public String getMessageUuid() {
            return messageUuid;
        }

        public Map<String, String> getMetadata() {
            return metadata;
        }

        public long getSize() {
            return size;
        }

        public Long getMessageId() {
            return messageId;
        }
    }

    public static String getFileContent(String fileContentCellValue) throws IOException {
        if (fileContentCellValue.startsWith(FILE_SCHEMA)) {
            URL url = new URL(fileContentCellValue);
            InputStream is = url.openStream();
            int c = 0;
            byte[] buf = new byte[2048];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((c = is.read(buf)) >= 0) {
                baos.write(buf, 0, c);
            }
            is.close();
            return new String(baos.toByteArray());
        } else {
            return fileContentCellValue;
        }
    }

    public static Reader getFileContentReader(String fileContentCellValue) throws IOException {
        if (fileContentCellValue.startsWith(FILE_SCHEMA)) {
            URL url = new URL(fileContentCellValue);
            InputStream is = url.openStream();
            return new InputStreamReader(is);
        } else {
            return new StringReader(fileContentCellValue);
        }
    }

    public static InputStream getFileContentInputStream(String fileContentCellValue) throws IOException {
        if (fileContentCellValue.startsWith(FILE_SCHEMA)) {
            URL url = new URL(fileContentCellValue);
            InputStream is = url.openStream();
            return is;
        } else {
            return new ByteArrayInputStream(fileContentCellValue.getBytes());
        }
    }

    public static class OutboundFile {
        private String filename;
        private String fileType;
        private String fileContent;
        private Long sourceId;
        private String messageUuid;
        private boolean isSavedOnDisk = false;

        public OutboundFile(String filename, String fileType, String fileContent, Long sourceId, String messageUuid) throws IOException {
            this.filename = filename;
            this.fileType = fileType;
//            this.fileContent = fileContent;
            this.sourceId = sourceId;
            this.messageUuid = messageUuid;

            if (fileContent != null && fileContent.length() > 1024 * 100) {
                File tempFile = File.createTempFile(fileType, filename);
//                if (!SystemMonitor.enoughAvailableSpace(tempFile, this.size)) {
//                    throw new IOException("There is not space available to store file " + tempFile.getAbsolutePath() + "/" + fileName + ".");
//                }
                FileWriter fw = new FileWriter(tempFile);
                fw.write(fileContent);
                fw.flush();
                fw.close();
                this.isSavedOnDisk = true;
                this.fileContent = FILE_SCHEMA + tempFile.getAbsolutePath();
            } else {
//                THIS NEEDS CHANGING TO ACCOMMODATE OTHER ENCODINGS!
                this.fileContent = fileContent;
            }

        }

        public boolean isSavedOnDisk() {
            return isSavedOnDisk;
        }

        public String getFilename() {
            return filename;
        }

        public String getFileType() {
            return fileType;
        }

        public String getFileContent() {
            return fileContent;
        }

        public Long getSourceId() {
            return sourceId;
        }

        public String getMessageUuid() {
            return messageUuid;
        }
    }
}
