package se.indiska.integration.framework.job;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation tells Indio that the class annotated is a Job that should be executed.
 * Required interface to implement: <code>IndioJob</code>
 * <p>
 * Created by heintz on 2017-06-27.
 */
@Retention(value = RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface IdentifiableIndioJob {
    String identifier();

    JobType jobType() default JobType.NULL;

    String schedule() default "";

    String triggerDataType() default "";

    String triggerEventStep() default "";

    String eventStep();

    String dataType() default "";

    String description();

    enum JobType {
        SCHEDULED, EVENT_BASED, NULL
    }
}
