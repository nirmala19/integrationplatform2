package se.indiska.integration.framework.datatyperesolver;

import org.xml.sax.SAXException;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

public interface ContentDatatypeResolver {
    boolean match(byte[] data) throws IOException, SAXException, XPathExpressionException;
}
