package se.indiska.integration.framework.datatyperesolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.xpath.XPathExpressionException;

public class ContentDatatypeResolverFactory {
    private static final Logger logger = LoggerFactory.getLogger(ContentDatatypeResolverFactory.class);

    public static ContentDatatypeResolver newResolver(ContentDatatypeResolverConfig config) throws XPathExpressionException {
        switch (config.getFileType().toLowerCase()) {
            case "xml":
                return new XMLContentDatatypeResolver(config);
        }
        return null;
    }


}
