package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The result of a Job Registration.
 * Created by heintz on 2017-06-28.
 */
public class JobRegistrationResult {
    private static final Logger logger = LoggerFactory.getLogger(JobRegistrationResult.class);
    private Status status = null;

    ;
    private String completionCallback = null;
    public JobRegistrationResult(Status status, String completionCallback) {
        this.status = status;
        this.completionCallback = completionCallback;
    }

    public JobRegistrationResult() {
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getCompletionCallback() {
        return completionCallback;
    }

    public void setCompletionCallback(String completionCallback) {
        this.completionCallback = completionCallback;
    }

    public enum Status {OK, ERROR}
}
