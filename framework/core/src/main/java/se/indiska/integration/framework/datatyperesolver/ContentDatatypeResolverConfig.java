package se.indiska.integration.framework.datatyperesolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Content Datatype Resolver bean. Used to resolve the datatype from a message content.
 */
public class ContentDatatypeResolverConfig {
    private static final Logger logger = LoggerFactory.getLogger(ContentDatatypeResolverConfig.class);
    private String fileType = null;
    private String contentResolveMatch = null;

    public ContentDatatypeResolverConfig() {
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getContentResolveMatch() {
        return contentResolveMatch;
    }

    public void setContentResolveMatch(String contentResolveMatch) {
        this.contentResolveMatch = contentResolveMatch;
    }
}
