package se.indiska.integration.framework.job;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Predicate;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import se.indiska.integration.framework.ExecutorsFactory;
import se.indiska.integration.framework.ShutdownListener;
import se.indiska.integration.framework.util.SystemMonitor;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * The main application class for all jobs.
 * <p>
 * Created by heintz on 2017-07-08.
 */
@SpringBootApplication
@ComponentScan({"se.indiska"})
public class JobServiceRunner {
    public static final int MAX_REGISTRATION_RETRIES = 3000;
    private static final Logger logger = LoggerFactory.getLogger(JobServiceRunner.class);
    private static Reflections reflections = new Reflections("se.indiska");
    private static ObjectMapper mapper = new ObjectMapper();
    private static Map<String, String> callbackUrls = new HashMap<>();
    private static Map<String, Class> jobClassMap = new HashMap<>();
    private JobRegistration jobRegistration = null;
    private boolean stopped = false;
    private Set<ShutdownListener> shutdownListeners = new HashSet<>();
    private static JobServiceRunner jobServiceRunner = null;
    private static SystemMonitor systemMonitor = new SystemMonitor();

    @Autowired
    Environment environment;

    public static Map<String, Class> getJobClassMap() {
        return jobClassMap;
    }

    /**
     * Startup of this "application", i.e. a job.
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(JobServiceRunner.class, new String[]{"server.port=0"});
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return (container -> {
            container.setPort(0);
        });
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            logger.debug("Started microservice on port " + environment.getProperty("local.server.port"));
            String localAddress = InetAddress.getLocalHost().getCanonicalHostName() + ":" + environment.getProperty("local.server.port");
            registerJobs(localAddress);
            initializeJobLifecycle();
            jobServiceRunner = this;
        };
    }

    /**
     * Locates any IndioJobs in this classpath which has the annotation <code>@IdentifiableIndioJob</code>,
     * registers the class as jobs and registers the jobs in the remote integration engine.
     *
     * @param localAddress
     */
    public void registerJobs(String localAddress) {
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(IdentifiableIndioJob.class);
        Map<IdentifiableIndioJob, Class> identifiableIndioJobs = new HashMap<>();
        for (Class cls : annotated) {
            if (IndioJob.class.isAssignableFrom(cls)) {
                Set<Annotation> annotations = ReflectionUtils.getAnnotations(cls, new Predicate<Annotation>() {
                    @Override
                    public boolean apply(Annotation annotation) {
                        return annotation.annotationType().equals(IdentifiableIndioJob.class);
                    }
                });
                if (annotations.isEmpty()) {
                    logger.error("Error in class lookup! Class " + cls.getName() + " should be annotated with " + IdentifiableIndioJob.class.getName() + "!");
                } else {
                    IdentifiableIndioJob job = (IdentifiableIndioJob) annotations.iterator().next();
                    identifiableIndioJobs.put(job, cls);
                    jobClassMap.put(job.identifier(), cls);
                }
            } else {
                logger.error("Class " + cls.getName() + " must implement interface " + IndioJob.class.getName());
            }
        }


        logger.info("Starting registration thread!");
        ExecutorsFactory.getExecutorService().submit(new Runnable() {
            @Override
            public void run() {
                long wait = 5000;
                boolean running = false;
                do {
                    try {
                        registerJobs(identifiableIndioJobs, localAddress);
                        if (!running) {
                            logger.info("Registered job in Indio engine");
                        }
                        running = true;
                    } catch (IOException e) {
                        running = false;
                        logger.error("Unable to register or verify registration of at indio engine! " + e.getMessage());
                    } finally {
                        try {
                            Thread.sleep(wait);
                        } catch (InterruptedException e) {
                            // Do nothing
                        }
                    }

                } while (!stopped);

            }
        });

    }

    /**
     * Register a set of jobs at this local address in the remote integration engine using http.
     *
     * @param identifiableIndioJobs A map of <code>IdentifiableIndioJob</code> (as key) and <code>Class</code> as value.
     * @param localAddress          This local address (host and port)
     * @throws IOException If the registration fails.
     */
    private void registerJobs(Map<IdentifiableIndioJob, Class> identifiableIndioJobs, String localAddress) throws IOException {
        for (IdentifiableIndioJob job : identifiableIndioJobs.keySet()) {
            jobRegistration = new JobRegistration("http://" + localAddress, job);
            JobRegistrationResult response = mapper.readValue(Request.Post("http://localhost:8080/registry/register")
                    .addHeader("Content-Type", ContentType.APPLICATION_JSON.toString())
                    .bodyString(mapper.writeValueAsString(jobRegistration), ContentType.APPLICATION_JSON)
                    .connectTimeout(10000)
                    .socketTimeout(10000)
                    .execute().returnContent().asStream(), JobRegistrationResult.class);
            callbackUrls.put(job.identifier(), response.getCompletionCallback());
        }
    }

    /**
     * Initialize job life cycle. I.e. execute any JobLifecycleListener implementations available in the classpath.
     * What happens is that the <code>JobLifecycleListener.onJobInitialization()</code> is called and
     * <code>JobLifecycleListener.onJobShutdown()</code> is scheduled to execute when the server shuts down.
     */
    public void initializeJobLifecycle() {
        Set<Class<? extends JobLifecycleListener>> lifecycleClasses = reflections.getSubTypesOf(JobLifecycleListener.class);
        for (Class<? extends JobLifecycleListener> lifecycleListenerClass : lifecycleClasses) {
            try {
                Constructor<? extends JobLifecycleListener> constructor = lifecycleListenerClass.getConstructor();
                JobLifecycleListener jobLifecycleListener = constructor.newInstance();
                jobLifecycleListener.onJobInitialization();
                addShutdownListener(() -> jobLifecycleListener.onJobShutdown());

            } catch (NoSuchMethodException e) {
                logger.error("Class " + lifecycleListenerClass.getName() + " is a JobLifecycleListener but doesn't implement a default constructor without arguments!");
            } catch (IllegalAccessException e) {
                logger.error("Error while trying to create an instance of " + lifecycleListenerClass.getName() + " using a constructor without arguments!", e);
            } catch (InstantiationException e) {
                logger.error("Error while trying to create an instance of " + lifecycleListenerClass.getName() + " using a constructor without arguments!", e);
            } catch (InvocationTargetException e) {
                logger.error("Error while trying to create an instance of " + lifecycleListenerClass.getName() + " using a constructor without arguments!", e);
            }
        }
    }

    /**
     * Get callback URL for a specific job.
     *
     * @param jobId
     * @return
     */
    public static String getCallbackUrl(String jobId) {
        return callbackUrls.get(jobId);
    }

    @PreDestroy
    private void onExit() {
        logger.info("Shutting down job application job, waiting for " + shutdownListeners.size() + " shutdown listeners");

        this.stopped = true;
        for (ShutdownListener listener : shutdownListeners) {
            listener.shutdown();
        }
    }

    public void addShutdownListener(ShutdownListener listener) {
        this.shutdownListeners.add(listener);
    }

    public void removeShutdownListener(ShutdownListener listener) {
        this.shutdownListeners.remove(listener);
    }

    public static JobServiceRunner getJobServiceRunner() {
        return jobServiceRunner;
    }
}
