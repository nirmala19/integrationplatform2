package se.indiska.integration.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SedRegexpSubstitute {
    private static final Logger logger = LoggerFactory.getLogger(SedRegexpSubstitute.class);

    public static String replace(String string, String substitutePattern) throws SedRegexpException {
        int slashPos;
        int previousSlashPosition = 0;

        int matchStart = -1;
        int matchEnd = -1;
        int substituteStart = -1;
        int substituteEnd = -1;


        while ((slashPos = substitutePattern.indexOf("/", previousSlashPosition)) >= 0) {
            boolean hasPrecedingBackslash = false;
            if (slashPos > 0) {
                hasPrecedingBackslash = substitutePattern.charAt(slashPos - 1) == '\\';
            }

            if (!hasPrecedingBackslash) {
                if (matchStart < 0) {
                    matchStart = slashPos + 1;
                } else if (matchEnd < 0) {
                    matchEnd = slashPos;
                    substituteStart = slashPos + 1;
                } else if (substituteEnd < 0) {
                    substituteEnd = slashPos;
                }
            }
            previousSlashPosition = slashPos + 1;
        }

        if (matchStart < 0 || matchEnd < 0 || substituteStart < 0 || substituteEnd < 0) {
            throw new SedRegexpException("The pattern '" + substitutePattern + "' is not a valid Sed Regexp pattern. A pattern must have the format '/<pattern to match>/<pattern to replace with>/'");
        }

        String matchPattern = substitutePattern.substring(matchStart, matchEnd);
        String substituteWith = substitutePattern.substring(substituteStart, substituteEnd);

        return string.replaceAll(matchPattern, substituteWith);
    }

    public static void main(String[] argv) throws Exception {
        String filename = "S290834753.xml";
        String substitutePattern = "/(^.*$)/od$1/";


        logger.debug(SedRegexpSubstitute.replace(filename, substitutePattern));
    }

    static class SedRegexpException extends Exception {
        public SedRegexpException(String message) {
            super(message);
        }
    }
}
