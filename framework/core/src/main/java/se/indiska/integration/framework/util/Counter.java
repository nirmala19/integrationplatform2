package se.indiska.integration.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple thread-safe counter.
 * Created by heintz on 2017-05-11.
 */
public class Counter {
    private static final Logger logger = LoggerFactory.getLogger(Counter.class);
    private int counter;

    public Counter(int counter) {
        this.counter = counter;
    }

    public Counter() {
        this.counter = 0;
    }

    public synchronized int add() {
        counter++;
        return counter;
    }

    public synchronized int add(int amount) {
        counter = counter + amount;
        return counter;
    }

    public synchronized int reduce() {
        counter--;
        return counter;
    }

    public synchronized int reduce(int amount) {
        counter = counter - amount;
        return counter;
    }

    public int get() {
        return counter;
    }

    @Override
    public String toString() {
        return get() + "";
    }

    public synchronized void reset() {
        this.counter = 0;
    }
}
