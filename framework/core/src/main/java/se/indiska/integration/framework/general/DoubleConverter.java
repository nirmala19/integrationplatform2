package se.indiska.integration.framework.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts String to Double.
 * Created by heintz on 23/02/17.
 */
public class DoubleConverter implements DatatypeConverter<Number> {
    private static final Logger logger = LoggerFactory.getLogger(DoubleConverter.class);

    @Override
    public Number convert(String value) {
        return Double.parseDouble(value);
    }

    @Override
    public DataType dataType() {
        return DataType.DECIMAL;
    }
}
