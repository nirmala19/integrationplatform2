package se.indiska.integration.framework;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by heintz on 2017-05-09.
 */
public class Settings {
    private static final Logger logger = LoggerFactory.getLogger(Settings.class);

    public static boolean isIndioPaused(Connection connection) throws SQLException {
        String sql = "SELECT value FROM settings WHERE key=?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, "paused");
        ResultSet rset = ps.executeQuery();
        boolean isPaused = false;
        if (rset.next()) {
            isPaused = !"FALSE".equals(rset.getString("value"));
        }
        rset.close();
        ps.close();
        return isPaused;
    }

    public static void setPaused(boolean paused, Connection connection) throws SQLException {
        String sql = "UPDATE settings SET value=? WHERE key=?";
        String value = paused ? "TRUE" : "FALSE";

        logger.debug("Setting paused to " + value);
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, value);
        ps.setString(2, "paused");
        ps.executeUpdate();
    }

    public static void setPaused(boolean paused) throws SQLException {
        try (Connection connection = DatasourceProvider.getConnection()) {
            setPaused(paused, connection);
        }
    }

}
