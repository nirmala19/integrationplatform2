package se.indiska.integration.framework.trace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.job.ProcessError;
import se.indiska.integration.framework.job.ProcessStatus;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by heintz on 06/04/17.
 */
public class DataTrace extends Trace {
    private static final Logger logger = LoggerFactory.getLogger(DataTrace.class);
    private String sourceTable;
    private String sourceId;
    private String targetTable;
    private String targetId;
    private String messageId;

    public DataTrace(String messageId, Long processId, String sourceTable, Long sourceId, String targetTable, Long targetId, ProcessStatus processStatus) {
        this(messageId, processId, sourceTable, sourceId == null ? null : sourceId.toString(), targetTable, targetId == null ? null : targetId.toString(), processStatus);
    }

    public DataTrace(String messageId, Long processId, String sourceTable, String sourceId, String targetTable, String targetId, ProcessStatus processStatus) {
        super(processId, processStatus);
        this.messageId = messageId;
        this.sourceTable = sourceTable;
        this.sourceId = sourceId;
        this.targetTable = targetTable;
        this.targetId = targetId;
    }

    public DataTrace(String messageId, Long processId, String sourceTable, Long sourceId, String targetTable, Long targetId, ProcessStatus processStatus, String error, Exception e, ProcessError processError) {
        this(messageId, processId, sourceTable, sourceId == null ? null : sourceId.toString(), targetTable, targetId == null ? null : targetId.toString(), processStatus, error, e, processError);
    }

    public DataTrace(String messageId, Long processId, String sourceTable, String sourceId, String targetTable, String targetId, ProcessStatus processStatus, String error, Exception e, ProcessError processError) {
        super(processId, processStatus);
        String errorToSave = error;
        if (e != null) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            errorToSave += "\n" + sw.toString();
        }
        this.setError(errorToSave);
        this.setProcessError(processError);
        this.messageId = messageId;
        this.sourceTable = sourceTable;
        this.sourceId = sourceId;
        this.targetTable = targetTable;
        this.targetId = targetId;
    }

    public String getSourceTable() {
        return sourceTable;
    }

    public String getSourceId() {
        return sourceId;
    }

    public String getTargetTable() {
        return targetTable;
    }

    public String getTargetId() {
        return targetId;
    }

    public String getMessageId() {
        return messageId;
    }
}
