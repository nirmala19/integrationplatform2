package se.indiska.integration.framework.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * Converts String to a date based on a pattern.
 * Created by heintz on 23/02/17.
 */
public class DateConverter implements DatatypeConverter<Timestamp> {
    private static final Logger logger = LoggerFactory.getLogger(DateConverter.class);
    private DateTimeFormatter formatter;

    public DateConverter(String format) {
        formatter = DateTimeFormatter.ofPattern(format);
    }

    @Override
    public Timestamp convert(String value) {
        return Timestamp.from(LocalDate.parse(value.trim(), formatter).atStartOfDay().toInstant(ZoneOffset.UTC));
    }

    @Override
    public DataType dataType() {
        return DataType.TIMESTAMP;
    }
}
