package se.indiska.integration.framework.general;

/**
 * A datatype converter is a converter which can convert from String to a SQL data type
 * such as Timestamp, Varchar, Integer, Decimal and Bigint.
 * Created by heintz on 23/02/17.
 */
public interface DatatypeConverter<T> {
    T convert(String value);

    DataType dataType();

    enum DataType {
        TIMESTAMP(false), VARCHAR(true), INTEGER(false), DECIMAL(true), BIGINT(false);

        private boolean hasLength;

        DataType(boolean hasLength) {
            this.hasLength = hasLength;
        }

        boolean hasLength() {
            return hasLength;
        }
    }
}
