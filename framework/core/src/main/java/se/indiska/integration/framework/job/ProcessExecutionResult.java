package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by heintz on 09/03/17.
 */
public class ProcessExecutionResult {
    private static final Logger logger = LoggerFactory.getLogger(ProcessExecutionResult.class);
    private ProcessStatus result;
    private String errorMessage = null;
    private ProcessError processError = null;
    private Long processId = null;
    private boolean completelyDone = true;

    public ProcessExecutionResult() {
    }

    public ProcessExecutionResult(Long processId, ProcessStatus result) {
        this.result = result;
        this.processId = processId;
    }

    public ProcessExecutionResult(Long processId, ProcessStatus result, boolean completelyDone) {
        this.result = result;
        this.processId = processId;
        this.completelyDone = completelyDone;
    }

    public ProcessExecutionResult(Long processId, String errorMessage) {
        this.errorMessage = errorMessage;
        this.result = ProcessStatus.ERROR;
        this.processId = processId;
    }

    public ProcessExecutionResult(Long processId, ProcessStatus result, String errorMessage, ProcessError processError) {
        this.processId = processId;
        this.result = result;
        this.errorMessage = errorMessage;
        this.processError = processError;
    }

    public Long getProcessId() {
        return processId;
    }

    public ProcessStatus getResult() {
        return result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public ProcessError getProcessError() {
        return processError;
    }

    public boolean isCompletelyDone() {
        return completelyDone;
    }

    public void setCompletelyDone(boolean completelyDone) {
        this.completelyDone = completelyDone;
    }
}
