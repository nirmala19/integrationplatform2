package se.indiska.integration.framework;

/**
 * Created by heintz on 2017-04-24.
 */
public interface ShutdownListener {
    void shutdown();
}
