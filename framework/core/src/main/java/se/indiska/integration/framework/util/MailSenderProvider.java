package se.indiska.integration.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

public class MailSenderProvider {
    private static final Logger logger = LoggerFactory.getLogger(MailSenderProvider.class);
    private static JavaMailSenderImpl mailSender = null;
    public static JavaMailSender getMailSender() {
        if (mailSender == null) {
            mailSender = new JavaMailSenderImpl();
            mailSender.setHost("localhost");
            mailSender.setPort(25);
            Properties props = mailSender.getJavaMailProperties();
            props.put("mail.transport.protocol", "smtp");
        }
        return mailSender;
    }

}
