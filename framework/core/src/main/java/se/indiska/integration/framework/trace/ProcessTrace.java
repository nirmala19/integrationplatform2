package se.indiska.integration.framework.trace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.job.ProcessError;
import se.indiska.integration.framework.job.ProcessResult;
import se.indiska.integration.framework.job.ProcessStatus;

/**
 * Created by heintz on 06/04/17.
 */
public class ProcessTrace extends Trace {
    private static final Logger logger = LoggerFactory.getLogger(ProcessTrace.class);
    private ProcessResult.EventStep eventStep = null;

    public ProcessTrace(Long processId, ProcessStatus processStatus) {
        super(processId, processStatus);
    }

    public ProcessTrace(Long processId, ProcessResult processResult) {
        super(processId, processResult.getProcessStatus());
        this.eventStep = processResult.getEventStep();
    }

    public ProcessTrace(Long processId, ProcessStatus processStatus, String error, ProcessError processError) {
        super(processId, processStatus, error, processError);
    }

    public ProcessResult.EventStep getEventStep() {
        return eventStep;
    }
}
