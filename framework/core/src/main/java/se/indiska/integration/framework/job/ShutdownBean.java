package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.util.function.Function;

/**
 * Workaround Spring Bean to handle that @PreDestroy annotation is not working in main spring boot application class.
 */
public class ShutdownBean {
    private static final Logger logger = LoggerFactory.getLogger(ShutdownBean.class);
    private Function shutdownFunction;

    public ShutdownBean(Function shutdownFunction) {
        this.shutdownFunction = shutdownFunction;
    }

    @PreDestroy
    public void shutdown() {
        logger.debug("Shutting down!");
        shutdownFunction.apply(null);
    }
}
