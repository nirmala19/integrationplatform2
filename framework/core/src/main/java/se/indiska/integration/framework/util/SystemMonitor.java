package se.indiska.integration.framework.util;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.Settings;

import java.io.File;
import java.sql.SQLException;
import java.text.NumberFormat;

/**
 * A set of utility methods to check the environment of Indio and make sure
 * we have enough heap memory and disk space.
 */
public class SystemMonitor implements Job {
    private static final Logger logger = LoggerFactory.getLogger(SystemMonitor.class);
    private static final MemoryWarningSystem mws = new MemoryWarningSystem();

    static {
        MemoryWarningSystem.setPercentageUsageThreshold(0.9);
        mws.addListener(new MemoryWarningSystem.Listener() {
            public void availeblMemoryLowWarning(long usedMemory, long maxMemory) {
                double percentageUsed = ((double) usedMemory) / (double) maxMemory;
                NumberFormat pf = NumberFormat.getPercentInstance();
                pf.setMaximumFractionDigits(2);
                logger.error("Heap size overload! " + pf.format(percentageUsed) + " in use! Pausing Indio!");
                pauseAndAlert("GC overhead limits approaching! Pausing to avoid errors!");
            }
        });
        mws.printMemoryStatus();
    }


    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

    }

    /**
     * Utility method to make sure there's enough available space where we want
     * to create a file. If not,
     *
     * @param file       The file for which we want to check if there's disk space available.
     * @param size       The file size (to check if it's available)
     * @param pauseIfNot If <pre>true</pre>, pause indio (pause all jobs) if there isn't enough disk space.
     * @return <pre>true</pre> if there's enough disk space to store file, otherwise <pre>false</pre>
     */
    public static boolean spaceAvailable(File file, long size, boolean pauseIfNot) {
        long availableSpace = 0;
        File f = new File(file.getAbsolutePath());
        while (f != null && !f.exists()) {
            f = f.getParentFile();
        }
        /**
         * Make sure we only pause IF we find something to check the size for. A file
         * MUST exist to be able to check available space. If no parent file for which it
         * exist it's probably an error, but shouldn't be handled here.
         */
        if (f != null) {
            availableSpace = f.getUsableSpace();
            boolean spaceAvailable = availableSpace > size;
            if (pauseIfNot && !spaceAvailable) {
                logger.info("spaceAvailable::"+spaceAvailable+" :And trying to pause Indio server due to low space:" );
                pauseAndAlert("There is not enough disk space available to create file " + file.getAbsolutePath() + " (" + size + " bytes)!\n\nIndio has been paused as a precaution and messages will be transferred!!!");
            }
            return availableSpace > size;
        } else {
            logger.warn("Cannot determine if there's enough disk space available for file " + file.getAbsolutePath() + " (" + size + " bytes)");
            return true;
        }
    }

    public static void main(String[] argv) throws Exception {
        File f = new File("/tmp/gurka/kalle/örjan.txt");
        long l = 120l * 1024l * 1024l * 1024l;
        spaceAvailable(f, l, true);
    }

    public static void pauseAndAlert(String message) {
        try {
            logger.info("::Pausing Indio::"+message );
            Settings.setPaused(true);
            Alerter.sendAlertMessage("Fatal system error, Indio is PAUSED", message);
        } catch (SQLException e) {
            logger.error("Unable to pause Indio!!!");
        }
    }
}
