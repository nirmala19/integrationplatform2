package se.indiska.integration.framework.job;

import java.io.IOException;
import java.util.Collection;

/**
 * Callback interface to use when IndioJobs are completed.
 * Created by heintz on 2017-05-09.
 */
public interface JobCompleteCallback {
    /**
     * Call when process is complete.
     *
     * @param result Provide information about the process termination, it's result and any error occurring.
     */
    void jobComplete(ProcessExecutionResult result);

    /**
     * Get the running process ids
     *
     * @return A collection of running process id's.
     * @throws IOException
     */
    Collection<Long> runningProcessIds() throws IOException;
}
