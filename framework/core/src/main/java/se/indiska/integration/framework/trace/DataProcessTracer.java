package se.indiska.integration.framework.trace;

/**
 * Created by heintz on 06/04/17.
 */
public interface DataProcessTracer {
    void trace(Trace... trace);
}
