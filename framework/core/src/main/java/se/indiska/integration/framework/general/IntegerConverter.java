package se.indiska.integration.framework.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts String to Integer
 * Created by heintz on 23/02/17.
 */
public class IntegerConverter implements DatatypeConverter<Integer> {
    private static final Logger logger = LoggerFactory.getLogger(IntegerConverter.class);

    @Override
    public Integer convert(String value) {
        return Integer.parseInt(value.replaceAll("\"", "").trim());
    }

    @Override
    public DataType dataType() {
        return DataType.INTEGER;
    }
}
