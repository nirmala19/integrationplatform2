package se.indiska.integration.framework.datatyperesolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class XMLContentDatatypeResolver implements ContentDatatypeResolver {
    private static final Logger logger = LoggerFactory.getLogger(XMLContentDatatypeResolver.class);
    private ContentDatatypeResolverConfig resolverConfig;

    private DocumentBuilder documentBuilder;
    private XPathExpression matchExpression = null;

    public XMLContentDatatypeResolver(ContentDatatypeResolverConfig resolverConfig) throws XPathExpressionException {
        this.resolverConfig = resolverConfig;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error("This should never happen since it's the default construction of document builder");
        }
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();

        matchExpression = xpath.compile(resolverConfig.getContentResolveMatch());

    }


    @Override
    public boolean match(byte[] data) throws IOException, SAXException, XPathExpressionException {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        Document xmlDoc = documentBuilder.parse(bais);
        return (Boolean) matchExpression.evaluate(xmlDoc, XPathConstants.BOOLEAN);
    }
}
