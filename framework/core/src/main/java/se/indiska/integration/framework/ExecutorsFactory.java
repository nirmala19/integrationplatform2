package se.indiska.integration.framework;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by heintz on 2017-06-28.
 */
public class ExecutorsFactory {
    private static final Logger logger = LoggerFactory.getLogger(ExecutorsFactory.class);
    private static ListeningExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(15));
    private static ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(15);

    public static void shutdown() {
        executorService.shutdown();
        scheduledExecutorService.shutdown();
    }

    public static ListeningExecutorService getExecutorService() {
        return executorService;
    }

    public static ScheduledExecutorService getScheduledExecutorService() {
        return scheduledExecutorService;
    }


}
