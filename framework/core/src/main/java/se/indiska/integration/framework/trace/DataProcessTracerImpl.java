package se.indiska.integration.framework.trace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.job.ProcessStatus;
import se.indiska.integration.framework.job.ProcessStatusHandler;

import java.sql.*;
import java.util.*;

/**
 * Created by heintz on 06/04/17.
 */
public class DataProcessTracerImpl implements DataProcessTracer {
    private static final Logger logger = LoggerFactory.getLogger(DataProcessTracerImpl.class);

    public DataProcessTracerImpl() {

    }


    private void trace(List<Trace> traces) {
        try {
            if (!traces.isEmpty()) {
                try {
                    try (Connection conn = DatasourceProvider.getConnection()) {
                        PreparedStatement ps = conn.prepareStatement("INSERT INTO process_data_trace (process_id, source, source_id, target, target_id, status, message_id) VALUES (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

                        Map<Long, String> errorMap = new HashMap<>();
                        List<ProcessTrace> processTraces = new ArrayList<>();
                        int dataTraceCount = 0;
                        for (Trace trace : traces) {
                            if (trace instanceof ProcessTrace) {
                                processTraces.add((ProcessTrace) trace);
                            } else if (trace instanceof DataTrace) {
                                DataTrace dataTrace = (DataTrace) trace;
                                if (logger.isTraceEnabled()) {
                                    logger.trace("Logging process data trace for " + dataTrace.getSourceTable() + "." + dataTrace.getSourceId() + " -> " + dataTrace.getTargetTable() + "." + dataTrace.getTargetId());
                                }
                                ps.setLong(1, dataTrace.getProcessId());
                                ps.setString(2, dataTrace.getSourceTable());
                                if (dataTrace.getSourceId() != null) {
                                    ps.setString(3, dataTrace.getSourceId() + "");
                                } else {
                                    ps.setNull(3, Types.VARCHAR);
                                }
                                ps.setString(4, dataTrace.getTargetTable());
                                if (dataTrace.getTargetId() != null) {
                                    ps.setString(5, dataTrace.getTargetId() + "");
                                } else {
                                    ps.setNull(5, Types.VARCHAR);
                                }
                                ps.setString(6, dataTrace.getProcessStatus().name());
                                ps.setString(7, dataTrace.getMessageId());
                                ps.addBatch();
                                if (dataTrace.getProcessStatus() == ProcessStatus.ERROR) {
                                    errorMap.put(dataTrace.getProcessId(), dataTrace.getError());
                                }
                            }
                        }
                        ps.executeBatch();

                        for (ProcessTrace trace : processTraces) {
                            Set<String> statuses = getStatuses(trace.getProcessId(), conn);
                            ProcessStatus status = trace.getProcessStatus();
                            if (status != ProcessStatus.ERROR && statuses.contains(ProcessStatus.ERROR.name())) {
                                status = ProcessStatus.ERROR;
                            }
                            ProcessStatusHandler.statusUpdate(trace.getProcessId(), status, trace.getError());
                        }

                        if (!traces.isEmpty()) {
                            ResultSet rset = ps.getGeneratedKeys();
                            while (rset.next()) {
                                ProcessStatus status = ProcessStatus.valueOf(rset.getString("status"));
                                if (status == ProcessStatus.ERROR) {
                                    Long processId = rset.getLong("process_id");
                                    String error = errorMap.get(processId);
                                    ProcessStatusHandler.reportError(processId, rset.getLong("id"), error, conn);
                                }
                            }
                        }
                    }
                } catch (SQLException e) {
                    logger.error("Unable to insert data trace!", e);
                }
            }
        } catch (Throwable e) {
            //We can never leave this loop until the server shuts down.
            logger.error("Error while processing Traces!", e);
        }
    }

    private Set<String> getStatuses(Long processId, Connection conn) throws SQLException {
        String sql = "SELECT DISTINCT(status) FROM process_data_trace WHERE process_id=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        Set<String> statuses = new HashSet<>();
        ps.setLong(1, processId);
        ResultSet rset = ps.executeQuery();
        while (rset.next()) {
            statuses.add(rset.getString("status"));
        }
        rset.close();
        ps.close();
        return statuses;
    }

    @Override
    public void trace(Trace... traces) {
        trace(Arrays.asList(traces));
    }
}
