package se.indiska.integration.framework.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserGroup {
    private static final Logger logger = LoggerFactory.getLogger(UserGroup.class);
    private String group;

    public UserGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }
}
