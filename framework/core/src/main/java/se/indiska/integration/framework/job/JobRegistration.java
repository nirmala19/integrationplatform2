package se.indiska.integration.framework.job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Request object used by Jobs to register themselves in the integration engine.
 * Created by heintz on 2017-06-28.
 */
public class JobRegistration {
    private static final Logger logger = LoggerFactory.getLogger(JobRegistration.class);
    private String address = null;
    private String identifier = null;
    private IdentifiableIndioJob.JobType jobType = null;
    private String schedule = null;
    private String triggerDataType = null;
    private String triggerEventStep = null;
    private String eventStep = null;
    private String dataType = null;
    private String description;

    public JobRegistration(String address, IdentifiableIndioJob job) {
        this.address = address;
        this.identifier = job.identifier();
        this.jobType = job.jobType();
        this.schedule = job.schedule();
        this.triggerDataType = job.triggerDataType();
        this.triggerEventStep = job.triggerEventStep();
        this.eventStep = job.eventStep();
        this.dataType = job.dataType();
        this.description = job.description();
    }

    public JobRegistration() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public IdentifiableIndioJob.JobType getJobType() {
        return jobType;
    }

    public void setJobType(IdentifiableIndioJob.JobType jobType) {
        this.jobType = jobType;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getTriggerDataType() {
        return triggerDataType;
    }

    public void setTriggerDataType(String triggerDataType) {
        this.triggerDataType = triggerDataType;
    }

    public String getTriggerEventStep() {
        return triggerEventStep;
    }

    public void setTriggerEventStep(String triggerEventStep) {
        this.triggerEventStep = triggerEventStep;
    }

    public String getEventStep() {
        return eventStep;
    }

    public void setEventStep(String eventStep) {
        this.eventStep = eventStep;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
