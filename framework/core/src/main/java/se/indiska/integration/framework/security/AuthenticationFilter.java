package se.indiska.integration.framework.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by heintz on 2017-08-22.
 */
public abstract class AuthenticationFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationFilter.class);
    private static ObjectMapper mapper = new ObjectMapper();

    private String group;

    public AuthenticationFilter(String group) {
        this.group = group;
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        String group = this.group;
        if (req.getServletPath().startsWith("/registry/") || req.getServletPath().startsWith("/job/") || req.getServletPath().startsWith("/es")) {
            chain.doFilter(request, response);
            return;
        }
        HttpServletResponse res = (HttpServletResponse) response;
        String token = req.getHeader("token");
        if (token == null) {
            res.setStatus(401);
            return;
        }
        String reqPath = req.getServletPath().replaceAll("//", "/");
        if ((reqPath.startsWith("/integrations/")
                || reqPath.startsWith("/jobs/")
                || reqPath.startsWith("/crud/"))
                && (req.getMethod().equals("POST") || req.getMethod().equals("PUT") || req.getMethod().equals("DELETE"))) {
            group = "indioadm";
        }

        if (Request.Get("http://localhost:8100/auth/validate/" + token + "/" + group).execute().returnResponse().getStatusLine().getStatusCode() != 200) {
            res.setStatus(401);
            return;
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
