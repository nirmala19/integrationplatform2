package se.indiska.integration.framework.job;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.indiska.integration.framework.DatasourceProvider;
import se.indiska.integration.framework.trace.ProcessTracerFactory;
import se.indiska.integration.framework.util.Alerter;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Created by heintz on 06/03/17.
 */
public class ProcessStatusHandler {
    private static final Logger logger = LoggerFactory.getLogger(ProcessStatusHandler.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");

    static ProcessTraceHandle newJob(String jobId, String eventStep, String dataType, Map jobDataMap, Boolean allowConcurrent) {
        return newStatus(jobId, eventStep, dataType, ProcessStatus.NEW, null, allowConcurrent, jobDataMap);
    }

    private static ProcessTraceHandle newStatus(String jobId, String eventStep, String dataType, ProcessStatus resultStatus, String error, Boolean allowConcurrent, Map jobDataMap) {
        try (Connection conn = DatasourceProvider.getConnection()) {
            if (countNew(jobId, dataType, conn) <= 3) {
                java.util.Date time = new java.util.Date();
                String seqNextVal = "SELECT nextval('process_status_id_seq')";
                PreparedStatement ps = conn.prepareStatement(seqNextVal);
                ResultSet rset = ps.executeQuery();
                rset.next();
                Long processId = rset.getLong(1);
                String sql = "INSERT INTO process_status (id, event_time, event_step, data_type, status,job_id,parameters, allow_concurrent) " +
                        "VALUES (?, now(), ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING ";
//                    "SELECT ?, now(), ?, ?, ?, ?, ?, ? " +
//                    "  WHERE\n" +
//                    "    NOT EXISTS(\n" +
//                    "        SELECT id\n" +
//                    "        FROM process_status\n" +
//                    "        WHERE\n" +
//                    "          job_id = ? AND (status = ? OR status = ? OR status = ?) AND\n" +
//                    "          data_type = ?);";
                ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setLong(1, processId);
                ps.setString(2, eventStep);
                ps.setString(3, dataType);
                ps.setString(4, resultStatus.name());
                ps.setString(5, jobId);
                if (jobDataMap != null) {
                    ps.setString(6, mapper.writeValueAsString(jobDataMap));
                } else {
                    ps.setNull(6, Types.VARCHAR);
                }
                ps.setBoolean(7, allowConcurrent);
//            ps.setString(8, jobId);
//            ps.setString(9, ProcessStatus.NEW.name());
//            ps.setString(10, ProcessStatus.RUNNING.name());
//            ps.setString(11, ProcessStatus.INITIATED.name());
//            ps.setString(12, dataType);
//            synchronized (ProcessStatusHandler.class) {
                long start = System.nanoTime();
                ps.executeUpdate();
                double tta = (System.nanoTime() - start) / 1e6d;
                if (tta > 2000 && logger.isDebugEnabled()) {
                    logger.warn("Insert new status with processId: " + processId + ", events_step: " + eventStep + ", dataType " + dataType + ", allowConcurrent: " + allowConcurrent + ", jobId: " + jobId + ". TTA: " + tta + "ms");
                }
//            }

                rset.close();
                rset = ps.getGeneratedKeys();
                if (rset.next()) {
                    ProcessTraceHandle processResult = new ProcessTraceHandle(processId, ProcessTracerFactory.getProcessTracer());
//            sendWebSocketUpdate(processId, dataType, eventStep, resultStatus);
                    addHistory(processId, null, resultStatus.name(), "Process Handler", conn);
                    if (resultStatus == ProcessStatus.SUCCESS) {
                        injectNextStatuses(processId, eventStep, dataType, conn);
                    }
                    if (error != null) {
                        reportError(processId, null, error, conn);
                    }
                    return processResult;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static long countNew(String jobId, String datatype, Connection conn) throws SQLException {
        long count = 0;
        String sql = "SELECT count(1) FROM process_status WHERE job_id=? AND data_type=? AND status=?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, jobId);
        ps.setString(2, datatype);
        ps.setString(3, "NEW");
        ResultSet rset = ps.executeQuery();
        if (rset.next()) {
            count = rset.getLong(1);
        }
        rset.close();
        ps.close();
        return count;
    }

    public static void statusUpdate(Long processId, ProcessStatus resultStatus) {
        updateProcessStatus(processId, resultStatus, null);
    }

    public static void statusUpdate(Long processId, ProcessStatus resultStatus, String error) {
        updateProcessStatus(processId, resultStatus, error);
    }


    public static void sendWebSocketUpdate(Long processId, String jobId, String dataType, String eventStep, ProcessStatus status, double seconds, double progress) {
        /**
         * This method was used for a live view of current running jobs, but was deemed unnecessary.
         * Leaving it here if the requirement rises again...
         */

//        String state = null;
//        switch (status) {
//            case NOOP:
//            case ERROR:
//            case TIMEOUT:
//            case SUCCESS:
//                state = "DONE";
//                break;
//            case RUNNING:
//                state = "RUNNING";
//                break;
//        }
//        if (state != null) {
//            Map<String, Object> message = new HashMap<>();
//            message.put("process_id", processId);
//            message.put("job_id", jobId);
//            message.put("data_type", dataType);
//            message.put("event_step", eventStep);
//            message.put("status", state);
//            message.put("processing_time", seconds);
//            message.put("progress", progress);
//            WebsocketEndpoint.broadcast(message);
//        }
    }

    private static void updateProcessStatus(Long processId, ProcessStatus status, String error) {
        String getPreviousStatus = "SELECT * FROM process_status WHERE id=?";
        String sql = "UPDATE process_status SET status=?, event_time=? WHERE id=?";
        Timestamp now = new Timestamp(System.currentTimeMillis());

        try (Connection conn = DatasourceProvider.getConnection()) {
            PreparedStatement ps = conn.prepareStatement(getPreviousStatus);
            ps.setLong(1, processId);
            ResultSet rset = ps.executeQuery();
            String currentStatus = null;
            String eventStep = null;
            String dataType = null;
            String jobId = null;
            Timestamp eventTime = null;
            if (rset.next()) {
                currentStatus = rset.getString("status");
                eventStep = rset.getString("event_step");
                dataType = rset.getString("data_type");
                jobId = rset.getString("job_id");
                eventTime = rset.getTimestamp("event_time");
            }
            rset.close();
            ps.close();

            ps = conn.prepareStatement(sql);
            ps.setString(1, status.name());
            ps.setTimestamp(2, now);
            ps.setLong(3, processId);
            ps.executeUpdate();

            long processingTime = now.getTime() - eventTime.getTime();

            Double progress = null;
            switch (status) {
                case ERROR:
                case SUCCESS:
                case NOOP:
                case TIMEOUT:
                    progress = 1d;
                    break;
                case NEW:
                case RUNNING:
                    progress = 0d;
                    break;

            }
            if (progress == null) {
                progress = 1d;
            }
            try {
                sendWebSocketUpdate(processId, jobId, dataType, eventStep, status, processingTime / 1000d, progress);
            } catch (NullPointerException npe) {
                logger.error("NPE!", npe);
            }

            addHistory(processId, currentStatus, status.name(), "Process Handler", conn);
            if (ProcessStatus.isTerminalStatus(status)) {
                injectNextStatuses(processId, eventStep, dataType, conn);
            }
            if (status == ProcessStatus.ERROR || error != null) {
                reportError(processId, null, error, conn);

                Alerter.alert(processId, conn);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static void injectNextStatuses(Long processId, String eventStep, String dataType, Connection connection) throws SQLException {
        String nextStepSql = "INSERT INTO process_status (event_time, event_step, job_id, data_type, status, process_initiator_id)\n" +
                "SELECT\n" +
                "  now(),\n" +
                "  job2.event_type,\n" +
                "  jte.job_id,\n" +
                "  jte.data_type,\n" +
                "  'NEW',\n" +
                "  ?\n" +
                "FROM job job1\n" +
                "  JOIN process_status ps ON ps.job_id = job1.id\n" +
                "  JOIN job_event_triggers jte\n" +
                "    ON jte.event_step = ps.event_step AND ps.data_type = jte.data_type\n" +
                "  JOIN job job2\n" +
                "  ON jte.job_id=job2.id\n" +
                "WHERE ps.id = ? AND job1.job_status='ACTIVE' AND jte.job_status='ACTIVE'\n";
        PreparedStatement ps = connection.prepareStatement(nextStepSql);
        ps.setLong(1, processId);
        ps.setLong(2, processId);
        ps.executeUpdate();
    }


    public static void reportError(Long executionId, Long processDataTraceId, String error, Connection connection) throws SQLException {
        if (error != null || processDataTraceId != null) {
            String statusSql = "INSERT INTO process_error (process_status_id, process_data_trace_id, error) VALUES (?,?,?)";
            PreparedStatement ps = connection.prepareStatement(statusSql);
            ps.setLong(1, executionId);
            if (processDataTraceId != null) {
                ps.setLong(2, processDataTraceId);
            } else {
                ps.setNull(2, Types.BIGINT);
            }
            ps.setString(3, error);
            ps.executeUpdate();
            ps.close();
        }
    }


    private static void addHistory(Long processStatusId, String fromStatus, String toStatus, String eventInitiator, Connection connection) throws SQLException {
        String sql = "INSERT INTO process_status_history (process_status_id, from_status, to_status, event_time, change_initiator) VALUES (?,?,?,?,?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setLong(1, processStatusId);
        ps.setString(2, fromStatus);
        ps.setString(3, toStatus);
        ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
        ps.setString(5, eventInitiator);
        ps.executeUpdate();
        ps.close();
    }


    static class JobToTrigger {
        private String jobId;
        private String jobStatus;
        private String dataType;

        public JobToTrigger(String jobId, String jobStatus, String dataType) {
            this.jobId = jobId;
            this.jobStatus = jobStatus;
            this.dataType = dataType;
        }

        public String getJobId() {
            return jobId;
        }

        public String getJobStatus() {
            return jobStatus;
        }

        public String getDataType() {
            return dataType;
        }
    }

}
