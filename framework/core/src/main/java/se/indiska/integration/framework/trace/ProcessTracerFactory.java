package se.indiska.integration.framework.trace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by heintz on 2017-06-28.
 */
public class ProcessTracerFactory {
    private static final Logger logger = LoggerFactory.getLogger(ProcessTracerFactory.class);
    private static DataProcessTracer processTracer = null;

    public static synchronized DataProcessTracer getProcessTracer() {
        if (processTracer == null) {
            processTracer = new DataProcessTracerImpl();
        }
        return processTracer;
    }

}
