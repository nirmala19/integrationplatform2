package se.indiska.integration.framework.job;

import java.util.Map;

/**
 * Interface for any job that needs to be executed. Can be triggered via Schedule or event.
 * A job needs to itself know the sources it will use and where to put it results. The framework
 * gives execution data (data type, job id, the event step and any other data provided by its trigger),
 * a process trace handle where Process Id and Tracer can be accessed as well as a
 * SQL Connection and a callback to use when the job is complete.
 * <p>
 * Created by heintz on 06/03/17.
 */
public interface IndioJob {
    /**
     * Executes this job/process.
     *
     * @param jobExecutionData See class JobExecutionData for more information.
     * @param traceHandle      A handle where access to Process ID and the process tracer (to use for data tracing).
     * @param callback         A callback to use when the job is completed.
     * @throws Exception Makes sure the calling method catches all exceptions. Anything the can be handled in the execute
     *                   method should of course be handled, but otherwise a proper exception should be thrown for the scheduler error handling
     *                   to handle.
     */
    void execute(JobExecutionData jobExecutionData, ProcessTraceHandle traceHandle, JobCompleteCallback callback) throws Exception;

    class JobExecutionData {
        private Long processId;
        private String dataType;
        private String jobId;
        private String eventStep;
        private Map dataMap;
        private boolean allowsConcurrent;

        public JobExecutionData() {
        }

        public JobExecutionData(Long processId, String dataType, String jobId, String eventStep, Map dataMap, boolean allowsConcurrent) {
            this.processId = processId;
            this.dataType = dataType;
            this.jobId = jobId;
            this.eventStep = eventStep;
            this.dataMap = dataMap;
            this.allowsConcurrent = allowsConcurrent;
        }

        public Long getProcessId() {
            return processId;
        }

        public String getDataType() {
            return dataType;
        }

        public String getJobId() {
            return jobId;
        }

        public String getEventStep() {
            return eventStep;
        }

        public Map getDataMap() {
            return dataMap;
        }

        public boolean allowsConcurrent() {
            return allowsConcurrent;
        }
    }

}
