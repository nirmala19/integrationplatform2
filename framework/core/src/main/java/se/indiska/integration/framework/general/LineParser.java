package se.indiska.integration.framework.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * An Extenda Format Line Parser for parsing content lines (not start or end entry).
 * Adds necessary line header intervals.
 * <p>
 * Created by heintz on 23/02/17.
 */
public class LineParser {
    private static final Logger logger = LoggerFactory.getLogger(LineParser.class);
    private static final int paddingSize = 16;
    private Map<String, LineItem> lineIntervals = new LinkedHashMap<>();
    private String tableName = null;

    public LineParser(String tableName, boolean storeIsKey) {
        this.tableName = tableName;
        addInterval("TRANSACTIONTYPE", 0, 3, null, false, false, false);
        addInterval("ACTION", 3, 1, null, false, false, false);
        addInterval("STORE_ID", 4, 6, new IntegerConverter(), false, storeIsKey, true);
        addInterval("SEQUENCE_NUMBER", 10, 6, new IntegerConverter(), false, false, false);
    }

    /**
     * Convenience method for <code>addInterval(String, int, int, DatatypeConverter, boolean, boolean, boolean)
     * where
     * <code>isPadded</code> is <code>true</code>
     * <code>isKey</code> is <code>false</code>
     * <code>createIndex</code> is <code>false</code>
     *
     * @param name      The name of the interval, which also becomes the table column name.
     * @param start     The position in the line where this field starts (use from Extenda ComTransar docs)
     * @param length    The length of the field (use from Extenda ComTransar docs)
     * @param converter A converter to convert to SQL datatype.
     */
    public void addInterval(String name, int start, int length, DatatypeConverter converter) {
        addInterval(name, start, length, converter, true, false, false);
    }

    /**
     * Convenience method for <code>addInterval(String, int, int, DatatypeConverter, boolean, boolean, boolean)
     * where
     * <code>isPadded</code> is <code>true</code>
     * <code>createIndex</code> is <code>false</code>
     *
     * @param name      The name of the interval, which also becomes the table column name.
     * @param start     The position in the line where this field starts (use from Extenda ComTransar docs)
     * @param length    The length of the field (use from Extenda ComTransar docs)
     * @param converter A converter to convert to SQL datatype.
     * @param isKey     If true, this column is added as a part of the primary key.
     */
    public void addInterval(String name, int start, int length, DatatypeConverter converter, boolean isKey) {
        addInterval(name, start, length, converter, true, isKey, false);
    }

    /**
     * Convenience method for <code>addInterval(String, int, int, DatatypeConverter, boolean, boolean, boolean)
     * where
     * <code>converter</code> is <code>null</code>
     * <code>isPadded</code> is <code>true</code>
     * <code>isKey</code> is <code>false</code>
     * <code>createIndex</code> is <code>false</code>
     *
     * @param name   The name of the interval, which also becomes the table column name.
     * @param start  The position in the line where this field starts (use from Extenda ComTransar docs)
     * @param length The length of the field (use from Extenda ComTransar docs)
     */
    public void addInterval(String name, int start, int length) {
        addInterval(name, start, length, null, true, false, false);
    }

    /**
     * Convenience method for <code>addInterval(String, int, int, DatatypeConverter, boolean, boolean, boolean)
     * where
     * <code>converter</code> is <code>null</code>
     * <code>isPadded</code> is <code>true</code>
     *
     * @param name        The name of the interval, which also becomes the table column name.
     * @param start       The position in the line where this field starts (use from Extenda ComTransar docs)
     * @param length      The length of the field (use from Extenda ComTransar docs)
     * @param createIndex If true, create an index for this column.
     * @param isKey       If true, this column should be part of the primary key.
     */
    public void addInterval(String name, int start, int length, boolean createIndex, boolean isKey) {
        addInterval(name, start, length, null, true, isKey, createIndex);
    }

    /**
     * Adds an interval to this Line with instructions on how values are converted and how the database
     * table is created.
     *
     * @param name        The name of the interval, which also becomes the table column name.
     * @param start       The position in the line where this field starts (use from Extenda ComTransar docs)
     * @param length      The length of the field (use from Extenda ComTransar docs)
     * @param converter   A converter to convert the string value from the file to a database type value.
     * @param isPadded    If this line is padded or not (normal transaction lines have a header padding of 16).
     * @param isKey       If true, this column should be part of the primary key.
     * @param createIndex If true, create an index for this column.
     */
    public void addInterval(String name, int start, int length, DatatypeConverter converter, boolean isPadded, boolean isKey, boolean createIndex) {
        if (converter == null) {
            converter = new StringConverter();
        }
        if (isPadded) {
            start += paddingSize;
        }
        lineIntervals.put(name, new LineItem(new LineInterval(start, length), converter, isKey, createIndex));
    }

    /**
     * Creates an SQL statement (postgresql dialect) for creating a database table for this line type.
     *
     * @param tableName The name of the table.
     * @return A String representing the SQL statement.
     */
    public String getInputCreateTable(String tableName) {
        Set<String> columns = new LinkedHashSet<>();
        for (String key : lineIntervals.keySet()) {
            columns.add(key);
        }
        String sql = "create table if not exists " + tableName + " (";
        Iterator<String> columnsIterator = columns.iterator();
        List<String> primaryKeys = new ArrayList<>();
        while (columnsIterator.hasNext()) {
            String column = columnsIterator.next();
            LineItem lineItem = lineIntervals.get(column);
            if (lineItem.isKey()) {
                primaryKeys.add(column);
            }

            sql += column + " " + lineItem.getDatatypeConverter().dataType();
            if (lineItem.getDatatypeConverter().dataType().hasLength()) {
                sql += "(" + lineItem.getInterval().getLength() + ")";
            }
            if (columnsIterator.hasNext()) {
                sql += ",";
            }
        }
        if (!primaryKeys.isEmpty()) {
            sql += ", PRIMARY KEY (";
        }
        Iterator<String> primaryKeyIterator = primaryKeys.iterator();
        while (primaryKeyIterator.hasNext()) {
            sql += primaryKeyIterator.next();
            if (primaryKeyIterator.hasNext()) {
                sql += ",";
            }
        }
        if (!primaryKeys.isEmpty()) {
            sql += ")";
        }
        sql += ")";
        return sql;
    }

    /**
     * Creates a list of indices which needs to be created for this database table.
     *
     * @param tableName The name of the table.
     * @return A list of SQL statements creating indices for this table according to line parser.
     */
    public List<String> getInputCreateIndices(String tableName) {
        List<String> indexCreateStmts = new ArrayList<>();
        Set<String> columns = new LinkedHashSet<>();
        for (String key : lineIntervals.keySet()) {
            columns.add(key);
        }
        Iterator<String> columnsIterator = columns.iterator();
        while (columnsIterator.hasNext()) {
            String column = columnsIterator.next();
            LineItem lineItem = lineIntervals.get(column);
            if (lineItem.isIndex()) {
                String indexPrefix = tableName;
                if (indexPrefix.indexOf(".") > 0) {
                    indexPrefix = indexPrefix.substring(indexPrefix.indexOf(".") + 1);
                }
                indexCreateStmts.add("CREATE INDEX IF NOT EXISTS " + indexPrefix + "_" + column + "_index ON " + tableName + " (" + column + ")");
            }
        }
        return indexCreateStmts;
    }


    /**
     * Parses a line of this type.
     *
     * @param line The line in string format.
     * @return A Map of the values where the field name is the key and the converted SQL datatype value for
     * the field in the line as value.
     */
    public Map<String, Object> parse(String line) {
        Map<String, Object> result = new LinkedHashMap<>();
        for (String key : lineIntervals.keySet()) {
            LineItem lineItem = lineIntervals.get(key);
            String value = line.substring(lineItem.getInterval().getStart(), lineItem.getInterval().getStart() + lineItem.getInterval().getLength());
            try {
//                logger.debug(key + " " + lineItem.getInterval().getStart() + " + > " + lineItem.getInterval().getLength() + ":\t" + value);
                result.put(key, lineItem.getDatatypeConverter().convert(value));
            } catch (Exception e) {
                logger.error("Unable to parse value at position " + (lineItem.getInterval().getStart() - paddingSize) + " and length " + lineItem.getInterval().getLength(), e);
            }
        }
        return result;
    }

    /**
     * Get the table name.
     *
     * @return The table name.
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * A line item is a definition of a field in a fixed length line.
     */
    class LineItem {
        /**
         * Defining start and end for this field.
         */
        private LineInterval interval;
        /**
         * Data type converter (may be null) converting a String value from file to Java Database datatype.
         */
        private DatatypeConverter datatypeConverter;
        /**
         * If this field should be a part of the primary key.
         */
        private boolean isKey = false;
        /**
         * If this field (column) should have an index.
         */
        private boolean isIndex = false;

        /**
         * Creates a new LineItem.
         *
         * @param interval          The interval (required)
         * @param datatypeConverter The data type converter (may be null)
         * @param isKey             If this field should be a part of the primary key
         * @param isIndex           If this field (column) should be indexed.
         */
        public LineItem(LineInterval interval, DatatypeConverter datatypeConverter, boolean isKey, boolean isIndex) {
            this.interval = interval;
            this.datatypeConverter = datatypeConverter;
            this.isKey = isKey;
            this.isIndex = isIndex;
        }

        public LineInterval getInterval() {
            return interval;
        }

        public DatatypeConverter getDatatypeConverter() {
            return datatypeConverter;
        }

        public boolean isKey() {
            return isKey;
        }

        public boolean isIndex() {
            return isIndex;
        }
    }

    /**
     * Line interval - start position and length of a field in a Extenda flat file transaction line.
     */
    class LineInterval {
        private int start;
        private int length;

        public LineInterval(int start, int length) {
            this.start = start;
            this.length = length;
        }

        public int getStart() {
            return start;
        }

        public int getLength() {
            return length;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            LineInterval that = (LineInterval) o;

            if (getStart() != that.getStart()) return false;
            return getLength() == that.getLength();
        }

        @Override
        public int hashCode() {
            int result = getStart();
            result = 31 * result + getLength();
            return result;
        }
    }
}
