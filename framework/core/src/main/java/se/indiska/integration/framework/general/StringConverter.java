package se.indiska.integration.framework.general;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts String to String (dummy converter because all fields needs to have a converter).
 * Created by heintz on 23/02/17.
 */
public class StringConverter implements DatatypeConverter<String> {
    private static final Logger logger = LoggerFactory.getLogger(StringConverter.class);

    @Override
    public String convert(String value) {
        return value.trim();
    }

    @Override
    public DataType dataType() {
        return DataType.VARCHAR;
    }
}
