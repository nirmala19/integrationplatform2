package se.indiska.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import se.indiska.integration.framework.security.AuthenticationFilter;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SecurityFilter extends AuthenticationFilter {
    private static final Logger logger = LoggerFactory.getLogger(SecurityFilter.class);

    public SecurityFilter() {
        super("indio");
    }

    public SecurityFilter(String group) {
        super(group);
    }

}
