package se.indiska.integration.framework.util;

import org.testng.annotations.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import static org.testng.Assert.assertEquals;

public class InboundFileUtilitiesTest {
    private static String content = "aslkdjölsdkfjöslafjqpw94rjapnaipv94h98u+0c9urfåoweäöalfåaÅPOÅAÄLÖFÅ#OR?ÅFELÅÄÖ:CÄL}{≠∏Œ¥ØﬂØ";

    @Test
    public void testMd5Sum() throws IOException, NoSuchAlgorithmException {

        File f = File.createTempFile("test", "content");
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(content.getBytes());
        fos.close();

        String contentMd5 = InboundFileUtilities.md5sum(content);
        String fileMd5 = InboundFileUtilities.md5sum(f);
        assertEquals(contentMd5, fileMd5);
    }

    @Test
    public void testWriteFileToArchive() throws Exception {
        File archiveDir = null;
        try {
            archiveDir = File.createTempFile("test", "dir");
            archiveDir.delete();
            archiveDir.mkdirs();
            File f1 = InboundFileUtilities.writeFileToArchive(archiveDir, new Date(), "temporary_archive_dir.file", content);
            assertEquals(InboundFileUtilities.md5sum(content), InboundFileUtilities.md5sum(content));
            File f2 = InboundFileUtilities.writeFileToArchive(archiveDir, new Date(), "temporary_archive_dir.file", content);
            assertEquals(f1.getAbsolutePath(), f2.getAbsolutePath());

            File f3 = InboundFileUtilities.writeFileToArchive(archiveDir, new Date(), "temporary_archive_dir.file", content + "1");
            assertEquals(f1.getAbsolutePath() + ".1", f3.getAbsolutePath());

        } finally {
            for (File f : archiveDir.listFiles()) {
                f.delete();
            }
            archiveDir.delete();
        }
    }
}