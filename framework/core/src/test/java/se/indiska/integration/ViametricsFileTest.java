package se.indiska.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by heintz on 2017-05-05.
 */
public class ViametricsFileTest {
    private static final Logger logger = LoggerFactory.getLogger(ViametricsFileTest.class);
    public static void main(String[] argv) throws  Exception {
        File dir = new File("/tmp/viametrics");

        File[] xmlFiles = dir.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith("xml");
            }
        });

        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

        for (File f : xmlFiles) {
            documentBuilder.parse(f);
        }
    }
}
