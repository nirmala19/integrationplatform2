package se.indiska.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by heintz on 2017-05-03.
 */
public class TestSuiteSetup {
    private static final Logger logger = LoggerFactory.getLogger(TestSuiteSetup.class);
    private static final String SOURCE_FILE_ROOT_PATH = "/Users/heintz/Projects/Indiska/ICC/testdata/livedata";
    private static final String SHOP_OUTBOUND_ROOT_PATH = "/Users/heintz/Projects/Indiska/ICC/testdata/ftp/shop/outbound";
    private static final String SHOP_INBOUND_ROOT_PATH = "/Users/heintz/Projects/Indiska/ICC/testdata/ftp/shop/inbound";
    private static final String ORACLE_OUTBOUND_ROOT_PATH = "/Users/heintz/Projects/Indiska/ICC/testdata/ftp/oracle/outbound";
    private static final long TIME_SPEEDUP = 5000;

    private static LocalDateTime startDate = null;
    private static LocalDateTime endDate = null;
    private static Map<String, LocalDateTime> currentDates = new HashMap<>();
    private static Set<String> alreadyCopiedFiles = new HashSet<>();
    private static int totalCount = 0;


    public static void main(String[] argv) throws Exception {
        startDate = LocalDateTime.of(2017, 4, 1, 0, 0);
        endDate = startDate.plus(1, ChronoUnit.MONTHS);

        currentDates.put("shop2hq", startDate.withSecond(0));
        currentDates.put("hq2shop", startDate.withSecond(0));

        ExecutorService executorService = Executors.newFixedThreadPool(4);
        Future f = executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    copyShopFiles();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        Future f2 = executorService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    copyHQStoreFiles();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        f.get();
        f2.get();
        logger.debug("Copied " + totalCount + " in total");
        executorService.shutdownNow();
    }

    private static void copyShopFiles() throws IOException, InterruptedException {
        LocalDateTime currentDate = currentDates.get("shop2hq");
        boolean end = false;
        boolean endOfDay = false;
        while (!end) {
            final LocalDateTime finalDate = currentDate;
            File path = new File(SOURCE_FILE_ROOT_PATH + "/shop/" + currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            long startTime = System.currentTimeMillis();
//            logger.debug("Getting files from " + path.getAbsolutePath() + " up until " + currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            File[] files = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    LocalDateTime modifiedTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(file.lastModified()), ZoneId.systemDefault());


                    return modifiedTime.isBefore(finalDate) && !alreadyCopiedFiles.contains(file.getAbsolutePath());
                }
            });
            Set<Integer> stores = new LinkedHashSet<>();
            if (files == null) {
                return;
            }
            for (File file : files) {
                int storeId = -1;
                if (file.getName().startsWith("POSLog") || file.getName().startsWith("POSLog")) {
                    storeId = Integer.parseInt(file.getName().substring(32, 37));
                } else if (file.getName().startsWith("HQ")) {
                    storeId = Integer.parseInt(file.getName().substring(file.getName().lastIndexOf(".") + 1));
                } else if (file.getName().startsWith("PMLog")) {
                    if (file.getName().length() < 34) {
                        storeId = Integer.parseInt(file.getName().substring(24, 28));
                    } else {
                        storeId = Integer.parseInt(file.getName().substring(29, 34));
                    }
                }

                if (storeId >= 0) {
                    alreadyCopiedFiles.add(file.getAbsolutePath());
                    stores.add(storeId);
                    File targetDir = new File(SHOP_OUTBOUND_ROOT_PATH + "/" + storeId);
                    if (!targetDir.exists()) {
                        targetDir.mkdirs();
                    }
                    Path source = FileSystems.getDefault().getPath(file.getAbsolutePath());
                    Path target = FileSystems.getDefault().getPath(targetDir.getAbsolutePath(), file.getName());
                    Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
                } else {
                    logger.warn("Unhandled file: " + file.getAbsolutePath());
                }

            }
            if (files.length > 0) {
                logger.debug("Copied " + files.length + " file(s) to its destination at time " + currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                totalCount += files.length;
            }
            for (Integer storeId : stores) {
                Path sp = FileSystems.getDefault().getPath(SHOP_INBOUND_ROOT_PATH, storeId + "");
                Path op = FileSystems.getDefault().getPath(ORACLE_OUTBOUND_ROOT_PATH, storeId + "");
                Files.createDirectories(sp);
                Files.createDirectories(op);
            }
            end = currentDate.isAfter(endDate);
            long speedup = TIME_SPEEDUP;
            LocalDateTime newDate = currentDate.plus((System.currentTimeMillis() - startTime) * speedup, ChronoUnit.MILLIS);
            if (newDate.getDayOfMonth() != currentDate.getDayOfMonth() && !endOfDay) {
                currentDate = currentDate.withHour(23).withMinute(59).withSecond(59).withNano(999999999);
                endOfDay = true;
            } else {
                endOfDay = false;
                currentDate = newDate;
            }
            currentDates.put("shop2hq", currentDate);
        }
    }

    private static void copyHQStoreFiles() throws IOException, InterruptedException {
        LocalDateTime currentDate = currentDates.get("hq2shop");

        boolean end = false;
        boolean endOfDay = false;
        while (!end) {
            final LocalDateTime finalDate = currentDate.withNano(0);
            File path = new File(SOURCE_FILE_ROOT_PATH + "/oracletoshop/" + currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            long startTime = System.currentTimeMillis();
//            logger.debug("Getting files from " + path.getAbsolutePath() + " up until " + currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            File[] files = path.listFiles(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    LocalDateTime modifiedTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(file.lastModified()), ZoneId.systemDefault());

                    return modifiedTime.isBefore(finalDate) && !alreadyCopiedFiles.contains(file.getAbsolutePath());
                }
            });
            Set<Integer> stores = new LinkedHashSet<>();
            if (files == null) {
                return;
            }
            for (File file : files) {
                int storeId = -1;
                if (file.getName().startsWith("HQ")) {
//                    storeId = Integer.parseInt(file.getName().substring(file.getName().lastIndexOf(".") + 1));
                    storeId = Integer.parseInt(file.getName().substring(20, 25));
                }

                if (storeId >= 0) {
                    alreadyCopiedFiles.add(file.getAbsolutePath());
                    stores.add(storeId);
                    File targetDir = new File(ORACLE_OUTBOUND_ROOT_PATH + "/" + storeId);
                    if (!targetDir.exists()) {
                        targetDir.mkdirs();
                    }
                    Path source = FileSystems.getDefault().getPath(file.getAbsolutePath());
                    Path target = FileSystems.getDefault().getPath(targetDir.getAbsolutePath(), file.getName());
                    Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
                } else {
                    logger.warn("Unhandled file: " + file.getAbsolutePath());
                }

            }
            if (files.length > 0) {
                logger.debug("Copied " + files.length + " file(s) from " + path.getAbsolutePath() + " to " + ORACLE_OUTBOUND_ROOT_PATH + " at time " + currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                totalCount += files.length;
            }
            for (Integer storeId : stores) {
                Path sp = FileSystems.getDefault().getPath(SHOP_INBOUND_ROOT_PATH, storeId + "");
                Path op = FileSystems.getDefault().getPath(ORACLE_OUTBOUND_ROOT_PATH, storeId + "");
                Files.createDirectories(sp);
                Files.createDirectories(op);
            }
            end = currentDate.isAfter(endDate);
            long speedup = TIME_SPEEDUP;
            LocalDateTime newDate = currentDate.plus((System.currentTimeMillis() - startTime) * speedup, ChronoUnit.MILLIS);
            if (newDate.getDayOfMonth() != currentDate.getDayOfMonth() && !endOfDay) {
                currentDate = currentDate.withHour(23).withMinute(59).withSecond(59).withNano(999999999);
                endOfDay = true;
            } else {
                endOfDay = false;
                currentDate = newDate;
            }
            currentDates.put("hq2shop", currentDate);
        }
    }

}
